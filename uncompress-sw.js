let AppVersion = 'digitcare-0.3';
let assets = [
    "/",
    "/home",
    "/index",
    "/login",
    "/profile",
    "/logout",

    "templates/app/body-parts/nav.html",
    "templates/app/body-parts/search-bar.html",
    "templates/app/body-parts/main-header.html",
    "templates/app/body-parts/main-menu.html",
    "templates/app/pages/505.html",
    "templates/app/pages/login-recommend.html",
    "templates/app/pages/home.html",
    "templates/app/pages/offline.html",
    "templates/app/pages/profile.html",
    "templates/app/pages/logout.html",
    "templates/app/pages/login.html",

    "static/app/assets/img/spinners/dotted.gif",
    "static/app/assets/img/default/no-image.png",
    "static/app/assets/img/default/profile.png",
    "static/app/assets/img/spinners/spinner.gif",
    "static/app/assets/img/default/item.png",
    "favicon.ico",
    "static/app/assets/icons/royal/icon-144x144.png",
    "static/app/assets/img/default/heshablogo.png",
    "static/app/assets/img/sidebar_head_bg.png",
    "static/app/assets/skins/jquery-fancytree/icons.png",

    "offline-request.json",
    "manifest.json",

    "static/app/assets/icons/material-design-icons/MaterialIcons-Regular.ttf",
    "static/app/assets/css/fonts/Roboto-Regular.ttf",
    "static/app/assets/icons/material-design-icons/MaterialIcons-Regular.woff2",
    "static/app/bower_components/uikit/fonts/fontawesome-webfont.woff2",
    "static/app/bower_components/uikit/fonts/fontawesome-webfont.woff",
    "static/app/bower_components/uikit/fonts/fontawesome-webfont.ttf",

    "static/app/assets/js/index.js",
    "static/app/assets/css/compress.min.css",
    "static/app/assets/js/compress.min.js",
    "static/app/assets/js/plugins/Drag-And-Drop-File-Uploader-With-Preview-Imageuploadify/dist/imageuploadify.min.css",
    "static/app/assets/js/plugins/Drag-And-Drop-File-Uploader-With-Preview-Imageuploadify/imageuploadify.js",

];
function messageHandler(event) {
    if (event.data === 'purge_cache') {
        caches.keys().then(cache_names => {
            Promise.all(cache_names.map(function (cache_name) {
                return caches.delete(cache_name);
            }))
        });

    }
}
self.addEventListener('message', (event) => {
    messageHandler(event);
});

self.addEventListener("install",function (event) {
    console.info("Service worker installed");
    event.waitUntil(
        caches.open(AppVersion).then(function (cache) {
            cache.addAll(assets)
        })
    );
    self.skipWaiting();
});
self.addEventListener("activate",function (event) {
    event.waitUntil(
        caches.keys().then(cache_names => {
            Promise.all(cache_names.map(function (cache_name) {
                if (cache_name !== AppVersion){
                    return caches.delete(cache_name);
                }
            }))
        })
    );
    return self.clients.claim();
    // console.info("Service worker Activated");
});
function extentionOfUrl(url){
    let parts_of_url = url.split("/");
    let last_part = parts_of_url[parts_of_url.length - 1];
    let devied_of_last_part = last_part.split(".");
    return devied_of_last_part[devied_of_last_part.length - 1];
}
self.addEventListener("fetch",function (event) {
    event.respondWith(
        caches.match(event.request).then(function (res) {
            // if(res){
            //     console.log('res match' + res);
            //     return res;
            // }
            // else{
            //     return fetch(event.request).then(response => {
            //         caches.open(AppVersion).then(cache => {
            //            cache.put(event.request, response);
            //         });
            //         return response.clone();
            //     });
            // }

            if (res !== undefined){
                return res;
            }
            else{
                if (!navigator.onLine){
                    let image_extensions = ["png","jpg","gif","jpeg"];
                    let extension = extentionOfUrl(event.request.url);

                    if (image_extensions.indexOf(extension) !== -1){
                        return caches.match("static/app/assets/img/default/no-image.png");
                    }
                    else if (extension === "html"){
                        return caches.match("templates/app/pages/offline.html");
                    }
                    else if (extension === "request"){
                        let resolve_response = new Response();
                        let config = {
                            data:event.request.formData(),
                            url:event.request.url,
                            method:event.request.method,

                        };
                        resolve_response.data = {};
                        resolve_response.status = 200;
                        resolve_response.statusText = "OK";
                        resolve_response.xhrStatus = "complete";
                        resolve_response.config = config;
                        return new Promise(function (resolve,reject) {
                            resolve(resolve_response);
                        });

                    }

                }
                let parts_of_url = event.request.url.split("/");
                console.log(parts_of_url);

                // return fetch(event.request).then(response => {
                //     caches.open(AppVersion).then(cache => {
                //         if (event.request.method.toLowerCase() !== "post"){
                //             cache.put(event.request, response);
                //         }
                //
                //     });
                //     return response.clone();
                // });

                return fetch(event.request);
            }
        })

    );
});