let AppVersion = 'march_edukit-0.13';
let assets = [
    "templates/app/pages/offline.html",
    "static/app/assets/img/default/no-image.png",

    "static/app/assets/js/index.js",
    "static/app/assets/css/compress.min.css",
    "static/app/assets/js/compress.min.js",
];

function messageHandler(event) {
    if (event.data === 'purge_cache') {
        caches.keys().then(cache_names => {
            Promise.all(cache_names.map(function (cache_name) {
                return caches.delete(cache_name);
            }))
        });

    }
}
self.addEventListener('message', (event) => {
    messageHandler(event);
});

self.addEventListener("install",function (event) {
    console.info("Service worker installed");
    event.waitUntil(
        caches.open(AppVersion).then(function (cache) {
            return cache.addAll(assets)
        })
    );
    self.skipWaiting();
});
self.addEventListener("activate",function (event) {
    event.waitUntil(
        caches.keys().then(cache_names => {
            return Promise.all(cache_names.map(function (cache_name) {
                if (cache_name !== AppVersion){
                    console.info("Old version delete");
                    return caches.delete(cache_name);
                }
            }))
        })
    );
    console.info("Service worker Activated");
    return self.clients.claim();

});
function extentionOfUrl(url){
    let parts_of_url = url.split("/");
    let last_part = parts_of_url[parts_of_url.length - 1];
    let devied_of_last_part = last_part.split(".");
    return devied_of_last_part[devied_of_last_part.length - 1];
}
self.addEventListener("fetch",function (event) {
    event.respondWith(
        caches.open(AppVersion).then(function (cache) {
            return cache.match(event.request).then(function (res) {
                return res || fetch(event.request).then(response => {

                    let cacheables_extensions = ["png","jpg","gif","jpeg","js","html","ico"];
                    let extension = extentionOfUrl(event.request.url);
                    let extension_allow = 0;
                    if (cacheables_extensions.indexOf(extension) !== -1){
                        extension_allow = 1;
                    }
                    if (event.request.method.toLowerCase() !== "post" && response.status === 200 && extension_allow){
                        cache.put(event.request, response.clone());
                    }

                    // console.log(event.request.url);
                    return response;
                });
            })
        }).catch(function (error) {
            console.info(error);
            return fetch(event.request);
        })

    );
});
