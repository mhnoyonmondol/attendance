// calender
app.register.controller("calendar",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let page_name = Models.page_name($location);
        $scope.page_title = Models.readable_text(page_name);
        let calendar_type = "default";
        let source = "";
        if (page_name === "institute-calendar"){
            calendar_type = "institute";
            source = $routeParams.source;
        }
        else if(page_name === "teacher-calendar" || page_name === "teacher-home"){
            calendar_type = "teacher";
            source = $routeParams.source;
        }
        else if(page_name === "student-calendar" || page_name === "student-home"){
            calendar_type = "student";
            source = $routeParams.source;
        }


        if(page_name === "system-admin-home" || page_name === "admin-home"){
            $scope.page_title = "Global calendar";
        }
        else if(page_name === "student-home"){
            $scope.page_title = "Student calendar";
        }
        else if(page_name === "teacher-home"){
            $scope.page_title = "Teacher calendar";
        }



        Models.calendar_source_id($http,calendar_type,source,function (source_id,institute_id,user_data) {

            let calender = Models.calendar_init($rootScope,$http);
            let options = {
                type:calendar_type,
                source_id:source_id,
            };

            if (calendar_type === "teacher"){
                options.calender_editable = false;
                // options.absent = true;
                options.institute_id = institute_id;
                let logged_user = Models.logged_user($rootScope);
                if (logged_user){
                    let designation = logged_user.designation;
                    if (designation === "Head teacher"){
                        options.calender_editable = true;
                    }

                }
            }
            else if (calendar_type === "student"){
                options.calender_editable = true;
                // options.absent = true;
                options.institute_id = institute_id;
                let logged_user = Models.logged_user($rootScope);
                let logged_user_type = Models.logged_user_type($rootScope);
                if (logged_user){
                    let designation = logged_user.designation;
                    if (logged_user_type === "student" || logged_user_type === "admin" || designation === "teacher"){
                        options.calender_editable = false;
                    }


                }
            }
            calender.init(options);
            if ((page_name === "teacher-calendar" || page_name === "student-calendar") && user_data !== undefined){
                $scope.page_title += " of ( " + user_data.name + " )";
            }
        });


    });



}]);
