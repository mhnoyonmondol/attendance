// teacher edit
app.register.controller("edit_teacher",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }
            if (!same_user){
                Models.admin_area_manager($rootScope,$http,$scope.user.designation,$scope.user.area,$scope.user.user_id);
            }
            $scope.same_user = same_user;
            if (same_user){
                $scope.designation_editable = false;
            }

            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_info",function (institute_data) {
                $scope.institute = institute_data;
                /// assign footer
                let footer_data = {
                    source:institute_data.name
                };
                Models.generate_footer(footer_data);

            },["'"+institute_id+"'",0,0]);

            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.member_id = data.member_id;
            $scope.user_name = data.name;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);
                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                    default_value:data.designation_id,
                    onChange:function (value,instance) {
                        $(".password-field").addClass("uk-hidden");
                        let options = Models.get_list_from_selectize(instance);
                        let selected_info = Models.filter_in_array(options,{value:value});
                        if (selected_info){
                            let designation = selected_info.label;
                            if (designation.toLowerCase() === "head teacher"){
                                $(".password-field").removeClass("uk-hidden");
                            }
                        }
                    }
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                if ($scope.user.designation && $scope.user.designation.toLowerCase() === "head teacher"){
                    $(".password-field").removeClass("uk-hidden");
                }
                /// assign trainings selector
                options = {
                    type:"trainings",
                    default_value:data.trainings,
                    class_name: ".trainings",
                    multiple:true
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                /// assign subject wise trainings selector
                options = {
                    type:"subject_wise_trainings",
                    default_value:data.subject_wise_trainings,
                    class_name:".subject-wise-trainings",
                    multiple:true
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                instance.setValue($scope.user.shift);
                            }
                        },[false,"'"+institute_id+"'"])
                    }
                });


            });

        });

        $scope.submit = function () {
            let others = {
                form_name:".edit-teacher",
                data: {
                    user_id:user_id,
                    before_member_id: $scope.user.member_id,
                    institute_id: $scope.institute.institute_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_nid: $scope.user.nid,
                    before_signature: $scope.user.signature,
                    before_active: $scope.user.active,
                    edit:1
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                others.data.password = "";

            }
            if ($scope.same_user){
                others.data.active = $scope.user.active;
                others.data.designation = $scope.user.designation_id;
            }

            Models.request_sender($http,"update","edit_teacher",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/teacher/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);
