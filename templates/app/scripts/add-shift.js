// Add shift
app.register.controller("add_shift",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Add shift";
    $timeout(function () {
        Models.init_requirement();
    });
    $scope.submit = function () {
        let other_data = {
            form_name:".add-shift",
        };
        Models.request_sender($http,'post',"add_shift",function (data) {
            // console.log(data);
            if (data.status){
                let url = "/edit-shift/"+data.id;
                Models.add_action(data,$location,url);
            }

        },[],other_data)
    };


}]);