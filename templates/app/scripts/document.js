// Invoice bill reciept
app.register.controller("document",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $rootScope.document_switch = 1;
    Models.multiple_script_loader([
        "static/app/assets/js/plugins/Simple-Inline-Autocomplete-Autosuggest-Plugin-suggest-js/src/jquery.suggest.js",
        "static/app/assets/js/js_jquery.scrollTo-min.js",
        "static/app/assets/js/pages/page_documents.js",
    ],function () {
        Models.get_basic_info($http,$rootScope,function () {
            $timeout(function () {
                $scope.root = $rootScope;
                // $rootScope.document_save_items = [];
                $scope.route_params = $routeParams;
                $scope.doc_type = 'application';
                let current_date = moment().format("YYYY-MM-DD");
                $scope.current_date = current_date;

                // console.log(current_date);
                let page_name = Models.page_name($location);
                if (page_name !== ""){
                    $scope.doc_type = page_name;
                }
                if (page_name === 'pending-application'){
                    $scope.doc_type = 'application';
                }

                ///model for  documents
                $scope.document_manager = {
                    training:0,
                    leave:0,
                };
                $scope.document_add_permission = 0;
                $scope.default_document_sub_type = "leave";
                // $scope.default_document_sub_type = "sale_return";
                // if ($scope.doc_type === "bill"){
                //     $scope.default_document_sub_type = "cash_bill";
                //     // $scope.default_document_sub_type = "transfer_bill";
                // }

                let url_document_sub_type = $routeParams.document_sub_type;
                if (url_document_sub_type !== undefined){
                    $scope.default_document_sub_type = url_document_sub_type;
                }
                $scope.document_switch_on = function(type){
                    for (let x in $scope.document_manager){
                        $scope.document_manager[x] = 0;
                    }
                    $scope.document_manager[type] = 1;
                };
                let without_add = [
                    // "cash_memo",
                ];
                $scope.document_add_permission_checker = function(document_sub_type){
                    let index = without_add.indexOf(document_sub_type);
                    if (index === -1){
                        $scope.document_add_permission = 1;
                    }
                    else{
                        $scope.document_add_permission = 0;
                    }

                };
                $scope.document_add_permission_checker($scope.default_document_sub_type);

                $scope.document_type_selector = function () {
                    // $scope.document_type_option_name = Models.document_type_option_name($scope.doc_type,$scope.default_document_sub_type,$rootScope);
                    let basic_info = $rootScope.basic_info;
                    Models.custom_selectize(".document-sub-types",{
                        onInitialize:function () {
                            if (basic_info !== undefined){
                                let document_types = basic_info.document_types;
                                if (document_types !== undefined){
                                    let doc_type_info = Models.custom_filter(document_types,"label",$scope.doc_type);
                                    if (doc_type_info !== undefined){
                                        let doc_type_options = doc_type_info.options;
                                        if (doc_type_options !== undefined){
                                            let i = 0;
                                            for (let x of doc_type_options){

                                                let select_object = {
                                                    label:x.name,
                                                    value:x.label
                                                };
                                                this.addOption(select_object);
                                                if (x.label === $scope.default_document_sub_type){
                                                    this.setValue(x.label,"silent");
                                                    $scope.document_sub_type = x.label;
                                                    $scope.document_type_option_name = x.name;
                                                    $("#invoice-filtering").attr("placeholder","Find "+x.name+"...");

                                                }
                                                i++;
                                            }
                                            this.removeOption("sale_delete");
                                        }
                                        else{
                                            Models.notify("Document type options undefined");
                                        }
                                    }
                                    else{
                                        Models.notify("Document type undefined");
                                    }


                                }

                            }
                        },
                        onChange:function (value) {
                            $scope.$apply(function () {
                                $scope.document_sub_type = value;
                            });

                            let options = Models.get_list_from_selectize(this);
                            let filter = Models.custom_filter(options,"value",value);
                            // console.log(filter);
                            if (filter !== undefined){
                                $scope.$apply(function () {
                                    $scope.document_switch_on(value);
                                    $scope.document_add_permission_checker(value);
                                    $scope.document_type_option_name = filter.label;
                                    $("#invoice-filtering").attr("placeholder","Find "+filter.label+"...");
                                });
                            }
                        }
                    });

                    /// new line maker by ctrl+enter button
                    $(document).on("keyup keydown","#document_form .service-item input",function (e) {
                        let that = this;
                        let keyCode = e.keyCode || e.which;
                        /// new service add by ctrl+enter
                        if (e.type === "keyup"){
                            if (e.ctrlKey && e.keyCode === 13) {
                                // Ctrl-Enter pressed
                                let empty = Models.empty_service_item();
                                if(!empty){
                                    $("#document_form_append_service_btn").click();
                                }

                            }
                        }
                        // enter button submit off
                        if (keyCode === 13) {
                            e.preventDefault();
                            return false;
                        }
                    });
                    // $(document).on("keyup keydown","body",function (e) {
                    //     let keyCode = e.keyCode || e.which;
                    //     if (e.type === "keydown"){
                    //         if (e.ctrlKey && e.keyCode === 83) {
                    //             e.preventDefault();
                    //             // Ctrl-s pressed
                    //             $("#document_form [data-button-name='save_and_new']").click();
                    //             return false;
                    //         }
                    //     }
                    // });


                };

                let logged_user = Models.logged_user($rootScope);
                let designation = logged_user.designation;
                let institute_id = logged_user.institute_id;

                //requirement for document list
                $scope.other = {};
                $scope.function_name = "documents";
                $scope.limit = 20;
                $scope.skip = 0;
                $scope.rows = [];
                $scope.total = 0;



                // initiate common function
                $scope.load_data = function(call_back=function () {}){
                    Models.load_data_with_limit($http,$scope,$timeout,function (data) {
                        call_back(data);
                    });

                };
                $scope.document_list_maker = function(call_back,empty_call_back=function () {}){
                    Models.load_data_with_limit_total_counter($http,$scope,function (data) {
                        let total = data.total;
                        $scope.total = total;
                        if (total){
                            $scope.load_data(function (data) {
                                call_back(data);
                            })
                        }
                        else{
                            empty_call_back();
                        }


                    })
                };

                $scope.default_actions = function(){
                    $scope.document_type_selector();
                    altair_md.card_single();
                    altair_md.list_outside(function () {
                        $scope.load_data();
                    });
                    altair_secondary_sidebar.init();
                    $scope.document_switch_on($scope.default_document_sub_type);
                    $scope.document_sub_type = $scope.default_document_sub_type;

                };
                $timeout(function () {
                    $scope.default_actions();
                });

                let document_sub_type = null;
                $scope.keyword = "";
                $scope.search = function(){
                    if (document_sub_type){
                        $scope.rows = [];
                        $scope.skip = 0;
                        $scope.other.data.keyword = $scope.keyword;
                        $scope.document_list_maker(function (data) {

                        })
                    }
                };

                $scope.templates_init = function () {
                    $timeout(function () {
                        document_sub_type = $scope.document_sub_type;
                        $scope.rows = [];
                        $scope.total = 0;
                        $scope.skip = 0;
                        $scope.other.data = {
                            document_type:$scope.doc_type,
                            type_option: document_sub_type,
                            designation:designation,
                            institute_id:institute_id,
                        };
                        if (page_name === "pending-application"){
                            $scope.other.data.pending_application = true;
                        }
                        $scope.document_list_maker(function (data) {
                            // console.log(data);
                            //if data available
                            $timeout(function () {
                                altair_documents.init($http,$scope);

                            });

                        },function () {
                            // if data not available
                            $timeout(function () {
                                altair_documents.init($http,$scope);
                            });
                        });
                    });

                };
                //// save the invoice in server
                $("#page_content").on("click","#document_form [data-button-name]",function (e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    let button_name = $(this).attr("data-button-name");
                    console.log($scope.document_sub_type);

                    if ($scope.document_sub_type){

                        if($scope.doc_type === "application"){
                            let instiute_id = null;
                            let institute_selector = $(".institute-selector");
                            if (institute_selector.length){
                                let institute_selector_selectize = institute_selector.get(0).selectize;
                                instiute_id = institute_selector_selectize.getValue();
                            }
                            else{
                                let logged_user = Models.logged_user($rootScope);
                                if (logged_user){
                                    instiute_id = logged_user.institute_id;
                                }
                            }

                            let other = {
                                form_name:$(this).closest("form"),
                                data:{
                                    document_sub_type:$scope.document_sub_type,
                                    document_type:$scope.doc_type
                                }
                            };

                            if (instiute_id === "" || instiute_id === null){
                                Models.notify("Institute_id not found");
                                retrurn ;
                            }
                            else{
                                other.data.institute_id = instiute_id;
                            }

                            if (button_name === "update"){
                                let document_row_id = $(".add-document").attr("data-id");
                                other.data.document_row_id = document_row_id;
                                other.data.edit = true;

                                Models.request_sender($http,"update","edit_application",function (data) {
                                    if (data.status){
                                        altair_documents.open_document($http,$scope);
                                    }

                                },[],other);
                            }
                            else{
                                Models.request_sender($http,"post","add_application",function (data) {
                                    if (data.status){
                                        let new_document_data = data.data;
                                        $scope.rows.unshift(new_document_data);
                                        $timeout(function () {
                                            if (button_name === "save"){
                                                altair_documents.open_document($http,$scope);
                                            }
                                            else{
                                                $("#document_add").click();
                                            }

                                        });
                                    }

                                },[],other);
                            }

                        }
                        // end receipt condition


                    }
                    else{
                        Models.notify("Document sub type undefined");
                    }
                });

                /// document delete
                $("#page_content").on("click",".document-delete",function () {
                    let document_row_id = $(this).attr("data-id");
                    let other = {};
                    Models.confirm(function () {
                        Models.request_sender($http,"other","document_deleter",function (data) {
                            console.log(data);
                            if (data.status){
                                let index = Models.index_number($scope.rows,"id",document_row_id);
                                if (index !== -1){
                                    $scope.rows.splice(index,1);
                                    $timeout(function () {
                                        altair_documents.open_document($http,$scope);
                                    },1000);
                                }
                                else{
                                    Models.notify("Delete row item not found");
                                }
                            }
                        },["'"+document_row_id+"'","'"+$scope.document_sub_type+"'"]);
                    });

                });

                /// document authorized
                $("#page_content").on("click","button.document-authorize",function () {
                    let document_row_id = $(this).attr("data-id");
                    let status = $(this).hasClass('done') ? 1 : 2;

                    let other = {
                        data:{
                            status
                        }
                    };
                    Models.confirm(function () {
                        Models.request_sender($http,"post","document_authorize",function (data) {
                            if (data.status){
                                altair_documents.open_document($http,$scope);
                                let document_element = $("#documents_list").find("[data-document-row-id='"+document_row_id+"']");
                                document_element.find(".authorize-status").removeClass("uk-badge-danger uk-badge-warning uk-badge-danger");
                                if (status == 1){
                                    document_element.find(".authorize-status").addClass("uk-badge-success").text("Permitted")
                                }
                                else if (status == 1){
                                    document_element.find(".authorize-status").addClass("uk-badge-danger").text("Canceled")
                                }
                            }
                        },["'"+document_row_id+"'"],other);
                    });

                });
            });


        });
    });

}]);
