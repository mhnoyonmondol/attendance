// institute device manage
app.register.controller("institute_device_manage",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let institute_member_id = $routeParams.institute_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            $scope.institute = data;
            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_device_info",function (data) {
                $scope.institute_device = data;
                $timeout(function () {
                    Models.init_requirement();
                });
            },["'"+institute_id+"'"])

        },["'"+institute_member_id+"'",1,0]);

    });

    $scope.submit = function () {

        let other_data = {
            form_name:".device-form",
            data:{
                institute_id:$scope.institute.institute_id
            }
        };

        if ($scope.institute_device.id){
            other_data.data.edit = true;
        }

        Models.request_sender($http,'post',"institute_device_manage",function (data) {
            if (data.status){
                Models.notify("Done");
                $rootScope.route_reload();

            }
            else{
                Models.notify("Failed");
            }

        },[],other_data)
    };

}]);

