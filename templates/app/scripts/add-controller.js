app.register.controller("add_controller",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        $scope.page_title = "";
        $scope.controller_type = undefined;
        $scope.access_menus_switch = 0;
        $scope.permission_parts_switch = 0;

        let controller_type = $routeParams.controller_type;
        $scope.page_title = Models.readable_text("Add new "+controller_type);

        if ($scope.page_title !== ""){
            $scope.controller_type = controller_type;
        }

        if ($scope.controller_type){

            if ($scope.controller_type === "system_admin"){
                $scope.access_menus_switch = 1;
                $scope.permission_parts_switch = 1;
            }


            Models.request_sender($http,"get","controller_enrol_basic_info",function (data) {
                // console.log(data);
                if (data.status){
                    let member_id = data.member_id;
                    $scope.member_id = member_id;
                    $scope.estimate_access_menus = data.access_menus;
                    $scope.permission_parts = data.permission_parts;
                    if ($scope.controller_type === "system_admin"){
                        $timeout(function () {
                            Models.tree_script_loader(function () {
                                altair_tree.tree_a();
                                altair_tree.tree_a(".access-permission-parts");
                            });
                        });
                    }
                    Models.init_requirement();


                }

            },["'"+$scope.controller_type+"'"]);

            /// let's work here for add information
            $timeout(function () {
                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                };
                let logged_controller_type = Models.logged_user_type($rootScope);
                if (logged_controller_type === "admin"){
                    let logged_user = $rootScope.basic_info.logged_user;
                    let designation = logged_user.designation;
                    let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
                    let allow = area_parent.allow;
                    options.allow = allow;
                    options.allow_compare_field = "tag";
                    options.editable = false;
                }
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign blood group selector
                options = {
                    type:"blood_group",
                    class_name:".blood-tag-chooser"

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign department selector
                options = {
                    type:"department",
                    class_name:".department-tag-chooser"

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                let weekly_calender = null;
                let weekly_calender_init = null;
                $(".login-schedule [data-switchery]").change(function () {
                    let status = $(this).prop("checked");
                    if (status){
                        if (!weekly_calender){
                            weekly_calender = Models.working_schedule_calendar_init();
                        }
                        if (!weekly_calender_init){
                            let calender_option = {};
                            weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                        }

                        $(".schedule-container").removeClass("uk-hidden");
                    }
                    else{
                        $(".schedule-container").addClass("uk-hidden");
                    }

                });


            });
            $scope.submit = function () {
                let others = {
                    form_name:".add-controller",
                    data:{
                        basic_salary:0
                    }
                };
                others.data.login_schedule_switch = 0;
                others.data.login_schedule = "";
                let login_schedule_switch = $(".login-schedule [data-switchery]").prop("checked");
                if (login_schedule_switch){
                    others.data.login_schedule_switch = 1;
                    if ($(".scheduler:visible").length){
                        let export_data = $(".scheduler").jqs('export');
                        let json_data = JSON.parse(export_data);
                        let readable_data = [];
                        for (let item of json_data){
                            let new_periods = [];
                            for (let period of item.periods){
                                let new_period = [period.start,period.end];
                                new_periods.push(new_period);
                            }
                            let new_schedule = {
                                day: item.day,
                                periods: new_periods
                            };
                            readable_data.push(new_schedule);
                        }

                        others.data.login_schedule = JSON.stringify(readable_data);
                    }

                }

                if ($scope.controller_type === "system_admin"){
                    let tree = [
                        {
                            element_name:".tree-checkbox" ,
                            selected_name:"menu_ids[]" ,
                            active_name:"parent"
                        },
                        {
                            element_name:".access-permission-parts" ,
                            selected_name:"permission_parts[]" ,
                            active_name:"parent"
                        },
                    ];
                    others.fancy_tree = tree;

                }


                Models.request_sender($http,'post',"add_controller",function (data) {
                    // console.log(data);
                    if (data.status){

                        if ($scope.controller_type === "system_designer"){
                            $location.path("/login-info-helper");
                            Models.add_action(data,$location);
                        }
                        else{
                            let url = "/profile/"+data.member_id;
                            Models.add_action(data,$location,url);
                        }

                    }

                },["'"+$scope.controller_type+"'"],others)
            };
        }
    });



}]);
