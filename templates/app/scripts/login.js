// login
app.register.controller("login",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let elements,body,page_content;
    $timeout(function () {
        elements = $("#header_main,#sidebar_main");
        body = $("body");
        page_content = $("#page_content");
        elements.addClass("uk-hidden");
        page_content.removeAttr("id");
        body.addClass("login_page login_page_v2");

        Models.init_requirement();
        $scope.login_card_in_middle = function () {
            let height_of_window = $(window).height();
            let card_height = $(".uk-container-center").closest("div").outerHeight();
            if (height_of_window > card_height){
                let net_height = height_of_window - card_height;
                if (Models.is_divisional(net_height,2)){
                    let one_side_height = net_height / 2;
                    $(".login_page").css({
                        padding:one_side_height+"px 24px"
                    });
                }
            }
        };
        $scope.login_card_in_middle();
        $(window).resize(function () {
            $scope.login_card_in_middle();
        });
    });
    $scope.login = function () {
        let others = {
            form_name: ".login-form"
        };
        Models.request_sender($http,"get","login",function (data) {
            if (data.status){
                $(".login_page").css({
                    padding:'48px 0 0 0'
                });

                $rootScope.basic_info = data.basic_info;
                $rootScope.main_menus = undefined;

                let logged_user = $rootScope.basic_info.logged_user;
                let member_id = logged_user.member_id;

                $scope.main_menu_switch = 0;
                service.main_menu_reload();
                let logged_user_type =  Models.logged_user_type($rootScope);
                $rootScope.logged_user_type = logged_user_type;
                elements.removeClass("uk-hidden");
                page_content.attr("id","page_content");
                body.removeClass("login_page login_page_v2");

                $location.path("/home");
                altair_main_sidebar.init();
            }

        },[],others);
    }

}]);
