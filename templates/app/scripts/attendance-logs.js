// institute teacher attendance logs
app.register.controller("attendance_logs",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.logs_switch = 0;
    Models.get_basic_info($http,$rootScope,function () {
        let institute_member_id = $routeParams.institute_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            $scope.institute = data;

            $timeout(function () {
                $scope.logs_switch = 1;
            });
        },["'"+institute_member_id+"'",1,0]);

    });
}]);

