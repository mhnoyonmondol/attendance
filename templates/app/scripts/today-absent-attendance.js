// Shortcut important list like items,customer,invoices in secondary sidebar
app.register.controller("today_absent_attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let user_type = $routeParams.user_type;
    let source = $routeParams.source;
    $scope.page_title = "Today's absent attendance";
    Models.get_basic_info($http,$rootScope,function () {
        Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
            if (source_user_id) {
                source_user_id = "'" + source_user_id + "'";
                $scope.page_title += " of "+source_info.name;
            }

            /// attendance report datatable
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"status",
                    name:"Status"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute"
                },


            ];
            // make table required format
            $scope.column_format = [];
            let i = 0;
            for(let item of $scope.table_columns_format){
                let ob = {
                    data:item.label
                };
                if (item.label === null){
                    ob.defaultContent = "";
                }
                // if label is time sortable by time assign
                if (item.label === "time" || item.label === "attendance_time"){
                    $scope.order = [[i,'desc']]
                }

                $scope.column_format.push(ob);
                i++;
            }

            Models.data_table_script_load(function () {
                $scope.$applyAsync(function () {
                    $timeout(function () {
                        let table = altair_datatables.dt_tableHeeshab({
                            "columns":$scope.column_format,
                            "columnDefs":$scope.columnDefs,
                            "order":$scope.order,
                            tableReady:function (table_instance) {
                                let button_container = table_instance.buttons().container();
                                let institute_select_element = $(".institute-select-template").html();

                                if (institute_select_element !== undefined){
                                    button_container.append( institute_select_element );
                                    let event_types = Models.event_types($rootScope);
                                    Models.custom_selectize(".institute-selectize",{
                                        onInitialize:function () {
                                            let instance = this;
                                            Models.my_institutes($http,designation,institute_id,function (data) {
                                                let i = 0;
                                                for (let item of data){
                                                    if (source_user_id){
                                                        if (item.institute_id === source_info.institute_id){
                                                            item.label = item.name;
                                                            item.value = item.institute_id;
                                                            instance.addOption(item);
                                                            instance.setValue(item.institute_id);
                                                        }

                                                    }
                                                    else{
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        if (i === 0){
                                                            instance.setValue(item.institute_id);
                                                        }
                                                    }
                                                    i++;

                                                }

                                            })
                                        },
                                        onChange:function (value) {
                                            table_instance.clear().draw();
                                            let current_date = moment().format("YYYY-MM-DD");
                                            Models.request_sender($http,"get","absent_attendance_data",function (data) {
                                                if (data.find_data !== undefined){
                                                    for(let item of data.find_data){
                                                        let status = item.event_type;
                                                        let filter = Models.filter_in_array(event_types,{label:status});
                                                        if (filter){
                                                            status = filter.name;
                                                        }
                                                        item.status = status;
                                                        table_instance.row.add(item).draw();
                                                    }
                                                }
                                            },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'"+user_type+"'",source_user_id]);
                                        }
                                    })
                                }

                            }
                        });
                        if (table !== undefined){
                            let container = table.table_container;
                            table.on("draw",function (e,dt) {


                            });

                        }

                    });
                });

            });


        });

    })
}]);
