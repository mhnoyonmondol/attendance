// area manager
app.register.controller("area_manager",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        /// area manager selectize init

        Models.custom_selectize(".area-parent-selectize",{
            onInitialize:function () {
                let instance = this;
                let area_parents = Models.area_parents($rootScope,"",true);
                for (let item of area_parents) {
                    item.value = item.label;
                    item.label = item.name;
                    this.addOption(item);
                }
            },
            onChange:function (value) {
                let instance = this;
                let child_selector = $(".child-selector");
                child_selector.empty();

                if (value !== ""){
                    let template_data = {

                    };
                    let parent_info = Models.area_parents($rootScope,value);

                    if (parent_info){
                        let child = parent_info.child;
                        template_data.child = value;
                        template_data.sub_child = child;

                    }
                    let template = $(".area-manager-template").html();
                    if (template !== undefined){
                        template = Models.compiler(template,template_data);
                        child_selector.html(template);
                        setTimeout(function() {
                            // reinitialize uikit margin
                            altair_uikit.reinitialize_grid_margin();
                        },560); //2 x animation duration
                    }
                    let create = false;
                    if (value === "country"){
                        create = function(input) {
                            return {
                                value: input,
                                label: input
                            }
                        };
                    }

                    Models.custom_selectize(".area-child-selectize",{
                        create: create,
                        onInitialize:function () {
                            let instance = this;
                            Models.request_sender($http,"get","child_areas",function (response) {

                                if (response.find_data !== undefined){
                                    for(let item of response.find_data){
                                        item.label = item.name;
                                        item.value = item.id;
                                        instance.addOption(item);
                                    }
                                }
                            },["'"+value+"'",true]);
                        },
                        onChange:function (value) {
                            let instance = this;
                            let child_element = $(".area-child-selectize");
                            let sub_child_element = $(".area-sub-child-selectize");
                            let sub_child_instance = sub_child_element.get(0).selectize;
                            sub_child_instance.clearOptions();

                            /// child type is cluster then Add new option will be disabled
                            let child_type = child_element.attr("data-type");
                            if (child_type === "cluster"){
                                sub_child_instance.destroy();
                                Models.custom_selectize(sub_child_element,{
                                    plugins: {
                                        'remove_button': {
                                            label: ''
                                        }
                                    },
                                    onInitialize:function () {
                                        let instance = this;
                                        Models.request_sender($http,"get","estimate_institutes",function (response) {
                                            if (response.find_data !== undefined){
                                                for (let item of response.find_data){
                                                    item.label = item.name;
                                                    item.value = item.institute_id;
                                                    instance.addOption(item);
                                                }

                                                Models.child_areas($http,sub_child_instance,value,true);
                                            }

                                        })
                                    }
                                });
                                sub_child_instance = sub_child_element.get(0).selectize;

                            }


                            if (value !== undefined){
                                if(child_type !== "cluster"){
                                    Models.child_areas($http,sub_child_instance,value);
                                }

                            }
                            instance.settings.heightAdjust(instance);

                        },
                        render: {
                            option: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            },
                            item: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            }
                        },
                    });

                    Models.custom_selectize(".area-sub-child-selectize",{
                        plugins: {
                            'remove_button': {
                                label: ''
                            }
                        },
                        create: function(input) {
                            return {
                                value: input,
                                label: input
                            }
                        },
                        render: {
                            option: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            },
                            item: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            }
                        },
                        onChange:function () {
                            this.settings.heightAdjust(this);
                        }
                    });

                }

            }
        });

    });

    $scope.submit = function () {
        let other_data = {
            form_name:".area-manager-form",
            data:{

            }
        };
        let sub_child_selectize = $(".area-sub-child-selectize");
        if (sub_child_selectize.length){
            let sub_child_type = sub_child_selectize.attr("data-type");
            other_data.data.child_type = sub_child_type;
        }
        else{
            Models.notify("Sub child element not found");
            return false;
        }
        Models.request_sender($http,'post',"area_manager",function (data) {

            if (data.status){
                Models.notify("Done");
                $(".child-selector").empty();
                let parent_instance = $(".area-parent-selectize").get(0).selectize;
                parent_instance.setValue("");
            }

        },[],other_data)
    };

}]);
