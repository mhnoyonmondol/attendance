// manual attendance
app.register.controller("attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.institute_id = "";
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let designation = logged_user.designation;
            Models.custom_selectize(".attendance-institute-chooser",{
                searchField: ['label','value','member_id'],
                onInitialize:function () {
                    let that = this;
                    Models.get_document_save_institutes($rootScope,function (data) {
                        let institutes = data;
                        for (let institute_info of institutes){
                            institute_info.label = institute_info.name;
                            institute_info.value = institute_info.institute_id;
                            that.addOption(institute_info);
                        }

                        if (!institutes.length){
                            Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                // console.log(data);
                                if (data.find_data !== undefined){
                                    for (let institute_info of data.find_data){
                                        $rootScope.document_save_institutes.push(institute_info);
                                        institute_info.label = institute_info.name;
                                        institute_info.value = institute_info.institute_id;
                                        that.addOption(institute_info);
                                    }
                                    // that.refreshOptions();
                                }

                            },["''","'"+designation+"'"]);
                        }
                    });
                },
                onType:function (value) {
                    let that = this;
                    if (!that.currentResults.total){
                        Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                            // console.log(data);
                            if (data.find_data !== undefined){
                                for (let institute_info of data.find_data){
                                    $rootScope.document_save_institutes.push(institute_info);
                                    institute_info.label = institute_info.name;
                                    institute_info.value = institute_info.institute_id;
                                    that.addOption(institute_info);
                                }
                                that.refreshOptions();
                            }

                        },["'"+value+"'","'"+designation+"'"]);
                    }

                },
                onChange:function (value) {
                    let institute_id = value;
                    $scope.institute_id = institute_id;
                    Models.request_sender($http,"get","institute_teachers",function (response) {
                        if (response.find_data !== undefined){
                            $scope.teachers = response.find_data;
                            $scope.current_date = moment().format("YYYY-MM-DD");
                            let date = $scope.current_date;
                            $timeout(function () {
                                Models.init_requirement();
                                // default checked
                                $(".md-list li").each(function () {
                                    let element = $(this);
                                    let absent_input = element.find("[value='none']");
                                    absent_input.iCheck('check');
                                });
                                $scope.assign_attendance = function (institute_id,date) {
                                    Models.request_sender($http,"get","manual_attendance",function (response) {
                                        if (response.find_data !== undefined){
                                            let attendance = response.find_data;
                                            for(let item of attendance){
                                                let event_type = item.event_type;
                                                let note = item.note;
                                                let user_id = item.user_id;
                                                let element = $("[data-id='"+user_id+"']");
                                                let input = element.find("[value='"+event_type+"']");
                                                input.iCheck('check');
                                                element.find("[name='notes[]']").val(note);

                                            }
                                            if (!attendance.length){
                                                $("[value='none']").each(function () {
                                                    $(this).iCheck('check');
                                                });
                                                $("[name='notes[]']").each(function () {
                                                    $(this).val("");
                                                });
                                            }
                                        }
                                    },["'"+institute_id+"'","'"+date+"'"]);
                                };

                                $scope.assign_attendance(institute_id,date);
                                $("[name='date']").on("change",function () {
                                    let this_date = $(this).val();
                                    $scope.assign_attendance(institute_id,this_date);
                                });

                            });

                        }
                        else{
                            Models.notify("Teachers not found");
                        }
                    },["'"+institute_id+"'"]);

                }

            });
            Models.init_requirement();
        }

    });

    $scope.submit = function () {

        let other_data = {
            form_name:".add-attendance",
            data:{
                institute_id:$scope.institute_id
            }
        };

        Models.request_sender($http,'post',"manual_attendance",function (data) {
            if (data.status){
                Models.notify("Done");

            }
            else{
                Models.notify("Failed");
            }

        },[],other_data)
    };

}]);
