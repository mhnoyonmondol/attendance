// School for head master
app.register.controller("school",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let institute_member_id = logged_user.institute_member_id;
            $location.path("/institute/"+institute_member_id);
        }
    });



}]);
