//head teacher home 2 for student dashboard
app.register.controller("head_teacher_home_2",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.data_table_script_load(function () {
        Models.get_basic_info($http,$rootScope,function (data) {
            let logged_user_id = Models.logged_user_id($rootScope);
            if (!logged_user_id){
                $location.path('/login');
                $rootScope.looged = 0;
            }
            else{
                let logged_user = Models.logged_user($rootScope);
                let designation = logged_user['designation'];
                let institute_id = logged_user['institute_id'];
                Models.request_sender($http,"get","dashboard_info",function (data) {
                    // console.log(data);
                    if (data.status){
                        let output = data.output;

                        let target_events = [
                            {
                                name: "Present",
                                total: output.total_student.total,
                                current: output.total_present.current,
                                link: "today-present-attendance/student",
                                card_type: "peity_orders",
                            },
                            {
                                name: "Absent",
                                total: output.total_student.total,
                                current: output.total_absent.current,
                                link: "today-absent-attendance/student",
                                card_type: "peity_orders",
                            },

                            // {
                            //     name: "In training",
                            //     total: output.total_student.total,
                            //     current: output.total_training.current,
                            //     link: "today-training-attendance",
                            //     card_type: "peity_orders",
                            // },
                            {
                                name: "Leave",
                                total: output.total_student.total,
                                current: output.total_leave.current,
                                link: "today-leave-attendance/student",
                                card_type: "peity_orders",
                            },
                            {
                                name: "Late in",
                                total: output.total_student.total,
                                current: output.total_late.current,
                                link: "today-late-attendance/student",
                                card_type: "peity_orders",
                            },
                            {
                                name: "Early gone",
                                total: output.total_student.total,
                                current: output.total_early.current,
                                link: "today-early-attendance/student",
                                card_type: "peity_orders",
                            },

                        ];
                        let peity_chart = Models.peity_charts_init();
                        let chartist = Models.chartist_init();
                        let card_template = $(".info-card").html();
                        if (card_template !== undefined){
                            let template_output = Models.compiler(card_template,target_events);

                            $(".top-cards").html(template_output);
                            chartist.init({
                                columns:[
                                    ['Present', output.total_present.current],
                                    ['Absent', output.total_absent.current],
                                    ['Training', output.total_training.current],
                                    ['Leave', output.total_leave.current],
                                ]
                            },function (instance) {
                                $timeout(function () {
                                    instance.resize();
                                },250);

                            });

                            altair_helpers.hierarchical_show();
                            peity_chart.init();

                            /// attendance report datatable

                            $scope.columnDefs = [
                                {
                                    targets:-2,
                                    render:function (data) {
                                        let time_for_event = data;
                                        if (time_for_event === "0000-00-00 00:00:00"){
                                            time_for_event = moment().format("YYYY-MM-DD");
                                        }
                                        return time_for_event;
                                    },
                                    orderable:false
                                }
                            ];
                            $scope.table_columns_format = [
                                {
                                    label:"source_name",
                                    name:"Student name"
                                },
                                {
                                    label:"status",
                                    name:"Status"
                                },
                                {
                                    label:"time_for_event",
                                    name:"Time for event"
                                },
                                {
                                    label:"institute_name",
                                    name:"Institute"
                                },


                            ];
                            // make table required format
                            $scope.column_format = [];
                            let i = 0;
                            for(let item of $scope.table_columns_format){
                                let ob = {
                                    data:item.label
                                };
                                if (item.label === null){
                                    ob.defaultContent = "";
                                }
                                // if label is time sortable by time assign
                                if (item.label === "time" || item.label === "attendance_time"){
                                    $scope.order = [[i,'desc']]
                                }

                                $scope.column_format.push(ob);
                                i++;
                            }

                            Models.data_table_script_load(function () {
                                $scope.$applyAsync(function () {
                                    $timeout(function () {
                                        let table = altair_datatables.dt_tableHeeshab({
                                            "columns":$scope.column_format,
                                            "columnDefs":$scope.columnDefs,
                                            "order":$scope.order,
                                            tableReady:function (table_instance) {
                                                let button_container = table_instance.buttons().container();
                                                let institute_select_element = $(".institute-select-template").html();

                                                if (institute_select_element !== undefined){
                                                    button_container.append( institute_select_element );
                                                    let event_types = Models.event_types($rootScope);
                                                    Models.custom_selectize(".institute-selectize",{
                                                        onInitialize:function () {
                                                            let instance = this;
                                                            Models.my_institutes($http,designation,institute_id,function (data) {
                                                                let i = 0;
                                                                for (let item of data){
                                                                    item.label = item.name;
                                                                    item.value = item.institute_id;
                                                                    instance.addOption(item);
                                                                    if (i === 0){
                                                                        instance.setValue(item.institute_id);
                                                                    }
                                                                    i++;
                                                                }
                                                            })
                                                        },
                                                        onChange:function (value) {
                                                            table_instance.clear().draw();
                                                            let current_date = moment().format("YYYY-MM-DD");
                                                            Models.request_sender($http,"get","events_of_institutes",function (data) {
                                                                // console.log(data);
                                                                if (data.find_data !== undefined){
                                                                    for(let item of data.find_data){
                                                                        let status = item.event_type;
                                                                        let filter = Models.filter_in_array(event_types,{label:status});
                                                                        if (filter){
                                                                            status = filter.name;
                                                                        }
                                                                        item.status = status;
                                                                        table_instance.row.add(item).draw();
                                                                    }
                                                                }
                                                            },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'","'student'"]);
                                                        }
                                                    })
                                                }

                                            }
                                        });

                                    });
                                });

                            });


                            $(window).resize();

                        }

                    }
                    else{
                        Models.notify("Data not found");
                    }


                },["'head_teacher_2'","'"+designation+"'","'"+institute_id+"'"]);

            }

        })
    });


}]);
