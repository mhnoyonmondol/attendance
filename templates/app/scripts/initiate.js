// compress
app.register.controller("initiate",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function () {
        $timeout(function () {
            Models.init_requirement();
            let overlay = altair_md.card_overlay();
            let expandable_height = 100;
            Models.custom_selectize(".user-type-selectize",{
                options:[
                    {
                        label:"Teacher",
                        value:"teacher",
                    },
                    {
                        label:"Student",
                        value:"student",
                    },
                ],

            });
            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    })
                },
                onFocus:function () {
                    let this_card = $(this.$input).closest(".md-card");
                    let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                    let height_of_truncate = truncate.outerHeight();
                    truncate.height(height_of_truncate + expandable_height);
                    overlay.rearrange();
                    setTimeout(function () {
                        $(window).resize();
                    },300)
                },
                onBlur:function () {
                    let this_card = $(this.$input).closest(".md-card");
                    let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                    let height_of_truncate = truncate.outerHeight();
                    truncate.height(height_of_truncate - expandable_height);
                    overlay.rearrange();
                    setTimeout(function () {
                        $(window).resize();
                    },300)
                }
            });
        });

    });
    $scope.script_compress = function () {
        Models.request_sender($http,'get','initiate',function (data,status) {
            Models.add_action(data,$location);
        });
    };

    $scope.main_domain_database_update = function () {
        Models.request_sender($http,'other','main_domain_database_update',function (data,status) {
            if (data.status){
                Models.add_action(data,$location);
            }

        });
    };
    $scope.main_database_backup = function () {
        Models.request_sender($http,'other','main_database_backup',function (data,status) {
            if (data.status){
                Models.add_action(data,$location);
            }

        });
    };
    $scope.shift_initiate = function () {
        let other = {
            form_name:".shift-initiate-form",
            data:{

            }
        };
        Models.request_sender($http,'update','shift_initiate',function (data) {
            if (data.status){
                Models.add_action(data,$location);
            }

        },[],other);
    };






}]);
