// data tables
app.register.controller("data_table",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.data_table_script_load(function () {
        $timeout(function () {
            Models.get_basic_info($http, $rootScope, function (basic_info) {
                let branch_id = basic_info.logged_branch_id;
                $scope.columnDefs = [];
                $scope.order = [];
                let page_name = Models.page_name($location);
                if (page_name === "items") {
                    $scope.query_data = {};
                    $scope.page_title = "Items";
                    $scope.collection_name = "items";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".item-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Item name"
                        },
                        {
                            label: "code",
                            name: "Item code"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },

                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }

                else if (page_name === "vendors") {

                    $scope.query_data = {
                        "sql": "where  user_id like :user_type",
                        "sql_data": {
                            ":user_type": Models.id_type("vendor", $rootScope) + "%"
                        },
                    };
                    $scope.page_title = "Vendors";
                    $scope.collection_name = "customer_vendors";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let id_type = Models.id_type_name(data.user_id, $rootScope);
                                data.user_type = id_type;
                                let html = $(".customer-vendor-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "company_name",
                            name: "Company name"
                        },
                        {
                            label: "name",
                            name: "Name"
                        },
                        {
                            label: "member_id",
                            name: "Member id"
                        },
                        {
                            label: "mobile",
                            name: "Mobile"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },

                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }
                else if (page_name === "shifts") {

                    $scope.query_data = {};
                    $scope.page_title = "Shifts";
                    $scope.collection_name = "shifts";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".shift-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        }
                    ];
                    $scope.table_columns_format = [

                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },

                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }
                else if (page_name === "devices") {

                    $scope.query_data = {};
                    $scope.page_title = "Devices";
                    $scope.collection_name = "institute_devices";
                    $scope.columnDefs = [
                        {
                            targets: 1,
                            render: function (data, type, row) {
                                let html = $(".institute-link").html();
                                let template = data;
                                if (html !== undefined) {
                                    template = Handlebars.compile(html)(row);
                                }
                                return template;
                            },
                        }
                    ];
                    $scope.table_columns_format = [

                        {
                            label: "serial_number",
                            name: "Serial number"
                        },
                        {
                            label: "institute_name",
                            name: "School name"
                        },

                        // {
                        //     label:"time",
                        //     name:"Time"
                        // },


                    ];
                }
                else if (page_name === "send-bulk-sms") {

                    $scope.query_data = {};
                    $scope.page_title = "Bulk sms";
                    $scope.collection_name = "bulk_sms";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data, type, row) {
                                let html = $(".bulk-sms-continue").html();
                                let template = data;
                                if (html !== undefined) {
                                    if (row.total !== row.delivered){
                                        template = Handlebars.compile(html)(row);
                                    }

                                }
                                return template;
                            },
                        }
                    ];
                    $scope.table_columns_format = [

                        {
                            label: "title",
                            name: "Title"
                        },
                        {
                            label: "total",
                            name: "Total"
                        },
                        {
                            label: "delivered",
                            name: "Delivered"
                        },


                        {
                            label: "description",
                            name: "Description"
                        },

                        {
                            label:"time",
                            name:"Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },


                    ];
                }
                else if (page_name === "send-sms-test") {

                    $scope.query_data = {};
                    $scope.page_title = "Android sms";
                    $scope.collection_name = "android_sms_records";
                    $scope.columnDefs = [
                        {
                            targets: 2,
                            render: function (data) {
                                let template = data;
                                if (data == 1) {
                                    template = "Delivered";

                                }
                                else{
                                    template = "Pending";
                                }
                                return template;
                            },
                        }
                    ];
                    $scope.table_columns_format = [


                        {
                            label: "mobile",
                            name: "Mobile"
                        },
                        {
                            label: "message",
                            name: "Message"
                        },
                        {
                            label: "active",
                            name: "Delivered"
                        },

                        {
                            label:"time",
                            name:"Time"
                        },

                    ];
                }


                else if (page_name === "customers") {

                    $scope.query_data = {
                        "sql": "where user_id like :user_type",
                        "sql_data": {
                            ":user_type": Models.id_type("customer", $rootScope) + "%"
                        },
                    };
                    $scope.page_title = "Customers";
                    $scope.collection_name = "customer_vendors";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let id_type = Models.id_type_name(data.user_id, $rootScope);
                                data.user_type = id_type;
                                let html = $(".customer-vendor-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "company_name",
                            name: "Company name"
                        },
                        {
                            label: "name",
                            name: "Name"
                        },
                        {
                            label: "member_id",
                            name: "Member id"
                        },
                        {
                            label: "mobile",
                            name: "Mobile"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },

                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }


                else if (page_name === "attendance-logs") {
                    let teacher_digit = Models.id_type("teacher", $rootScope);
                    $scope.query_data = {
                        "institute_id": $scope.institute.institute_id,
                        "sql": "where attendance_logs.user_id like :user_type",
                        "sql_data": {
                            ":user_type": teacher_digit + "%"
                        },
                    };
                    $scope.page_title = "Teacher attendance logs";
                    $scope.collection_name = "attendance_logs";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".raw-item-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "teacher_name",
                            name: "Teacher name"
                        },
                        {
                            label: "device_user_id",
                            name: "User id"
                        },

                        {
                            label: "attendance_type",
                            name: "Type"
                        },
                        {
                            label: "attendance_time",
                            name: "Attendance time"
                        },

                    ];
                }
                else if (page_name === "student-attendance-logs") {
                    let student_digit = Models.id_type("student", $rootScope);
                    $scope.query_data = {
                        "institute_id": $scope.institute.institute_id,
                        "sql": "where attendance_logs.user_id like :user_type",
                        "sql_data": {
                            ":user_type": student_digit + "%"
                        },
                    };
                    $scope.page_title = "Student attendance logs";
                    $scope.collection_name = "attendance_logs";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".raw-item-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "teacher_name",
                            name: "Student name"
                        },
                        {
                            label: "device_user_id",
                            name: "User id"
                        },

                        {
                            label: "attendance_type",
                            name: "Type"
                        },
                        {
                            label: "attendance_time",
                            name: "Attendance time"
                        },

                    ];
                }

                else if (page_name === "admin-daily-attendance" || page_name === "admin-home") {
                    $scope.query_data = {};
                    $scope.page_title = "Daily attendance logs";
                    $scope.collection_name = "attendance_logs";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".raw-item-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // }
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "teacher_name",
                            name: "Teacher name"
                        },
                        {
                            label: "device_user_id",
                            name: "User id"
                        },

                        {
                            label: "attendance_type",
                            name: "Type"
                        },
                        {
                            label: "attendance_time",
                            name: "Attendance time"
                        },

                    ];
                }
                else if (page_name === "teacher-list") {
                    let teacher_type_digit = Models.id_type("teacher", $rootScope);
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        "sql": "where controllers.user_id like :user_id",
                        "sql_data": {
                            ":user_id": teacher_type_digit + "%"
                        },
                        teacher_registration_report: true,
                        designation: designation,
                        institute_id: institute_id,
                    };
                    $scope.page_title = "Teacher registration report";
                    $scope.collection_name = "controllers";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".bank-cash-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "designation",
                            name: "Designation"
                        },
                        {
                            label: "mobile",
                            name: "mobile"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },


                        {
                            label: "time",
                            name: "Time"
                        },
                        // {
                        //     label:null,
                        //     name:"Action"
                        // },

                    ];
                }
                else if (page_name === "student-list") {
                    let student_type_digit = Models.id_type("student", $rootScope);
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        "sql": "where controllers.user_id like :user_id",
                        "sql_data": {
                            ":user_id": student_type_digit + "%"
                        },
                        student_registration_report: true,
                        designation: designation,
                        institute_id: institute_id,
                    };
                    $scope.page_title = "Student registration report";
                    $scope.collection_name = "controllers";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".bank-cash-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "class_name",
                            name: "Class"
                        },
                        {
                            label: "department_name",
                            name: "Department"
                        },

                        {
                            label: "mobile",
                            name: "mobile"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        // {
                        //     label:null,
                        //     name:"Action"
                        // },

                    ];
                }
                else if (page_name === "institute-list") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        teacher_registration_report: true,
                        designation: designation,
                        institute_id: institute_id,
                    };
                    $scope.page_title = "Institute registration report";
                    $scope.collection_name = "institutes";
                    $scope.columnDefs = [
                        // {
                        //     targets:-1,
                        //     render:function (data) {
                        //         let html = $(".bank-cash-action").html();
                        //         let template = Handlebars.compile(html)(data);
                        //         return template;
                        //     },
                        //     orderable:false
                        // },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "contact_number",
                            name: "Contact number"
                        },
                        {
                            label: "address",
                            name: "Address"
                        },
                        {
                            label: "time",
                            name: "Time"
                        },
                        // {
                        //     label:null,
                        //     name:"Action"
                        // },

                    ];
                }
                else if (page_name === "today-present-attendance") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        today_attendance: true,
                        designation: designation,
                        institute_id: institute_id,
                        event_type: "present",
                        user_type: $routeParams.user_type,
                        user_id: $scope.source_user_id,
                    };
                    $scope.page_title = "Today present attendance";
                    $scope.collection_name = "calendar";
                    $scope.columnDefs = [
                        {
                            targets: -2,
                            render: function (data) {
                                let time_for_event = data;
                                if (time_for_event === "0000-00-00 00:00:00") {
                                    time_for_event = moment().format("YYYY-MM-DD");
                                }
                                return time_for_event;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "source_name",
                            name: "Teacher name"
                        },
                        {
                            label: "time_for_event",
                            name: "Time for event"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                    ];
                }
                else if (page_name === "today-training-attendance") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        today_attendance: true,
                        designation: designation,
                        institute_id: institute_id,
                        event_type: "training",
                        user_type: $routeParams.user_type,
                        user_id: $scope.source_user_id,
                    };
                    $scope.page_title = "Today Training attendance";
                    $scope.collection_name = "calendar";
                    $scope.columnDefs = [
                        {
                            targets: -2,
                            render: function (data) {
                                let time_for_event = data;
                                if (time_for_event === "0000-00-00 00:00:00") {
                                    time_for_event = moment().format("YYYY-MM-DD");
                                }
                                return time_for_event;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "source_name",
                            name: "Teacher name"
                        },
                        {
                            label: "time_for_event",
                            name: "Time for event"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                    ];
                }
                else if (page_name === "today-leave-attendance") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        today_attendance: true,
                        designation: designation,
                        institute_id: institute_id,
                        event_type: "leave",
                        user_type: $routeParams.user_type,
                        user_id: $scope.source_user_id,
                    };
                    $scope.page_title = "Today leave attendance";
                    $scope.collection_name = "calendar";
                    $scope.columnDefs = [
                        {
                            targets: -2,
                            render: function (data) {
                                let time_for_event = data;
                                if (time_for_event === "0000-00-00 00:00:00") {
                                    time_for_event = moment().format("YYYY-MM-DD");
                                }
                                return time_for_event;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "source_name",
                            name: "Teacher name"
                        },
                        {
                            label: "time_for_event",
                            name: "Time for event"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                    ];
                }
                else if (page_name === "today-late-attendance") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        today_attendance: true,
                        designation: designation,
                        institute_id: institute_id,
                        event_type: "late",
                        user_type: $routeParams.user_type,
                        user_id: $scope.source_user_id,
                    };
                    $scope.page_title = "Today late attendance";
                    $scope.collection_name = "calendar";
                    $scope.columnDefs = [
                        {
                            targets: -2,
                            render: function (data) {
                                let time_for_event = data;
                                if (time_for_event === "0000-00-00 00:00:00") {
                                    time_for_event = moment().format("YYYY-MM-DD");
                                }
                                return time_for_event;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "source_name",
                            name: "Teacher name"
                        },
                        {
                            label: "time_for_event",
                            name: "Time for event"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                    ];
                }
                else if (page_name === "today-early-attendance") {
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user.designation;
                    let institute_id = logged_user.institute_id;
                    $scope.query_data = {
                        today_attendance: true,
                        designation: designation,
                        institute_id: institute_id,
                        event_type: "early",
                        user_type: $routeParams.user_type,
                        user_id: $scope.source_user_id,
                    };
                    $scope.page_title = "Today early attendance";
                    $scope.collection_name = "calendar";
                    $scope.columnDefs = [
                        {
                            targets: -2,
                            render: function (data) {
                                let time_for_event = data;
                                if (time_for_event === "0000-00-00 00:00:00") {
                                    time_for_event = moment().format("YYYY-MM-DD");
                                }
                                return time_for_event;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "source_name",
                            name: "Teacher name"
                        },
                        {
                            label: "time_for_event",
                            name: "Time for event"
                        },
                        {
                            label: "institute_name",
                            name: "Institute name"
                        },

                    ];
                }

                else if (page_name === "bank-reconciliations") {
                    $scope.query_data = {};
                    $scope.page_title = "Bank reconciliations";
                    $scope.collection_name = "payment_requests";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".payment-request-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        },
                        {
                            targets: -3,
                            render: function (data) {
                                let status = Number(data);
                                let result = "Pending";
                                if (status === 1) {
                                    result = "Honored";
                                }
                                if (status === 3) {
                                    result = "Canceled";
                                }
                                return data = result;

                            },
                            orderable: false
                        },
                        {
                            targets: 0,
                            render: function (data, type, row) {
                                let html = $(".payment-request-sender").html();
                                let template = Handlebars.compile(html)(row);
                                return template;
                            }
                        },


                    ];
                    $scope.table_columns_format = [
                        {
                            label: "sender_name",
                            name: "To"
                        },
                        {
                            label: "bank_name",
                            name: "Bank name"
                        },
                        {
                            label: "amount",
                            name: "Amount."
                        },

                        {
                            label: "check_date",
                            name: "Check date"
                        },
                        {
                            label: "transaction_number",
                            name: "Transaction number"
                        },
                        {
                            label: "note",
                            name: "Note"
                        },
                        {
                            label: "active",
                            name: "Status"
                        },
                        {
                            label: "time",
                            name: "Time"
                        },

                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }


                else if (page_name === "bank-cashes") {
                    $scope.query_data = {};
                    $scope.page_title = "Bank cashes";
                    $scope.collection_name = "bank_cashes";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".bank-cash-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "balance",
                            name: "Balance"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }

                else if (page_name === "transports") {
                    $scope.query_data = {};
                    $scope.page_title = "Transports";
                    $scope.collection_name = "transports";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".transport-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Vichle title"
                        },

                        {
                            label: "model",
                            name: "Model"
                        },
                        {
                            label: "number",
                            name: "Number"
                        },


                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }
                else if (page_name === "branch-items") {
                    $scope.query_data = {
                        add_to_branch: true
                    };
                    $scope.page_title = "Branch items";
                    $scope.collection_name = "items";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".item-add-action-template").html();
                                html = Models.compiler(html, data);
                                return html;
                            },
                            orderable: false
                        },
                        {
                            targets: 1,
                            render: function (data, type, row) {
                                let html = $(".item-price-template").html();
                                html = Models.compiler(html, row);
                                return html;
                            },
                            orderable: false
                        },
                        {
                            targets: 2,
                            render: function (data, type, row) {
                                let html = $(".item-amount-template").html();
                                return html;
                            },
                            orderable: false
                        },

                        {
                            targets: 3,
                            render: function (data, type, row) {
                                let html = $(".item-unit-template").html();
                                html = Models.compiler(html, row);
                                return html;
                            },
                            orderable: false
                        },

                        {
                            targets: 4,
                            render: function (data, type, row) {
                                let date = moment().format("YYYY-MM-DD");
                                let html = $(".item-date-template").html();
                                let template = Models.compiler(html, {date: date});
                                return template;
                            },
                            orderable: false
                        },
                        {
                            targets: 5,
                            render: function (data, type, row) {
                                if (row.stock) {
                                    return "Added";
                                }

                            },
                            orderable: false
                        },
                        {
                            targets: 6,
                            render: function (data, type, row) {
                                if (row.stock) {
                                    let stock = row.stock;
                                    let unit_name = row.unit_name;
                                    let bangla_switch = Models.bangla_switch();
                                    if (bangla_switch) {
                                        stock = Models.number_conversion(stock, "e2b");
                                    }
                                    return stock + " " + unit_name;
                                }
                            },
                            orderable: false
                        },


                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Item name"
                        },

                        {
                            label: null,
                            name: "price"
                        },
                        {
                            label: null,
                            name: "Amount"
                        },
                        {
                            label: null,
                            name: "Unit"
                        },
                        {
                            label: null,
                            name: "Date"
                        },
                        {
                            label: null,
                            name: "Status"
                        },
                        {
                            label: null,
                            name: "Stock"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: ""
                        },

                    ];
                }
                else if (page_name === "branch-bank-cashes") {
                    $scope.query_data = {
                        branch_permission: true
                    };
                    $scope.page_title = "Branch bank cashes";
                    $scope.collection_name = "bank_cashes";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".bank-cash-permission").html();
                                html = Models.compiler(html, data);
                                return html;
                            },
                            orderable: false
                        },

                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Bank name"
                        },
                        {
                            label: "account_number",
                            name: "Account number"
                        },


                        {
                            label: "balance",
                            name: "Balance"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: ""
                        },

                    ];
                }

                else if (page_name === "special-items") {
                    $scope.query_data = {};
                    $scope.page_title = "Special items";
                    $scope.collection_name = "special_items";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".special-item-action").html();
                                html = Models.compiler(html, data);
                                return html;
                            },
                            orderable: false
                        },


                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }

                else if (page_name === "reports") {
                    $scope.query_data = {};
                    $scope.page_title = "Reports";
                    $scope.collection_name = "reports";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".report-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        // {
                        //     label:"source_name",
                        //     name:"Source name"
                        // },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }
                else if (page_name === "dynamic-report-templates") {
                    $scope.query_data = {};
                    $scope.page_title = "Dynamic report templates";
                    $scope.collection_name = "dynamic_report_templates";
                    $scope.columnDefs = [
                        {
                            targets: -1,
                            render: function (data) {
                                let html = $(".dynamic-report-action").html();
                                let template = Handlebars.compile(html)(data);
                                return template;
                            },
                            orderable: false
                        },
                    ];
                    $scope.table_columns_format = [
                        {
                            label: "name",
                            name: "Name"
                        },

                        {
                            label: "time",
                            name: "Time"
                        },
                        {
                            label: null,
                            name: "Action"
                        },

                    ];
                }

                if ($scope.page_title_plus !== undefined) {
                    $scope.page_title += $scope.page_title_plus;
                }


                // make table required format
                $scope.column_format = [];
                let i = 0;
                for (let item of $scope.table_columns_format) {
                    let ob = {
                        data: item.label
                    };
                    if (item.label === null) {
                        ob.defaultContent = "";
                    }
                    // if label is time sortable by time assign
                    if (item.label === "time" || item.label === "attendance_time") {
                        $scope.order = [[i, 'desc']]
                    }

                    $scope.column_format.push(ob);
                    i++;
                }
                let data = {
                    "columns": "*",
                    "sql": "",
                    "sql_data": {},
                    "table_name": $scope.collection_name,
                    "data_table": true
                };

                Object.assign(data, $scope.query_data);
                let data_object = Models.retrive_request_object("request_data_list()");
                data_object['query_data'] = JSON.stringify(data);
                // console.log(data_object);
                // let form_data = Models.form_data_maker("",data_object);
                $timeout(function () {
                    let table = altair_datatables.dt_tableHeeshab({
                        "processing": true,
                        "serverSide": true,
                        "ajax": {
                            url: "request",
                            data: data_object
                        },
                        "columns": $scope.column_format,
                        "columnDefs": $scope.columnDefs,
                        "order": $scope.order,
                        pressExport: function (e, dt, node, config) {
                            let params = dt.ajax.params();
                            let options = {
                                table_name: $scope.collection_name,
                                limit: 0,
                                skip: params.start,
                                data: {},
                            };
                            // if (params.order){
                            //     let order_column = params.order[0];
                            //     let order_column_name = params.columns[order_column.column].data;
                            //     let order_dir = order_column.dir;
                            //     options.data.order_column = order_column_name;
                            //     options.data.order_dir = order_dir;
                            // }

                            Models.export_data_from_data_table($http, options);
                        },
                        onTableInit:function (table) {

                            if(page_name === 'send-sms-test'){

                                let table_load_interval;
                                function table_load_on() {
                                    table_load_interval = setInterval(function () {
                                        table.draw();
                                    },2000);
                                    Models.intervals.push({
                                        key:"test_sms_send",
                                        id:table_load_interval
                                    });
                                }
                                function table_load_off(){
                                    clearInterval(table_load_interval);
                                }
                                table_load_on();
                                $("#page_content").on("mouseover",".documents-flow-table #page_content_inner",function () {

                                    table_load_off();
                                });
                                $("#page_content").on("mouseout",".documents-flow-table #page_content_inner",function () {
                                    table_load_on();
                                });



                            }


                        }
                    });
                    if (table !== undefined) {
                        let container = table.table_container;
                        table.on("draw", function (e, dt) {

                            // let params = table.ajax.params();

                            if ($(".item-unit:visible").length) {
                                Models.custom_selectize(".item-unit");
                                Models.init_requirement();
                            }
                            else if ($(".bank-cash-permission-checkbox:visible").length) {
                                Models.init_requirement();
                            }

                            if (page_name === "branch-items") {
                                let action_column = container.find("thead tr:first th:last");
                                let mark_template = $(".mark-action-template").html();
                                action_column.html(mark_template);
                                let footer_action_column = container.find("tfoot tr:first th:last");
                                let mark_save_button = $(".mark-save-button").html();
                                footer_action_column.html(mark_save_button);
                            }
                        });
                        container.on("click", ".delete", function () {
                            let row = table.row($(this).closest("tr"));
                            let data = row.data();
                            let id = data.id;
                            let table_name = $scope.collection_name;
                            Models.confirm(function () {
                                Models.request_sender($http, "other", "deleter", function (data) {
                                    if (data.status) {
                                        row.remove().draw();
                                    }
                                }, ["'" + table_name + "'", id]);
                            });

                        });
                        container.on("click", ".mark-all", function () {
                            container.find("tbody .add-item-to-branch").addClass("uk-hidden");
                            container.find("tbody .mark-checkbox").removeClass("uk-hidden");
                            container.find("tbody .mark-checkbox").each(function () {
                                let check_box = $(this).find("input");
                                check_box.iCheck('check');
                            });
                            container.find(".save-marked").removeClass("uk-hidden");
                            container.find(".unmark-all").removeClass("uk-hidden");

                        });
                        container.on("click", ".unmark-all", function () {
                            container.find("tbody .add-item-to-branch").removeClass("uk-hidden");
                            container.find("tbody .mark-checkbox").addClass("uk-hidden");
                            container.find("tbody .mark-checkbox").each(function () {
                                let check_box = $(this).find("input");
                                check_box.iCheck('uncheck');
                            });
                            container.find(".save-marked").addClass("uk-hidden");
                            container.find(".unmark-all").addClass("uk-hidden");

                        });


                        container.on("click", ".save-marked", function () {
                            let form_data = [];
                            container.find("tbody tr").each(function () {
                                let this_row = $(this);
                                let checkbox = this_row.find(".mark-checkbox input").prop("checked");
                                if (checkbox) {
                                    let price = this_row.find("[name='price']").val();
                                    let quantity = this_row.find("[name='quantity']").val();
                                    let date = this_row.find("[name='date']").val();
                                    let unit = this_row.find(".item-unit").val();
                                    let row_data = table.row(this_row).data();
                                    let item_id = row_data.item_id;
                                    let default_unit_id = row_data.default_unit_id;
                                    let assign = 0;
                                    if (row_data.stock) {
                                        assign = 1;
                                    }
                                    form_data = form_data.concat([
                                        {
                                            name: "price[]",
                                            value: price
                                        },
                                        {
                                            name: "quantity[]",
                                            value: quantity
                                        },
                                        {
                                            name: "date[]",
                                            value: date
                                        },
                                        {
                                            name: "unit[]",
                                            value: unit
                                        },
                                        {
                                            name: "item_id[]",
                                            value: item_id
                                        },
                                        {
                                            name: "assign[]",
                                            value: assign
                                        },
                                        {
                                            name: "default_unit_id[]",
                                            value: default_unit_id
                                        },


                                    ])
                                }

                            });
                            let other_options = {
                                data_list: form_data
                            }

                            if (!form_data.length) {
                                Models.notify("At least 1 item required");
                                return;
                            }
                            Models.confirm(function () {
                                Models.request_sender($http, "post", "add_item_to_branch_multiple", function (response) {
                                    if (response.status) {
                                        Models.notify("Add to store done.");
                                        table.draw();
                                    }
                                    else {
                                        Models.notify("Data initiate failed");
                                    }
                                }, [], other_options);
                            })
                        });

                        container.on("click", ".bulk-continue", function () {
                            let id = $(this).attr("data-id");
                            Models.request_sender($http,"update","resend_bulk_sms",function (response) {
                                if (response.status){
                                    Models.notify("All message delivered");
                                }
                                table.draw(false);


                            },["'"+id+"'"]);

                        });

                        container.on("click", ".add-item-to-branch", function () {
                            let this_tr = $(this).closest("tr");
                            let row = table.row(this_tr);
                            Models.confirm(function () {
                                let data = row.data();
                                let item_id = data.item_id;
                                let quantity = this_tr.find("[name='quantity']").val();
                                let item_unit = this_tr.find("[name='item_unit']").val();
                                let date = this_tr.find("[name='date']").val();
                                let price = this_tr.find("[name='price']").val();
                                let assign = 0;
                                if (data.stock) {
                                    assign = 1;
                                }
                                let other_option = {
                                    data: {
                                        item_id: item_id,
                                        quantity: quantity,
                                        item_unit: item_unit,
                                        date: date,
                                        assign: assign,
                                        price: price
                                    }
                                };
                                Models.request_sender($http, "post", "add_item_to_branch", function (data) {
                                    if (data.status) {
                                        Models.notify("Add to store done.");
                                        table.draw();
                                    }
                                }, [], other_option);
                            });

                        });
                        container.on("ifChanged", ".bank-cash-permission-checkbox input", function () {
                            let check_box = $(this);
                            let status = check_box.prop("checked");
                            let this_tr = $(this).closest("tr");
                            let row = table.row(this_tr);
                            let data = row.data();
                            if (status) {
                                status = 1;
                            }
                            else {
                                status = 0;
                            }
                            let bank_id = data.bank_id;
                            let assign = 0;
                            if (data.branch_id) {
                                assign = 1;
                            }
                            let other_option = {
                                data: {
                                    bank_id: bank_id,
                                    status: status,
                                    assign: assign
                                }
                            };

                            Models.request_sender($http, "post", "bank_cash_permission", function (data) {
                                if (data.status) {
                                    Models.notify("Done.");
                                    // table.draw();
                                }
                            }, [], other_option);

                        });


                        container.on("click", ".request-honor,.request-cancel", function () {
                            let activity_type = 1;
                            if ($(this).hasClass("request-cancel")) {
                                activity_type = 3;
                            }
                            let row = table.row($(this).closest("tr"));
                            let data = row.data();
                            Models.confirm(function () {
                                let tb_row = $(this).closest("tr");

                                let request_id = data.id;
                                let other = {
                                    data: {
                                        request_id: request_id,
                                        activity: activity_type
                                    }
                                };
                                Models.request_sender($http, "update", "change_payment_request_status", function (data) {
                                    if (data.status) {
                                        table.draw();
                                    }

                                }, [], other);
                            });


                        });


                    }

                });
            });
        });

    });
}]);

