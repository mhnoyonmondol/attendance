// Add shift
app.register.controller("send_bulk_sms",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Send bulk sms";
    Models.get_basic_info($http,$rootScope,function () {
        $timeout(function () {
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);
            let to_type_actions = {
                all_off:function () {
                    $(".for-student,.for-teacher,.for-institute").addClass('uk-hidden');
                },
                student_on:function () {
                    $(".for-student").removeClass('uk-hidden');
                },
                teacher_on:function () {
                    $(".for-teacher").removeClass('uk-hidden');
                },
                school_on:function () {
                    $(".for-school").removeClass('uk-hidden');
                },


                search_receivers:function () {
                    let other = {
                        data:{
                            class:$("select[name='class[]']").val() || '',
                            department:$("select[name='department[]']").val() || '',
                            branch:$("select[name='branch[]']").val() || '',
                            session:$("select[name='session[]']").val() || '',
                            gender:$("select[name='gender[]']").val() || '',
                            religion:$("select[name='religion[]']").val() || '',
                            institute:$("select[name='institute[]']").val() || '',
                            to_type:$("select[name='to_type']").val() || '',
                            designation:Models.logged_user($rootScope).designation,
                            institute_id:Models.logged_user($rootScope).institute_id,
                        }
                    };

                    Models.request_sender($http,"get","get_sms_receivers",function (response) {
                        let receiver_selectize = Models.get_selectize_instance(".receiver-selectize");
                        receiver_selectize.clearOptions();
                        if (response.find_data.length){

                            if (receiver_selectize){
                                receiver_selectize.clearOptions();
                                receiver_selectize.addOption({
                                    label:"All",
                                    value:'all'
                                });
                                receiver_selectize.setValue('all');

                                for (let item of response.find_data ){
                                    receiver_selectize.addOption(item);
                                }
                            }
                        }
                    },[],other);
                }
            };
            Models.custom_selectize(".to-type-selectize",{
                options:[
                    {
                        label:"School",
                        value:"school"
                    },
                    {
                        label:"Teacher",
                        value:"teacher"
                    },
                    {
                        label:"Student",
                        value:"student"
                    },


                ],
                onInitialize:function () {
                    this.setValue('school');
                    if ($rootScope.logged_user_type === 'teacher'){
                        this.setValue('teacher');
                        this.removeOption('school');
                    }

                },
                onChange:function (value) {
                    to_type_actions.all_off();
                    if (value === 'student'){
                        to_type_actions.student_on();
                    }
                    else if (value === 'teacher'){
                        to_type_actions.teacher_on();
                    }
                    else if (value === 'school'){
                        to_type_actions.school_on();
                    }
                    if ($rootScope.logged_user_type === 'teacher'){
                        $("select.institute-selectize").closest('div').addClass('uk-hidden');
                    }


                    to_type_actions.search_receivers();
                    altair_uikit.reinitialize_grid_margin();
                }
            });
            Models.custom_selectize(".receiver-selectize",{
                options:[
                    {
                        label:"All",
                        value:"all"
                    },

                ],
                plugins:{
                    remove_button:{
                        label: ''
                    }
                }
                ,
                onInitialize:function () {
                    this.setValue('all');
                },
                onChange:function () {
                    Models.selectize_height_reinitialized(this);
                }
            });
            Models.custom_selectize(".institute-selectize",{
                options:[
                    {
                        label:"All",
                        value:"all"
                    },

                ],
                plugins:{
                    remove_button:{
                        label: ''
                    }
                }
                ,
                onInitialize:function () {
                    this.setValue('all');

                    if ($rootScope.logged_user_type === 'teacher'){
                        $("select.institute-selectize").closest('div').addClass('uk-hidden');
                    }

                    let designation = Models.logged_user($rootScope).designation;
                    let institute_id = Models.logged_user($rootScope).institute_id;

                    Models.request_sender($http,"get","my_institutes",function (response) {
                        let receiver_selectize = Models.get_selectize_instance(".institute-selectize");
                        receiver_selectize.clearOptions();

                        if (response.find_data !== undefined){
                            receiver_selectize.addOption({
                                label:"All",
                                value:'all'
                            });
                            receiver_selectize.setValue('all');
                            for (let item of response.find_data){
                                item.label = item.name;
                                item.value = item.institute_id;
                                receiver_selectize.addOption(item);

                            }
                        }
                    },["'"+designation+"'","'"+institute_id+"'"]);


                },
                onChange:function () {
                    Models.selectize_height_reinitialized(this);
                    to_type_actions.search_receivers();
                }
            });


            let options = {
                type:"class",
                multiple:true,
                editable:false,
                onChange:function () {
                    to_type_actions.search_receivers();
                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"department",
                multiple:true,
                editable:false,
                class_name: ".department-chooser",
                onChange:function () {
                    to_type_actions.search_receivers();
                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"session",
                multiple:true,
                editable:false,
                class_name:".session-tag-chooser",
                onChange:function () {
                    to_type_actions.search_receivers();
                }

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"branch",
                multiple:true,
                editable:false,
                class_name:".branch-tag-chooser",
                onChange:function () {
                    to_type_actions.search_receivers();
                }

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);


            options = {
                type:"religion",
                multiple:true,
                editable:false,
                class_name:".religion-tag-chooser",
                onChange:function () {
                    to_type_actions.search_receivers();
                }

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"gender",
                multiple:true,
                editable:false,
                class_name:".gender-tag-chooser",
                onChange:function () {
                    to_type_actions.search_receivers();
                }

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            altair_forms.textarea_autosize();
            Models.init_requirement();
        });
    });

    $scope.submit = function () {
        let other_data = {
            form_name:".send-sms-form",
            data:{
                designation:Models.logged_user($rootScope).designation,
                institute_id:Models.logged_user($rootScope).institute_id,
            }
        };
        Models.request_sender($http,'post',"send_bulk_sms",function (data) {
            if (data.status){
                Models.notify('All message delivered');
            }
            if ($( '#dt_tableHeeshab' ).dataTable !== undefined){
                let table = $( '#dt_tableHeeshab' ).dataTable().api();
                if (table.context.length){
                    table.draw();
                }
            }

        },[],other_data)
    };


}]);