// Add shift
app.register.controller("send_sms_test",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Android app sms api";
    Models.get_basic_info($http,$rootScope,function () {
        $timeout(function () {
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);

            altair_forms.textarea_autosize();
            Models.init_requirement();
        });
    });

    $scope.submit = function () {
        let other_data = {
            form_name:".send-sms-form",

        };
        Models.request_sender($http,'post',"send_sms_test",function (data) {
            if (data.status){
                Models.notify('Saved');
            }
            if ($( '#dt_tableHeeshab' ).dataTable !== undefined){
                let table = $( '#dt_tableHeeshab' ).dataTable().api();
                if (table.context.length){
                    table.draw();
                }
            }

        },[],other_data)
    };


}]);