// Activities
app.register.controller("activities",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_type_of_activities($http,$rootScope,function (data) {
            let type_of_activities = data;
            //requirement for activities
            $scope.other = {};
            $scope.function_name = "activities";
            $scope.limit = 12;
            $scope.skip = 0;
            $scope.rows = [];
            $scope.total = 0;
            // initiate common function
            $scope.load_data = function(call_back=function () {}){
                Models.load_data_with_limit($http,$scope,$timeout,function (data) {
                    call_back(data);
                });
            };
            $scope.activities_maker = function(call_back){
                Models.load_data_with_limit_total_counter($http,$scope,function (data) {
                    let total = data.total;
                    $scope.total = total;
                    $scope.load_data(function (data) {
                        call_back(data);
                    })

                })
            };
            $scope.activities_maker(function (data) {
                $scope.activity_switch = 1;
            });
            Models.scroll_finish(window,function () {
                $scope.load_data();
            })
        });

    });
}]);
