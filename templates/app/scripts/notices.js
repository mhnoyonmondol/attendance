// notices for teacher
app.register.controller("notices",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let institute_id = logged_user.institute_id;
            let designation = logged_user.designation;
            /// mailbox init
            let mailbox_instance = Models.mailbox_init();
            //// mails
            $scope.limit = '20';
            $scope.total = 0;
            $scope.skip = 0;
            $scope.query_data = {};
            $scope.collection_name = "notices";
            $scope.rows = [];
            // console.log(Models.id_types);
            let teacher_id_type = Models.id_type("teacher",$rootScope);
            let sql = "left join controllers on controllers.user_id = notices.sender " +
                "where notices.receiver = :receiver and notices.active != :active";
            let sql_data = {
                ":receiver": institute_id,
                ":active": 2,
            };
            $scope.query_data.sql = sql;
            $scope.query_data.sql_data = sql_data;
            $scope.query_data.columns = "notices.*,controllers.name as receiver_name,controllers.image as receiver_image," +
                "concat(extract(day from notices.time),' ',MONTHNAME(notices.time)) as time_string";
            $scope.contact_list_init = function(){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    $timeout(function () {
                        // altair_helpers.hierarchical_slide();
                        Models.init_requirement();
                        Models.scroll_finish(window,function () {
                            $scope.$apply(function () {
                                $scope.contact_list_load(function () {
                                });
                            });
                        });

                    });
                })
            };
            $scope.contact_list_load = function(call_back=function () {}){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    call_back(data);
                    $timeout(function () {
                        Models.init_requirement();
                    });
                })
            };
            Models.total_counter($http,$scope,function (data) {
                $scope.total = data.total;
                $scope.contact_list_init();
            });

            $timeout(function () {
                Models.init_requirement();
            });

            $scope.search_value = "";
            let joining = 0;
            $scope.search = function () {
                if ($scope.search_value.length >= 0){
                    let search_sql = "";
                    let search_sql_data = {};
                    if (sql === ""){
                        if (!joining){
                            search_sql = "where name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    else{
                        if (!joining){
                            search_sql = " and name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    joining = 1;
                    sql = sql+search_sql;
                    Object.assign(sql_data,search_sql_data);
                    $scope.query_data.sql = sql;
                    $scope.query_data.sql_data = sql_data;
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.contact_list_load();
                }

            };
            $scope.delete = function (id) {
                Models.confirm(function () {
                    Models.deleter($http,"notices",id,function (data) {
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",id);
                            if (index !==-1){
                                $scope.rows.splice(index, 1);
                            }
                        }
                        // console.log(data);
                    })
                });

            };
            $scope.delete_all = function () {
                let data_list = [];
                $(".md-card-list li").each(function () {
                    let element = $(this);
                    let prop = element.find("[name='mark']").prop("checked");
                    if (prop !== undefined){
                        if (prop){
                            let id = element.attr("data-id");
                            data_list.push({
                                name:"ids[]",
                                value:id
                            });
                        }

                    }


                });
                if (data_list.length){
                    Models.confirm(function () {
                        let other = {};
                        other.data_list = data_list;
                        Models.request_sender($http,"other","marked_notice_delete",function (response) {
                            if (response.status){
                                for(let item of data_list){
                                    let id = item.value;
                                    let index = Models.index_number($scope.rows,"id",id);
                                    if (index !==-1){
                                        $scope.rows.splice(index, 1);
                                    }
                                }
                                Models.notify("Delete done");
                            }
                            else{
                                Models.notify("Delete failed");
                            }
                        },[],other);
                    });
                }
                else{
                    Models.notify("At least one item required");
                }


            };





            mailbox_instance.init({
                onModalShow:function () {
                    let element  = $(".institute-selector");
                    if (element.get(0).selectize === undefined){
                        Models.custom_selectize(element,{
                            plugins: {
                                'remove_button': {
                                    label: ''
                                }
                            },
                            searchField: ['label','value','member_id'],
                            onInitialize:function () {
                                let that = this;
                                Models.get_document_save_institutes($rootScope,function (data) {
                                    let institutes = data;
                                    for (let institute_info of institutes){
                                        institute_info.label = institute_info.name;
                                        institute_info.value = institute_info.institute_id;
                                        that.addOption(institute_info);
                                    }
                                });
                            },
                            onType:function (value) {
                                let that = this;
                                if (!that.currentResults.total){
                                    Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                        // console.log(data);
                                        if (data.find_data !== undefined){
                                            for (let institute_info of data.find_data){
                                                $rootScope.document_save_institutes.push(institute_info);
                                                institute_info.label = institute_info.name;
                                                institute_info.value = institute_info.institute_id;
                                                that.addOption(institute_info);
                                            }
                                            that.refreshOptions();
                                        }

                                    },["'"+value+"'","'"+designation+"'"]);
                                }

                            },

                        });
                    }

                }
            });



        }

    });

    $scope.submit = function () {
        let other_data = {
            form_name:".notice",
        };

        Models.request_sender($http,'post',"send_notice",function (data) {
            if (data.status){
                Models.notify("Done");
                if (data.data !== undefined){
                    for (let notice of data.data){
                        $scope.rows.unshift(notice);
                    }
                    $timeout(function () {
                        Models.init_requirement();
                    });
                }
                else{
                    Models.notify("New data retrieve failed");
                }

            }
            else{
                Models.notify("Failed");
            }
            $(".uk-modal-close").click();

        },[],other_data)
    };

}]);

