// Institute settings
app.register.controller("institute_settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let member_id = $routeParams.institute_id;
    Models.request_sender($http,"get","institute_info",function (data) {
        /// assign footer
        let footer_data = {
            source:data.name
        };
        Models.generate_footer(footer_data);
        $scope.institute = data;
        $scope.institute_member_id = member_id;
        let institute_id = data.institute_id;
        Models.request_sender($http,"get","institute_settings",function (data) {
            $scope.settings = data;
            // console.log(data);
            $timeout(function () {
                Models.init_requirement();
            });
        },["'"+institute_id+"'",false,true]);
        /// update settings
        $scope.submit = function(event){
            let target_elem = $(event.target);
            let this_form = target_elem.closest("form");
            let form_type = this_form.attr("data-type");
            let function_name = "institute_settings_update";
            let other = {};
            Models.small_loading(target_elem);
            if (form_type === "absence_person_a_day"){
                let day = this_form.find("[name='day']").val();
                other.form_name = this_form;
                let data = {
                    type: "absence_person_a_day",
                    value: day
                };
                other.data = data;

            }

            other.data.institute_id = institute_id;

            Models.request_sender($http,"update",function_name,function (data) {
                if (data.status){
                    Models.small_loading(target_elem,0,"Updated");
                }
                else{
                    Models.small_loading(target_elem, 0, "Failed");
                }
            },[],other);
        };


    },["'"+member_id+"'",1,0]);



}]);
