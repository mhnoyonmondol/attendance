//admin home
app.register.controller("admin_home",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.data_table_script_load(function () {
        $scope.$applyAsync(function () {
            Models.get_basic_info($http,$rootScope,function (data) {
                let logged_user_id = Models.logged_user_id($rootScope);
                if (!logged_user_id){
                    $location.path('/login');
                    $rootScope.looged = 0;
                }
                else{
                    let logged_user = Models.logged_user($rootScope);
                    let designation = logged_user['designation'];
                    let institute_id = logged_user['institute_id'];
                    Models.request_sender($http,"get","dashboard_info",function (data) {
                        $scope.pending_application = 0;
                        if (data.status){
                            let output = data.output;
                            $scope.pending_application = output.pending_application;
                            $scope.sms_balance = output.sms_balance;

                            let target_events = [
                                {
                                    name: "Total school",
                                    total: output.total_institute.total,
                                    data: output.total_institute.data,
                                    link: "institute-list",
                                    card_type: "peity_visitors",
                                },
                                {
                                    name: "Total teacher",
                                    total: output.total_teacher.total,
                                    data: output.total_teacher.data,
                                    link: "teacher-list",
                                    card_type: "peity_visitors",
                                },
                                {
                                    name: "Total student",
                                    total: output.total_student.total,
                                    data: output.total_student.data,
                                    link: "student-list",
                                    card_type: "peity_visitors",
                                },

                                {
                                    name: "Present",
                                    total: output.total_present.total,
                                    current: output.total_present.current,
                                    link: "today-present-attendance/teacher",
                                    card_type: "peity_orders",
                                },
                                {
                                    name: "Absent",
                                    total: output.total_absent.total,
                                    current: output.total_absent.current,
                                    link: "today-absent-attendance/teacher",
                                    card_type: "peity_orders",
                                },
                                {
                                    name: "In training",
                                    total: output.total_training.total,
                                    current: output.total_training.current,
                                    link: "today-training-attendance/teacher",
                                    card_type: "peity_orders",
                                },
                                {
                                    name: "Leave",
                                    total: output.total_leave.total,
                                    current: output.total_leave.current,
                                    link: "today-leave-attendance/teacher",
                                    card_type: "peity_orders",
                                },
                                {
                                    name: "Late in",
                                    total: output.total_late.total,
                                    current: output.total_late.current,
                                    link: "today-late-attendance/teacher",
                                    card_type: "peity_orders",
                                },
                                {
                                    name: "Early gone",
                                    total: output.total_early.total,
                                    current: output.total_early.current,
                                    link: "today-early-attendance/teacher",
                                    card_type: "peity_orders",
                                },

                            ];
                            let peity_chart = Models.peity_charts_init();
                            let chartist = Models.chartist_init();
                            let card_template = $(".info-card").html();
                            if (card_template !== undefined){
                                let template_output = Models.compiler(card_template,target_events);
                                $(".top-cards").html(template_output);
                                chartist.init({
                                    columns:[
                                        ['Present', output.total_present.current],
                                        ['Absent', output.total_absent.current],
                                        ['Training', output.total_training.current],
                                        ['Leave', output.total_leave.current],
                                    ]
                                },function (instance) {
                                    $timeout(function () {
                                        instance.resize();
                                    },250);

                                });

                                altair_helpers.hierarchical_show();
                                peity_chart.init();

                                /// attendance report datatable

                                $scope.columnDefs = [
                                    {
                                        targets:-2,
                                        render:function (data) {
                                            let time_for_event = data;
                                            if (time_for_event === "0000-00-00 00:00:00"){
                                                time_for_event = moment().format("YYYY-MM-DD");
                                            }
                                            return time_for_event;
                                        },
                                        orderable:false
                                    }
                                ];
                                $scope.table_columns_format = [
                                    {
                                        label:"source_name",
                                        name:"Teacher name"
                                    },
                                    {
                                        label:"status",
                                        name:"Status"
                                    },
                                    {
                                        label:"time_for_event",
                                        name:"Time for event"
                                    },
                                    {
                                        label:"institute_name",
                                        name:"Institute"
                                    },


                                ];
                                // make table required format
                                $scope.column_format = [];
                                let i = 0;
                                for(let item of $scope.table_columns_format){
                                    let ob = {
                                        data:item.label
                                    };
                                    if (item.label === null){
                                        ob.defaultContent = "";
                                    }
                                    // if label is time sortable by time assign
                                    if (item.label === "time" || item.label === "attendance_time"){
                                        $scope.order = [[i,'desc']]
                                    }

                                    $scope.column_format.push(ob);
                                    i++;
                                }

                                $timeout(function () {
                                    let table = altair_datatables.dt_tableHeeshab({
                                        "columns":$scope.column_format,
                                        "columnDefs":$scope.columnDefs,
                                        "order":$scope.order,
                                        tableReady:function (table_instance) {
                                            let button_container = table_instance.buttons().container();
                                            let institute_select_element = $(".institute-select-template").html();

                                            if (institute_select_element !== undefined){
                                                button_container.append( institute_select_element );
                                                let event_types = Models.event_types($rootScope);
                                                Models.custom_selectize(".institute-selectize",{
                                                    onInitialize:function () {
                                                        let instance = this;
                                                        Models.my_institutes($http,designation,institute_id,function (data) {
                                                            let i = 0;
                                                            for (let item of data){
                                                                item.label = item.name;
                                                                item.value = item.institute_id;
                                                                instance.addOption(item);
                                                                if (i === 0){
                                                                    instance.setValue(item.institute_id);
                                                                }
                                                                i++;
                                                            }
                                                        })
                                                    },
                                                    onChange:function (value) {
                                                        table_instance.clear().draw();
                                                        let current_date = moment().format("YYYY-MM-DD");
                                                        Models.request_sender($http,"get","events_of_institutes",function (data) {

                                                            if (data.find_data !== undefined){
                                                                for(let item of data.find_data){
                                                                    let status = item.event_type;
                                                                    let filter = Models.filter_in_array(event_types,{label:status});
                                                                    if (filter){
                                                                        status = filter.name;
                                                                    }
                                                                    item.status = status;
                                                                    table_instance.row.add(item).draw();
                                                                }
                                                            }
                                                        },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'"]);
                                                    }
                                                })
                                            }

                                        }
                                    });
                                    if (table !== undefined){
                                        let container = table.table_container;
                                        table.on("draw",function (e,dt) {


                                        });

                                    }

                                });



                                $(window).resize();

                            }

                        }
                        else{
                            Models.notify("Data not found");
                        }


                    },["'admin'","'"+designation+"'"]);

                }

            })
        });
    });


}]);
