// institute list
app.register.controller("institutes",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.limit = '20';
    $scope.total = 0;
    $scope.skip = 0;
    $scope.query_data = {};
    $scope.more_data = {};
    $scope.collection_name = "institutes";
    $scope.rows = [];
    $scope.school_delete_permission = true;
    Models.get_basic_info($http,$rootScope,function (data) {
        // console.log(Models.id_types);
        $scope.school_delete_permission = Models.permission_of_part("school_delete",$rootScope);
        /// logged user designation
        let designation = "";
        let sql = "";
        let sql_data = {};
        if (Models.logged_user_type($rootScope) === "admin"){
            if ($rootScope.basic_info.logged_user !== undefined){
                let logged_user = $rootScope.basic_info.logged_user;
                designation = logged_user.designation;
                $scope.more_data.user_designation = designation;
                sql = " where controllers.user_id = :user_id ";
                sql_data[":user_id"] = logged_user.user_id;
            }
        }

        $scope.query_data.sql = sql;
        $scope.query_data.sql_data = sql_data;
        $scope.contact_list_init = function(){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                // console.log(data);
                $timeout(function () {
                    Models.scroll_finish(window,function () {
                        $scope.$apply(function () {
                            $scope.contact_list_load(function () {
                            });
                        });
                    });

                });
            })
        };
        $scope.contact_list_load = function(call_back=function () {}){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {

                call_back(data);
            })
        };
        Models.total_counter($http,$scope,function (data) {
            // console.log(data);
            $scope.total = data.total;
            $scope.contact_list_init();
        });

        $timeout(function () {
            Models.init_requirement();
        });

        $scope.search_value = "";
        let joining = 0;
        $scope.search = function () {
            if ($scope.search_value.length >= 0){
                let search_sql = "";
                let search_sql_data = {};
                if (sql === ""){
                    if (!joining){
                        search_sql = "where institutes.name like :name or institutes.code like :code or institutes.member_id like :member_id or head_teacher.mobile like :teacher_mobile ";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    search_sql_data[":code"] = "%"+$scope.search_value+"%";
                    search_sql_data[":member_id"] = "%"+$scope.search_value+"%";
                    search_sql_data[":teacher_mobile"] = "%"+$scope.search_value+"%";
                }
                else{
                    if (!joining){
                        search_sql = " and (institutes.name like :name or institutes.code like :code or institutes.member_id like :member_id or head_teacher.mobile like :teacher_mobile) ";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    search_sql_data[":code"] = "%"+$scope.search_value+"%";
                    search_sql_data[":member_id"] = "%"+$scope.search_value+"%";
                    search_sql_data[":teacher_mobile"] = "%"+$scope.search_value+"%";
                }
                joining = 1;
                sql = sql+search_sql;
                Object.assign(sql_data,search_sql_data);
                $scope.query_data.sql = sql;
                $scope.query_data.sql_data = sql_data;
                $scope.rows = [];
                $scope.skip = 0;
                $scope.contact_list_load();
            }

        };
        $scope.delete = function (id) {
            Models.confirm(function () {
                Models.deleter($http,"institutes",id,function (data) {
                    if (data.status){
                        let index = Models.index_number($scope.rows,"id",id);
                        if (index !==-1){
                            $scope.rows.splice(index, 1);
                        }
                    }
                    // console.log(data);
                })
            });

        };
        $scope.activate = function (user_id,$event) {
            let target = $event.target;
            let user_elem = $(target).closest(".md-card");
            Models.request_sender($http,"other","activate_as_worker",function (data) {
                if (data.status){
                    let activate_worker = data.activate_worker;
                    $rootScope.basic_info.activate_worker = activate_worker;
                    Models.notify("Activated.");
                    $("body").click();
                    /// deactivate before user
                    $(".active-worker").removeClass("active-worker");
                    user_elem.addClass("active-worker");
                }
            },["'"+user_id+"'"]);
        };

        $scope.export = function () {
            let prefix = $("#contact_list_filter li.uk-active a").text();
            let options = {
                table_name:"controllers",
                data:{},
                sql:$scope.query_data.sql,
                sql_data:$scope.query_data.sql_data,
                data:{
                    prefix: prefix
                }
            };
            Models.export_data_from_data_table($http,options);
        }
    });


}]);
