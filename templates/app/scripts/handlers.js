// controller list
app.register.controller("controllers-contact-list",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.limit = '20';
    $scope.total = 0;
    $scope.skip = 0;
    $scope.query_data = {};
    $scope.collection_name = "controllers";
    $scope.rows = [];
    let controller_type = $routeParams.controller_type;
    let attr_type = controller_type;
    if (attr_type === undefined){
        attr_type = "All";
    }

    Models.get_basic_info($http,$rootScope,function (data) {

        let logged_controller_type = Models.logged_user_type($rootScope);
        if (logged_controller_type === "admin"){
            let logged_user = $rootScope.basic_info.logged_user;
            let designation = logged_user.designation;
            let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
            let allow = area_parent.allow;
            $scope.options = allow;
        }
        else{
            $scope.options = Models.admin_designations();
            if (logged_controller_type === "system_designer"){
                $scope.options.push("System admin");
                $scope.options.push("System designer");
            }
            else if(logged_controller_type === "system_admin"){
                $scope.options.push("System admin");
            }

        }
        // if controller type admin then first admin designation select automatic
        $timeout(function () {
            if (controller_type === "admin"){
                let first_designation = $scope.options[0];
                controller_type = first_designation;
                attr_type = controller_type;
            }
            $("[data-type='"+attr_type+"']").addClass("uk-active");
        });


        /// change the controller type of system designer and system admin
        if (controller_type === "System designer"){
            controller_type = "system_designer";
        }
        else if(controller_type === "System admin"){
            controller_type = "system_admin";
        }

        let sql = "";
        let sql_data = {};
        if (controller_type !== undefined){
            let type_id = Models.id_type(controller_type,$rootScope);
            if (controller_type === "system_designer" || controller_type === "system_admin"){
                sql = " where controllers.user_id like :match_id ";
                sql_data[":match_id"] = type_id+"%";
            }
            else{
                let admin_type_id = Models.id_type("admin",$rootScope);
                sql = " where tags.tag = :designation and controllers.user_id like :match_id";
                sql_data[":designation"] = controller_type;
                sql_data[":match_id"] = admin_type_id+"%";
            }

        }
        // if all select
        else{
            let teacher_type_id = Models.id_type("teacher",$rootScope);
            sql = " where controllers.user_id not like :match_id ";
            sql_data[":match_id"] = teacher_type_id+"%";

            if (Models.logged_user_type($rootScope) === "system_admin"){
                let system_designer_type_id = Models.id_type("system_designer",$rootScope);
                sql += " and controllers.user_id not like :match_id_2 ";
                sql_data[":match_id_2"] = system_designer_type_id+"%";
            }
            else if (Models.logged_user_type($rootScope) === "admin"){
                let sql_in = Models.sql_in($scope.options);
                sql += " and tags.tag in("+sql_in+")";
            }
        }

        $scope.query_data.sql = sql;
        $scope.query_data.sql_data = sql_data;
        $scope.query_data.area = Models.logged_user($rootScope).area;
        $scope.contact_list_init = function(){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {

                $timeout(function () {
                    Models.scroll_finish(window,function () {
                        $scope.$apply(function () {
                            $scope.contact_list_load(function () {
                            });
                        });
                    });

                });
            })
        };
        $scope.contact_list_load = function(call_back=function () {}){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                call_back(data);

            })
        };
        Models.total_counter($http,$scope,function (data) {
            // console.log(data);
            $scope.total = data.total;
            $scope.contact_list_init();
        });

        $timeout(function () {
            Models.init_requirement();
        });

        $scope.search_value = "";
        let joining = 0;
        $scope.search = function () {
            if ($scope.search_value.length >= 0){
                let search_sql = "";
                let search_sql_data = {};
                if (sql === ""){
                    if (!joining){
                        search_sql = "where controllers.name like :name";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                }
                else{
                    if (!joining){
                        search_sql = " and controllers.name like :name";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                }
                joining = 1;
                sql = sql+search_sql;
                Object.assign(sql_data,search_sql_data);
                $scope.query_data.sql = sql;
                $scope.query_data.sql_data = sql_data;
                $scope.rows = [];
                $scope.skip = 0;
                $scope.contact_list_load();
            }

        };
        $scope.delete = function (id) {
            Models.confirm(function () {
                Models.deleter($http,"controllers",id,function (data) {
                    if (data.status){
                        let index = Models.index_number($scope.rows,"id",id);
                        if (index !==-1){
                            $scope.rows.splice(index, 1);
                        }
                    }
                    // console.log(data);
                })
            });

        };
        $scope.activate = function (user_id,$event) {
            let target = $event.target;
            let user_elem = $(target).closest(".md-card");
            Models.request_sender($http,"other","activate_as_worker",function (data) {
                if (data.status){
                    let activate_worker = data.activate_worker;
                    $rootScope.basic_info.activate_worker = activate_worker;
                    Models.notify("Activated.");
                    $("body").click();
                    /// deactivate before user
                    $(".active-worker").removeClass("active-worker");
                    user_elem.addClass("active-worker");
                }
            },["'"+user_id+"'"]);
        };

        $scope.export = function () {
            let prefix = $("#contact_list_filter li.uk-active a").text();
            let options = {
                table_name:"controllers",
                data:{},
                sql:$scope.query_data.sql,
                sql_data:$scope.query_data.sql_data,
                data:{
                    prefix: prefix
                }
            };
            Models.export_data_from_data_table($http,options);
        }
    });


}]);

