// branch edit
app.register.controller("edit_shift",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Edit shift";
    Models.get_basic_info($http,$rootScope,function () {
        let shift_id = $routeParams.id;

        $scope.page_title = "Edit shift";
        // Get profile information
        Models.request_sender($http,"get","shift_info",function (data) {
            // console.log(data);
            $scope.shift = data;
            $timeout(function () {
                Models.init_requirement();
            });

        },["'"+shift_id+"'"]);

        $scope.submit = function () {
            let others = {
                form_name:".edit-shift",
                data: {
                    edit:1,
                    id:$scope.shift.id
                }
            };

            Models.request_sender($http,"update","edit_shift",function (data) {
                if (data.status){
                    Models.notify("Changed has been saved");
                }
            },[],others);
        };
    });
}]);

