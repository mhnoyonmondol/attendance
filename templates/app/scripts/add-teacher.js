//add teacher
app.register.controller("add_teacher",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_id = $routeParams.institute_id;
    $scope.page_title = "Add teacher";
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        $scope.institute = institute_data;
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let page_name = Models.page_name($location);
        let user_type = "teacher";
        Models.request_sender($http,"get","teacher_enrol_basic_info",function (data) {
            if (data.status){
                let member_id = data.member_id;
                $scope.member_id = member_id;
                $scope.estimate_access_menus = data.access_menus;
                $scope.permission_parts = data.permission_parts;

                Models.init_requirement();
            }

        },["'"+user_type+"'"]);

        /// let's work here for add information
        $timeout(function () {
            Models.init_requirement();
            Models.dropify({
                messages: {
                    'default': 'Upload',
                    'replace': '',
                    'remove':  'x',
                    'error':   'Ooops'
                }
            },".profile-dropify");
            Models.dropify({});

            let options = {
                type:"designation",
                onChange:function (value,instance) {
                    $(".password-field").addClass("uk-hidden");
                    let options = Models.get_list_from_selectize(instance);
                    let selected_info = Models.filter_in_array(options,{value:value});
                    if (selected_info){
                        let designation = selected_info.label;
                        if (designation.toLowerCase() === "head teacher"){
                            $(".password-field").removeClass("uk-hidden");
                        }
                    }
                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            /// assign trainings selector
            options = {
                type:"trainings",
                class_name: ".trainings",
                multiple:true
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            /// assign subject wise trainings selector
            options = {
                type:"subject_wise_trainings",
                class_name:".subject-wise-trainings",
                multiple:true
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    },[false,"'"+$scope.institute.institute_id+"'"]);
                }
            });

        });
        $scope.submit = function () {
            let other_data = {
                form_name:".add-teacher",
                data:{
                    institute_id:$scope.institute.institute_id
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                other_data.data.password = "455454441";

            }
            Models.request_sender($http,'post',"add_teacher",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/"+user_type+"/"+data.member_id;
                    Models.add_action(data,$location,url);
                }

            },["'"+user_type+"'"],other_data)
        };
    },["'"+institute_id+"'",1,0]);


}]);
