// student
app.register.controller("student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            /// assign footer
            let footer_data = {
                source:data.institute_name
            };
            Models.generate_footer(footer_data);
            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.user_type = Models.id_type_name($scope.user.user_id,$rootScope);
        },true);
    });
}]);