//add student
app.register.controller("add_student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_id = $routeParams.institute_id;
    $scope.page_title = "Add student";
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        $scope.institute = institute_data;
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let page_name = Models.page_name($location);
        let user_type = "student";
        Models.request_sender($http,"get","student_enrol_basic_info",function (data) {
            if (data.status){
                let member_id = data.member_id;
                $scope.member_id = member_id;

                Models.init_requirement();
            }

        },["'"+user_type+"'"]);

        /// let's work here for add information
        $timeout(function () {
            Models.init_requirement();
            Models.dropify({
                messages: {
                    'default': 'Upload',
                    'replace': '',
                    'remove':  'x',
                    'error':   'Ooops'
                }
            },".profile-dropify");

            let options = {
                type:"class",
                onChange:function (value,instance) {

                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"department",
                class_name: ".department-chooser",
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"session",
                class_name:".session-tag-chooser",

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"branch",
                class_name:".branch-tag-chooser",

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);


            options = {
                type:"religion",
                class_name:".religion-tag-chooser",

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"gender",
                class_name:".gender-tag-chooser",

            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    },[false,"'"+$scope.institute.institute_id+"'"]);
                }
            });

        });
        $scope.submit = function () {
            let other_data = {
                form_name:".add-student",
                data:{
                    institute_id:$scope.institute.institute_id
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                other_data.data.password = "455454441";

            }
            Models.request_sender($http,'post',"add_student",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/"+user_type+"/"+data.member_id;
                    Models.add_action(data,$location,url);
                }

            },["'"+user_type+"'"],other_data)
        };
    },["'"+institute_id+"'",1,0]);


}]);
