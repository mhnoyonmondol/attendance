// home
app.register.controller("home",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_text = "Dashboard loading...";
    let category = $routeParams.category;
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user_type = Models.logged_user_type($rootScope);
            if (logged_user_type){
                let logged_user = Models.logged_user($rootScope);
                let member_id = logged_user.member_id;
                let designation = logged_user.designation;
                if (logged_user_type === "teacher" && designation.toLowerCase() === "head teacher"){
                    if (category !== undefined){
                        $location.path("/head-teacher-home-2/"+member_id);
                    }
                    else{
                        $location.path("/head-teacher-home/"+member_id);
                    }


                }
                else if (logged_user_type === "teacher" && designation.toLowerCase() !== "head teacher"){
                    // $location.path("/teacher-home");
                }
                else if (logged_user_type === "admin"){

                    if (category !== undefined){
                        $location.path("/admin-home-2");
                    }
                    else{
                        $location.path("/admin-home");
                    }

                }
                else if (logged_user_type === "system_admin" || logged_user_type === "system_designer"){
                    $location.path("/system-admin-home");
                }
                else if (logged_user_type === "student"){
                    $location.path("/student-home/"+member_id);
                }

                else{
                    $scope.page_text = "Failed loading, for unknown user.";
                }
            }
        }

    })

}]);
