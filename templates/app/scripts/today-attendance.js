// Today attendance
app.register.controller("today_attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.table_switch = 0;
    let user_type = $routeParams.user_type;
    Models.uri_source_info($http, $routeParams, function (source_user_id, source_info) {
        if (source_user_id) {
            $scope.page_title_plus = " of " + source_info.name;
        }
        $scope.source_user_id = source_user_id;
        $scope.table_switch = 1;
    });
}]);