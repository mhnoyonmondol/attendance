// teacher edit
app.register.controller("edit_student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            console.log(data);
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }

            $scope.same_user = same_user;


            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_info",function (institute_data) {
                $scope.institute = institute_data;
                /// assign footer
                let footer_data = {
                    source:institute_data.name
                };
                Models.generate_footer(footer_data);

            },["'"+institute_id+"'",0,0]);

            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.member_id = data.member_id;
            $scope.user_name = data.name;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);
                Models.init_requirement();

                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"class",
                    default_value:data.class,
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                options = {
                    type:"department",
                    default_value:data.department_id,
                    class_name: ".department-chooser",
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                options = {
                    type:"session",
                    class_name:".session-tag-chooser",
                    default_value:data.session_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                options = {
                    type:"branch",
                    class_name:".branch-tag-chooser",
                    default_value:data.branch_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);


                options = {
                    type:"religion",
                    class_name:".religion-tag-chooser",
                    default_value:data.religion_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                options = {
                    type:"gender",
                    class_name:".gender-tag-chooser",
                    default_value:data.gender_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                instance.setValue($scope.user.shift);
                            }
                        })
                    }
                });


            });

        });

        $scope.submit = function () {
            let others = {
                form_name:".edit-student",
                data: {
                    user_id:user_id,
                    before_member_id: $scope.user.member_id,
                    institute_id: $scope.institute.institute_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_active: $scope.user.active,
                    edit:1
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                others.data.password = "";

            }
            if ($scope.same_user){
                others.data.active = $scope.user.active;
                others.data.designation = $scope.user.designation_id;
            }

            Models.request_sender($http,"update","edit_student",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/student/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);

