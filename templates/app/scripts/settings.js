// software settings
app.register.controller("settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.request_sender($http,"get","software_info",function (data) {
        $scope.software = data;
        $timeout(function () {
            Models.init_requirement();
            altair_md.card_overlay();
            Models.dropify({},".dropify-logo");
            Models.dropify({},".dropify-favicon");
        });
    });
    /// update settings
    $scope.submit = function(event){
        let target_elem = $(event.target);
        let this_form = target_elem.closest("form");
        let form_type = this_form.attr("data-type");
        let function_name = "software_info_update";
        let other = {};
        Models.small_loading(target_elem);
        if (form_type === "software_title"){
            let title = this_form.find("[name='title']").val();
            other.form_name = this_form;
            let data = {
                type: "software_title",
                value: title
            };
            other.data = data;

        }
        else if (form_type === "software_currency"){
            let currency = this_form.find("[name='currency']").val();
            other.form_name = this_form;
            let data = {
                type: "software_currency",
                value: currency
            };
            other.data = data;
        }
        else if (form_type === "software_time_zone"){
            let time_zone = this_form.find("[name='time_zone']").val();
            other.form_name = this_form;
            let data = {
                type: "software_time_zone",
                value: time_zone
            };
            other.data = data;
        }
        else if (form_type === "software_logo"){
            other.form_name = this_form;
            let data = {
                type: "software_logo",
                value: "",
                before_file:$scope.software.logo
            };
            other.data = data;
        }
        else if (form_type === "software_favicon"){
            other.form_name = this_form;
            let data = {
                type: "software_favicon",
                value: "",
                before_file:$scope.software.favicon
            };
            other.data = data;
        }
        else if (form_type === "software_login_banner"){
            other.form_name = this_form;
            let data = {
                type: "software_login_banner",
                value: "",
                before_file:$scope.software.login_banner
            };
            other.data = data;
        }

        else if (form_type === "software_email"){
            other.form_name = this_form;
            let email = this_form.find("[name='email']").val();
            let data = {
                type: "software_email",
                value: email,
            };
            other.data = data;
        }
        else if (form_type === "software_company_name"){
            other.form_name = this_form;
            let company_name = this_form.find("[name='company_name']").val();
            let data = {
                type: "software_company_name",
                value: company_name,
            };
            other.data = data;
        }
        else if (form_type === "software_https"){
            other.form_name = this_form;
            let active = 0;
            let status = this_form.find("[name='active']").prop("checked");
            if (status){
                active = 1;
            }
            let data = {
                type: "software_https",
                value: active,
            };
            other.data = data;
        }

        else if (form_type === "software_construction_mode"){
            other.form_name = this_form;
            let active = 0;
            let status = this_form.find("[name='active']").prop("checked");
            if (status){
                active = 1;
            }
            let date = this_form.find("[name='date']").val();
            let time = this_form.find("[name='time']").val();
            let obj = {
                date:date,
                time:time,
                active:active
            };
            let value = JSON.stringify(obj);
            let data = {
                type: "software_construction_mode",
                value: value,
            };
            other.data = data;
        }




        Models.request_sender($http,"update",function_name,function (data) {
            if (data.status){
                Models.small_loading(target_elem,0,"Updated");
                if (data.software_favicon !== undefined){
                    $scope.software.favicon = data.software_favicon;
                }
                else if(data.software_logo !== undefined){
                    $scope.software.logo = data.software_logo;
                }
            }
            else{
                Models.small_loading(target_elem, 0, "Failed");
            }
        },[],other);
    };

}]);
