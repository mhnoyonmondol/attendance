// profile
app.register.controller("profile",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.user_type = Models.id_type_name($scope.user.user_id,$rootScope);
            let edit_page = "edit-profile";
            if($scope.user_type === "student"){
                edit_page = "edit-student";
            }
            else if($scope.user_type === "teacher"){
                edit_page = "edit-teacher";
            }
            $scope.edit_page = edit_page;

        },true);
    });
}]);