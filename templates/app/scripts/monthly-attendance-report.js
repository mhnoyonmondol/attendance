// Monthly attendance report
app.register.controller("monthly_attendance_report",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    /// attendance report datatable
    let category = $routeParams.category;
    let page_name = Models.page_name($location);
    let event_user_type = "teacher";
    if (page_name === "student-monthly-attendance-report"){
        event_user_type = "student";
    }
    if (category !== undefined){
        event_user_type = "student";
    }


    $scope.user_name = null;
    let years = [];
    let current_year = moment().format("YYYY");
    let current_month = moment().format("MM");
    let days_in_month = moment().daysInMonth();
    for (let i = 2018;i<=current_year;i++){
        years.push({
            label:i,
            value:i
        });
    }
    let months = [
        {
            label:"January",
            value:"01",
        },
        {
            label:"February",
            value:"02",
        },
        {
            label:"March",
            value:"03",
        },
        {
            label:"April",
            value:"04",
        },
        {
            label:"May",
            value:"05",
        },
        {
            label:"June",
            value:"06",
        },
        {
            label:"July",
            value:"07",
        },
        {
            label:"August",
            value:"08",
        },
        {
            label:"September",
            value:"09",
        },
        {
            label:"October",
            value:"10",
        },
        {
            label:"November",
            value:"11",
        },
        {
            label:"December",
            value:"12",
        },

    ];
    Models.get_basic_info($http,$rootScope,function () {
        Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
            if (source_user_id){
                $scope.user_name = source_info.name;
                source_user_id = "'"+source_user_id+"'";

            }
            // console.log(source_user_id);

            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".item-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // }
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Teacher name"
                },

                // {
                //     label:"institute_name",
                //     name:"Institute"
                // },

            ];
            let manual_column_length = $scope.table_columns_format.length;

            for (let i = 1;i<=31;i++){
                $scope.table_columns_format.push({
                    label:i,
                    name:i
                });
                let index = Models.index_number($scope.table_columns_format,"label",i);
                if (index !== -1){
                    $scope.columnDefs.push({
                        targets:index,
                        orderable:false,
                        render: function (data) {
                            return "<span data-attentance-character='"+data+"'>"+data+"</span>";
                        },
                    });
                }
            }
            $scope.table_columns_format.push({
                label:"total_symbols",
                name:"Total"
            });

            // make table required format
            $scope.column_format = [];
            let i = 0;
            for(let item of $scope.table_columns_format){
                let ob = {
                    data:item.label
                };
                if (item.label === null){
                    ob.defaultContent = "";
                }
                // if label is time sortable by time assign
                if (item.label === "time" || item.label === "attendance_time"){
                    $scope.order = [[i,'desc']]
                }

                $scope.column_format.push(ob);
                i++;
            }

            let table;
            $scope.table_load = function(table_instance,institute_id,year,month){
                Models.request_sender($http,"get","monthly_events_of_institute",function (data) {
                    $scope.institute_info = data.institute_info;
                    $scope.date_string = data.date_string;
                    if (data.find_data !== undefined){
                        let table_headers = data.headers;
                        let days_of_month = Number(data.days_of_month);
                        // console.log(days_of_month);
                        //// visible last 5 columns
                        for (let i = 1;i <= 5; i++){
                            table_instance.column(Models.negative_number(i)).visible(true);
                        }
                        if (days_of_month < 31){
                            let extra_days = 31 - days_of_month;
                            let j = 0;
                            for(let item of data.find_data){
                                for (let i = 1; i <= extra_days;i++){
                                    let key = i+days_of_month;
                                    let column_number = manual_column_length + key - 1;
                                    item[key] = "";
                                    table_instance.column(column_number).visible(false);
                                }
                                data.find_data[j] = item;
                                j++;
                            }
                        }


                        for(let item of data.find_data){
                            table_instance.row.add(item).draw();
                        }
                    }
                },["'"+institute_id+"'","'"+year+"'","'"+month+"'","'"+event_user_type+"'",source_user_id]);
            };

            $scope.table_initiate = function(headers=null){
                if (headers){
                    $scope.table_columns_format = headers;
                }
                Models.data_table_script_load(function () {
                    $scope.$applyAsync(function () {
                        $timeout(function () {
                            table = altair_datatables.dt_tableHeeshab({
                                "columns":$scope.column_format,
                                "columnDefs":$scope.columnDefs,
                                "order":$scope.order,
                                tableReady:function (table_instance) {
                                    let button_container = table_instance.buttons().container();
                                    let institute_select_element = $(".option-select-template").html();

                                    if (institute_select_element !== undefined){
                                        button_container.append( institute_select_element );
                                        let event_types = Models.event_types($rootScope);
                                        let logged_user = Models.logged_user($rootScope);
                                        let designation = logged_user.designation;
                                        let institute_id = logged_user.institute_id;
                                        Models.custom_selectize(".institute-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                Models.my_institutes($http,designation,institute_id,function (data) {
                                                    let i = 0;
                                                    for (let item of data){
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        i++;
                                                    }

                                                    if (source_info !== undefined){
                                                        instance.setValue(source_info.institute_id);
                                                    }
                                                    else{
                                                        if (data.length){
                                                            instance.setValue(data[0].institute_id);
                                                        }
                                                    }

                                                });
                                                if (page_name === "student-monthly-attendance-report" || page_name === "teacher-monthly-attendance-report") {
                                                    $(".institute-selectize").addClass("uk-hidden");
                                                }

                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                if (value !== ""){
                                                    let year = $("select.year-selectize").val();
                                                    if (year === ""){
                                                        year = current_year;
                                                    }
                                                    let month = $("select.month-selectize").val();
                                                    if (month === ""){
                                                        month = current_month;
                                                    }

                                                    $scope.table_load(table_instance,value,year,month);
                                                }

                                            }
                                        });
                                        Models.custom_selectize(".year-selectize",{
                                            options:years,
                                            onInitialize:function () {
                                                this.setValue(current_year,"silent");
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let institute_id = $('select.institute-selectize').val();
                                                let month = $("select.month-selectize").val();
                                                if (institute_id !== "" && value !== "" && month !== ""){
                                                    $scope.table_load(table_instance,institute_id,value,month);
                                                }

                                            }
                                        });
                                        Models.custom_selectize(".month-selectize",{
                                            options:months,
                                            onInitialize:function () {
                                                this.setValue(current_month,"silent");
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let institute_id = $('select.institute-selectize').val();
                                                let year = $("select.year-selectize").val();
                                                if (institute_id !== "" && value !== "" && year !== ""){
                                                    $scope.table_load(table_instance,institute_id,year,value);
                                                }

                                            }
                                        });

                                    }

                                }
                            });


                        });
                    });

                });

            };
            $scope.table_initiate();
        });


    });

}]);
