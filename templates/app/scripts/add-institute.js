// Add institute
app.register.controller("add_institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Add new school";
    $timeout(function () {
        Models.init_requirement();
        Models.dropify({
            messages: {
                'default': 'Upload banner (1650 x 290)',
            }
        });
        Models.dropify({
            messages: {
                'default': 'Upload',
                'replace': '',
                'remove':  'x',
                'error':   'Ooops'
            }
        },".profile-dropify");

        Models.custom_selectize(".shift-selectize",{
            onInitialize:function () {
                let instance = this;
                Models.request_sender($http,"get","shifts",function (data) {
                    if (data.find_data !== undefined){
                        for (let item of data.find_data){
                            item.label = item.name;
                            item.value = item.id;
                            instance.addOption(item);
                        }
                    }
                })
            },
            onChange:function () {
                this.settings.heightAdjust(this);
            },
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
        });


    });
    $scope.submit = function () {
        let other_data = {
            form_name:".add-institute",
        };
        Models.request_sender($http,'post',"add_institute",function (data) {
            // console.log(data);
            if (data.status){
                let url = "/institute/"+data.member_id;
                Models.add_action(data,$location,url);
            }

        },[],other_data)
    };


}]);
