// logout
app.register.controller("logout",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.request_sender($http,"other","logout",function (data) {
        if (data.status){
            window.location = "login";
            let software_info = $rootScope.basic_info.software_info;
            $(".sidebar_logo img").attr("src",software_info.logo);
            $rootScope.basic_info = undefined;
            $rootScope.main_menus = undefined;
            service.main_menu_reload();

            $(".sidebar_actions").addClass("uk-hidden");
            $rootScope.logged = 0;
            // $location.path("/login");
        }
    })
}]);