app.register.controller("system_admin_home",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                if (data.status){
                    let output = data.output;
                    let target_events = [
                        {
                            name: "Total school",
                            total: output.total_institute.total,
                            data: output.total_institute.data,
                            link: "institutes",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total teacher",
                            total: output.total_teacher.total,
                            data: output.total_teacher.data,
                            link: "teacher-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total student",
                            total: output.total_student.total,
                            data: output.total_student.data,
                            link: "student-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total admin",
                            total: output.total_admin.total,
                            data: output.total_admin.data,
                            link: "handlers/admin",
                            card_type: "peity_visitors",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let output = Models.compiler(card_template,target_events);
                        $(".top-cards").html(output);
                        altair_helpers.hierarchical_show();
                        peity_chart.init();
                        $(window).resize();

                    }
                }
                else{
                    Models.notify("Data not found");
                }


            },["'system_admin'","'"+designation+"'"]);





        }

    })

}]);

