// institute
app.register.controller("institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let member_id = $routeParams.member_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            $scope.institute = data;
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            // console.log(data);
        },["'"+member_id+"'",1,1,1]);
    })
}]);
