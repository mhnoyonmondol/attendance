// branch edit
app.register.controller("edit_institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Edit institute";
    let latitude = 0;
    let longitude = 0;
    Models.get_basic_info($http,$rootScope,function () {
        let institute_id = $routeParams.institute_id;
        let member_id = "";

        $scope.page_title = Models.readable_text("Edit institute" );
        // Get profile information
        Models.request_sender($http,"get","institute_info",function (data) {
            // console.log(data);
            $scope.institute = data;
            $scope.name = data.name;
            $scope.member_id = data.member_id;
            member_id = data.member_id;
            $timeout(function () {
                let overlay = altair_md.card_overlay();
                Models.init_requirement();
                Models.dropify({
                    messages: {
                        'default': 'Upload banner (1650 x 290)',
                    }
                });
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                for (let item of $scope.institute.shift_data){
                                    instance.addItem(item.shift);
                                }
                            }
                        })
                    },
                    onChange:function () {
                        this.settings.heightAdjust(this);
                    },
                    plugins: {
                        'remove_button': {
                            label: ''
                        }
                    },
                });

            });

        },["'"+institute_id+"'"]);

        $scope.submit = function () {
            let others = {
                form_name:".edit-institute",
                data: {
                    institute_id:institute_id,
                    member_id:$scope.member_id,
                    before_banner:$scope.institute.banner,
                    before_logo:$scope.institute.logo,
                    edit:1,
                }
            };

            Models.request_sender($http,"update","edit_institute",function (data) {
                if (data.status){
                    let url = "/institute/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };

        $scope.balance_update = function () {
            let others = {
                data: {
                    institute_id:institute_id,
                    amount:$("[name='amount']").val()
                }
            };

            Models.request_sender($http,"post","institute_sms_balance_update",function (data) {
                if (data.status){
                    $(".user-balance").text(data.balance);
                    $("[name='amount']").val(0);
                    Models.init_requirement();
                    Models.notify("Balance added");
                }
            },[],others,$scope);
        };
    });
}]);
