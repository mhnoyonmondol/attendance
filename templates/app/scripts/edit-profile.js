// profile edit
app.register.controller("edit-profile",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;

        let member_id = "";
        let controller_type = Models.id_type_name(user_id,$rootScope);
        $scope.controller_type = controller_type;

        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }
            if (!same_user){
                Models.admin_area_manager($rootScope,$http,$scope.user.designation,$scope.user.area,$scope.user.user_id);
            }
            $scope.same_user = same_user;
            if (same_user){
                $scope.designation_editable = false;
            }

            if ($scope.user.login_schedule === ""){
                $scope.user.login_schedule = null;
            }
            $scope.name = data.name;
            $scope.member_id = data.member_id;
            member_id = data.member_id;
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);
                let overlay = altair_md.card_overlay();

                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                    default_value:data.designation_id,
                    onChange:function (value,des_instance) {
                        let options = Models.get_list_from_selectize(des_instance);
                        if (!same_user){
                            let area_element = $(".area");
                            if (area_element.length){
                                let area_instance = area_element.get(0).selectize;
                                area_instance.destroy();
                                let designation_info = Models.filter_in_array(options,{value:value});
                                let new_designation = "";
                                if (designation_info){
                                    new_designation = designation_info.label;
                                }
                                Models.admin_area_manager($rootScope,$http,new_designation,$scope.user.area,$scope.user.user_id);

                            }
                        }
                    }

                };
                let logged_controller_type = Models.logged_user_type($rootScope);
                if (logged_controller_type === "admin"){
                    let logged_user = $rootScope.basic_info.logged_user;
                    let designation = logged_user.designation;
                    let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
                    let allow = area_parent.allow;
                    options.allow = allow;
                    options.allow_compare_field = "tag";
                    options.editable = false;
                }
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign blood group selector
                options = {
                    type:"blood_group",
                    class_name:".blood-tag-chooser",
                    default_value:data.blood_group_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                

                // assign department selector
                options = {
                    type:"department",
                    class_name:".department-tag-chooser",
                    default_value:data.department_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                

                
                
                

                if (data.designation === "MPO"){
                    $scope.mpo_switch = 1;
                }
                let weekly_calender = null;
                let weekly_calender_init = null;
                if (Number($scope.user.login_schedule_switch)){
                    $(".schedule-container").removeClass("uk-hidden");
                    weekly_calender = Models.working_schedule_calendar_init();
                    let schedule_data = $scope.user.login_schedule;
                    schedule_data = JSON.parse(schedule_data);
                    let calender_option = {};
                    if (schedule_data){
                        calender_option.data = schedule_data;
                    }
                    weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                }
                $(".login-schedule [data-switchery]").change(function () {
                    let status = $(this).prop("checked");
                    if (status){
                        if (!weekly_calender){
                            weekly_calender = Models.working_schedule_calendar_init();
                        }
                        if (!weekly_calender_init){
                            let schedule_data = $scope.user.login_schedule;
                            schedule_data = JSON.parse(schedule_data);
                            let calender_option = {};
                            if (schedule_data){
                                calender_option.data = schedule_data;
                            }
                            weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                        }

                        $(".schedule-container").removeClass("uk-hidden");
                    }
                    else{
                        $(".schedule-container").addClass("uk-hidden");
                    }

                });

            });
            if ($rootScope.basic_info.logged_user_id !== $scope.user.user_id){
                // access menu and permission parts switch on
                if ($scope.controller_type === "system_admin"){
                    $scope.access_menus_switch = 1;
                    $scope.permission_parts_switch = 1;
                }

                //get initial data for access menus and permission parts
                Models.request_sender($http,"get","controller_enrol_basic_info",function (data) {
                    if (data.status){
                        $scope.estimate_access_menus = data.access_menus;
                        $scope.permission_parts = data.permission_parts;
                        $timeout(function () {
                            Models.tree_script_loader(function () {
                                altair_tree.tree_a();
                                altair_tree.tree_a(".access-permission-parts");
                                if ($scope.controller_type === "system_admin") {
                                    //// initial checked items for access menus
                                    let access_menus = $scope.user.access_menus;
                                    let tree = $(".tree-checkbox").fancytree("getTree");
                                    for (let x of access_menus) {
                                        let node = tree.getNodeByKey(x.value);
                                        if (node) {
                                            node.setSelected(true);
                                        }

                                    }

                                    //// initial checked items for access menus
                                    let permission_parts = $scope.user.permission_parts;
                                    tree = $(".access-permission-parts").fancytree("getTree");
                                    for (let x of permission_parts) {
                                        let node = tree.getNodeByKey(x.value);
                                        if (node) {
                                            node.setSelected(true);
                                        }

                                    }
                                }
                            });

                        });
                    }

                },["'"+$scope.controller_type+"'"]);
            }

        });


        $scope.submit = function () {
            let others = {
                form_name:".edit-profile",
                data: {
                    user_id:user_id,
                    before_member_id: member_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_nid: $scope.user.nid,
                    before_signature: $scope.user.signature,
                    before_active: $scope.user.active,
                    before_login_schedule: "",
                    login_schedule: "",
                    edit:1,
                    basic_salary:0
                }
            };
            if (!$scope.designation_editable){
                others.data.designation = $scope.user.designation_id;
            }
            if ($scope.user.login_schedule){
                others.data.login_schedule = $scope.user.login_schedule;
            }
            others.data.login_schedule_switch = 0;
            if ($rootScope.basic_info.logged_user_id !== $scope.user.user_id) {
                if ($scope.controller_type === "system_admin") {
                    let tree = [
                        {
                            element_name: ".tree-checkbox",
                            selected_name: "menu_ids[]",
                            active_name: "parent"
                        },
                        {
                            element_name: ".access-permission-parts",
                            selected_name: "permission_parts[]",
                            active_name: "parent"
                        },
                    ];
                    others.fancy_tree = tree;
                }


                others.data.login_schedule = "";
                let login_schedule_switch = $(".login-schedule [data-switchery]").prop("checked");
                if (login_schedule_switch){
                    others.data.login_schedule_switch = 1;
                    if ($(".scheduler:visible").length){
                        let export_data = $(".scheduler").jqs('export');
                        let json_data = JSON.parse(export_data);
                        let readable_data = [];
                        for (let item of json_data){
                            let new_periods = [];
                            for (let period of item.periods){
                                let new_period = [period.start,period.end];
                                new_periods.push(new_period);
                            }
                            let new_schedule = {
                                day: item.day,
                                periods: new_periods
                            };
                            readable_data.push(new_schedule);
                        }

                        others.data.login_schedule = JSON.stringify(readable_data);
                    }

                }
            }
            // console.log(others);
            // return;
            Models.request_sender($http,"update","edit_profile",function (data) {
                // console.log(data);
                if (data.status){
                    if (data.profile_info !== undefined){
                        $rootScope.basic_info.logged_user = data.profile_info;
                    }
                    let url = "/profile/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
        $scope.balance_update = function () {
            let others = {
                data: {
                    institute_id:user_id,
                    amount:$("[name='amount']").val()
                }
            };

            Models.request_sender($http,"post","institute_sms_balance_update",function (data) {
                if (data.status){
                    $(".user-balance").text(data.balance);
                    $("[name='amount']").val(0);
                    Models.init_requirement();
                    Models.notify("Balance added");
                }
            },[],others,$scope);
        };
    });
}]);
