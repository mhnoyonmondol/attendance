/// forgot password
app.register.controller("forgot_password",['$scope','$http','$routeParams','$rootScope','$location','$timeout',function ($scope,$http,$routeParams,$rootScope,$location,$timeout) {
    $scope.all_off = function(){
        $scope.find_form_switch = 0;
        $scope.confirmation_switch = 0;
        $scope.find_list_switch = 0;
        $scope.new_password_switch = 0;
        $scope.not_found_switch = 0;
    };
    $scope.all_off();
    $scope.find_form_switch = 1;

    $timeout(function () {
        Models.init_requirement();
    });


    $scope.find_forgots = function () {
        let other = {
            form_name:".find-form"
        };
        Models.request_sender($http,"get","find_users",function (response) {
            if (response.find_data !== undefined){
                if (response.find_data.length){
                    $scope.find_list = response.find_data;
                    $scope.all_off();
                    $scope.find_list_switch = 1;
                }
                else {
                    $scope.all_off();
                    $scope.not_found_switch = 1;
                }
            }
        },[],other);
    };
    $scope.user_select = function (user_id, user_email, mobile) {
        let other = {
            data:{
                email:user_email,
                user_id: user_id,
                mobile:mobile
            }
        };
        Models.request_sender($http,"other","send_code_forgot_user",function (response) {
            if(response.status){
                $scope.code_sender_email = user_email;
                $scope.all_off();
                $scope.confirmation_switch = 1;
                $timeout(function () {
                    Models.init_requirement();
                })
            }
        },[],other);
    };
    $scope.confirm_code = function () {
        let other = {
            form_name:".confirm-form"
        };
        Models.request_sender($http,"other","confirm_code",function (response) {
            // console.log(response);
            if (response.status){
                $scope.all_off();
                $scope.new_password_switch = 1;
            }
            $timeout(function () {
                Models.init_requirement();
            })
        },[],other);
    };
    $scope.new_password = function () {
        let other = {
            form_name:".new-password-form"
        };
        Models.request_sender($http,"update","new_password",function (response) {
            // console.log(response);
            if (response.status){
                $scope.all_off();
                $scope.done_switch = 1;
            }
            $timeout(function () {
                Models.init_requirement();
            })
        },[],other);
    };
    $scope.back_to_find = function () {
        $scope.all_off();
        $scope.find_form_switch = 1;
        $timeout(function () {
            Models.init_requirement();
        })
    }

}]);
