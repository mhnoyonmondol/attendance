
// shift settings
app.register.controller("shift_settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let id = $routeParams.id;
    Models.request_sender($http,"get","shift_info",function (data) {
        $scope.shift = data;
        $scope.settings = data.settings;
        // console.log(data);
        $timeout(function () {
            Models.init_requirement();
            let overlay = altair_md.card_overlay();
            Models.dropify({},".dropify-logo");
            Models.dropify({},".dropify-favicon");


            let expandable_height = 100;
            $("#page_content").on("focus","[data-uk-timepicker]",function () {
                let this_card = $(this).closest(".md-card");
                let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                let height_of_truncate = truncate.outerHeight();
                truncate.height(height_of_truncate + expandable_height);
                overlay.rearrange();
                setTimeout(function () {
                    $(window).resize();
                },300)

            });
            $("#page_content").on("blur","[data-uk-timepicker]",function () {
                let this_card = $(this).closest(".md-card");
                let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                let height_of_truncate = truncate.outerHeight();
                truncate.height(height_of_truncate - expandable_height);
                overlay.rearrange();
                setTimeout(function () {
                    $(window).resize();
                },300)

            });


        });
    },["'"+id+"'",true,true]);
    /// update settings
    $scope.submit = function(event){
        let target_elem = $(event.target);
        let this_form = target_elem.closest("form");
        let form_type = this_form.attr("data-type");
        let function_name = "shift_settings_update";
        let other = {};
        Models.small_loading(target_elem);
        if (form_type === "absence_person_a_day"){
            let day = this_form.find("[name='day']").val();
            other.form_name = this_form;
            let data = {
                type: "absence_person_a_day",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "in_time"){
            let day = this_form.find("[name='in_time']").val();
            day = Models.getTwentyFourHourTime(day);
            other.form_name = this_form;
            let data = {
                type: "in_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "out_time"){
            let day = this_form.find("[name='out_time']").val();
            day = Models.getTwentyFourHourTime(day);
            other.form_name = this_form;
            let data = {
                type: "out_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "late_time"){
            let day = this_form.find("[name='late_time']").val();
            other.form_name = this_form;
            let data = {
                type: "late_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "early_time"){
            let day = this_form.find("[name='early_time']").val();
            other.form_name = this_form;
            let data = {
                type: "early_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "in_time_scan_range"){
            let in_range_from = this_form.find("[name='in_range_from']").val();
            in_range_from = Models.getTwentyFourHourTime(in_range_from);
            let in_range_to = this_form.find("[name='in_range_to']").val();
            in_range_to = Models.getTwentyFourHourTime(in_range_to);
            other.form_name = this_form;
            let data = {
                type: "in_time_scan_range",
                in_range_from: in_range_from,
                in_range_to: in_range_to,
                value: true,

            };
            other.data = data;

        }
        else if (form_type === "out_time_scan_range"){
            let out_range_from = this_form.find("[name='out_range_from']").val();
            out_range_from = Models.getTwentyFourHourTime(out_range_from);
            let out_range_to = this_form.find("[name='out_range_to']").val();
            out_range_to = Models.getTwentyFourHourTime(out_range_to);
            other.form_name = this_form;
            let data = {
                type: "out_time_scan_range",
                out_range_from: out_range_from,
                out_range_to: out_range_to,
                value: true,

            };
            other.data = data;

        }


        other.data.shift_id = $routeParams.id;

        Models.request_sender($http,"update",function_name,function (data) {
            if (data.status){
                Models.small_loading(target_elem,0,"Updated");
            }
            else{
                Models.small_loading(target_elem, 0, "Failed");
            }
        },[],other);
    };



}]);

