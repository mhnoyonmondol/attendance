<?php
    $software_title = "";
    $software_favicon = "";
    require_once(project_root."controllers/packages/vendor/autoload.php");
    $info = new retrieve();
    $software_info = $info->software_info();
    $software_title = $software_info['title'];
    $software_favicon = $software_info['favicon'];
    if ($_SERVER['SERVER_NAME'] != "localhost"){
        $https = $software_info['https'];
        if ($https){
            $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
            if ($protocol == "http"){
                $https_url = "https". "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                header("location:$https_url");
            }
        }
    }

    $server_name = $_SERVER['HTTP_HOST'];
    $common = new common();
    $browser_name = $common->getBrowser()['name'];
    $compress = 1;
    if (($server_name == "attendance-localhost.com" or $server_name == "localhost") and $browser_name == "Mozilla Firefox"){
//    if (($server_name == "localtest.com" or $server_name == "localhost")){
        $compress = 0;
    }

?>

<!doctype html>
<html lang=" en" data-ng-app="heeshab">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=2.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no"/>
        <meta name="theme-color" content="#2196f3"/>
        <title><?php echo $software_title ?></title>
        <base href="<?php echo project_base;?>"/>
        <link rel="icon" href="<?php echo $software_favicon ?>">
        <link rel="apple-touch-icon" href="static/app/assets/icons/march_edukit/icon-192x192.png">
        <?php if ($compress):?>
        <style>
            html{font:400 14px/20px "Helvetica Neue", Helvetica, Arial, sans-serif;text-size-adjust:100%;background:rgb(236, 236, 236);color:rgb(68, 68, 68);height:100%;overflow:hidden auto;}body{margin:0px;min-height:100%;font:400 14px/1.42857 Roboto, sans-serif;padding-top:48px;box-sizing:border-box;}article, aside, details, figcaption, figure, footer, header, main, nav, section, summary{display:block;}#header_main{background:rgb(25, 118, 210);box-shadow:rgba(0, 0, 0, 0.16) 0px 3px 6px, rgba(0, 0, 0, 0.23) 0px 3px 6px;padding:0px 25px;height:48px;left:0px;position:fixed;right:0px;top:0px;z-index:1104;}#header_main, #page_content, #top_bar{will-change:margin;transition:margin 280ms ease 0s;}.sidebar_main_open #header_main, .sidebar_main_open #page_content, .sidebar_main_open #top_bar{margin-left:240px;}.disable_transitions #header_main, .disable_transitions #header_main .sSwitch .sSwitchIcon, .disable_transitions #header_main .sSwitch .sSwitchIcon::after, .disable_transitions #header_main .sSwitch .sSwitchIcon::before, .disable_transitions #page_content, .disable_transitions #sidebar_main, .disable_transitions #top_bar{transition:none 0s ease 0s !important;}.uk-navbar{background:rgb(245, 245, 245);color:rgb(68, 68, 68);border:1px solid rgba(0, 0, 0, 0.06);border-radius:4px;}#header_main .uk-navbar{border:none;background:0px 0px;}#sidebar_main{width:240px;border-right:1px solid rgba(0, 0, 0, 0.12);position:fixed;height:100%;top:0px;bottom:0px;left:0px;transform:translate3d(-240px, 0px, 0px);z-index:1204;background:rgb(255, 255, 255);transition:all 280ms cubic-bezier(0.4, 0, 0.2, 1) 0s;}#sidebar_main, #sidebar_main *, #sidebar_main ::after, #sidebar_main ::before, #sidebar_main::after, #sidebar_main::before{box-sizing:border-box;}.sidebar_main_open #sidebar_main{transform:translate3d(0px, 0px, 0px);}#sidebar_main .sidebar_main_header{height:89px;border-bottom:1px solid rgba(0, 0, 0, 0.12);margin-bottom:20px;background-image:url("static/app/assets/img/sidebar_head_bg.png");background-repeat:no-repeat;background-position:0px 0px;position:relative;}#page_content_inner{padding:24px 24px 100px;}h1, h2, h3, h4, h5, h6{margin:0px 0px 15px;font-family:Roboto, "Helvetica Neue", Helvetica, Arial, sans-serif;font-weight:500;color:rgb(68, 68, 68);text-transform:none;}.uk-h1, h1{font-size:36px;line-height:42px;}
        </style>
        <?php endif ?>
        <link rel="manifest" href="manifest.json">
    </head>
    <body class="disable_transitions sidebar_main_open sidebar_main_swipe ((document_switch | if : 'header_double_height')) ((logged | if : '' : '' : 'login_page login_page_v2'))" ng-controller="basic">
        <!-- main header -->
        <header id="header_main">
            <div class="header_main_content">
                <nav class="uk-navbar" ng-if="nav_switch"  ng-include="'templates/app/body-parts/nav.html'" onload="nav_init()">
                    <!-- nav bar -->
                </nav>
            </div>
        </header><!-- main header end -->
        <!-- main sidebar -->
        <aside id="sidebar_main">
            <div class="sidebar_main_header" ng-if="main_header_switch" ng-include="'templates/app/body-parts/main-header.html'" onload="main_header_init()">
                <!-- side bar main -->
            </div>
            <div class="menu_section" ng-if="main_menu_switch" ng-include="'templates/app/body-parts/main-menu.html'" onload="main_menu_init()">
                <!-- main menu -->
            </div>
        </aside>
        <!-- main sidebar end -->

        <div id="page_content" ng-view role="main" ng-if="logged">
            <div id="page_content_inner">
                <h1>Please wait...</h1>
                <span>processing... </span>
            </div>
        </div>
        <div id="page_content" ng-if="!page_access && logged">
            <ng-include ng-if="denied_page_switch" src="'templates/app/pages/505.html'"></ng-include>
        </div>

        <div ng-if="!logged" >
            <ng-include src="'templates/app/pages/login-recommend.html'" onload="login_init()"></ng-include>
        </div>


        <!-- secondary sidebar -->
        <aside id="sidebar_secondary" class="tabbed_sidebar" ng-controller="shortcut_important_list" ng-if="shortcut_important_list_switch" src="'templates/app/pages/parts/shortcut-important-list.html'" onload="shortcut_list_on()">
            <!-- shortcut work place -->
        </aside>
        <!-- message box error -->
        <div class="md-card message-box" >
            <i class="material-icons message-box-close md-icon">close</i>
            <div class="md-card-content">

            </div>
        </div>
        <?php if ($compress):?>
            <script src="static/app/assets/js/index.js"></script>
            <script>
                if("serviceWorker" in navigator){
                    navigator.serviceWorker.register("sw.js")
                        .then(function (reg) {
                            console.log("Service worker registered");
                            reg.onupdatefound = () => {
                                const installingWorker = reg.installing;
                                installingWorker.onstatechange = () => {
                                    if (installingWorker.state === 'installed' &&
                                        navigator.serviceWorker.controller) {
                                        // Preferably, display a message asking the user to reload...
                                        Models.confirm(function () {
                                            location.reload();
                                        },"New version found, Update now. <br> <span class='uk-text-small uk-text-muted'> </span>");
                                        // reg.active.postMessage('purge_cache');
                                        // let version = 1;

                                    }
                                };
                            };
                        }).catch(function (result) {
                        console.log("Service worker registration failed");
                        console.log(result);
                    });
                }
            </script>
        <?php else:?>
        <link rel="stylesheet" href="static/app/bower_components/uikit/css/uikit.almost-flat.min.css" media="all">
        <!-- altair admin -->
        <link rel="stylesheet" href="static/app/assets/skins/dropify/css/dropify.css" media="all">
        <link rel="stylesheet" href="static/app/assets/css/main.min.css" media="all">
        <!-- themes -->
        <link rel="stylesheet" href="static/app/assets/css/themes/themes_combined.min.css" media="all">
        <link rel="stylesheet" href="static/app/assets/css/login_page.min.css" media="all">
        <link rel="stylesheet" href="static/app/assets/css/print.css" >
        <link rel="stylesheet" href="static/app/assets/css/style.css" media="all" >
        <!-- common functions -->
        <script src="static/app/assets/js/angular.min.js"></script>
        <script src="static/app/assets/js/angular-route.min.js"></script>
        <script src="static/app/assets/js/angular-sanitize.min.js"></script>
        <script src="static/app/assets/js/common.min.js"></script>
        <!-- uikit functions -->
        <script src="static/app/assets/js/uikit_custom.js"></script>
        <!-- altair common functions/helpers -->
        <script src="static/app/assets/js/altair_admin_common.js"></script>
        <!-- plugins -->
        <script src="static/app/assets/js/custom/uikit_fileinput.min.js"></script>
        <script src="static/app/bower_components/dropify/dist/js/dropify.min.js"></script>
        <script src="static/app/bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- jquery transalator -->
        <script src="static/app/bower_components/handlebars/handlebars.min.js"></script>
        <script src="static/app/assets/js/custom/handlebars_helpers.min.js"></script>
        <script src="static/app/assets/js/functions.js"></script>
        <script src="static/app/assets/js/custom.js"></script>
        <script src="static/app/assets/js/app.js"></script>
        <script src="static/app/assets/js/high-density.js"></script>
        <?php endif ?>
    </body>
    <script class="footer-template" type="footer-template">
        <footer id="footer">
            {{source}} || March edukit - 01756-018979
        </footer>
    </script>
</html>



