<?php
    require_once "define.php";
    require_once project_root."controllers/installer/db-config.php";
    if (isset($db_name) and isset($db_user_name) and isset($db_password) and isset($db_host)){
        if (empty($db_name) and empty($db_user_name) and empty($db_host)){
            header("location:install");
        }
        elseif (empty($db_name) or empty($db_user_name) or empty($db_host)){
            echo "Installation not correctly!";
            echo "<a href='install'> Update app</a>";
        }
        else{
            require_once project_root."templates/app/index.php";
        }

    }
    else{
        echo "Configuration of Database connection not correct!";
    }
?>