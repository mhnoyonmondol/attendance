let base_path = '';
let template_dir = base_path+"templates/app/pages/";
let template_script_dir = base_path+"templates/app/scripts/";
let static_dir = "static/app/assets/";
let app = angular.module("heeshab",["ngRoute","ngSanitize"]);
/// url key for find location / keyword like /home and page is html file for which is content of this page
let urls = {
    home:{"url":"/","page":"home",},
    home2:{"url":"/home/:category?","page":"home"},
    home3:{"url":"/index","page":"home"},
    logout:{"url":"/logout","page":"logout"},
    not_found:{"url":"/404","page":"404"},
    undefined_branch:{"url":"/branch-error","page":"branch-error"},
    login:{"url":"/login","page":"login"},
    initiate:{"url":"/initiate","page":"initiate"},
    import_items:{"url":"/import-items","page":"import-items"},
    add_controller:{"url":"/add-controller/:controller_type","page":"add-controller"},
    profile:{"url":"/profile/:member_id","page":"profile"},
    edit_profile:{"url":"/edit-profile/:user_id","page":"edit-profile"},
    login_info_helper:{"url":"/login-info-helper","page":"login-info-helper"},
    controllers:{"url":"/handlers/:controller_type?","page":"handlers"},
    add_item:{"url":"/add-item","page":"add-item"},
    item:{"url":"/item/:item_id","page":"item"},
    edit_item:{"url":"/edit-item/:item_id","page":"edit-item"},
    settings:{"url":"/settings","page":"settings"},
    institute_settings:{"url":"/institute-settings/:institute_id","page":"institute-settings"},
    items:{"url":"/items","page":"data-table"},
    vendors:{"url":"/vendors","page":"data-table"},
    customers:{"url":"/customers","page":"data-table"},
    transports:{"url":"/transports","page":"data-table"},
    bank_cashes:{"url":"/bank-cashes","page":"data-table"},
    branch_items:{"url":"/branch-items","page":"data-table"},
    special_items:{"url":"/special-items","page":"data-table"},
    branch_bank_cashes:{"url":"/branch-bank-cashes","page":"data-table"},
    reports:{"url":"/reports","page":"data-table"},
    admin_daily_attendance:{"url":"/admin-daily-attendance","page":"data-table"},
    institute_list:{"url":"/institute-list","page":"data-table"},
    teacher_list:{"url":"/teacher-list","page":"data-table"},
    student_list:{"url":"/student-list","page":"data-table"},
    today_present_attendance:{"url":"/today-present-attendance/:user_type/:source?","page":"today-attendance"},
    today_absent_attendance:{"url":"/today-absent-attendance/:user_type/:source?","page":"today-absent-attendance"},
    today_training_attendance:{"url":"/today-training-attendance/:user_type/:source?","page":"today-attendance"},
    today_leave_attendance:{"url":"/today-leave-attendance/:user_type/:source?","page":"today-attendance"},
    today_late_attendance:{"url":"/today-late-attendance/:user_type/:source?","page":"today-attendance"},
    today_early_attendance:{"url":"/today-early-attendance/:user_type/:source?","page":"today-attendance"},
    attendance_logs:{"url":"/attendance-logs/:institute_id","page":"attendance-logs"},
    student_attendance_logs:{"url":"/student-attendance-logs/:institute_id","page":"attendance-logs"},
    dynamic_report_templates:{"url":"/dynamic-report-templates","page":"data-table"},
    bank_reconciliations:{"url":"/bank-reconciliations","page":"data-table"},
    batch_maker:{"url":"/batch-maker","page":"data-table"},
    shifts:{"url":"/shifts","page":"data-table"},
    devices:{"url":"/devices","page":"data-table"},
    forgot_password:{"url":"/forgot-password","page":"forgot-password"},
    construction_mode:{"url":"/construction-mode","page":"construction-mode"},
    account_source:{"url":"/account-source","page":"account-source"},
    document_source:{"url":"/document-source","page":"document-source"},
    mapping:{"url":"/mapping","page":"mapping"},
    application:{"url":"/application/:document_sub_type?/:document_id?","page":"document"},
    invoice:{"url":"/invoice/:document_sub_type?/:document_id?","page":"document"},
    bill:{"url":"/bill","page":"document"},
    receipt:{"url":"/receipt","page":"document"},
    add_vendor:{"url":"/add-vendor","page":"add-customer-vendor"},
    add_teacher:{"url":"/add-teacher/:institute_id","page":"add-teacher"},
    teacher:{"url":"/teacher/:member_id","page":"teacher"},
    add_student:{"url":"/add-student/:institute_id","page":"add-student"},
    student:{"url":"/student/:member_id","page":"student"},

    customer:{"url":"/customer/:member_id","page":"customer-vendor"},
    edit_teacher:{"url":"/edit-teacher/:user_id","page":"edit-teacher"},
    edit_student:{"url":"/edit-student/:user_id","page":"edit-student"},
    edit_customer:{"url":"/edit-customer/:user_id","page":"edit-customer-vendor"},
    tree:{"url":"/network/:user_id?","page":"tree"},
    activities:{"url":"/activities","page":"activities"},
    add_bank_cash:{"url":"/add-bank-cash","page":"add-bank-cash"},
    edit_bank_cash:{"url":"/edit-bank-cash/:bank_cash_id","page":"edit-bank-cash"},
    bank_cash:{"url":"/bank-cash/:bank_cash_id","page":"bank-cash"},
    report_generate:{"url":"/report-generate","page":"report-generate"},
    add_institute:{"url":"/add-institute","page":"add-institute"},
    edit_institute:{"url":"/edit-institute/:institute_id","page":"edit-institute"},
    institute:{"url":"/institute/:member_id","page":"institute"},
    institutes:{"url":"/institutes","page":"institutes"},
    teachers:{"url":"/teachers/:institute_id","page":"teachers"},
    students:{"url":"/students/:institute_id","page":"students"},
    add_transport:{"url":"/add-transport","page":"add-transport"},
    edit_transport:{"url":"/edit-transport/:transport_id","page":"edit-transport"},
    transport:{"url":"/transport/:member_id","page":"transport"},
    add_special_item:{"url":"/add-special-item","page":"add-special-item"},
    edit_special_item:{"url":"/edit-special-item/:special_item_id","page":"edit-special-item"},
    special_item:{"url":"/special-item/:member_id","page":"special-item"},
    dynamic_report_maker:{"url":"/dynamic-report-maker/","page":"dynamic-report-maker"},
    report:{"url":"/report/:report_id","page":"report"},
    edit_dynamic_report_template:{"url":"/edit-dynamic-report-template/:report_id","page":"edit-dynamic-report-template"},
    area_manager:{"url":"/area-manager","page":"area-manager"},
    global_calendar:{"url":"/global-calendar","page":"calendar"},
    institute_calendar:{"url":"/institute-calendar/:source","page":"calendar"},
    teacher_calendar:{"url":"/teacher-calendar/:source","page":"calendar"},
    student_calendar:{"url":"/student-calendar/:source","page":"calendar"},
    institute_device_manage:{"url":"/institute-device-manage/:institute_id","page":"institute-device-manage"},
    school:{"url":"/school","page":"school"},
    attendance:{"url":"/attendance","page":"attendance"},
    notice:{"url":"/notice","page":"notice"},
    notices:{"url":"/notices","page":"notices"},
    head_teacher_home:{"url":"/head-teacher-home/:source","page":"head-teacher-home"},
    head_teacher_home_2:{"url":"/head-teacher-home-2/:source","page":"head-teacher-home-2"},
    admin_home:{"url":"/admin-home","page":"admin-home"},
    admin_home_2:{"url":"/admin-home-2","page":"admin-home-2"},
    system_admin_home:{"url":"/system-admin-home","page":"system-admin-home"},
    student_home:{"url":"/student-home/:source","page":"student-home"},
    teacher_home:{"url":"/teacher-home/:source","page":"teacher-home"},
    monthly_attendance_report:{"url":"/monthly-attendance-report/:category?","page":"monthly-attendance-report"},
    student_monthly_attendance_report:{"url":"/student-monthly-attendance-report/:source?","page":"monthly-attendance-report"},
    teacher_monthly_attendance_report:{"url":"/teacher-monthly-attendance-report/:source?","page":"monthly-attendance-report"},
    add_shift:{"url":"/add-shift","page":"add-shift"},
    edit_shift:{"url":"/edit-shift/:id","page":"edit-shift"},
    shift_settings:{"url":"/shift-settings/:id","page":"shift-settings"},
};


app.config(['$interpolateProvider','$routeProvider','$locationProvider','$httpProvider',function($interpolateProvider,$routeProvider,$locationProvider,$httpProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $locationProvider.html5Mode(true);

    //provider for csft token security reason
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    //single page development
    for (let url_info in urls){
        let url = urls[url_info].url;
        let page = urls[url_info].page;

        $routeProvider
        .when(url, {
            templateUrl : template_dir+page+".html",
            resolve:{
                script_load:function () {
                    return new Promise(function (resolve,reject) {
                        let list_of_script = [template_script_dir+page+".js"];
                        if (urls[url_info].controllers !== undefined){
                            list_of_script = [];
                            for (let controller_name of urls[url_info].controllers){
                                list_of_script.push(template_script_dir+controller_name+".js")
                            }
                        }

                        Models.multiple_script_loader(list_of_script,function () {
                            resolve();
                        });
                    })
                }
            }
        })
    }
    $routeProvider
    .otherwise(urls.not_found.url,{
        redirect: urls.not_found.page
    });
}]);
app.run(['$rootScope','$location','$route',function ($rootScope,$location,$route) {
   $rootScope.currency = "৳";
   $rootScope.site_logo = "/base/media/default/om-bazar-logo.png";
   $rootScope.rows = [];
   $rootScope.require = function(){

   };
   $rootScope.error_page = function(){
        let url = "/404";
        $location.path(url);
    };
   $rootScope.registration_complete = 0;
   $rootScope.rows_load = function(){};
   $rootScope.stores = [];
   $rootScope.basic_info = undefined;
   $rootScope.main_menus = undefined;
   $rootScope.previous_page = function () {
        window.history.back();
   };
   $rootScope.menu_called = 0;
   $rootScope.page_access = 1;
   $rootScope.logged = 1;
   $rootScope.denied_page_switch = 0;
   $rootScope.route_reload = function () {
       $route.reload();
   }
   $rootScope.self_profile_path = "profile";
   // basic info scopes
    // ***************************
    //**
    //**
    //**
    // ***************************
   // $rootScope.table_pagination_path = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/parts/table-pagination.html';

}]);
app.filter('range', [function() {
  return function(input, total) {
    total = parseInt(total);

    for (let i=0; i<total; i++) {
        if (i>0){
            input.push(i);
        }

    }

    return input;
  };
}]);
app.filter('if', [function() {
  return function(input, result, comparism='',else_result) {
      if (comparism !== ""){
          if (input===comparism) {
              return result;
          }
          else{
              return else_result;
          }
      }
      else{
          if(input){
              return result;
          }
          else{
              return else_result;
          }
      }
  };
}]);

app.filter('time', [function() {
  return function(input) {
      return moment(input).format("MMMM Do YYYY, h:mm:ss a");
  };
}]);

app.filter('activity_icon', ['$rootScope',function($rootScope) {
  return function(input) {
      let type_of_activities = $rootScope.type_of_activities;
      if (type_of_activities !== undefined){
          let filter = Models.custom_filter(type_of_activities,"label",input);
          if(filter !== undefined){
              return filter.icon;
          }
      }
  };
}]);

app.filter('activity_template', ['$rootScope',function($rootScope) {
  return function(input) {
        return Models.activity_template(input,$rootScope);
  };
}]);

app.filter('controller_type_name', ['$rootScope',function($rootScope) {
  return function(input) {
        let id_type_name = Models.id_type_name(input,$rootScope);
        let text = Models.readable_text(id_type_name);
        return text;
  };
}]);



app.filter('activate_worker', ['$rootScope',function($rootScope) {
  return function(input, result="",else_result="") {
      let activate_worker_id = Models.activate_worker_id($rootScope);
      if (activate_worker_id === input){
          return result;
      }
      else{
          return else_result;
      }
  };
}]);

app.filter('default', [function() {
  return function(input, result) {
      if (input === undefined){
          return result
      }
  };
}]);
app.filter('object_line_total', [function() {
  return function(input) {
      let item = input;
      let price = item.price;
      let sale = item.sale;
      let vat = item.vat;
      let quantity = item.quantity;

      //for gather rice grand total
      let line_total = 0;
      if (item.sale){
          line_total = item.sale*item.quantity;
      }
      else{
          line_total = item.price*item.quantity;
      }
      let line_vat = line_total*(vat/100);
      let line_total_with_vat = line_total+line_vat;
      return line_total_with_vat;

  };
}]);
app.filter('object_exist', [function() {
  return function(input, objects,key_name, result = 0) {
      let item = Models.custom_filter(objects,key_name,input);
      if (item !== undefined){
          if (result){
              return result;
          }
          else{
              return item;
          }
      }
      else{
          return "not-found";
      }

  };
}]);

app.filter('point', ['$rootScope',function($rootScope) {
  return function(input,quantity=0) {
      return  Models.point_maker($rootScope,input,quantity);
  };
}]);

app.filter('user_tooltip', function() {
    return function(input) {
        let html = $(".user-tooltip").html();
        let compile = Handlebars.compile(html)(input);
        return compile;
    };
});



app.directive("stalks",function () {
    let template  = ""+
        "<ul ng-if=\"stalk.stalk.length>0\">\n" +
        '<li data-info="((stalk))" ng-repeat="stalk in stalk.stalk">'+
        '<div class="tree-user">'+
        '<a class="" href="network/((stalk.user_id))" data-uk-tooltip="{pos:\'right\'}" title="((stalk | user_tooltip))">'+
        '<img src="((stalk.image))">'+
        '<span>((stalk.name))</span>'+
        '</a>'+
        '</div>'+
        '<!-- directive: stalks -->'+
        '</li>'+
        "</ul>\n";
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
app.service('service',['$rootScope',function ($rootScope) {
    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };

    this.main_menu_reload = function () {
        $rootScope.$emit("main_menu_reload");
    };
    this.container_load = function (callback = function () {}) {
        $rootScope.$emit("container_load");
        callback();
    }



}]);

app.directive("subMenus",function () {
    let template  = ''+
            '<ul ng-if="menu.menus.length>0">\n' +
            '<li title="Dashboard" ng-repeat="menu in menu.menus " >\n' +
            '<a href="((menu.link))">\n' +
            '<span class="menu_title">((menu.menu_name))</span>\n' +
            '</a>\n' +
            '\n' +
            '<!-- directive: sub-menus -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
app.directive("subCategories",function () {
    let template  = ''+
        '<ul ng-if="category.categories.length>0">\n' +
        '<li id="((category.id))" ng-repeat="category in category.categories " >\n' +
        '((category.name))\n' +
        '<!-- directive: sub-categories -->' +
        '</li>\n' +
        '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
/// sub document source
app.directive("subDocumentSource",function () {
    let template = '<ul class="md-list md-list-addon" data-uk-sortable="{group:\'connected-group\'}">\n' +
        '                        <li id="((source.id))" ng-repeat="source in source.children">\n' +
        '                            <div class="md-list-addon-element sortable-handler" ng-bind-html="source.icon">\n' +
        '\n' +
        '                            </div>\n' +
        '                            <div class="md-list-content">\n' +
        '                                <span class="md-list-heading">((source.name))</span>\n' +
        '                                <span class="uk-text-small uk-text-muted">((source.description))</span>\n' +
        '                            </div>\n' +
        '<!-- directive: sub-document-source -->' +
        '                        </li>\n' +
        '                    </ul>';

    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

//sub account source
app.directive("subAccountSource",function () {
    let template = '<ul class="md-list md-list-addon uk-sortable" data-uk-sortable="{group:\'connected-group\'}">\n' +
        '                        <li id="((source.id))" ng-repeat="source in source.children">\n' +
        '                            <div class="md-list-addon-element sortable-handler" ng-bind-html="source.icon">\n' +
        '\n' +
        '                            </div>\n' +
        '                            <div class="md-list-content">\n' +
        '                                <span class="md-list-heading">((source.name))</span>\n' +
        '                                <span class="uk-text-small uk-text-muted">((source.description))</span>\n' +
        '                            </div>\n' +
        '<!-- directive: sub-account-source -->' +
        '                        </li>\n' +
        '                    </ul>';

    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// install or import collections
app.controller("install",['$scope','$http','$rootScope','service','$timeout',function ($scope,$http,$rootScope,service,$timeout) {
    let function_name = "installed_collections()";
    let extra_data = Models.retrive_request_object(function_name);
    let form_data = Models.form_data_maker("",extra_data);
    $scope.collections = [
    ];
    Models.$http($http,form_data,function (response,status) {
        let find_data = response.find_data;
        for(let collection in find_data){
            $scope.collections.push(find_data[collection]);

        }
    });

    ///upload new collection
    let  token = Models.csrftoken;
    let params = {
        request_type: "post",
        request_name: "upload_collection(request)",
        csrfmiddlewaretoken: token
    };
    Models.drug_drop_upload(params,function (response) {
        let new_data = response.new_collection;
        let collection_name = new_data.collection_name;
        let available = Models.find_index($scope.collections,"collection_name",collection_name);
        $scope.$apply(function () {
            if (available === -1) {
                $scope.collections.push(new_data);
            }else{
                $scope.collections[available] = new_data;
            }
        })
    });



}]);

// Page header to footer basic controller
app.controller("basic",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let public_pages = [
        'profile',
        'login',
        'logout',
        "add-controller/system_designer",
        "forgot-password",
        "login-info-helper",
        "branch-error",
        "index",
        "",
        "home",
    ];

    let initial_pages = [
        "add-controller/system_designer",
        "settings",
        "account-source",
        "document-source",
        "mapping",
        "add-branch",
        "handlers/system_designer",
        "activities",
        "branches",
        "profile",
        "index",
        "",
        "home",
        "branch",
        "edit-branch",
        "edit-profile",
        "branch-error",
        "add-special-item",
        "edit-special-item",
        "special-items",
        "special-item",
        "add-item",
        "edit-item",
        "items",
        "item",
    ];

    $scope.public_pages = public_pages;
    $scope.initial_pages = initial_pages;

    // When nav template data loaded
    $scope.nav_init = function(){
        $scope.main_header_switch = 1;
    };
    // When main header / logo and branch selector template data loaded
    $scope.main_header_init = function(){
        $scope.main_menu_switch = 1;
    };
    // When main menu template data loaded
    $scope.main_menu_init = function(){
        // altair_main_header.init();
        altair_helpers.full_screen();
        $scope.basic_info_init();
        // console.log('menu init all ok')
    };
    $scope.login_init = function(){
        $location.path("/login");
    };
    $rootScope.document_switch = 0;
    $rootScope.shortcut_important_list_switch = 0;
    $scope.nav_switch = 1;
    $scope.main_header_switch = 0;
    $scope.main_menu_switch = 0;

    $scope.basic_info_init = function(){

        Models.get_basic_info($http,$rootScope,function (data) {
            // console.log(data);
            let basic_info = data;
            let logged_user_id = basic_info.logged_user_id;
            if (logged_user_id !== undefined){
                $(".sidebar_actions").removeClass("uk-hidden");
            }

            // let db_config = Models.db_init($rootScope.basic_info.tables_schema);
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);
            // Get main menus data and initiate main menu
            $scope.menu_init = function(){
                if ($rootScope.basic_info!== undefined){
                    Models.user_menu_init($http,$rootScope,function (menu_list) {
                        $scope.menus = menu_list;
                        $timeout(function () {
                            Models.active_menu($location);
                            altair_main_sidebar.init();
                        });

                    });
                }
                else{
                    $scope.menus = [];
                }

            };
            $scope.menu_init();
            if ($rootScope.basic_info !== undefined) {
                if ($rootScope.basic_info.software_info !== undefined){
                    let software_info = $rootScope.basic_info.software_info;
                    $(".sidebar_logo img").attr('src',software_info.logo);
                    $rootScope.currency = software_info.currency;
                }
            }
            $rootScope.$on("main_menu_reload",function () {
                $scope.menu_init();
            });

            $scope.login_check();
        });

    };
    $scope.login_check = function(){
        ///public page
        let page_name = Models.page_name($location,[2]);
        let root_page_name = Models.page_name($location);

        for (let p of public_pages){
            if (page_name === p || p === root_page_name){
                $rootScope.logged = 1;
                return true;
            }
        }

        if(!$rootScope.basic_info.logged_user_id){
            // $location.path("/login");
            $rootScope.logged = 0;
            return false;
        }
        else{
            $rootScope.logged = 1;
            return true;
        }
    };

    $scope.page_controller = function(){

        Models.get_basic_info($http,$rootScope,function (basic_data) {
            let page = Models.page_name($location);
            let page_name = Models.page_name($location,[2]);
            let controller_type = Models.logged_user_type($rootScope);
            $scope.denied_page = function () {
                $rootScope.page_access = 0;
                $rootScope.denied_page_switch = 1;
                $rootScope.document_switch = 0;
                $rootScope.logged = 0;
                return false;
            };
            $scope.access_open = function () {
                $rootScope.page_access = 1;
                $rootScope.logged = 1;
                $rootScope.denied_page_switch = 0;
            };

            /// initiate document
            let logged_user_id = $rootScope.basic_info.logged_user_id;
            if (logged_user_id !== undefined){
                if (page === 'receipt' || page === 'application'){
                    $rootScope.document_switch = 1;
                }
                else{
                    $rootScope.document_switch = 0;
                }

                if (page_name === 'pos'){
                    $rootScope.shortcut_important_list_switch = 1;
                }
                else{
                    $rootScope.shortcut_important_list_switch = 0;
                }
            }
            let public_page = $scope.public_pages.indexOf(page_name);
            if (public_page !== -1){
                $scope.access_open();
                return false;
            }

            if (controller_type === "system_designer"){
                return ;
            }
            else{
                let software_info = basic_data.software_info;
                if (software_info !== undefined){
                    let construction_mode = software_info.construction_mode;
                    if (construction_mode !== undefined){
                        let status = construction_mode.active;
                        if (status){
                            $rootScope.construction_time = construction_mode.date +" "+ construction_mode.time;
                            // console.log($rootScope.construction_time);
                            if (Date.parse($rootScope.construction_time) > Date.now()) {
                                $location.path('/construction-mode');
                                let logged_user = $rootScope.basic_info.logged_user;
                                if (logged_user!==undefined){
                                    let access_menus = logged_user.access_menus;
                                    if (access_menus !== undefined){
                                        $rootScope.basic_info.logged_user.access_menus = [];
                                        service.main_menu_reload();
                                    }
                                }
                            }

                        }
                        else{
                            $rootScope.construction_time = null;
                        }
                    }
                }
            }
            if (page === "profile"){
                $rootScope.logged = 1;
                return ;
            }
            let user_access_menus = Models.user_access_menus($rootScope);
            let get_access_keys = [];

            if ($rootScope.basic_info !== undefined && user_access_menus !== undefined){
                for (let x of user_access_menus){
                    get_access_keys.push(Number(x.value));
                }
                Models.get_main_menus($http,$rootScope,function (main_menus) {
                    let menu_info = Models.custom_filter(main_menus,"link",page_name);
                    if (menu_info !== undefined){
                        let menu_id = menu_info.id;
                        let exist = get_access_keys.indexOf(menu_id);
                        if (exist === -1){
                            $scope.denied_page();

                        }
                        else{
                            $rootScope.page_access = 1;
                            $rootScope.logged = 1;
                            // $rootScope.denied_page_switch = 1;
                        }
                    }
                    // else if (menu_info === undefined) {
                    //     $scope.denied_page();
                    // }
                    else{
                        $scope.access_open();
                    }
                })
            }
            else if(user_access_menus === undefined){
                $scope.denied_page();
            }
        });


    };
    $scope.sidebar_manage = function(){
        let window_width = $(window).width();
        if (window_width <= 1220){
            altair_main_sidebar.hide_sidebar();
        }
    };

    $scope.$on('$routeChangeStart', function($event, next, current) {
        $scope.page_controller();
        $scope.sidebar_manage();
        Models.page_title_generator($location,$scope,$rootScope);
        /// destroy the before initiate data table for error handle of invoice/document page
        let table = $( '#dt_tableHeeshab' ).dataTable().api();
        if (table.context.length){
            table.destroy();
        }
        Models.footer_remove();


    });
    $scope.$on('$routeChangeSuccess', function($event, next, current) {
        $("body").click();
    });

}]);

// home
app.controller("home",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_text = "Dashboard loading...";
    let category = $routeParams.category;
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user_type = Models.logged_user_type($rootScope);
            if (logged_user_type){
                let logged_user = Models.logged_user($rootScope);
                let member_id = logged_user.member_id;
                let designation = logged_user.designation;
                if (logged_user_type === "teacher" && designation.toLowerCase() === "head teacher"){
                    if (category !== undefined){
                        $location.path("/head-teacher-home-2/"+member_id);
                    }
                    else{
                        $location.path("/head-teacher-home/"+member_id);
                    }


                }
                else if (logged_user_type === "teacher" && designation.toLowerCase() !== "head teacher"){
                    // $location.path("/teacher-home");
                }
                else if (logged_user_type === "admin"){

                    if (category !== undefined){
                        $location.path("/admin-home-2");
                    }
                    else{
                        $location.path("/admin-home");
                    }

                }
                else if (logged_user_type === "system_admin" || logged_user_type === "system_designer"){
                    $location.path("/system-admin-home");
                }
                else if (logged_user_type === "student"){
                    $location.path("/student-home/"+member_id);
                }

                else{
                    $scope.page_text = "Failed loading, for unknown user.";
                }
            }
        }

    })

}]);

//admin home
app.controller("admin_home",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                console.log(data);
                if (data.status){
                    let output = data.output;

                    let target_events = [
                        {
                            name: "Total school",
                            total: output.total_institute.total,
                            data: output.total_institute.data,
                            link: "institute-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total teacher",
                            total: output.total_teacher.total,
                            data: output.total_teacher.data,
                            link: "teacher-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total student",
                            total: output.total_student.total,
                            data: output.total_student.data,
                            link: "student-list",
                            card_type: "peity_visitors",
                        },

                        {
                            name: "Present",
                            total: output.total_present.total,
                            current: output.total_present.current,
                            link: "today-present-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Absent",
                            total: output.total_absent.total,
                            current: output.total_absent.current,
                            link: "today-absent-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "In training",
                            total: output.total_training.total,
                            current: output.total_training.current,
                            link: "today-training-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Leave",
                            total: output.total_leave.total,
                            current: output.total_leave.current,
                            link: "today-leave-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Late in",
                            total: output.total_late.total,
                            current: output.total_late.current,
                            link: "today-late-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Early gone",
                            total: output.total_early.total,
                            current: output.total_early.current,
                            link: "today-early-attendance/teacher",
                            card_type: "peity_orders",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let chartist = Models.chartist_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let template_output = Models.compiler(card_template,target_events);
                        $(".top-cards").html(template_output);
                        chartist.init({
                            columns:[
                                ['Present', output.total_present.current],
                                ['Absent', output.total_absent.current],
                                ['Training', output.total_training.current],
                                ['Leave', output.total_leave.current],
                            ]
                        },function (instance) {
                            $timeout(function () {
                                instance.resize();
                            },250);

                        });

                        altair_helpers.hierarchical_show();
                        peity_chart.init();

                        /// attendance report datatable

                        $scope.columnDefs = [
                            {
                                targets:-2,
                                render:function (data) {
                                    let time_for_event = data;
                                    if (time_for_event === "0000-00-00 00:00:00"){
                                        time_for_event = moment().format("YYYY-MM-DD");
                                    }
                                    return time_for_event;
                                },
                                orderable:false
                            }
                        ];
                        $scope.table_columns_format = [
                            {
                                label:"source_name",
                                name:"Teacher name"
                            },
                            {
                                label:"status",
                                name:"Status"
                            },
                            {
                                label:"time_for_event",
                                name:"Time for event"
                            },
                            {
                                label:"institute_name",
                                name:"Institute"
                            },


                        ];
                        // make table required format
                        $scope.column_format = [];
                        let i = 0;
                        for(let item of $scope.table_columns_format){
                            let ob = {
                                data:item.label
                            };
                            if (item.label === null){
                                ob.defaultContent = "";
                            }
                            // if label is time sortable by time assign
                            if (item.label === "time" || item.label === "attendance_time"){
                                $scope.order = [[i,'desc']]
                            }

                            $scope.column_format.push(ob);
                            i++;
                        }

                        $timeout(function () {
                            let table = altair_datatables.dt_tableHeeshab({
                                "columns":$scope.column_format,
                                "columnDefs":$scope.columnDefs,
                                "order":$scope.order,
                                tableReady:function (table_instance) {
                                    let button_container = table_instance.buttons().container();
                                    let institute_select_element = $(".institute-select-template").html();

                                    if (institute_select_element !== undefined){
                                        button_container.append( institute_select_element );
                                        let event_types = Models.event_types($rootScope);
                                        Models.custom_selectize(".institute-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                Models.my_institutes($http,designation,institute_id,function (data) {
                                                    let i = 0;
                                                    for (let item of data){
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        if (i === 0){
                                                            instance.setValue(item.institute_id);
                                                        }
                                                        i++;
                                                    }
                                                })
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let current_date = moment().format("YYYY-MM-DD");
                                                Models.request_sender($http,"get","events_of_institutes",function (data) {

                                                    if (data.find_data !== undefined){
                                                        for(let item of data.find_data){
                                                            let status = item.event_type;
                                                            let filter = Models.filter_in_array(event_types,{label:status});
                                                            if (filter){
                                                                status = filter.name;
                                                            }
                                                            item.status = status;
                                                            table_instance.row.add(item).draw();
                                                        }
                                                    }
                                                },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'"]);
                                            }
                                        })
                                    }

                                }
                            });
                            if (table !== undefined){
                                let container = table.table_container;
                                table.on("draw",function (e,dt) {


                                });

                            }

                        });



                        $(window).resize();

                    }

                }
                else{
                    Models.notify("Data not found");
                }


            },["'admin'","'"+designation+"'"]);

        }

    })

}]);
//admin home 2
app.controller("admin_home_2",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                console.log(data);
                if (data.status){
                    let output = data.output;

                    let target_events = [
                        // {
                        //     name: "Total school",
                        //     total: output.total_institute.total,
                        //     data: output.total_institute.data,
                        //     link: "institute-list",
                        //     card_type: "peity_visitors",
                        // },
                        // {
                        //     name: "Total teacher",
                        //     total: output.total_teacher.total,
                        //     data: output.total_teacher.data,
                        //     link: "teacher-list",
                        //     card_type: "peity_visitors",
                        // },
                        // {
                        //     name: "Total student",
                        //     total: output.total_student.total,
                        //     data: output.total_student.data,
                        //     link: "student-list",
                        //     card_type: "peity_visitors",
                        // },

                        {
                            name: "Present",
                            total: output.total_student.total,
                            current: output.total_present.current,
                            link: "today-present-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Absent",
                            total: output.total_student.total,
                            current: output.total_absent.current,
                            link: "today-absent-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "In training",
                            total: output.total_student.total,
                            current: output.total_training.current,
                            link: "today-training-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Leave",
                            total: output.total_student.total,
                            current: output.total_leave.current,
                            link: "today-leave-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Late in",
                            total: output.total_student.total,
                            current: output.total_late.current,
                            link: "today-late-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Early gone",
                            total: output.total_student.total,
                            current: output.total_early.current,
                            link: "today-early-attendance/student",
                            card_type: "peity_orders",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let chartist = Models.chartist_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let template_output = Models.compiler(card_template,target_events);
                        $(".top-cards").html(template_output);
                        chartist.init({
                            columns:[
                                ['Present', output.total_present.current],
                                ['Absent', output.total_absent.current],
                                ['Training', output.total_training.current],
                                ['Leave', output.total_leave.current],
                            ]
                        },function (instance) {
                            $timeout(function () {
                                instance.resize();
                            },250);

                        });

                        altair_helpers.hierarchical_show();
                        peity_chart.init();

                        /// attendance report datatable

                        $scope.columnDefs = [
                            {
                                targets:-2,
                                render:function (data) {
                                    let time_for_event = data;
                                    if (time_for_event === "0000-00-00 00:00:00"){
                                        time_for_event = moment().format("YYYY-MM-DD");
                                    }
                                    return time_for_event;
                                },
                                orderable:false
                            }
                        ];
                        $scope.table_columns_format = [
                            {
                                label:"source_name",
                                name:"Teacher name"
                            },
                            {
                                label:"status",
                                name:"Status"
                            },
                            {
                                label:"time_for_event",
                                name:"Time for event"
                            },
                            {
                                label:"institute_name",
                                name:"Institute"
                            },


                        ];
                        // make table required format
                        $scope.column_format = [];
                        let i = 0;
                        for(let item of $scope.table_columns_format){
                            let ob = {
                                data:item.label
                            };
                            if (item.label === null){
                                ob.defaultContent = "";
                            }
                            // if label is time sortable by time assign
                            if (item.label === "time" || item.label === "attendance_time"){
                                $scope.order = [[i,'desc']]
                            }

                            $scope.column_format.push(ob);
                            i++;
                        }

                        $timeout(function () {
                            let table = altair_datatables.dt_tableHeeshab({
                                "columns":$scope.column_format,
                                "columnDefs":$scope.columnDefs,
                                "order":$scope.order,
                                tableReady:function (table_instance) {
                                    let button_container = table_instance.buttons().container();
                                    let institute_select_element = $(".institute-select-template").html();

                                    if (institute_select_element !== undefined){
                                        button_container.append( institute_select_element );
                                        let event_types = Models.event_types($rootScope);
                                        Models.custom_selectize(".institute-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                Models.my_institutes($http,designation,institute_id,function (data) {
                                                    let i = 0;
                                                    for (let item of data){
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        if (i === 0){
                                                            instance.setValue(item.institute_id);
                                                        }
                                                        i++;
                                                    }
                                                })
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let current_date = moment().format("YYYY-MM-DD");
                                                Models.request_sender($http,"get","events_of_institutes",function (data) {

                                                    if (data.find_data !== undefined){
                                                        for(let item of data.find_data){
                                                            let status = item.event_type;
                                                            let filter = Models.filter_in_array(event_types,{label:status});
                                                            if (filter){
                                                                status = filter.name;
                                                            }
                                                            item.status = status;
                                                            table_instance.row.add(item).draw();
                                                        }
                                                    }
                                                },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'","'student'"]);
                                            }
                                        })
                                    }

                                }
                            });


                        });



                        $(window).resize();

                    }

                }
                else{
                    Models.notify("Data not found");
                }


            },["'admin_2'","'"+designation+"'"]);

        }

    })

}]);

app.controller("system_admin_home",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                if (data.status){
                    let output = data.output;
                    let target_events = [
                        {
                            name: "Total school",
                            total: output.total_institute.total,
                            data: output.total_institute.data,
                            link: "institutes",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total teacher",
                            total: output.total_teacher.total,
                            data: output.total_teacher.data,
                            link: "teacher-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total student",
                            total: output.total_student.total,
                            data: output.total_student.data,
                            link: "student-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total admin",
                            total: output.total_admin.total,
                            data: output.total_admin.data,
                            link: "handlers/admin",
                            card_type: "peity_visitors",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let output = Models.compiler(card_template,target_events);
                        $(".top-cards").html(output);
                        altair_helpers.hierarchical_show();
                        peity_chart.init();
                        $(window).resize();

                    }
                }
                else{
                    Models.notify("Data not found");
                }


            },["'system_admin'","'"+designation+"'"]);





        }

    })

}]);

//head teacher home
app.controller("head_teacher_home",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                console.log(data);
                if (data.status){
                    let output = data.output;

                    let target_events = [
                        {
                            name: "Total school",
                            total: output.total_institute.total,
                            data: output.total_institute.data,
                            link: "institute-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total teacher",
                            total: output.total_teacher.total,
                            data: output.total_teacher.data,
                            link: "teacher-list",
                            card_type: "peity_visitors",
                        },
                        {
                            name: "Total student",
                            total: output.total_student.total,
                            data: output.total_student.data,
                            link: "student-list",
                            card_type: "peity_visitors",
                        },

                        {
                            name: "Present",
                            total: output.total_present.total,
                            current: output.total_present.current,
                            link: "today-present-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Absent",
                            total: output.total_absent.total,
                            current: output.total_absent.current,
                            link: "today-absent-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "In training",
                            total: output.total_training.total,
                            current: output.total_training.current,
                            link: "today-training-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Leave",
                            total: output.total_leave.total,
                            current: output.total_leave.current,
                            link: "today-leave-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Late in",
                            total: output.total_late.total,
                            current: output.total_late.current,
                            link: "today-late-attendance/teacher",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Early gone",
                            total: output.total_early.total,
                            current: output.total_early.current,
                            link: "today-early-attendance/teacher",
                            card_type: "peity_orders",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let chartist = Models.chartist_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let template_output = Models.compiler(card_template,target_events);

                        $(".top-cards").html(template_output);
                        chartist.init({
                            columns:[
                                ['Present', output.total_present.current],
                                ['Absent', output.total_absent.current],
                                ['Training', output.total_training.current],
                                ['Leave', output.total_leave.current],
                            ]
                        },function (instance) {
                            $timeout(function () {
                                instance.resize();
                            },250);

                        });

                        altair_helpers.hierarchical_show();
                        peity_chart.init();

                        /// attendance report datatable

                        $scope.columnDefs = [
                            {
                                targets:-2,
                                render:function (data) {
                                    let time_for_event = data;
                                    if (time_for_event === "0000-00-00 00:00:00"){
                                        time_for_event = moment().format("YYYY-MM-DD");
                                    }
                                    return time_for_event;
                                },
                                orderable:false
                            }
                        ];
                        $scope.table_columns_format = [
                            {
                                label:"source_name",
                                name:"Teacher name"
                            },
                            {
                                label:"status",
                                name:"Status"
                            },
                            {
                                label:"time_for_event",
                                name:"Time for event"
                            },
                            {
                                label:"institute_name",
                                name:"Institute"
                            },


                        ];
                        // make table required format
                        $scope.column_format = [];
                        let i = 0;
                        for(let item of $scope.table_columns_format){
                            let ob = {
                                data:item.label
                            };
                            if (item.label === null){
                                ob.defaultContent = "";
                            }
                            // if label is time sortable by time assign
                            if (item.label === "time" || item.label === "attendance_time"){
                                $scope.order = [[i,'desc']]
                            }

                            $scope.column_format.push(ob);
                            i++;
                        }

                        $timeout(function () {
                            let table = altair_datatables.dt_tableHeeshab({
                                "columns":$scope.column_format,
                                "columnDefs":$scope.columnDefs,
                                "order":$scope.order,
                                tableReady:function (table_instance) {
                                    let button_container = table_instance.buttons().container();
                                    let institute_select_element = $(".institute-select-template").html();

                                    if (institute_select_element !== undefined){
                                        button_container.append( institute_select_element );
                                        let event_types = Models.event_types($rootScope);
                                        Models.custom_selectize(".institute-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                Models.my_institutes($http,designation,institute_id,function (data) {
                                                    let i = 0;
                                                    for (let item of data){
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        if (i === 0){
                                                            instance.setValue(item.institute_id);
                                                        }
                                                        i++;
                                                    }
                                                })
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let current_date = moment().format("YYYY-MM-DD");
                                                Models.request_sender($http,"get","events_of_institutes",function (data) {
                                                    console.log(data);
                                                    if (data.find_data !== undefined){
                                                        for(let item of data.find_data){
                                                            let status = item.event_type;
                                                            let filter = Models.filter_in_array(event_types,{label:status});
                                                            if (filter){
                                                                status = filter.name;
                                                            }
                                                            item.status = status;
                                                            table_instance.row.add(item).draw();
                                                        }
                                                    }
                                                },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'"]);
                                            }
                                        })
                                    }

                                }
                            });
                            if (table !== undefined){
                                let container = table.table_container;
                                table.on("draw",function (e,dt) {


                                });

                            }

                        });



                        $(window).resize();

                    }

                }
                else{
                    Models.notify("Data not found");
                }


            },["'head_teacher'","'"+designation+"'","'"+institute_id+"'"]);

        }

    })

}]);
//head teacher home 2 for student dashboard
app.controller("head_teacher_home_2",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            Models.request_sender($http,"get","dashboard_info",function (data) {
                console.log(data);
                if (data.status){
                    let output = data.output;

                    let target_events = [
                        {
                            name: "Present",
                            total: output.total_student.total,
                            current: output.total_present.current,
                            link: "today-present-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Absent",
                            total: output.total_student.total,
                            current: output.total_absent.current,
                            link: "today-absent-attendance/student",
                            card_type: "peity_orders",
                        },

                        // {
                        //     name: "In training",
                        //     total: output.total_student.total,
                        //     current: output.total_training.current,
                        //     link: "today-training-attendance",
                        //     card_type: "peity_orders",
                        // },
                        {
                            name: "Leave",
                            total: output.total_student.total,
                            current: output.total_leave.current,
                            link: "today-leave-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Late in",
                            total: output.total_student.total,
                            current: output.total_late.current,
                            link: "today-late-attendance/student",
                            card_type: "peity_orders",
                        },
                        {
                            name: "Early gone",
                            total: output.total_student.total,
                            current: output.total_early.current,
                            link: "today-early-attendance/student",
                            card_type: "peity_orders",
                        },

                    ];
                    let peity_chart = Models.peity_charts_init();
                    let chartist = Models.chartist_init();
                    let card_template = $(".info-card").html();
                    if (card_template !== undefined){
                        let template_output = Models.compiler(card_template,target_events);

                        $(".top-cards").html(template_output);
                        chartist.init({
                            columns:[
                                ['Present', output.total_present.current],
                                ['Absent', output.total_absent.current],
                                ['Training', output.total_training.current],
                                ['Leave', output.total_leave.current],
                            ]
                        },function (instance) {
                            $timeout(function () {
                                instance.resize();
                            },250);

                        });

                        altair_helpers.hierarchical_show();
                        peity_chart.init();

                        /// attendance report datatable

                        $scope.columnDefs = [
                            {
                                targets:-2,
                                render:function (data) {
                                    let time_for_event = data;
                                    if (time_for_event === "0000-00-00 00:00:00"){
                                        time_for_event = moment().format("YYYY-MM-DD");
                                    }
                                    return time_for_event;
                                },
                                orderable:false
                            }
                        ];
                        $scope.table_columns_format = [
                            {
                                label:"source_name",
                                name:"Student name"
                            },
                            {
                                label:"status",
                                name:"Status"
                            },
                            {
                                label:"time_for_event",
                                name:"Time for event"
                            },
                            {
                                label:"institute_name",
                                name:"Institute"
                            },


                        ];
                        // make table required format
                        $scope.column_format = [];
                        let i = 0;
                        for(let item of $scope.table_columns_format){
                            let ob = {
                                data:item.label
                            };
                            if (item.label === null){
                                ob.defaultContent = "";
                            }
                            // if label is time sortable by time assign
                            if (item.label === "time" || item.label === "attendance_time"){
                                $scope.order = [[i,'desc']]
                            }

                            $scope.column_format.push(ob);
                            i++;
                        }

                        $timeout(function () {
                            let table = altair_datatables.dt_tableHeeshab({
                                "columns":$scope.column_format,
                                "columnDefs":$scope.columnDefs,
                                "order":$scope.order,
                                tableReady:function (table_instance) {
                                    let button_container = table_instance.buttons().container();
                                    let institute_select_element = $(".institute-select-template").html();

                                    if (institute_select_element !== undefined){
                                        button_container.append( institute_select_element );
                                        let event_types = Models.event_types($rootScope);
                                        Models.custom_selectize(".institute-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                Models.my_institutes($http,designation,institute_id,function (data) {
                                                    let i = 0;
                                                    for (let item of data){
                                                        item.label = item.name;
                                                        item.value = item.institute_id;
                                                        instance.addOption(item);
                                                        if (i === 0){
                                                            instance.setValue(item.institute_id);
                                                        }
                                                        i++;
                                                    }
                                                })
                                            },
                                            onChange:function (value) {
                                                table_instance.clear().draw();
                                                let current_date = moment().format("YYYY-MM-DD");
                                                Models.request_sender($http,"get","events_of_institutes",function (data) {
                                                    console.log(data);
                                                    if (data.find_data !== undefined){
                                                        for(let item of data.find_data){
                                                            let status = item.event_type;
                                                            let filter = Models.filter_in_array(event_types,{label:status});
                                                            if (filter){
                                                                status = filter.name;
                                                            }
                                                            item.status = status;
                                                            table_instance.row.add(item).draw();
                                                        }
                                                    }
                                                },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'","'student'"]);
                                            }
                                        })
                                    }

                                }
                            });

                        });
                        $(window).resize();

                    }

                }
                else{
                    Models.notify("Data not found");
                }


            },["'head_teacher_2'","'"+designation+"'","'"+institute_id+"'"]);

        }

    })

}]);

//student home
app.controller("student_home",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {

    $scope.user_name = null;
    $scope.source = $routeParams.source;
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
                if (source_user_id){
                    source_user_id = "'"+source_user_id+"'";
                }
                let logged_user = Models.logged_user($rootScope);
                let designation = logged_user['designation'];
                let institute_id = logged_user['institute_id'];
                if (Models.logged_user_type($rootScope) !== "student"){
                    if (source_info !== undefined){
                        $scope.user_name = source_info.name;
                    }
                }
                Models.request_sender($http,"get","dashboard_info",function (data) {
                    console.log(data);
                    if (data.status){
                        let output = data.output;

                        let target_events = [

                            {
                                name: "Present",
                                total: output.total_present.total,
                                current: output.total_present.current,
                                link: "today-present-attendance/student/"+$scope.source,
                                card_type: "peity_orders",
                            },
                            {
                                name: "Absent",
                                total: output.total_absent.total,
                                current: output.total_absent.current,
                                link: "today-absent-attendance/student/"+$scope.source,
                                card_type: "peity_orders",
                            },

                            // {
                            //     name: "In training",
                            //     total: output.total_training.total,
                            //     current: output.total_training.current,
                            //     link: "today-training-attendance",
                            //     card_type: "peity_orders",
                            // },
                            // {
                            //     name: "Leave",
                            //     total: output.total_leave.total,
                            //     current: output.total_leave.current,
                            //     link: "today-leave-attendance",
                            //     card_type: "peity_orders",
                            // },
                            {
                                name: "Late in",
                                total: output.total_late.total,
                                current: output.total_late.current,
                                link: "today-late-attendance/student/"+$scope.source,
                                card_type: "peity_orders",
                            },
                            {
                                name: "Early gone",
                                total: output.total_early.total,
                                current: output.total_early.current,
                                link: "today-early-attendance/student/"+$scope.source,
                                card_type: "peity_orders",
                            },

                        ];

                        $scope.today_attendance_status = output.today_attendance_status;
                        let peity_chart = Models.peity_charts_init();
                        let chartist = Models.chartist_init();
                        let card_template = $(".info-card").html();
                        if (card_template !== undefined){
                            let template_output = Models.compiler(card_template,target_events);

                            $(".top-cards").html(template_output);
                            chartist.init({
                                columns:[
                                    ['Present', output.total_present.current],
                                    ['Absent', output.total_absent.current],
                                    // ['Training', output.total_training.current],
                                    // ['Leave', output.total_leave.current],
                                ]
                            },function (instance) {
                                $timeout(function () {
                                    instance.resize();
                                },250);

                            });

                            altair_helpers.hierarchical_show();
                            peity_chart.init();

                            /// attendance report datatable

                            $scope.columnDefs = [
                                {
                                    targets:-2,
                                    render:function (data) {
                                        let time_for_event = data;
                                        if (time_for_event === "0000-00-00 00:00:00"){
                                            time_for_event = moment().format("YYYY-MM-DD");
                                        }
                                        return time_for_event;
                                    },
                                    orderable:false
                                }
                            ];
                            $scope.table_columns_format = [
                                {
                                    label:"source_name",
                                    name:"Teacher name"
                                },
                                {
                                    label:"status",
                                    name:"Status"
                                },
                                {
                                    label:"time_for_event",
                                    name:"Time for event"
                                },
                                {
                                    label:"institute_name",
                                    name:"Institute"
                                },


                            ];
                            // make table required format
                            $scope.column_format = [];
                            let i = 0;
                            for(let item of $scope.table_columns_format){
                                let ob = {
                                    data:item.label
                                };
                                if (item.label === null){
                                    ob.defaultContent = "";
                                }
                                // if label is time sortable by time assign
                                if (item.label === "time" || item.label === "attendance_time"){
                                    $scope.order = [[i,'desc']]
                                }

                                $scope.column_format.push(ob);
                                i++;
                            }

                            $timeout(function () {
                                let table = altair_datatables.dt_tableHeeshab({
                                    "columns":$scope.column_format,
                                    "columnDefs":$scope.columnDefs,
                                    "order":$scope.order,
                                    tableReady:function (table_instance) {
                                        let button_container = table_instance.buttons().container();
                                        let institute_select_element = $(".institute-select-template").html();

                                        if (institute_select_element !== undefined){
                                            button_container.append( institute_select_element );
                                            let event_types = Models.event_types($rootScope);
                                            Models.custom_selectize(".institute-selectize",{
                                                onInitialize:function () {
                                                    let instance = this;
                                                    Models.my_institutes($http,designation,institute_id,function (data) {
                                                        let i = 0;
                                                        for (let item of data){
                                                            item.label = item.name;
                                                            item.value = item.institute_id;
                                                            instance.addOption(item);
                                                            if (i === 0){
                                                                instance.setValue(item.institute_id);
                                                            }
                                                            i++;
                                                        }
                                                    })
                                                },
                                                onChange:function (value) {
                                                    table_instance.clear().draw();
                                                    let current_date = moment().format("YYYY-MM-DD");
                                                    Models.request_sender($http,"get","events_of_institutes",function (data) {
                                                        console.log(data);
                                                        if (data.find_data !== undefined){
                                                            for(let item of data.find_data){
                                                                let status = item.event_type;
                                                                let filter = Models.filter_in_array(event_types,{label:status});
                                                                if (filter){
                                                                    status = filter.name;
                                                                }
                                                                item.status = status;
                                                                table_instance.row.add(item).draw();
                                                            }
                                                        }
                                                    },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'"]);
                                                }
                                            })
                                        }

                                    }
                                });
                                if (table !== undefined){
                                    let container = table.table_container;
                                    table.on("draw",function (e,dt) {


                                    });

                                }

                            });



                            $(window).resize();

                        }

                    }
                    else{
                        Models.notify("Data not found");
                    }


                },["'student'","'"+designation+"'","'"+institute_id+"'",source_user_id]);

            })

        }

    })

}]);
//teacher home
app.controller("teacher_home",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {

    $scope.user_name = null;
    $scope.source = $routeParams.source;
    Models.get_basic_info($http,$rootScope,function (data) {
        let logged_user_id = Models.logged_user_id($rootScope);
        if (!logged_user_id){
            $location.path('/login');
            $rootScope.looged = 0;
        }
        else{
            Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
                if (source_user_id){
                    source_user_id = "'"+source_user_id+"'";
                }
                let logged_user = Models.logged_user($rootScope);
                let designation = logged_user['designation'];
                let institute_id = logged_user['institute_id'];
                if (Models.logged_user_type($rootScope) !== "teacher"){
                    if (source_info !== undefined){
                        $scope.user_name = source_info.name;
                    }
                }
                Models.request_sender($http,"get","dashboard_info",function (data) {
                    console.log(data);
                    if (data.status){
                        let output = data.output;

                        let target_events = [

                            {
                                name: "Present",
                                total: output.total_present.total,
                                current: output.total_present.current,
                                link: "today-present-attendance/teacher/"+$scope.source,
                                card_type: "peity_orders",
                            },
                            {
                                name: "Absent",
                                total: output.total_absent.total,
                                current: output.total_absent.current,
                                link: "today-absent-attendance/teacher/"+$scope.source,
                                card_type: "peity_orders",
                            },

                            // {
                            //     name: "In training",
                            //     total: output.total_training.total,
                            //     current: output.total_training.current,
                            //     link: "today-training-attendance",
                            //     card_type: "peity_orders",
                            // },
                            // {
                            //     name: "Leave",
                            //     total: output.total_leave.total,
                            //     current: output.total_leave.current,
                            //     link: "today-leave-attendance",
                            //     card_type: "peity_orders",
                            // },
                            {
                                name: "Late in",
                                total: output.total_late.total,
                                current: output.total_late.current,
                                link: "today-late-attendance/teacher/"+$scope.source,
                                card_type: "peity_orders",
                            },
                            {
                                name: "Early gone",
                                total: output.total_early.total,
                                current: output.total_early.current,
                                link: "today-early-attendance/teacher/"+$scope.source,
                                card_type: "peity_orders",
                            },

                        ];

                        $scope.today_attendance_status = output.today_attendance_status;
                        let peity_chart = Models.peity_charts_init();
                        let chartist = Models.chartist_init();
                        let card_template = $(".info-card").html();
                        if (card_template !== undefined){
                            let template_output = Models.compiler(card_template,target_events);

                            $(".top-cards").html(template_output);
                            chartist.init({
                                columns:[
                                    ['Present', output.total_present.current],
                                    ['Absent', output.total_absent.current],
                                    // ['Training', output.total_training.current],
                                    // ['Leave', output.total_leave.current],
                                ]
                            },function (instance) {
                                $timeout(function () {
                                    instance.resize();
                                },250);

                            });

                            altair_helpers.hierarchical_show();
                            peity_chart.init();

                            /// attendance report datatable

                            $scope.columnDefs = [
                                {
                                    targets:-2,
                                    render:function (data) {
                                        let time_for_event = data;
                                        if (time_for_event === "0000-00-00 00:00:00"){
                                            time_for_event = moment().format("YYYY-MM-DD");
                                        }
                                        return time_for_event;
                                    },
                                    orderable:false
                                }
                            ];
                            $scope.table_columns_format = [
                                {
                                    label:"source_name",
                                    name:"Teacher name"
                                },
                                {
                                    label:"status",
                                    name:"Status"
                                },
                                {
                                    label:"time_for_event",
                                    name:"Time for event"
                                },
                                {
                                    label:"institute_name",
                                    name:"Institute"
                                },


                            ];
                            // make table required format
                            $scope.column_format = [];
                            let i = 0;
                            for(let item of $scope.table_columns_format){
                                let ob = {
                                    data:item.label
                                };
                                if (item.label === null){
                                    ob.defaultContent = "";
                                }
                                // if label is time sortable by time assign
                                if (item.label === "time" || item.label === "attendance_time"){
                                    $scope.order = [[i,'desc']]
                                }

                                $scope.column_format.push(ob);
                                i++;
                            }

                            $timeout(function () {
                                let table = altair_datatables.dt_tableHeeshab({
                                    "columns":$scope.column_format,
                                    "columnDefs":$scope.columnDefs,
                                    "order":$scope.order,
                                    tableReady:function (table_instance) {
                                        let button_container = table_instance.buttons().container();
                                        let institute_select_element = $(".institute-select-template").html();

                                        if (institute_select_element !== undefined){
                                            button_container.append( institute_select_element );
                                            let event_types = Models.event_types($rootScope);
                                            Models.custom_selectize(".institute-selectize",{
                                                onInitialize:function () {
                                                    let instance = this;
                                                    Models.my_institutes($http,designation,institute_id,function (data) {
                                                        let i = 0;
                                                        for (let item of data){
                                                            item.label = item.name;
                                                            item.value = item.institute_id;
                                                            instance.addOption(item);
                                                            if (i === 0){
                                                                instance.setValue(item.institute_id);
                                                            }
                                                            i++;
                                                        }
                                                    })
                                                },
                                                onChange:function (value) {
                                                    table_instance.clear().draw();
                                                    let current_date = moment().format("YYYY-MM-DD");
                                                    Models.request_sender($http,"get","events_of_institutes",function (data) {
                                                        console.log(data);
                                                        if (data.find_data !== undefined){
                                                            for(let item of data.find_data){
                                                                let status = item.event_type;
                                                                let filter = Models.filter_in_array(event_types,{label:status});
                                                                if (filter){
                                                                    status = filter.name;
                                                                }
                                                                item.status = status;
                                                                table_instance.row.add(item).draw();
                                                            }
                                                        }
                                                    },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'late,early'"]);
                                                }
                                            })
                                        }

                                    }
                                });
                                if (table !== undefined){
                                    let container = table.table_container;
                                    table.on("draw",function (e,dt) {


                                    });

                                }

                            });



                            $(window).resize();

                        }

                    }
                    else{
                        Models.notify("Data not found");
                    }


                },["'teacher'","'"+designation+"'","'"+institute_id+"'",source_user_id]);

            })

        }

    })

}]);


// Shortcut important list like items,customer,invoices in secondary sidebar
app.controller("shortcut_important_list",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    $scope.shortcut_list_on = function () {
        altair_secondary_sidebar.init();
    }
}]);
// Today attendance
app.controller("today_attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.table_switch = 0;
    let user_type = $routeParams.user_type;
    Models.uri_source_info($http, $routeParams, function (source_user_id, source_info) {
        if (source_user_id) {
            $scope.page_title_plus = " of " + source_info.name;
        }
        $scope.source_user_id = source_user_id;
        $scope.table_switch = 1;
    });
}]);

// Shortcut important list like items,customer,invoices in secondary sidebar
app.controller("today_absent_attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let user_type = $routeParams.user_type;
    let source = $routeParams.source;
    $scope.page_title = "Today's absent attendance";
    Models.get_basic_info($http,$rootScope,function () {
        Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
            if (source_user_id) {
                source_user_id = "'" + source_user_id + "'";
                $scope.page_title += " of "+source_info.name;
            }

            /// attendance report datatable
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user['designation'];
            let institute_id = logged_user['institute_id'];
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"status",
                    name:"Status"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute"
                },


            ];
            // make table required format
            $scope.column_format = [];
            let i = 0;
            for(let item of $scope.table_columns_format){
                let ob = {
                    data:item.label
                };
                if (item.label === null){
                    ob.defaultContent = "";
                }
                // if label is time sortable by time assign
                if (item.label === "time" || item.label === "attendance_time"){
                    $scope.order = [[i,'desc']]
                }

                $scope.column_format.push(ob);
                i++;
            }

            $timeout(function () {
                let table = altair_datatables.dt_tableHeeshab({
                    "columns":$scope.column_format,
                    "columnDefs":$scope.columnDefs,
                    "order":$scope.order,
                    tableReady:function (table_instance) {
                        let button_container = table_instance.buttons().container();
                        let institute_select_element = $(".institute-select-template").html();

                        if (institute_select_element !== undefined){
                            button_container.append( institute_select_element );
                            let event_types = Models.event_types($rootScope);
                            Models.custom_selectize(".institute-selectize",{
                                onInitialize:function () {
                                    let instance = this;
                                    Models.my_institutes($http,designation,institute_id,function (data) {
                                        let i = 0;
                                        for (let item of data){
                                            if (source_user_id){
                                                if (item.institute_id === source_info.institute_id){
                                                    item.label = item.name;
                                                    item.value = item.institute_id;
                                                    instance.addOption(item);
                                                    instance.setValue(item.institute_id);
                                                }

                                            }
                                            else{
                                                item.label = item.name;
                                                item.value = item.institute_id;
                                                instance.addOption(item);
                                                if (i === 0){
                                                    instance.setValue(item.institute_id);
                                                }
                                            }
                                            i++;

                                        }

                                    })
                                },
                                onChange:function (value) {
                                    table_instance.clear().draw();
                                    let current_date = moment().format("YYYY-MM-DD");
                                    Models.request_sender($http,"get","absent_attendance_data",function (data) {
                                        if (data.find_data !== undefined){
                                            for(let item of data.find_data){
                                                let status = item.event_type;
                                                let filter = Models.filter_in_array(event_types,{label:status});
                                                if (filter){
                                                    status = filter.name;
                                                }
                                                item.status = status;
                                                table_instance.row.add(item).draw();
                                            }
                                        }
                                    },["'"+value+"'","'"+current_date+"'","'"+current_date+"'","'"+user_type+"'",source_user_id]);
                                }
                            })
                        }

                    }
                });
                if (table !== undefined){
                    let container = table.table_container;
                    table.on("draw",function (e,dt) {


                    });

                }

            });

        });

    })
}]);

// Monthly attendance report
app.controller("monthly_attendance_report",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    /// attendance report datatable
    let category = $routeParams.category;
    let page_name = Models.page_name($location);
    let event_user_type = "teacher";
    if (page_name === "student-monthly-attendance-report"){
        event_user_type = "student";
    }
    if (category !== undefined){
        event_user_type = "student";
    }


    $scope.user_name = null;
    let years = [];
    let current_year = moment().format("YYYY");
    let current_month = moment().format("MM");
    let days_in_month = moment().daysInMonth();
    for (let i = 2018;i<=current_year;i++){
        years.push({
            label:i,
            value:i
        });
    }
    let months = [
        {
            label:"January",
            value:"01",
        },
        {
            label:"February",
            value:"02",
        },
        {
            label:"March",
            value:"03",
        },
        {
            label:"April",
            value:"04",
        },
        {
            label:"May",
            value:"05",
        },
        {
            label:"June",
            value:"06",
        },
        {
            label:"July",
            value:"07",
        },
        {
            label:"August",
            value:"08",
        },
        {
            label:"September",
            value:"09",
        },
        {
            label:"October",
            value:"10",
        },
        {
            label:"November",
            value:"11",
        },
        {
            label:"December",
            value:"12",
        },

    ];
    Models.get_basic_info($http,$rootScope,function () {
        Models.uri_source_info($http,$routeParams,function (source_user_id,source_info) {
            if (source_user_id){
                $scope.user_name = source_info.name;
                source_user_id = "'"+source_user_id+"'";

            }
            // console.log(source_user_id);

            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".item-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // }
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Teacher name"
                },

                // {
                //     label:"institute_name",
                //     name:"Institute"
                // },

            ];
            let manual_column_length = $scope.table_columns_format.length;

            for (let i = 1;i<=31;i++){
                $scope.table_columns_format.push({
                    label:i,
                    name:i
                });
                let index = Models.index_number($scope.table_columns_format,"label",i);
                if (index !== -1){
                    $scope.columnDefs.push({
                        targets:index,
                        orderable:false
                    });
                }
            }
            $scope.table_columns_format.push({
                label:"total_symbols",
                name:"Total"
            });

            // make table required format
            $scope.column_format = [];
            let i = 0;
            for(let item of $scope.table_columns_format){
                let ob = {
                    data:item.label
                };
                if (item.label === null){
                    ob.defaultContent = "";
                }
                // if label is time sortable by time assign
                if (item.label === "time" || item.label === "attendance_time"){
                    $scope.order = [[i,'desc']]
                }

                $scope.column_format.push(ob);
                i++;
            }

            let table;
            $scope.table_load = function(table_instance,institute_id,year,month){
                Models.request_sender($http,"get","monthly_events_of_institute",function (data) {
                    $scope.institute_info = data.institute_info;
                    $scope.date_string = data.date_string;
                    if (data.find_data !== undefined){
                        let table_headers = data.headers;
                        let days_of_month = Number(data.days_of_month);
                        // console.log(days_of_month);
                        //// visible last 5 columns
                        for (let i = 1;i <= 5; i++){
                            table_instance.column(Models.negative_number(i)).visible(true);
                        }
                        if (days_of_month < 31){
                            let extra_days = 31 - days_of_month;
                            let j = 0;
                            for(let item of data.find_data){
                                for (let i = 1; i <= extra_days;i++){
                                    let key = i+days_of_month;
                                    let column_number = manual_column_length + key - 1;
                                    item[key] = "";
                                    table_instance.column(column_number).visible(false);
                                }
                                data.find_data[j] = item;
                                j++;
                            }
                        }


                        for(let item of data.find_data){
                            table_instance.row.add(item).draw();
                        }
                    }
                },["'"+institute_id+"'","'"+year+"'","'"+month+"'","'"+event_user_type+"'",source_user_id]);
            };

            $scope.table_initiate = function(headers=null){
                if (headers){
                    $scope.table_columns_format = headers;
                }
                $timeout(function () {
                    table = altair_datatables.dt_tableHeeshab({
                        "columns":$scope.column_format,
                        "columnDefs":$scope.columnDefs,
                        "order":$scope.order,
                        tableReady:function (table_instance) {
                            let button_container = table_instance.buttons().container();
                            let institute_select_element = $(".option-select-template").html();

                            if (institute_select_element !== undefined){
                                button_container.append( institute_select_element );
                                let event_types = Models.event_types($rootScope);
                                let logged_user = Models.logged_user($rootScope);
                                let designation = logged_user.designation;
                                let institute_id = logged_user.institute_id;
                                Models.custom_selectize(".institute-selectize",{
                                    onInitialize:function () {
                                        let instance = this;
                                        Models.my_institutes($http,designation,institute_id,function (data) {
                                            let i = 0;
                                            for (let item of data){
                                                item.label = item.name;
                                                item.value = item.institute_id;
                                                instance.addOption(item);
                                                if (source_info !== undefined){
                                                    instance.setValue(source_info.institute_id);
                                                }
                                                else{
                                                    if (i === 0){
                                                        instance.setValue(item.institute_id);
                                                    }
                                                }

                                                i++;
                                            }
                                        });
                                        if (page_name === "student-monthly-attendance-report" || page_name === "teacher-monthly-attendance-report") {
                                            $(".institute-selectize").addClass("uk-hidden");
                                        }

                                    },
                                    onChange:function (value) {
                                        table_instance.clear().draw();
                                        if (value !== ""){
                                            let year = $("select.year-selectize").val();
                                            if (year === ""){
                                                year = current_year;
                                            }
                                            let month = $("select.month-selectize").val();
                                            if (month === ""){
                                                month = current_month;
                                            }

                                            $scope.table_load(table_instance,value,year,month);
                                        }

                                    }
                                });
                                Models.custom_selectize(".year-selectize",{
                                    options:years,
                                    onInitialize:function () {
                                        this.setValue(current_year,"silent");
                                    },
                                    onChange:function (value) {
                                        table_instance.clear().draw();
                                        let institute_id = $('select.institute-selectize').val();
                                        let month = $("select.month-selectize").val();
                                        if (institute_id !== "" && value !== "" && month !== ""){
                                            $scope.table_load(table_instance,institute_id,value,month);
                                        }

                                    }
                                });
                                Models.custom_selectize(".month-selectize",{
                                    options:months,
                                    onInitialize:function () {
                                        this.setValue(current_month,"silent");
                                    },
                                    onChange:function (value) {
                                        table_instance.clear().draw();
                                        let institute_id = $('select.institute-selectize').val();
                                        let year = $("select.year-selectize").val();
                                        if (institute_id !== "" && value !== "" && year !== ""){
                                            $scope.table_load(table_instance,institute_id,year,value);
                                        }

                                    }
                                });

                            }

                        }
                    });


                });
            };
            $scope.table_initiate();
        });


    });

}]);

// Invoice bill reciept
app.controller("document",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {

    Models.get_basic_info($http,$rootScope,function () {
        $timeout(function () {
            $scope.root = $rootScope;
            // $rootScope.document_save_items = [];
            $scope.route_params = $routeParams;
            $scope.doc_type = 'application';
            let current_date = moment().format("YYYY-MM-DD");
            $scope.current_date = current_date;

            // console.log(current_date);
            let page_name = Models.page_name($location);
            if (page_name !== ""){
                $scope.doc_type = page_name;
            }
            ///model for  documents
            $scope.document_manager = {
                training:0,
                leave:0,
            };
            $scope.document_add_permission = 0;
            $scope.default_document_sub_type = "leave";
            // $scope.default_document_sub_type = "sale_return";
            // if ($scope.doc_type === "bill"){
            //     $scope.default_document_sub_type = "cash_bill";
            //     // $scope.default_document_sub_type = "transfer_bill";
            // }

            let url_document_sub_type = $routeParams.document_sub_type;
            if (url_document_sub_type !== undefined){
                $scope.default_document_sub_type = url_document_sub_type;
            }
            $scope.document_switch_on = function(type){
                for (let x in $scope.document_manager){
                    $scope.document_manager[x] = 0;
                }
                $scope.document_manager[type] = 1;
            };
            let without_add = [
                // "cash_memo",
            ];
            $scope.document_add_permission_checker = function(document_sub_type){
                let index = without_add.indexOf(document_sub_type);
                if (index === -1){
                    $scope.document_add_permission = 1;
                }
                else{
                    $scope.document_add_permission = 0;
                }

            };
            $scope.document_add_permission_checker($scope.default_document_sub_type);

            $scope.document_type_selector = function () {
                // $scope.document_type_option_name = Models.document_type_option_name($scope.doc_type,$scope.default_document_sub_type,$rootScope);
                let basic_info = $rootScope.basic_info;
                Models.custom_selectize(".document-sub-types",{
                    onInitialize:function () {
                        if (basic_info !== undefined){
                            let document_types = basic_info.document_types;
                            if (document_types !== undefined){
                                let doc_type_info = Models.custom_filter(document_types,"label",$scope.doc_type);
                                if (doc_type_info !== undefined){
                                    let doc_type_options = doc_type_info.options;
                                    if (doc_type_options !== undefined){
                                        let i = 0;
                                        for (let x of doc_type_options){

                                            let select_object = {
                                                label:x.name,
                                                value:x.label
                                            };
                                            this.addOption(select_object);
                                            if (x.label === $scope.default_document_sub_type){
                                                this.setValue(x.label,"silent");
                                                $scope.document_sub_type = x.label;
                                                $scope.document_type_option_name = x.name;
                                                $("#invoice-filtering").attr("placeholder","Find "+x.name+"...");

                                            }
                                            i++;
                                        }
                                        this.removeOption("sale_delete");
                                    }
                                    else{
                                        Models.notify("Document type options undefined");
                                    }
                                }
                                else{
                                    Models.notify("Document type undefined");
                                }


                            }

                        }
                    },
                    onChange:function (value) {
                        $scope.$apply(function () {
                            $scope.document_sub_type = value;
                        });

                        let options = Models.get_list_from_selectize(this);
                        let filter = Models.custom_filter(options,"value",value);
                        // console.log(filter);
                        if (filter !== undefined){
                            $scope.$apply(function () {
                                $scope.document_switch_on(value);
                                $scope.document_add_permission_checker(value);
                                $scope.document_type_option_name = filter.label;
                                $("#invoice-filtering").attr("placeholder","Find "+filter.label+"...");
                            });
                        }
                    }
                });

                /// new line maker by ctrl+enter button
                $(document).on("keyup keydown","#document_form .service-item input",function (e) {
                    let that = this;
                    let keyCode = e.keyCode || e.which;
                    /// new service add by ctrl+enter
                    if (e.type === "keyup"){
                        if (e.ctrlKey && e.keyCode === 13) {
                            // Ctrl-Enter pressed
                            let empty = Models.empty_service_item();
                            if(!empty){
                                $("#document_form_append_service_btn").click();
                            }

                        }
                    }
                    // enter button submit off
                    if (keyCode === 13) {
                        e.preventDefault();
                        return false;
                    }
                });
                // $(document).on("keyup keydown","body",function (e) {
                //     let keyCode = e.keyCode || e.which;
                //     if (e.type === "keydown"){
                //         if (e.ctrlKey && e.keyCode === 83) {
                //             e.preventDefault();
                //             // Ctrl-s pressed
                //             $("#document_form [data-button-name='save_and_new']").click();
                //             return false;
                //         }
                //     }
                // });


            };

            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;

            //requirement for document list
            $scope.other = {};
            $scope.function_name = "documents";
            $scope.limit = 20;
            $scope.skip = 0;
            $scope.rows = [];
            $scope.total = 0;


            // initiate common function
            $scope.load_data = function(call_back=function () {}){
                Models.load_data_with_limit($http,$scope,$timeout,function (data) {
                    call_back(data);
                });

            };
            $scope.document_list_maker = function(call_back,empty_call_back=function () {}){
                Models.load_data_with_limit_total_counter($http,$scope,function (data) {
                    let total = data.total;
                    $scope.total = total;
                    if (total){
                        $scope.load_data(function (data) {
                            call_back(data);
                        })
                    }
                    else{
                        empty_call_back();
                    }


                })
            };

            $scope.default_actions = function(){
                $scope.document_type_selector();
                altair_md.card_single();
                altair_md.list_outside(function () {
                    $scope.load_data();
                });
                altair_secondary_sidebar.init();
                $scope.document_switch_on($scope.default_document_sub_type);
                $scope.document_sub_type = $scope.default_document_sub_type;

            };
            $timeout(function () {
                $scope.default_actions();
            });

            let document_sub_type = null;
            $scope.keyword = "";
            $scope.search = function(){
                if (document_sub_type){
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.other.data.keyword = $scope.keyword;
                    $scope.document_list_maker(function (data) {

                    })
                }
            };

            $scope.templates_init = function () {
                $timeout(function () {
                    document_sub_type = $scope.document_sub_type;
                    $scope.rows = [];
                    $scope.total = 0;
                    $scope.skip = 0;
                    $scope.other.data = {
                        document_type:$scope.doc_type,
                        type_option: document_sub_type,
                        designation:designation,
                        institute_id:institute_id,
                    };
                    $scope.document_list_maker(function (data) {
                        // console.log(data);
                        //if data available
                        $timeout(function () {
                            altair_documents.init($http,$scope);

                        });

                    },function () {
                        // if data not available
                        $timeout(function () {
                            altair_documents.init($http,$scope);
                        });
                    });
                });

            };
            //// save the invoice in server
            $("#page_content").on("click","#document_form [data-button-name]",function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                let button_name = $(this).attr("data-button-name");
                console.log($scope.document_sub_type);

                if ($scope.document_sub_type){

                    if($scope.doc_type === "application"){
                        let instiute_id = null;
                        let institute_selector = $(".institute-selector");
                        if (institute_selector.length){
                            let institute_selector_selectize = institute_selector.get(0).selectize;
                            instiute_id = institute_selector_selectize.getValue();
                        }
                        else{
                            let logged_user = Models.logged_user($rootScope);
                            if (logged_user){
                                instiute_id = logged_user.institute_id;
                            }
                        }

                        let other = {
                            form_name:$(this).closest("form"),
                            data:{
                                document_sub_type:$scope.document_sub_type,
                                document_type:$scope.doc_type
                            }
                        };

                        if (instiute_id === "" || instiute_id === null){
                            Models.notify("Institute_id not found");
                            retrurn ;
                        }
                        else{
                            other.data.institute_id = instiute_id;
                        }

                        if (button_name === "update"){
                            let document_row_id = $(".add-document").attr("data-id");
                            other.data.document_row_id = document_row_id;
                            other.data.edit = true;

                            Models.request_sender($http,"update","edit_application",function (data) {
                                if (data.status){
                                    altair_documents.open_document($http,$scope);
                                }

                            },[],other);
                        }
                        else{
                            Models.request_sender($http,"post","add_application",function (data) {
                                if (data.status){
                                    let new_document_data = data.data;
                                    $scope.rows.unshift(new_document_data);
                                    $timeout(function () {
                                        if (button_name === "save"){
                                            altair_documents.open_document($http,$scope);
                                        }
                                        else{
                                            $("#document_add").click();
                                        }

                                    });
                                }

                            },[],other);
                        }

                    }
                    // end receipt condition


                }
                else{
                    Models.notify("Document sub type undefined");
                }
            });

            /// document delete
            $("#page_content").on("click",".document-delete",function () {
                let document_row_id = $(this).attr("data-id");
                let other = {};
                Models.confirm(function () {
                    Models.request_sender($http,"other","document_deleter",function (data) {
                        console.log(data);
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",document_row_id);
                            if (index !== -1){
                                $scope.rows.splice(index,1);
                                $timeout(function () {
                                    altair_documents.open_document($http,$scope);
                                },1000);
                            }
                            else{
                                Models.notify("Delete row item not found");
                            }
                        }
                    },["'"+document_row_id+"'","'"+$scope.document_sub_type+"'"]);
                });

            });
            /// document authorized
            $("#page_content").on("click",".document-authorize",function () {
                let document_row_id = $(this).attr("data-id");
                let other = {};
                Models.confirm(function () {
                    Models.request_sender($http,"post","document_authorize",function (data) {
                        if (data.status){
                            altair_documents.open_document($http,$scope);
                            let document_element = $("#documents_list").find("[data-document-row-id='"+document_row_id+"']");
                            document_element.find(".authorize-status").addClass("uk-badge-success").removeClass("uk-badge-danger").text("Authorized");
                        }
                    },["'"+document_row_id+"'"]);
                });

            });
        });


    });
}]);

// Help
app.controller("help",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    $timeout(function () {
        altair_md.video_player();
    });
}]);
// compress
app.controller("initiate",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    Models.get_basic_info($http,$rootScope,function () {
        $timeout(function () {
            Models.init_requirement();
            let overlay = altair_md.card_overlay();
            let expandable_height = 100;
            Models.custom_selectize(".user-type-selectize",{
                options:[
                    {
                        label:"Teacher",
                        value:"teacher",
                    },
                    {
                        label:"Student",
                        value:"student",
                    },
                ],

            });
            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    })
                },
                onFocus:function () {
                    let this_card = $(this.$input).closest(".md-card");
                    let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                    let height_of_truncate = truncate.outerHeight();
                    truncate.height(height_of_truncate + expandable_height);
                    overlay.rearrange();
                    setTimeout(function () {
                        $(window).resize();
                    },300)
                },
                onBlur:function () {
                    let this_card = $(this.$input).closest(".md-card");
                    let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                    let height_of_truncate = truncate.outerHeight();
                    truncate.height(height_of_truncate - expandable_height);
                    overlay.rearrange();
                    setTimeout(function () {
                        $(window).resize();
                    },300)
                }
            });
        });

    });
    $scope.script_compress = function () {
        Models.request_sender($http,'get','initiate',function (data,status) {
            Models.add_action(data,$location);
        });
    };

    $scope.main_domain_database_update = function () {
        Models.request_sender($http,'other','main_domain_database_update',function (data,status) {
            if (data.status){
                Models.add_action(data,$location);
            }

        });
    };
    $scope.main_database_backup = function () {
        Models.request_sender($http,'other','main_database_backup',function (data,status) {
            if (data.status){
                Models.add_action(data,$location);
            }

        });
    };
    $scope.shift_initiate = function () {
        let other = {
            form_name:".shift-initiate-form",
            data:{

            }
        };
        Models.request_sender($http,'update','shift_initiate',function (data) {
            if (data.status){
                Models.add_action(data,$location);
            }

        },[],other);
    };






}]);

app.controller("add_controller",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        $scope.page_title = "";
        $scope.controller_type = undefined;
        $scope.access_menus_switch = 0;
        $scope.permission_parts_switch = 0;

        let controller_type = $routeParams.controller_type;
        $scope.page_title = Models.readable_text("Add new "+controller_type);

        if ($scope.page_title !== ""){
            $scope.controller_type = controller_type;
        }

        if ($scope.controller_type){

            if ($scope.controller_type === "system_admin"){
                $scope.access_menus_switch = 1;
                $scope.permission_parts_switch = 1;
            }


            Models.request_sender($http,"get","controller_enrol_basic_info",function (data) {
                // console.log(data);
                if (data.status){
                    let member_id = data.member_id;
                    $scope.member_id = member_id;
                    $scope.estimate_access_menus = data.access_menus;
                    $scope.permission_parts = data.permission_parts;
                    if ($scope.controller_type === "system_admin"){
                        $timeout(function () {
                            altair_tree.tree_a();
                            altair_tree.tree_a(".access-permission-parts");
                        });
                    }
                    Models.init_requirement();


                }

            },["'"+$scope.controller_type+"'"]);

            /// let's work here for add information
            $timeout(function () {
                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                };
                let logged_controller_type = Models.logged_user_type($rootScope);
                if (logged_controller_type === "admin"){
                    let logged_user = $rootScope.basic_info.logged_user;
                    let designation = logged_user.designation;
                    let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
                    let allow = area_parent.allow;
                    options.allow = allow;
                    options.allow_compare_field = "tag";
                    options.editable = false;
                }
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign blood group selector
                options = {
                    type:"blood_group",
                    class_name:".blood-tag-chooser"

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign department selector
                options = {
                    type:"department",
                    class_name:".department-tag-chooser"

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                let weekly_calender = null;
                let weekly_calender_init = null;
                $(".login-schedule [data-switchery]").change(function () {
                    let status = $(this).prop("checked");
                    if (status){
                        if (!weekly_calender){
                            weekly_calender = Models.working_schedule_calendar_init();
                        }
                        if (!weekly_calender_init){
                            let calender_option = {};
                            weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                        }

                        $(".schedule-container").removeClass("uk-hidden");
                    }
                    else{
                        $(".schedule-container").addClass("uk-hidden");
                    }

                });


            });
            $scope.submit = function () {
                let others = {
                    form_name:".add-controller",
                    data:{
                        basic_salary:0
                    }
                };
                others.data.login_schedule_switch = 0;
                others.data.login_schedule = "";
                let login_schedule_switch = $(".login-schedule [data-switchery]").prop("checked");
                if (login_schedule_switch){
                    others.data.login_schedule_switch = 1;
                    if ($(".scheduler:visible").length){
                        let export_data = $(".scheduler").jqs('export');
                        let json_data = JSON.parse(export_data);
                        let readable_data = [];
                        for (let item of json_data){
                            let new_periods = [];
                            for (let period of item.periods){
                                let new_period = [period.start,period.end];
                                new_periods.push(new_period);
                            }
                            let new_schedule = {
                                day: item.day,
                                periods: new_periods
                            };
                            readable_data.push(new_schedule);
                        }

                        others.data.login_schedule = JSON.stringify(readable_data);
                    }

                }

                if ($scope.controller_type === "system_admin"){
                    let tree = [
                        {
                            element_name:".tree-checkbox" ,
                            selected_name:"menu_ids[]" ,
                            active_name:"parent"
                        },
                        {
                            element_name:".access-permission-parts" ,
                            selected_name:"permission_parts[]" ,
                            active_name:"parent"
                        },
                    ];
                    others.fancy_tree = tree;

                }


                Models.request_sender($http,'post',"add_controller",function (data) {
                    // console.log(data);
                    if (data.status){

                        if ($scope.controller_type === "system_designer"){
                            $location.path("/login-info-helper");
                            Models.add_action(data,$location);
                        }
                        else{
                            let url = "/profile/"+data.member_id;
                            Models.add_action(data,$location,url);
                        }

                    }

                },["'"+$scope.controller_type+"'"],others)
            };
        }
    });



}]);
//add teacher
app.controller("add_teacher",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_id = $routeParams.institute_id;
    $scope.page_title = "Add teacher";
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        $scope.institute = institute_data;
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let page_name = Models.page_name($location);
        let user_type = "teacher";
        Models.request_sender($http,"get","teacher_enrol_basic_info",function (data) {
            if (data.status){
                let member_id = data.member_id;
                $scope.member_id = member_id;
                $scope.estimate_access_menus = data.access_menus;
                $scope.permission_parts = data.permission_parts;

                Models.init_requirement();
            }

        },["'"+user_type+"'"]);

        /// let's work here for add information
        $timeout(function () {
            Models.init_requirement();
            Models.dropify({
                messages: {
                    'default': 'Upload',
                    'replace': '',
                    'remove':  'x',
                    'error':   'Ooops'
                }
            },".profile-dropify");
            Models.dropify({});

            let options = {
                type:"designation",
                onChange:function (value,instance) {
                    $(".password-field").addClass("uk-hidden");
                    let options = Models.get_list_from_selectize(instance);
                    let selected_info = Models.filter_in_array(options,{value:value});
                    if (selected_info){
                        let designation = selected_info.label;
                        if (designation.toLowerCase() === "head teacher"){
                            $(".password-field").removeClass("uk-hidden");
                        }
                    }
                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            /// assign trainings selector
            options = {
                type:"trainings",
                class_name: ".trainings",
                multiple:true
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            /// assign subject wise trainings selector
            options = {
                type:"subject_wise_trainings",
                class_name:".subject-wise-trainings",
                multiple:true
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    })
                }
            });

        });
        $scope.submit = function () {
            let other_data = {
                form_name:".add-teacher",
                data:{
                    institute_id:$scope.institute.institute_id
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                other_data.data.password = "455454441";

            }
            Models.request_sender($http,'post',"add_teacher",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/"+user_type+"/"+data.member_id;
                    Models.add_action(data,$location,url);
                }

            },["'"+user_type+"'"],other_data)
        };
    },["'"+institute_id+"'",1,0]);


}]);

// teacher edit
app.controller("edit_teacher",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }
            if (!same_user){
                Models.admin_area_manager($rootScope,$http,$scope.user.designation,$scope.user.area,$scope.user.user_id);
            }
            $scope.same_user = same_user;
            if (same_user){
                $scope.designation_editable = false;
            }

            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_info",function (institute_data) {
                $scope.institute = institute_data;
                /// assign footer
                let footer_data = {
                    source:institute_data.name
                };
                Models.generate_footer(footer_data);

            },["'"+institute_id+"'",0,0]);

            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.member_id = data.member_id;
            $scope.user_name = data.name;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);
                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                    default_value:data.designation_id,
                    onChange:function (value,instance) {
                        $(".password-field").addClass("uk-hidden");
                        let options = Models.get_list_from_selectize(instance);
                        let selected_info = Models.filter_in_array(options,{value:value});
                        if (selected_info){
                            let designation = selected_info.label;
                            if (designation.toLowerCase() === "head teacher"){
                                $(".password-field").removeClass("uk-hidden");
                            }
                        }
                    }
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                if ($scope.user.designation && $scope.user.designation.toLowerCase() === "head teacher"){
                    $(".password-field").removeClass("uk-hidden");
                }
                /// assign trainings selector
                options = {
                    type:"trainings",
                    default_value:data.trainings,
                    class_name: ".trainings",
                    multiple:true
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                /// assign subject wise trainings selector
                options = {
                    type:"subject_wise_trainings",
                    default_value:data.subject_wise_trainings,
                    class_name:".subject-wise-trainings",
                    multiple:true
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                instance.setValue($scope.user.shift);
                            }
                        },[false,"'"+institute_id+"'"])
                    }
                });


            });

        });

        $scope.submit = function () {
            let others = {
                form_name:".edit-teacher",
                data: {
                    user_id:user_id,
                    before_member_id: $scope.user.member_id,
                    institute_id: $scope.institute.institute_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_nid: $scope.user.nid,
                    before_signature: $scope.user.signature,
                    before_active: $scope.user.active,
                    edit:1
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                others.data.password = "";

            }
            if ($scope.same_user){
                others.data.active = $scope.user.active;
                others.data.designation = $scope.user.designation_id;
            }

            Models.request_sender($http,"update","edit_teacher",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/teacher/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);

// teacher
app.controller("teacher",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            /// assign footer
            let footer_data = {
                source:data.institute_name
            };
            Models.generate_footer(footer_data);
            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.user_type = Models.id_type_name($scope.user.user_id,$rootScope);
        },true);
    });
}]);

// teacher list
app.controller("teachers",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_member_id = $routeParams.institute_id;
    $scope.institute_member_id = institute_member_id;
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let institute_id = institute_data.institute_id;
        $scope.limit = '20';
        $scope.total = 0;
        $scope.skip = 0;
        $scope.query_data = {};
        $scope.collection_name = "controllers";
        $scope.rows = [];
        Models.get_basic_info($http,$rootScope,function (data) {
            // console.log(Models.id_types);
            let teacher_id_type = Models.id_type("teacher",$rootScope);
            let sql = "where controllers.user_id like :user_id and controllers.institute_id = :institute_id";
            let sql_data = {
                ":user_id": teacher_id_type+"%",
                ":institute_id": institute_id,
            };
            $scope.query_data.sql = sql;
            $scope.query_data.sql_data = sql_data;
            $scope.contact_list_init = function(){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    $timeout(function () {
                        Models.scroll_finish(window,function () {
                            $scope.$apply(function () {
                                $scope.contact_list_load(function () {
                                });
                            });
                        });

                    });
                })
            };
            $scope.contact_list_load = function(call_back=function () {}){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    call_back(data);
                })
            };
            Models.total_counter($http,$scope,function (data) {
                // console.log(data);
                $scope.total = data.total;
                $scope.contact_list_init();
            });

            $timeout(function () {
                Models.init_requirement();
            });

            $scope.search_value = "";
            let joining = 0;
            $scope.search = function () {
                if ($scope.search_value.length >= 0){
                    let search_sql = "";
                    let search_sql_data = {};
                    if (sql === ""){
                        if (!joining){
                            search_sql = "where controllers.name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    else{
                        if (!joining){
                            search_sql = " and controllers.name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    joining = 1;
                    sql = sql+search_sql;
                    Object.assign(sql_data,search_sql_data);
                    $scope.query_data.sql = sql;
                    $scope.query_data.sql_data = sql_data;
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.contact_list_load();
                }

            };
            $scope.delete = function (id) {
                Models.confirm(function () {
                    Models.deleter($http,"controllers",id,function (data) {
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",id);
                            if (index !==-1){
                                $scope.rows.splice(index, 1);
                            }
                        }
                        // console.log(data);
                    })
                });

            };
            $scope.activate = function (user_id,$event) {
                let target = $event.target;
                let user_elem = $(target).closest(".md-card");
                Models.request_sender($http,"other","activate_as_worker",function (data) {
                    if (data.status){
                        let activate_worker = data.activate_worker;
                        $rootScope.basic_info.activate_worker = activate_worker;
                        Models.notify("Activated.");
                        $("body").click();
                        /// deactivate before user
                        $(".active-worker").removeClass("active-worker");
                        user_elem.addClass("active-worker");
                    }
                },["'"+user_id+"'"]);
            };

            $scope.export = function () {
                let prefix = $("#contact_list_filter li.uk-active a").text();
                let options = {
                    table_name:"controllers",
                    data:{},
                    sql:$scope.query_data.sql,
                    sql_data:$scope.query_data.sql_data,
                    data:{
                        prefix: prefix
                    }
                };
                Models.export_data_from_data_table($http,options);
            }
        });
    },["'"+institute_member_id+"'",1,0]);

}]);

//add student
app.controller("add_student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_id = $routeParams.institute_id;
    $scope.page_title = "Add student";
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        $scope.institute = institute_data;
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let page_name = Models.page_name($location);
        let user_type = "student";
        Models.request_sender($http,"get","student_enrol_basic_info",function (data) {
            if (data.status){
                let member_id = data.member_id;
                $scope.member_id = member_id;

                Models.init_requirement();
            }

        },["'"+user_type+"'"]);

        /// let's work here for add information
        $timeout(function () {
            Models.init_requirement();
            Models.dropify({
                messages: {
                    'default': 'Upload',
                    'replace': '',
                    'remove':  'x',
                    'error':   'Ooops'
                }
            },".profile-dropify");

            let options = {
                type:"class",
                onChange:function (value,instance) {

                }
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);

            options = {
                type:"department",
                class_name: ".department-chooser",
            };
            Models.tag_chooser($http,$scope,function (element) {

            },options);
            Models.custom_selectize(".shift-selectize",{
                onInitialize:function () {
                    let instance = this;
                    Models.request_sender($http,"get","shifts",function (data) {
                        if (data.find_data !== undefined){
                            for (let item of data.find_data){
                                item.label = item.name;
                                item.value = item.id;
                                instance.addOption(item);
                            }
                        }
                    })
                }
            });

        });
        $scope.submit = function () {
            let other_data = {
                form_name:".add-student",
                data:{
                    institute_id:$scope.institute.institute_id
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                other_data.data.password = "455454441";

            }
            Models.request_sender($http,'post',"add_student",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/"+user_type+"/"+data.member_id;
                    Models.add_action(data,$location,url);
                }

            },["'"+user_type+"'"],other_data)
        };
    },["'"+institute_id+"'",1,0]);


}]);
// teacher edit
app.controller("edit_student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }

            $scope.same_user = same_user;


            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_info",function (institute_data) {
                $scope.institute = institute_data;
                /// assign footer
                let footer_data = {
                    source:institute_data.name
                };
                Models.generate_footer(footer_data);

            },["'"+institute_id+"'",0,0]);

            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.member_id = data.member_id;
            $scope.user_name = data.name;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);
                Models.init_requirement();

                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"class",
                    default_value:data.class,
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                options = {
                    type:"department",
                    default_value:data.department_id,
                    class_name: ".department-chooser",
                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);
                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                instance.setValue($scope.user.shift);
                            }
                        })
                    }
                });


            });

        });

        $scope.submit = function () {
            let others = {
                form_name:".edit-student",
                data: {
                    user_id:user_id,
                    before_member_id: $scope.user.member_id,
                    institute_id: $scope.institute.institute_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_active: $scope.user.active,
                    edit:1
                }
            };
            if ($(".password-field").hasClass("uk-hidden")){
                others.data.password = "";

            }
            if ($scope.same_user){
                others.data.active = $scope.user.active;
                others.data.designation = $scope.user.designation_id;
            }

            Models.request_sender($http,"update","edit_student",function (data) {
                // console.log(data);
                if (data.status){
                    let url = "/student/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);

// student
app.controller("student",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            /// assign footer
            let footer_data = {
                source:data.institute_name
            };
            Models.generate_footer(footer_data);
            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.user_type = Models.id_type_name($scope.user.user_id,$rootScope);
        },true);
    });
}]);

// student list
app.controller("students",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let institute_member_id = $routeParams.institute_id;
    $scope.institute_member_id = institute_member_id;
    Models.request_sender($http,"get","institute_info",function (institute_data) {
        /// assign footer
        let footer_data = {
            source:institute_data.name
        };
        Models.generate_footer(footer_data);
        let institute_id = institute_data.institute_id;
        $scope.limit = '20';
        $scope.total = 0;
        $scope.skip = 0;
        $scope.query_data = {};
        $scope.collection_name = "controllers";
        $scope.rows = [];
        Models.get_basic_info($http,$rootScope,function (data) {
            // console.log(Models.id_types);
            let teacher_id_type = Models.id_type("student",$rootScope);
            let sql = "where controllers.user_id like :user_id and controllers.institute_id = :institute_id";
            let sql_data = {
                ":user_id": teacher_id_type+"%",
                ":institute_id": institute_id,
            };
            $scope.query_data.sql = sql;
            $scope.query_data.sql_data = sql_data;
            $scope.contact_list_init = function(){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    $timeout(function () {
                        Models.scroll_finish(window,function () {
                            $scope.$apply(function () {
                                $scope.contact_list_load(function () {
                                });
                            });
                        });

                    });
                })
            };
            $scope.contact_list_load = function(call_back=function () {}){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    call_back(data);
                })
            };
            Models.total_counter($http,$scope,function (data) {
                // console.log(data);
                $scope.total = data.total;
                $scope.contact_list_init();
            });

            $timeout(function () {
                Models.init_requirement();
            });

            $scope.search_value = "";
            let joining = 0;
            $scope.search = function () {
                if ($scope.search_value.length >= 0){
                    let search_sql = "";
                    let search_sql_data = {};
                    if (sql === ""){
                        if (!joining){
                            search_sql = "where controllers.name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    else{
                        if (!joining){
                            search_sql = " and controllers.name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    joining = 1;
                    sql = sql+search_sql;
                    Object.assign(sql_data,search_sql_data);
                    $scope.query_data.sql = sql;
                    $scope.query_data.sql_data = sql_data;
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.contact_list_load();
                }

            };
            $scope.delete = function (id) {
                Models.confirm(function () {
                    Models.deleter($http,"controllers",id,function (data) {
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",id);
                            if (index !==-1){
                                $scope.rows.splice(index, 1);
                            }
                        }
                        // console.log(data);
                    })
                });

            };
            $scope.activate = function (user_id,$event) {
                let target = $event.target;
                let user_elem = $(target).closest(".md-card");
                Models.request_sender($http,"other","activate_as_worker",function (data) {
                    if (data.status){
                        let activate_worker = data.activate_worker;
                        $rootScope.basic_info.activate_worker = activate_worker;
                        Models.notify("Activated.");
                        $("body").click();
                        /// deactivate before user
                        $(".active-worker").removeClass("active-worker");
                        user_elem.addClass("active-worker");
                    }
                },["'"+user_id+"'"]);
            };

            $scope.export = function () {
                let prefix = $("#contact_list_filter li.uk-active a").text();
                let options = {
                    table_name:"controllers",
                    data:{},
                    sql:$scope.query_data.sql,
                    sql_data:$scope.query_data.sql_data,
                    data:{
                        prefix: prefix
                    }
                };
                Models.export_data_from_data_table($http,options);
            }
        });
    },["'"+institute_member_id+"'",1,0]);

}]);


// profile
app.controller("profile",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            // console.log(data);
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $scope.user_type = Models.id_type_name($scope.user.user_id,$rootScope);
            let edit_page = "edit-profile";
            if($scope.user_type === "student"){
                edit_page = "edit-student";
            }
            else if($scope.user_type === "teacher"){
                edit_page = "edit-teacher";
            }
            $scope.edit_page = edit_page;

        },true);
    });
}]);

// profile edit
app.controller("edit-profile",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let user_id = $routeParams.user_id;
        let same_user = false;
        $scope.designation_editable = true;

        let member_id = "";
        let controller_type = Models.id_type_name(user_id,$rootScope);
        $scope.controller_type = controller_type;

        Models.get_profile_content($http,$rootScope,$routeParams,function (data) {
            $scope.user = data;
            if ($rootScope.basic_info.logged_user_id === $scope.user.user_id){
                same_user = true;
            }
            if (!same_user){
                Models.admin_area_manager($rootScope,$http,$scope.user.designation,$scope.user.area,$scope.user.user_id);
            }
            $scope.same_user = same_user;
            if (same_user){
                $scope.designation_editable = false;
            }

            if ($scope.user.login_schedule === ""){
                $scope.user.login_schedule = null;
            }
            $scope.name = data.name;
            $scope.member_id = data.member_id;
            member_id = data.member_id;
            // reformat joining date for date picker
            let joining_date = $scope.user.joining_date;
            let split = joining_date.split(" ");
            let date = split[0];
            $scope.user.joining_date = date;
            $timeout(function () {
                /// clear auto complete password
                $timeout(function () {
                    $("[name='password']").val("");
                    Models.init_requirement();
                },1000);

                Models.init_requirement();
                Models.dropify();
                Models.dropify({
                    error: {
                        'minWidth': 'The image width is too small 50px min.',
                        'maxWidth': 'The image width is too big 50px max.',
                        'minHeight': 'The image height is too small 120px min).',
                        'maxHeight': 'The image height is too big 120px max.',
                    }
                },".signature-dropify");
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                /// assign designation selector
                let options = {
                    type:"designation",
                    default_value:data.designation_id,
                    onChange:function (value,des_instance) {
                        let options = Models.get_list_from_selectize(des_instance);
                        if (!same_user){
                            let area_element = $(".area");
                            if (area_element.length){
                                let area_instance = area_element.get(0).selectize;
                                area_instance.destroy();
                                let designation_info = Models.filter_in_array(options,{value:value});
                                let new_designation = "";
                                if (designation_info){
                                    new_designation = designation_info.label;
                                }
                                Models.admin_area_manager($rootScope,$http,new_designation,$scope.user.area,$scope.user.user_id);

                            }
                        }
                    }

                };
                let logged_controller_type = Models.logged_user_type($rootScope);
                if (logged_controller_type === "admin"){
                    let logged_user = $rootScope.basic_info.logged_user;
                    let designation = logged_user.designation;
                    let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
                    let allow = area_parent.allow;
                    options.allow = allow;
                    options.allow_compare_field = "tag";
                    options.editable = false;
                }
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign blood group selector
                options = {
                    type:"blood_group",
                    class_name:".blood-tag-chooser",
                    default_value:data.blood_group_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                // assign department selector
                options = {
                    type:"department",
                    class_name:".department-tag-chooser",
                    default_value:data.department_id

                };
                Models.tag_chooser($http,$scope,function (element) {

                },options);

                if (data.designation === "MPO"){
                    $scope.mpo_switch = 1;
                }
                let weekly_calender = null;
                let weekly_calender_init = null;
                if (Number($scope.user.login_schedule_switch)){
                    $(".schedule-container").removeClass("uk-hidden");
                    weekly_calender = Models.working_schedule_calendar_init();
                    let schedule_data = $scope.user.login_schedule;
                    schedule_data = JSON.parse(schedule_data);
                    let calender_option = {};
                    if (schedule_data){
                        calender_option.data = schedule_data;
                    }
                    weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                }
                $(".login-schedule [data-switchery]").change(function () {
                    let status = $(this).prop("checked");
                    if (status){
                        if (!weekly_calender){
                            weekly_calender = Models.working_schedule_calendar_init();
                        }
                        if (!weekly_calender_init){
                            let schedule_data = $scope.user.login_schedule;
                                schedule_data = JSON.parse(schedule_data);
                            let calender_option = {};
                            if (schedule_data){
                                calender_option.data = schedule_data;
                            }
                            weekly_calender_init = weekly_calender.init($(".scheduler").not(".jqs"),calender_option);
                        }

                        $(".schedule-container").removeClass("uk-hidden");
                    }
                    else{
                        $(".schedule-container").addClass("uk-hidden");
                    }

                });

            });
            if ($rootScope.basic_info.logged_user_id !== $scope.user.user_id){
                // access menu and permission parts switch on
                if ($scope.controller_type === "system_admin"){
                    $scope.access_menus_switch = 1;
                    $scope.permission_parts_switch = 1;
                }

                //get initial data for access menus and permission parts
                Models.request_sender($http,"get","controller_enrol_basic_info",function (data) {
                    if (data.status){
                        $scope.estimate_access_menus = data.access_menus;
                        $scope.permission_parts = data.permission_parts;
                        $timeout(function () {
                            altair_tree.tree_a();
                            altair_tree.tree_a(".access-permission-parts");
                            if ($scope.controller_type === "system_admin"){
                                //// initial checked items for access menus
                                let access_menus = $scope.user.access_menus;
                                let tree = $(".tree-checkbox").fancytree("getTree");
                                for (let x of access_menus) {
                                    let node = tree.getNodeByKey(x.value);
                                    if (node){
                                        node.setSelected(true);
                                    }

                                }

                                //// initial checked items for access menus
                                let permission_parts = $scope.user.permission_parts;
                                tree = $(".access-permission-parts").fancytree("getTree");
                                for (let x of permission_parts) {
                                    let node = tree.getNodeByKey(x.value);
                                    if (node){
                                        node.setSelected(true);
                                    }

                                }
                            }

                        });
                    }

                },["'"+$scope.controller_type+"'"]);
            }

        });


        $scope.submit = function () {
            let others = {
                form_name:".edit-profile",
                data: {
                    user_id:user_id,
                    before_member_id: member_id,
                    before_password:$scope.user.password,
                    before_mobile:$scope.user.mobile,
                    before_image:$scope.user.image,
                    before_nid: $scope.user.nid,
                    before_signature: $scope.user.signature,
                    before_active: $scope.user.active,
                    before_login_schedule: "",
                    login_schedule: "",
                    edit:1,
                    basic_salary:0
                }
            };
            if (!$scope.designation_editable){
                others.data.designation = $scope.user.designation_id;
            }
            if ($scope.user.login_schedule){
                others.data.login_schedule = $scope.user.login_schedule;
            }
            others.data.login_schedule_switch = 0;
            if ($rootScope.basic_info.logged_user_id !== $scope.user.user_id) {
                if ($scope.controller_type === "system_admin") {
                    let tree = [
                        {
                            element_name: ".tree-checkbox",
                            selected_name: "menu_ids[]",
                            active_name: "parent"
                        },
                        {
                            element_name: ".access-permission-parts",
                            selected_name: "permission_parts[]",
                            active_name: "parent"
                        },
                    ];
                    others.fancy_tree = tree;
                }


                others.data.login_schedule = "";
                let login_schedule_switch = $(".login-schedule [data-switchery]").prop("checked");
                if (login_schedule_switch){
                    others.data.login_schedule_switch = 1;
                    if ($(".scheduler:visible").length){
                        let export_data = $(".scheduler").jqs('export');
                        let json_data = JSON.parse(export_data);
                        let readable_data = [];
                        for (let item of json_data){
                            let new_periods = [];
                            for (let period of item.periods){
                                let new_period = [period.start,period.end];
                                new_periods.push(new_period);
                            }
                            let new_schedule = {
                                day: item.day,
                                periods: new_periods
                            };
                            readable_data.push(new_schedule);
                        }

                        others.data.login_schedule = JSON.stringify(readable_data);
                    }

                }
            }
            // console.log(others);
            // return;
            Models.request_sender($http,"update","edit_profile",function (data) {
                // console.log(data);
                if (data.status){
                    if (data.profile_info !== undefined){
                        $rootScope.basic_info.logged_user = data.profile_info;
                    }
                    let url = "/profile/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);

// login
app.controller("login",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let elements,body,page_content;
    $timeout(function () {
        elements = $("#header_main,#sidebar_main");
        body = $("body");
        page_content = $("#page_content");
        elements.addClass("uk-hidden");
        page_content.removeAttr("id");
        body.addClass("login_page login_page_v2");

        Models.init_requirement();
        $scope.login_card_in_middle = function () {
            let height_of_window = $(window).height();
            let card_height = $(".uk-container-center").closest("div").outerHeight();
            if (height_of_window > card_height){
                let net_height = height_of_window - card_height;
                if (Models.is_divisional(net_height,2)){
                    let one_side_height = net_height / 2;
                    $(".login_page").css({
                        padding:one_side_height+"px 24px"
                    });
                }
            }
        };
        $scope.login_card_in_middle();
        $(window).resize(function () {
            $scope.login_card_in_middle();
        });
    });
    $scope.login = function () {
        let others = {
            form_name: ".login-form"
        };
        Models.request_sender($http,"get","login",function (data) {
            if (data.status){
                $(".login_page").css({
                    padding:'48px 0 0 0'
                });

                $rootScope.basic_info = data.basic_info;
                $rootScope.main_menus = undefined;

                let logged_user = $rootScope.basic_info.logged_user;
                let member_id = logged_user.member_id;

                $scope.main_menu_switch = 0;
                service.main_menu_reload();
                let logged_user_type =  Models.logged_user_type($rootScope);
                $rootScope.logged_user_type = logged_user_type;
                elements.removeClass("uk-hidden");
                page_content.attr("id","page_content");
                body.removeClass("login_page login_page_v2");
                let profile_page = "profile";
                if (logged_user_type === "teacher"){
                    profile_page = "teacher";
                }
                else if (logged_user_type === "student"){
                    profile_page = "student";
                }

                $location.path("/"+profile_page+"/"+member_id);
                altair_main_sidebar.init();
            }

        },[],others);
    }

}]);
// logout
app.controller("logout",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.request_sender($http,"other","logout",function (data) {
        if (data.status){
            window.location = "login";
            let software_info = $rootScope.basic_info.software_info;
            $(".sidebar_logo img").attr("src",software_info.logo);
            $rootScope.basic_info = undefined;
            $rootScope.main_menus = undefined;
            service.main_menu_reload();

            $(".sidebar_actions").addClass("uk-hidden");
            $rootScope.logged = 0;
            // $location.path("/login");
        }
    })
}]);

// controller list
app.controller("controllers-contact-list",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.limit = '20';
    $scope.total = 0;
    $scope.skip = 0;
    $scope.query_data = {};
    $scope.collection_name = "controllers";
    $scope.rows = [];
    let controller_type = $routeParams.controller_type;
    let attr_type = controller_type;
    if (attr_type === undefined){
        attr_type = "All";
    }

    Models.get_basic_info($http,$rootScope,function (data) {

        let logged_controller_type = Models.logged_user_type($rootScope);
        if (logged_controller_type === "admin"){
            let logged_user = $rootScope.basic_info.logged_user;
            let designation = logged_user.designation;
            let area_parent = Models.filter_in_array(Models.area_parents($rootScope,"",true),{designation:designation});
            let allow = area_parent.allow;
            $scope.options = allow;
        }
        else{
            $scope.options = Models.admin_designations();
            if (logged_controller_type === "system_designer"){
                $scope.options.push("System admin");
                $scope.options.push("System designer");
            }
            else if(logged_controller_type === "system_admin"){
                $scope.options.push("System admin");
            }

        }
        // if controller type admin then first admin designation select automatic
        $timeout(function () {
            if (controller_type === "admin"){
                let first_designation = $scope.options[0];
                controller_type = first_designation;
                attr_type = controller_type;
            }
            $("[data-type='"+attr_type+"']").addClass("uk-active");
        });


        /// change the controller type of system designer and system admin
        if (controller_type === "System designer"){
            controller_type = "system_designer";
        }
        else if(controller_type === "System admin"){
            controller_type = "system_admin";
        }

        let sql = "";
        let sql_data = {};
        if (controller_type !== undefined){
            let type_id = Models.id_type(controller_type,$rootScope);
            if (controller_type === "system_designer" || controller_type === "system_admin"){
                sql = " where controllers.user_id like :match_id ";
                sql_data[":match_id"] = type_id+"%";
            }
            else{
                let admin_type_id = Models.id_type("admin",$rootScope);
                sql = " where tags.tag = :designation and controllers.user_id like :match_id";
                sql_data[":designation"] = controller_type;
                sql_data[":match_id"] = admin_type_id+"%";
            }

        }
        // if all select
        else{
            let teacher_type_id = Models.id_type("teacher",$rootScope);
            sql = " where controllers.user_id not like :match_id ";
            sql_data[":match_id"] = teacher_type_id+"%";

            if (Models.logged_user_type($rootScope) === "system_admin"){
                let system_designer_type_id = Models.id_type("system_designer",$rootScope);
                sql += " and controllers.user_id not like :match_id_2 ";
                sql_data[":match_id_2"] = system_designer_type_id+"%";
            }
            else if (Models.logged_user_type($rootScope) === "admin"){
                let sql_in = Models.sql_in($scope.options);
                sql += " and tags.tag in("+sql_in+")";
            }
        }

        $scope.query_data.sql = sql;
        $scope.query_data.sql_data = sql_data;
        $scope.query_data.area = Models.logged_user($rootScope).area;
        $scope.contact_list_init = function(){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {

                $timeout(function () {
                    Models.scroll_finish(window,function () {
                        $scope.$apply(function () {
                            $scope.contact_list_load(function () {
                            });
                        });
                    });

                });
            })
        };
        $scope.contact_list_load = function(call_back=function () {}){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                call_back(data);

            })
        };
        Models.total_counter($http,$scope,function (data) {
            // console.log(data);
            $scope.total = data.total;
            $scope.contact_list_init();
        });

        $timeout(function () {
            Models.init_requirement();
        });

        $scope.search_value = "";
        let joining = 0;
        $scope.search = function () {
            if ($scope.search_value.length >= 0){
                let search_sql = "";
                let search_sql_data = {};
                if (sql === ""){
                    if (!joining){
                        search_sql = "where controllers.name like :name";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                }
                else{
                    if (!joining){
                        search_sql = " and controllers.name like :name";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                }
                joining = 1;
                sql = sql+search_sql;
                Object.assign(sql_data,search_sql_data);
                $scope.query_data.sql = sql;
                $scope.query_data.sql_data = sql_data;
                $scope.rows = [];
                $scope.skip = 0;
                $scope.contact_list_load();
            }

        };
        $scope.delete = function (id) {
            Models.confirm(function () {
                Models.deleter($http,"controllers",id,function (data) {
                    if (data.status){
                        let index = Models.index_number($scope.rows,"id",id);
                        if (index !==-1){
                            $scope.rows.splice(index, 1);
                        }
                    }
                    // console.log(data);
                })
            });

        };
        $scope.activate = function (user_id,$event) {
            let target = $event.target;
            let user_elem = $(target).closest(".md-card");
            Models.request_sender($http,"other","activate_as_worker",function (data) {
                if (data.status){
                    let activate_worker = data.activate_worker;
                    $rootScope.basic_info.activate_worker = activate_worker;
                    Models.notify("Activated.");
                    $("body").click();
                    /// deactivate before user
                    $(".active-worker").removeClass("active-worker");
                    user_elem.addClass("active-worker");
                }
            },["'"+user_id+"'"]);
        };

        $scope.export = function () {
            let prefix = $("#contact_list_filter li.uk-active a").text();
            let options = {
                table_name:"controllers",
                data:{},
                sql:$scope.query_data.sql,
                sql_data:$scope.query_data.sql_data,
                data:{
                    prefix: prefix
                }
            };
            Models.export_data_from_data_table($http,options);
        }
    });


}]);



// software settings
app.controller("settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.request_sender($http,"get","software_info",function (data) {
        $scope.software = data;
        $timeout(function () {
            Models.init_requirement();
            altair_md.card_overlay();
            Models.dropify({},".dropify-logo");
            Models.dropify({},".dropify-favicon");
        });
    });
    /// update settings
    $scope.submit = function(event){
        let target_elem = $(event.target);
        let this_form = target_elem.closest("form");
        let form_type = this_form.attr("data-type");
        let function_name = "software_info_update";
        let other = {};
        Models.small_loading(target_elem);
        if (form_type === "software_title"){
            let title = this_form.find("[name='title']").val();
            other.form_name = this_form;
            let data = {
                type: "software_title",
                value: title
            };
            other.data = data;

        }
        else if (form_type === "software_currency"){
            let currency = this_form.find("[name='currency']").val();
            other.form_name = this_form;
            let data = {
                type: "software_currency",
                value: currency
            };
            other.data = data;
        }
        else if (form_type === "software_time_zone"){
            let time_zone = this_form.find("[name='time_zone']").val();
            other.form_name = this_form;
            let data = {
                type: "software_time_zone",
                value: time_zone
            };
            other.data = data;
        }
        else if (form_type === "software_logo"){
            other.form_name = this_form;
            let data = {
                type: "software_logo",
                value: "",
                before_file:$scope.software.logo
            };
            other.data = data;
        }
        else if (form_type === "software_favicon"){
            other.form_name = this_form;
            let data = {
                type: "software_favicon",
                value: "",
                before_file:$scope.software.favicon
            };
            other.data = data;
        }
        else if (form_type === "software_login_banner"){
            other.form_name = this_form;
            let data = {
                type: "software_login_banner",
                value: "",
                before_file:$scope.software.login_banner
            };
            other.data = data;
        }

        else if (form_type === "software_email"){
            other.form_name = this_form;
            let email = this_form.find("[name='email']").val();
            let data = {
                type: "software_email",
                value: email,
            };
            other.data = data;
        }
        else if (form_type === "software_company_name"){
            other.form_name = this_form;
            let company_name = this_form.find("[name='company_name']").val();
            let data = {
                type: "software_company_name",
                value: company_name,
            };
            other.data = data;
        }
        else if (form_type === "software_https"){
            other.form_name = this_form;
            let active = 0;
            let status = this_form.find("[name='active']").prop("checked");
            if (status){
                active = 1;
            }
            let data = {
                type: "software_https",
                value: active,
            };
            other.data = data;
        }

        else if (form_type === "software_construction_mode"){
            other.form_name = this_form;
            let active = 0;
            let status = this_form.find("[name='active']").prop("checked");
            if (status){
                active = 1;
            }
            let date = this_form.find("[name='date']").val();
            let time = this_form.find("[name='time']").val();
            let obj = {
                date:date,
                time:time,
                active:active
            };
            let value = JSON.stringify(obj);
            let data = {
                type: "software_construction_mode",
                value: value,
            };
            other.data = data;
        }




        Models.request_sender($http,"update",function_name,function (data) {
            if (data.status){
                Models.small_loading(target_elem,0,"Updated");
                if (data.software_favicon !== undefined){
                    $scope.software.favicon = data.software_favicon;
                }
                else if(data.software_logo !== undefined){
                    $scope.software.logo = data.software_logo;
                }
            }
            else{
                Models.small_loading(target_elem, 0, "Failed");
            }
        },[],other);
    };

}]);
// Institute settings
app.controller("institute_settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let member_id = $routeParams.institute_id;
    Models.request_sender($http,"get","institute_info",function (data) {
        /// assign footer
        let footer_data = {
            source:data.name
        };
        Models.generate_footer(footer_data);
        $scope.institute = data;
        $scope.institute_member_id = member_id;
        let institute_id = data.institute_id;
        Models.request_sender($http,"get","institute_settings",function (data) {
            $scope.settings = data;
            // console.log(data);
            $timeout(function () {
                Models.init_requirement();
            });
        },["'"+institute_id+"'",false,true]);
        /// update settings
        $scope.submit = function(event){
            let target_elem = $(event.target);
            let this_form = target_elem.closest("form");
            let form_type = this_form.attr("data-type");
            let function_name = "institute_settings_update";
            let other = {};
            Models.small_loading(target_elem);
            if (form_type === "absence_person_a_day"){
                let day = this_form.find("[name='day']").val();
                other.form_name = this_form;
                let data = {
                    type: "absence_person_a_day",
                    value: day
                };
                other.data = data;

            }

            other.data.institute_id = institute_id;

            Models.request_sender($http,"update",function_name,function (data) {
                if (data.status){
                    Models.small_loading(target_elem,0,"Updated");
                }
                else{
                    Models.small_loading(target_elem, 0, "Failed");
                }
            },[],other);
        };


    },["'"+member_id+"'",1,0]);



}]);

// shift settings
app.controller("shift_settings",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    let id = $routeParams.id;
    Models.request_sender($http,"get","shift_info",function (data) {
        $scope.shift = data;
        $scope.settings = data.settings;
        // console.log(data);
        $timeout(function () {
            Models.init_requirement();
            let overlay = altair_md.card_overlay();
            Models.dropify({},".dropify-logo");
            Models.dropify({},".dropify-favicon");


            let expandable_height = 100;
            $("#page_content").on("focus","[data-uk-timepicker]",function () {
                let this_card = $(this).closest(".md-card");
                let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                let height_of_truncate = truncate.outerHeight();
                truncate.height(height_of_truncate + expandable_height);
                overlay.rearrange();
                setTimeout(function () {
                    $(window).resize();
                },300)

            });
            $("#page_content").on("blur","[data-uk-timepicker]",function () {
                let this_card = $(this).closest(".md-card");
                let truncate = this_card.find(".md-card-overlay-content .truncate-text");
                let height_of_truncate = truncate.outerHeight();
                truncate.height(height_of_truncate - expandable_height);
                overlay.rearrange();
                setTimeout(function () {
                    $(window).resize();
                },300)

            });


        });
    },["'"+id+"'",true,true]);
    /// update settings
    $scope.submit = function(event){
        let target_elem = $(event.target);
        let this_form = target_elem.closest("form");
        let form_type = this_form.attr("data-type");
        let function_name = "shift_settings_update";
        let other = {};
        Models.small_loading(target_elem);
        if (form_type === "absence_person_a_day"){
            let day = this_form.find("[name='day']").val();
            other.form_name = this_form;
            let data = {
                type: "absence_person_a_day",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "in_time"){
            let day = this_form.find("[name='in_time']").val();
            day = Models.getTwentyFourHourTime(day);
            other.form_name = this_form;
            let data = {
                type: "in_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "out_time"){
            let day = this_form.find("[name='out_time']").val();
            day = Models.getTwentyFourHourTime(day);
            other.form_name = this_form;
            let data = {
                type: "out_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "late_time"){
            let day = this_form.find("[name='late_time']").val();
            other.form_name = this_form;
            let data = {
                type: "late_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "early_time"){
            let day = this_form.find("[name='early_time']").val();
            other.form_name = this_form;
            let data = {
                type: "early_time",
                value: day
            };
            other.data = data;

        }
        else if (form_type === "in_time_scan_range"){
            let in_range_from = this_form.find("[name='in_range_from']").val();
            in_range_from = Models.getTwentyFourHourTime(in_range_from);
            let in_range_to = this_form.find("[name='in_range_to']").val();
            in_range_to = Models.getTwentyFourHourTime(in_range_to);
            other.form_name = this_form;
            let data = {
                type: "in_time_scan_range",
                in_range_from: in_range_from,
                in_range_to: in_range_to,
                value: true,

            };
            other.data = data;

        }
        else if (form_type === "out_time_scan_range"){
            let out_range_from = this_form.find("[name='out_range_from']").val();
            out_range_from = Models.getTwentyFourHourTime(out_range_from);
            let out_range_to = this_form.find("[name='out_range_to']").val();
            out_range_to = Models.getTwentyFourHourTime(out_range_to);
            other.form_name = this_form;
            let data = {
                type: "out_time_scan_range",
                out_range_from: out_range_from,
                out_range_to: out_range_to,
                value: true,

            };
            other.data = data;

        }


        other.data.shift_id = $routeParams.id;

        Models.request_sender($http,"update",function_name,function (data) {
            if (data.status){
                Models.small_loading(target_elem,0,"Updated");
            }
            else{
                Models.small_loading(target_elem, 0, "Failed");
            }
        },[],other);
    };



}]);

/// forgot password
app.controller("forgot_password",['$scope','$http','$routeParams','$rootScope','$location','$timeout',function ($scope,$http,$routeParams,$rootScope,$location,$timeout) {
    $scope.all_off = function(){
        $scope.find_form_switch = 0;
        $scope.confirmation_switch = 0;
        $scope.find_list_switch = 0;
        $scope.new_password_switch = 0;
        $scope.not_found_switch = 0;
    };
    $scope.all_off();
    $scope.find_form_switch = 1;

    $timeout(function () {
        Models.init_requirement();
    });


    $scope.find_forgots = function () {
        let other = {
            form_name:".find-form"
        };
        Models.request_sender($http,"get","find_users",function (response) {
            if (response.find_data !== undefined){
                if (response.find_data.length){
                    $scope.find_list = response.find_data;
                    $scope.all_off();
                    $scope.find_list_switch = 1;
                }
                else {
                    $scope.all_off();
                    $scope.not_found_switch = 1;
                }
            }
        },[],other);
    };
    $scope.user_select = function (user_id, user_email, mobile) {
        let other = {
            data:{
                email:user_email,
                user_id: user_id,
                mobile:mobile
            }
        };
        Models.request_sender($http,"other","send_code_forgot_user",function (response) {
            if(response.status){
                $scope.code_sender_email = user_email;
                $scope.all_off();
                $scope.confirmation_switch = 1;
                $timeout(function () {
                    Models.init_requirement();
                })
            }
        },[],other);
    };
    $scope.confirm_code = function () {
        let other = {
            form_name:".confirm-form"
        };
        Models.request_sender($http,"other","confirm_code",function (response) {
            // console.log(response);
            if (response.status){
                $scope.all_off();
                $scope.new_password_switch = 1;
            }
            $timeout(function () {
                Models.init_requirement();
            })
        },[],other);
    };
    $scope.new_password = function () {
        let other = {
            form_name:".new-password-form"
        };
        Models.request_sender($http,"update","new_password",function (response) {
            // console.log(response);
            if (response.status){
                $scope.all_off();
                $scope.done_switch = 1;
            }
            $timeout(function () {
                Models.init_requirement();
            })
        },[],other);
    };
    $scope.back_to_find = function () {
        $scope.all_off();
        $scope.find_form_switch = 1;
        $timeout(function () {
            Models.init_requirement();
        })
    }

}]);



// Activities
app.controller("activities",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_type_of_activities($http,$rootScope,function (data) {
            let type_of_activities = data;
            //requirement for activities
            $scope.other = {};
            $scope.function_name = "activities";
            $scope.limit = 12;
            $scope.skip = 0;
            $scope.rows = [];
            $scope.total = 0;
            // initiate common function
            $scope.load_data = function(call_back=function () {}){
                Models.load_data_with_limit($http,$scope,$timeout,function (data) {
                    call_back(data);
                });
            };
            $scope.activities_maker = function(call_back){
                Models.load_data_with_limit_total_counter($http,$scope,function (data) {
                    let total = data.total;
                    $scope.total = total;
                    $scope.load_data(function (data) {
                        call_back(data);
                    })

                })
            };
            $scope.activities_maker(function (data) {
                $scope.activity_switch = 1;
            });
            Models.scroll_finish(window,function () {
                $scope.load_data();
            })
        });

    });
}]);

// Add institute
app.controller("add_institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Add new school";
    $timeout(function () {
        Models.init_requirement();
        Models.dropify({
            messages: {
                'default': 'Upload banner (1650 x 290)',
            }
        });
        Models.dropify({
            messages: {
                'default': 'Upload',
                'replace': '',
                'remove':  'x',
                'error':   'Ooops'
            }
        },".profile-dropify");

        Models.custom_selectize(".shift-selectize",{
            onInitialize:function () {
                let instance = this;
                Models.request_sender($http,"get","shifts",function (data) {
                    if (data.find_data !== undefined){
                        for (let item of data.find_data){
                            item.label = item.name;
                            item.value = item.id;
                            instance.addOption(item);
                        }
                    }
                })
            },
            onChange:function () {
                this.settings.heightAdjust(this);
            },
            plugins: {
                'remove_button': {
                    label: ''
                }
            },
        });


    });
    $scope.submit = function () {
        let other_data = {
            form_name:".add-institute",
        };
        Models.request_sender($http,'post',"add_institute",function (data) {
            // console.log(data);
            if (data.status){
                let url = "/institute/"+data.member_id;
                Models.add_action(data,$location,url);
            }

        },[],other_data)
    };


}]);

// institute
app.controller("institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let member_id = $routeParams.member_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            $scope.institute = data;
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            // console.log(data);
        },["'"+member_id+"'",1,1,1]);
    })
}]);
// branch edit
app.controller("edit_institute",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Edit institute";
    let latitude = 0;
    let longitude = 0;
    Models.get_basic_info($http,$rootScope,function () {
        let institute_id = $routeParams.institute_id;
        let member_id = "";

        $scope.page_title = Models.readable_text("Edit institute" );
        // Get profile information
        Models.request_sender($http,"get","institute_info",function (data) {
            // console.log(data);
            $scope.institute = data;
            $scope.name = data.name;
            $scope.member_id = data.member_id;
            member_id = data.member_id;
            $timeout(function () {
                Models.init_requirement();
                Models.dropify({
                    messages: {
                        'default': 'Upload banner (1650 x 290)',
                    }
                });
                Models.dropify({
                    messages: {
                        'default': 'Upload',
                        'replace': '',
                        'remove':  'x',
                        'error':   'Ooops'
                    }
                },".profile-dropify");
                Models.custom_selectize(".shift-selectize",{
                    onInitialize:function () {
                        let instance = this;
                        Models.request_sender($http,"get","shifts",function (data) {
                            if (data.find_data !== undefined){
                                for (let item of data.find_data){
                                    item.label = item.name;
                                    item.value = item.id;
                                    instance.addOption(item);
                                }
                                for (let item of $scope.institute.shift_data){
                                    instance.addItem(item.shift);
                                }
                            }
                        })
                    },
                    onChange:function () {
                        this.settings.heightAdjust(this);
                    },
                    plugins: {
                        'remove_button': {
                            label: ''
                        }
                    },
                });

            });

        },["'"+institute_id+"'"]);

        $scope.submit = function () {
            let others = {
                form_name:".edit-institute",
                data: {
                    institute_id:institute_id,
                    member_id:$scope.member_id,
                    before_banner:$scope.institute.banner,
                    before_logo:$scope.institute.logo,
                    edit:1,
                }
            };

            Models.request_sender($http,"update","edit_institute",function (data) {
                if (data.status){
                    let url = "/institute/"+data.member_id;
                    Models.add_action(data,$location,url);
                }
            },[],others);
        };
    });
}]);

// institute list
app.controller("institutes",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.limit = '20';
    $scope.total = 0;
    $scope.skip = 0;
    $scope.query_data = {};
    $scope.more_data = {};
    $scope.collection_name = "institutes";
    $scope.rows = [];
    $scope.school_delete_permission = true;
    Models.get_basic_info($http,$rootScope,function (data) {
        // console.log(Models.id_types);
        $scope.school_delete_permission = Models.permission_of_part("school_delete",$rootScope);
        /// logged user designation
        let designation = "";
        let sql = "";
        let sql_data = {};
        if (Models.logged_user_type($rootScope) === "admin"){
            if ($rootScope.basic_info.logged_user !== undefined){
                let logged_user = $rootScope.basic_info.logged_user;
                designation = logged_user.designation;
                $scope.more_data.user_designation = designation;
                sql = " where controllers.user_id = :user_id ";
                sql_data[":user_id"] = logged_user.user_id;
            }
        }

        $scope.query_data.sql = sql;
        $scope.query_data.sql_data = sql_data;
        $scope.contact_list_init = function(){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                // console.log(data);
                $timeout(function () {
                    Models.scroll_finish(window,function () {
                        $scope.$apply(function () {
                            $scope.contact_list_load(function () {
                            });
                        });
                    });

                });
            })
        };
        $scope.contact_list_load = function(call_back=function () {}){
            Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {

                call_back(data);
            })
        };
        Models.total_counter($http,$scope,function (data) {
            // console.log(data);
            $scope.total = data.total;
            $scope.contact_list_init();
        });

        $timeout(function () {
            Models.init_requirement();
        });

        $scope.search_value = "";
        let joining = 0;
        $scope.search = function () {
            if ($scope.search_value.length >= 0){
                let search_sql = "";
                let search_sql_data = {};
                if (sql === ""){
                    if (!joining){
                        search_sql = "where institutes.name like :name or institutes.code like :code or institutes.member_id like :member_id or head_teacher.mobile like :teacher_mobile ";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    search_sql_data[":code"] = "%"+$scope.search_value+"%";
                    search_sql_data[":member_id"] = "%"+$scope.search_value+"%";
                    search_sql_data[":teacher_mobile"] = "%"+$scope.search_value+"%";
                }
                else{
                    if (!joining){
                        search_sql = " and (institutes.name like :name or institutes.code like :code or institutes.member_id like :member_id or head_teacher.mobile like :teacher_mobile) ";
                    }
                    search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    search_sql_data[":code"] = "%"+$scope.search_value+"%";
                    search_sql_data[":member_id"] = "%"+$scope.search_value+"%";
                    search_sql_data[":teacher_mobile"] = "%"+$scope.search_value+"%";
                }
                joining = 1;
                sql = sql+search_sql;
                Object.assign(sql_data,search_sql_data);
                $scope.query_data.sql = sql;
                $scope.query_data.sql_data = sql_data;
                $scope.rows = [];
                $scope.skip = 0;
                $scope.contact_list_load();
            }

        };
        $scope.delete = function (id) {
            Models.confirm(function () {
                Models.deleter($http,"institutes",id,function (data) {
                    if (data.status){
                        let index = Models.index_number($scope.rows,"id",id);
                        if (index !==-1){
                            $scope.rows.splice(index, 1);
                        }
                    }
                    // console.log(data);
                })
            });

        };
        $scope.activate = function (user_id,$event) {
            let target = $event.target;
            let user_elem = $(target).closest(".md-card");
            Models.request_sender($http,"other","activate_as_worker",function (data) {
                if (data.status){
                    let activate_worker = data.activate_worker;
                    $rootScope.basic_info.activate_worker = activate_worker;
                    Models.notify("Activated.");
                    $("body").click();
                    /// deactivate before user
                    $(".active-worker").removeClass("active-worker");
                    user_elem.addClass("active-worker");
                }
            },["'"+user_id+"'"]);
        };

        $scope.export = function () {
            let prefix = $("#contact_list_filter li.uk-active a").text();
            let options = {
                table_name:"controllers",
                data:{},
                sql:$scope.query_data.sql,
                sql_data:$scope.query_data.sql_data,
                data:{
                    prefix: prefix
                }
            };
            Models.export_data_from_data_table($http,options);
        }
    });


}]);


// Add shift
app.controller("add_shift",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Add shift";
    $timeout(function () {
        Models.init_requirement();
    });
    $scope.submit = function () {
        let other_data = {
            form_name:".add-shift",
        };
        Models.request_sender($http,'post',"add_shift",function (data) {
            // console.log(data);
            if (data.status){
                let url = "/edit-shift/"+data.id;
                Models.add_action(data,$location,url);
            }

        },[],other_data)
    };


}]);
// branch edit
app.controller("edit_shift",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.page_title = "Edit shift";
    Models.get_basic_info($http,$rootScope,function () {
        let shift_id = $routeParams.id;

        $scope.page_title = "Edit shift";
        // Get profile information
        Models.request_sender($http,"get","shift_info",function (data) {
            // console.log(data);
            $scope.shift = data;
            $timeout(function () {
                Models.init_requirement();
            });

        },["'"+shift_id+"'"]);

        $scope.submit = function () {
            let others = {
                form_name:".edit-shift",
                data: {
                    edit:1,
                    id:$scope.shift.id
                }
            };

            Models.request_sender($http,"update","edit_shift",function (data) {
                if (data.status){
                    Models.notify("Changed has been saved");
                }
            },[],others);
        };
    });
}]);

// area manager
app.controller("area_manager",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        /// area manager selectize init

        Models.custom_selectize(".area-parent-selectize",{
            onInitialize:function () {
                let instance = this;
                let area_parents = Models.area_parents($rootScope,"",true);
                for (let item of area_parents) {
                    item.value = item.label;
                    item.label = item.name;
                    this.addOption(item);
                }
            },
            onChange:function (value) {
                let instance = this;
                let child_selector = $(".child-selector");
                child_selector.empty();

                if (value !== ""){
                    let template_data = {

                    };
                    let parent_info = Models.area_parents($rootScope,value);

                    if (parent_info){
                        let child = parent_info.child;
                        template_data.child = value;
                        template_data.sub_child = child;

                    }
                    let template = $(".area-manager-template").html();
                    if (template !== undefined){
                        template = Models.compiler(template,template_data);
                        child_selector.html(template);
                        setTimeout(function() {
                            // reinitialize uikit margin
                            altair_uikit.reinitialize_grid_margin();
                        },560); //2 x animation duration
                    }
                    let create = false;
                    if (value === "country"){
                        create = function(input) {
                            return {
                                value: input,
                                label: input
                            }
                        };
                    }

                    Models.custom_selectize(".area-child-selectize",{
                        create: create,
                        onInitialize:function () {
                            let instance = this;
                            Models.request_sender($http,"get","child_areas",function (response) {

                                if (response.find_data !== undefined){
                                    for(let item of response.find_data){
                                        item.label = item.name;
                                        item.value = item.id;
                                        instance.addOption(item);
                                    }
                                }
                            },["'"+value+"'",true]);
                        },
                        onChange:function (value) {
                            let instance = this;
                            let child_element = $(".area-child-selectize");
                            let sub_child_element = $(".area-sub-child-selectize");
                            let sub_child_instance = sub_child_element.get(0).selectize;
                            sub_child_instance.clearOptions();

                            /// child type is cluster then Add new option will be disabled
                            let child_type = child_element.attr("data-type");
                            if (child_type === "cluster"){
                                sub_child_instance.destroy();
                                Models.custom_selectize(sub_child_element,{
                                    plugins: {
                                        'remove_button': {
                                            label: ''
                                        }
                                    },
                                    onInitialize:function () {
                                        let instance = this;
                                        Models.request_sender($http,"get","estimate_institutes",function (response) {
                                            if (response.find_data !== undefined){
                                                for (let item of response.find_data){
                                                    item.label = item.name;
                                                    item.value = item.institute_id;
                                                    instance.addOption(item);
                                                }

                                                Models.child_areas($http,sub_child_instance,value,true);
                                            }

                                        })
                                    }
                                });
                                sub_child_instance = sub_child_element.get(0).selectize;

                            }


                            if (value !== undefined){
                                if(child_type !== "cluster"){
                                    Models.child_areas($http,sub_child_instance,value);
                                }

                            }
                            instance.settings.heightAdjust(instance);

                        },
                        render: {
                            option: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            },
                            item: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            }
                        },
                    });

                    Models.custom_selectize(".area-sub-child-selectize",{
                        plugins: {
                            'remove_button': {
                                label: ''
                            }
                        },
                        create: function(input) {
                            return {
                                value: input,
                                label: input
                            }
                        },
                        render: {
                            option: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            },
                            item: function(data, escape) {
                                let grand_parent_name = null;
                                if (data.grand_parent_name !== undefined){
                                    grand_parent_name = data.grand_parent_name;
                                }
                                let render_data = "";
                                render_data += '<div class="option">' + escape(data.label);
                                if (grand_parent_name){
                                    render_data += '<span class="uk-display-block area-manager-parent">(from ' + escape(grand_parent_name) + ')</span>';
                                }
                                render_data += '</div>';
                                return render_data;
                            }
                        },
                        onChange:function () {
                            this.settings.heightAdjust(this);
                        }
                    });

                }

            }
        });

    });

    $scope.submit = function () {
        let other_data = {
            form_name:".area-manager-form",
            data:{

            }
        };
        let sub_child_selectize = $(".area-sub-child-selectize");
        if (sub_child_selectize.length){
            let sub_child_type = sub_child_selectize.attr("data-type");
            other_data.data.child_type = sub_child_type;
        }
        else{
            Models.notify("Sub child element not found");
            return false;
        }
        Models.request_sender($http,'post',"area_manager",function (data) {

            if (data.status){
                Models.notify("Done");
                $(".child-selector").empty();
                let parent_instance = $(".area-parent-selectize").get(0).selectize;
                parent_instance.setValue("");
            }

        },[],other_data)
    };

}]);

// calender
app.controller("calendar",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let page_name = Models.page_name($location);
        $scope.page_title = Models.readable_text(page_name);
        let calendar_type = "default";
        let source = "";
        if (page_name === "institute-calendar"){
            calendar_type = "institute";
            source = $routeParams.source;
        }
        else if(page_name === "teacher-calendar" || page_name === "teacher-home"){
            calendar_type = "teacher";
            source = $routeParams.source;
        }
        else if(page_name === "student-calendar" || page_name === "student-home"){
            calendar_type = "student";
            source = $routeParams.source;
        }


        if(page_name === "system-admin-home" || page_name === "admin-home"){
            $scope.page_title = "Global calendar";
        }
        else if(page_name === "student-home"){
            $scope.page_title = "Student calendar";
        }
        else if(page_name === "teacher-home"){
            $scope.page_title = "Teacher calendar";
        }



        Models.calendar_source_id($http,calendar_type,source,function (source_id,institute_id,user_data) {

            let calender = Models.calendar_init($rootScope,$http);
            let options = {
                type:calendar_type,
                source_id:source_id,
            };

            if (calendar_type === "teacher"){
                options.calender_editable = false;
                // options.absent = true;
                options.institute_id = institute_id;
                let logged_user = Models.logged_user($rootScope);
                if (logged_user){
                    let designation = logged_user.designation;
                    if (designation === "Head teacher"){
                        options.calender_editable = true;
                    }

                }
            }
            else if (calendar_type === "student"){
                options.calender_editable = true;
                // options.absent = true;
                options.institute_id = institute_id;
                let logged_user = Models.logged_user($rootScope);
                let logged_user_type = Models.logged_user_type($rootScope);
                if (logged_user){
                    let designation = logged_user.designation;
                    if (logged_user_type === "student" || logged_user_type === "admin" || designation === "teacher"){
                        options.calender_editable = false;
                    }


                }
            }
            calender.init(options);
            if ((page_name === "teacher-calendar" || page_name === "student-calendar") && user_data !== undefined){
                $scope.page_title += " of ( " + user_data.name + " )";
            }
        });


    });



}]);

// School for head master
app.controller("school",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let institute_member_id = logged_user.institute_member_id;
            $location.path("/institute/"+institute_member_id);
        }
    });



}]);

// institute device manage
app.controller("institute_device_manage",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let institute_member_id = $routeParams.institute_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            $scope.institute = data;
            let institute_id = data.institute_id;
            Models.request_sender($http,"get","institute_device_info",function (data) {
                $scope.institute_device = data;
                $timeout(function () {
                    Models.init_requirement();
                });
            },["'"+institute_id+"'"])

        },["'"+institute_member_id+"'",1,0]);

    });

    $scope.submit = function () {

        let other_data = {
            form_name:".device-form",
            data:{
                institute_id:$scope.institute.institute_id
            }
        };

        if ($scope.institute_device.id){
            other_data.data.edit = true;
        }

        Models.request_sender($http,'post',"institute_device_manage",function (data) {
            if (data.status){
                Models.notify("Done");
                $rootScope.route_reload();

            }
            else{
                Models.notify("Failed");
            }

        },[],other_data)
    };

}]);


// manual attendance
app.controller("attendance",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.institute_id = "";
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let designation = logged_user.designation;
            Models.custom_selectize(".attendance-institute-chooser",{
                searchField: ['label','value','member_id'],
                onInitialize:function () {
                    let that = this;
                    Models.get_document_save_institutes($rootScope,function (data) {
                        let institutes = data;
                        for (let institute_info of institutes){
                            institute_info.label = institute_info.name;
                            institute_info.value = institute_info.institute_id;
                            that.addOption(institute_info);
                        }

                        if (!institutes.length){
                            Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                // console.log(data);
                                if (data.find_data !== undefined){
                                    for (let institute_info of data.find_data){
                                        $rootScope.document_save_institutes.push(institute_info);
                                        institute_info.label = institute_info.name;
                                        institute_info.value = institute_info.institute_id;
                                        that.addOption(institute_info);
                                    }
                                    // that.refreshOptions();
                                }

                            },["''","'"+designation+"'"]);
                        }
                    });
                },
                onType:function (value) {
                    let that = this;
                    if (!that.currentResults.total){
                        Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                            // console.log(data);
                            if (data.find_data !== undefined){
                                for (let institute_info of data.find_data){
                                    $rootScope.document_save_institutes.push(institute_info);
                                    institute_info.label = institute_info.name;
                                    institute_info.value = institute_info.institute_id;
                                    that.addOption(institute_info);
                                }
                                that.refreshOptions();
                            }

                        },["'"+value+"'","'"+designation+"'"]);
                    }

                },
                onChange:function (value) {
                    let institute_id = value;
                    $scope.institute_id = institute_id;
                    Models.request_sender($http,"get","institute_teachers",function (response) {
                        if (response.find_data !== undefined){
                            $scope.teachers = response.find_data;
                            $scope.current_date = moment().format("YYYY-MM-DD");
                            let date = $scope.current_date;
                            $timeout(function () {
                                Models.init_requirement();
                                // default checked
                                $(".md-list li").each(function () {
                                    let element = $(this);
                                    let absent_input = element.find("[value='none']");
                                    absent_input.iCheck('check');
                                });
                                $scope.assign_attendance = function (institute_id,date) {
                                    Models.request_sender($http,"get","manual_attendance",function (response) {
                                        if (response.find_data !== undefined){
                                            let attendance = response.find_data;
                                            for(let item of attendance){
                                                let event_type = item.event_type;
                                                let note = item.note;
                                                let user_id = item.user_id;
                                                let element = $("[data-id='"+user_id+"']");
                                                let input = element.find("[value='"+event_type+"']");
                                                input.iCheck('check');
                                                element.find("[name='notes[]']").val(note);

                                            }
                                            if (!attendance.length){
                                                $("[value='none']").each(function () {
                                                    $(this).iCheck('check');
                                                });
                                                $("[name='notes[]']").each(function () {
                                                    $(this).val("");
                                                });
                                            }
                                        }
                                    },["'"+institute_id+"'","'"+date+"'"]);
                                };

                                $scope.assign_attendance(institute_id,date);
                                $("[name='date']").on("change",function () {
                                    let this_date = $(this).val();
                                    $scope.assign_attendance(institute_id,this_date);
                                });

                            });

                        }
                        else{
                            Models.notify("Teachers not found");
                        }
                    },["'"+institute_id+"'"]);

                }

            });
            Models.init_requirement();
        }

    });

    $scope.submit = function () {

        let other_data = {
            form_name:".add-attendance",
            data:{
                institute_id:$scope.institute_id
            }
        };

        Models.request_sender($http,'post',"manual_attendance",function (data) {
            if (data.status){
                Models.notify("Done");

            }
            else{
                Models.notify("Failed");
            }

        },[],other_data)
    };

}]);


// notice
app.controller("notice",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let logged_user_id = logged_user.user_id;
            let designation = logged_user.designation;
            /// mailbox init

            //// mails
            $scope.limit = '20';
            $scope.total = 0;
            $scope.skip = 0;
            $scope.query_data = {};
            $scope.collection_name = "notices";
            $scope.rows = [];
            let mailbox_instance = Models.mailbox_init();
            // console.log(Models.id_types);
            let teacher_id_type = Models.id_type("teacher",$rootScope);
            let sql = "left join institutes on institutes.institute_id = notices.receiver " +
                "where notices.sender = :sender and notices.active != :active";
            let sql_data = {
                ":sender": logged_user_id,
                ":active": 2,
            };
            $scope.query_data.sql = sql;
            $scope.query_data.sql_data = sql_data;
            $scope.query_data.columns = "notices.*,institutes.name as receiver_name,institutes.logo as receiver_image," +
                "concat(extract(day from notices.time),' ',MONTHNAME(notices.time)) as time_string";
            $scope.contact_list_init = function(){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    $timeout(function () {
                        // altair_helpers.hierarchical_slide();

                        Models.init_requirement();
                        Models.scroll_finish(window,function () {
                            $scope.$apply(function () {
                                $scope.contact_list_load(function () {
                                });
                            });
                        });

                    });
                })
            };
            $scope.contact_list_load = function(call_back=function () {}){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    call_back(data);
                    $timeout(function () {
                        Models.init_requirement();
                    });
                })
            };
            Models.total_counter($http,$scope,function (data) {
                $scope.total = data.total;
                $scope.contact_list_init();
            });

            $timeout(function () {
                Models.init_requirement();
            });

            $scope.search_value = "";
            let joining = 0;
            $scope.search = function () {
                if ($scope.search_value.length >= 0){
                    let search_sql = "";
                    let search_sql_data = {};
                    if (sql === ""){
                        if (!joining){
                            search_sql = "where name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    else{
                        if (!joining){
                            search_sql = " and name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    joining = 1;
                    sql = sql+search_sql;
                    Object.assign(sql_data,search_sql_data);
                    $scope.query_data.sql = sql;
                    $scope.query_data.sql_data = sql_data;
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.contact_list_load();
                }

            };
            $scope.delete = function (id) {
                Models.confirm(function () {
                    Models.deleter($http,"notices",id,function (data) {
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",id);
                            if (index !==-1){
                                $scope.rows.splice(index, 1);
                            }
                        }
                        // console.log(data);
                    })
                });

            };
            $scope.delete_all = function () {
                let data_list = [];
                $(".md-card-list li").each(function () {
                    let element = $(this);
                    let prop = element.find("[name='mark']").prop("checked");
                    if (prop !== undefined){
                        if (prop){
                            let id = element.attr("data-id");
                            data_list.push({
                                name:"ids[]",
                                value:id
                            });
                        }

                    }


                });
                if (data_list.length){
                    Models.confirm(function () {
                        let other = {};
                        other.data_list = data_list;
                        Models.request_sender($http,"other","marked_notice_delete",function (response) {
                            if (response.status){
                                for(let item of data_list){
                                    let id = item.value;
                                    let index = Models.index_number($scope.rows,"id",id);
                                    if (index !==-1){
                                        $scope.rows.splice(index, 1);
                                    }
                                }
                                Models.notify("Delete done");
                            }
                            else{
                                Models.notify("Delete failed");
                            }
                        },[],other);
                    });
                }
                else{
                    Models.notify("At least one item required");
                }


            };





            mailbox_instance.init({
                onModalShow:function () {
                    let element  = $(".institute-selector");
                    if (element.get(0).selectize === undefined){
                        Models.custom_selectize(element,{
                            plugins: {
                                'remove_button': {
                                    label: ''
                                }
                            },
                            searchField: ['label','value','member_id'],
                            onInitialize:function () {
                                let that = this;
                                Models.get_document_save_institutes($rootScope,function (data) {
                                    let institutes = data;
                                    for (let institute_info of institutes){
                                        institute_info.label = institute_info.name;
                                        institute_info.value = institute_info.institute_id;
                                        that.addOption(institute_info);
                                    }
                                });
                            },
                            onType:function (value) {
                                let that = this;
                                if (!that.currentResults.total){
                                    Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                        // console.log(data);
                                        if (data.find_data !== undefined){
                                            for (let institute_info of data.find_data){
                                                $rootScope.document_save_institutes.push(institute_info);
                                                institute_info.label = institute_info.name;
                                                institute_info.value = institute_info.institute_id;
                                                that.addOption(institute_info);
                                            }
                                            that.refreshOptions();
                                        }

                                    },["'"+value+"'","'"+designation+"'"]);
                                }

                            },

                        });
                    }

                }
            });



        }

    });

    $scope.submit = function () {
        let other_data = {
            form_name:".notice",
        };

        Models.request_sender($http,'post',"send_notice",function (data) {
            if (data.status){
                Models.notify("Done");
                if (data.data !== undefined){
                    for (let notice of data.data){
                        $scope.rows.unshift(notice);
                    }
                    $timeout(function () {
                        Models.init_requirement();
                    });
                }
                else{
                    Models.notify("New data retrieve failed");
                }

            }
            else{
                Models.notify("Failed");
            }
            $(".uk-modal-close").click();

        },[],other_data)
    };

}]);

// notices for teacher
app.controller("notices",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function () {
        let logged_user = Models.logged_user($rootScope);
        if (logged_user){
            let institute_id = logged_user.institute_id;
            let designation = logged_user.designation;
            /// mailbox init
            let mailbox_instance = Models.mailbox_init();
            //// mails
            $scope.limit = '20';
            $scope.total = 0;
            $scope.skip = 0;
            $scope.query_data = {};
            $scope.collection_name = "notices";
            $scope.rows = [];
            // console.log(Models.id_types);
            let teacher_id_type = Models.id_type("teacher",$rootScope);
            let sql = "left join controllers on controllers.user_id = notices.sender " +
                "where notices.receiver = :receiver and notices.active != :active";
            let sql_data = {
                ":receiver": institute_id,
                ":active": 2,
            };
            $scope.query_data.sql = sql;
            $scope.query_data.sql_data = sql_data;
            $scope.query_data.columns = "notices.*,controllers.name as receiver_name,controllers.image as receiver_image," +
                "concat(extract(day from notices.time),' ',MONTHNAME(notices.time)) as time_string";
            $scope.contact_list_init = function(){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    $timeout(function () {
                        // altair_helpers.hierarchical_slide();
                        Models.init_requirement();
                        Models.scroll_finish(window,function () {
                            $scope.$apply(function () {
                                $scope.contact_list_load(function () {
                                });
                            });
                        });

                    });
                })
            };
            $scope.contact_list_load = function(call_back=function () {}){
                Models.request_data_load($http,$scope,$rootScope,$timeout,function (data) {
                    call_back(data);
                    $timeout(function () {
                        Models.init_requirement();
                    });
                })
            };
            Models.total_counter($http,$scope,function (data) {
                $scope.total = data.total;
                $scope.contact_list_init();
            });

            $timeout(function () {
                Models.init_requirement();
            });

            $scope.search_value = "";
            let joining = 0;
            $scope.search = function () {
                if ($scope.search_value.length >= 0){
                    let search_sql = "";
                    let search_sql_data = {};
                    if (sql === ""){
                        if (!joining){
                            search_sql = "where name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    else{
                        if (!joining){
                            search_sql = " and name like :name";
                        }
                        search_sql_data[":name"] = "%"+$scope.search_value+"%";
                    }
                    joining = 1;
                    sql = sql+search_sql;
                    Object.assign(sql_data,search_sql_data);
                    $scope.query_data.sql = sql;
                    $scope.query_data.sql_data = sql_data;
                    $scope.rows = [];
                    $scope.skip = 0;
                    $scope.contact_list_load();
                }

            };
            $scope.delete = function (id) {
                Models.confirm(function () {
                    Models.deleter($http,"notices",id,function (data) {
                        if (data.status){
                            let index = Models.index_number($scope.rows,"id",id);
                            if (index !==-1){
                                $scope.rows.splice(index, 1);
                            }
                        }
                        // console.log(data);
                    })
                });

            };
            $scope.delete_all = function () {
                let data_list = [];
                $(".md-card-list li").each(function () {
                    let element = $(this);
                    let prop = element.find("[name='mark']").prop("checked");
                    if (prop !== undefined){
                        if (prop){
                            let id = element.attr("data-id");
                            data_list.push({
                                name:"ids[]",
                                value:id
                            });
                        }

                    }


                });
                if (data_list.length){
                    Models.confirm(function () {
                        let other = {};
                        other.data_list = data_list;
                        Models.request_sender($http,"other","marked_notice_delete",function (response) {
                            if (response.status){
                                for(let item of data_list){
                                    let id = item.value;
                                    let index = Models.index_number($scope.rows,"id",id);
                                    if (index !==-1){
                                        $scope.rows.splice(index, 1);
                                    }
                                }
                                Models.notify("Delete done");
                            }
                            else{
                                Models.notify("Delete failed");
                            }
                        },[],other);
                    });
                }
                else{
                    Models.notify("At least one item required");
                }


            };





            mailbox_instance.init({
                onModalShow:function () {
                    let element  = $(".institute-selector");
                    if (element.get(0).selectize === undefined){
                        Models.custom_selectize(element,{
                            plugins: {
                                'remove_button': {
                                    label: ''
                                }
                            },
                            searchField: ['label','value','member_id'],
                            onInitialize:function () {
                                let that = this;
                                Models.get_document_save_institutes($rootScope,function (data) {
                                    let institutes = data;
                                    for (let institute_info of institutes){
                                        institute_info.label = institute_info.name;
                                        institute_info.value = institute_info.institute_id;
                                        that.addOption(institute_info);
                                    }
                                });
                            },
                            onType:function (value) {
                                let that = this;
                                if (!that.currentResults.total){
                                    Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                        // console.log(data);
                                        if (data.find_data !== undefined){
                                            for (let institute_info of data.find_data){
                                                $rootScope.document_save_institutes.push(institute_info);
                                                institute_info.label = institute_info.name;
                                                institute_info.value = institute_info.institute_id;
                                                that.addOption(institute_info);
                                            }
                                            that.refreshOptions();
                                        }

                                    },["'"+value+"'","'"+designation+"'"]);
                                }

                            },

                        });
                    }

                }
            });



        }

    });

    $scope.submit = function () {
        let other_data = {
            form_name:".notice",
        };

        Models.request_sender($http,'post',"send_notice",function (data) {
            if (data.status){
                Models.notify("Done");
                if (data.data !== undefined){
                    for (let notice of data.data){
                        $scope.rows.unshift(notice);
                    }
                    $timeout(function () {
                        Models.init_requirement();
                    });
                }
                else{
                    Models.notify("New data retrieve failed");
                }

            }
            else{
                Models.notify("Failed");
            }
            $(".uk-modal-close").click();

        },[],other_data)
    };

}]);



// institute teacher attendance logs
app.controller("attendance_logs",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    $scope.logs_switch = 0;
    Models.get_basic_info($http,$rootScope,function () {
        let institute_member_id = $routeParams.institute_id;
        Models.request_sender($http,"get","institute_info",function (data) {
            /// assign footer
            let footer_data = {
                source:data.name
            };
            Models.generate_footer(footer_data);
            $scope.institute = data;

            $timeout(function () {
                $scope.logs_switch = 1;
            });
        },["'"+institute_member_id+"'",1,0]);

    });
}]);



// data tables
app.controller("data_table",['$scope','$http','$rootScope','service','$location','$route','$timeout','$routeParams',function ($scope,$http,$rootScope,service,$location,$route,$timeout,$routeParams) {
    Models.get_basic_info($http,$rootScope,function (basic_info) {
        let branch_id = basic_info.logged_branch_id;
        $scope.columnDefs = [];
        $scope.order = [];
        let page_name = Models.page_name($location);
        if (page_name === "items"){
            $scope.query_data = {
            };
            $scope.page_title = "Items";
            $scope.collection_name = "items";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".item-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Item name"
                },
                {
                    label:"code",
                    name:"Item code"
                },

                {
                    label:"time",
                    name:"Time"
                },

                {
                    label:null,
                    name:"Action"
                },

            ];
        }

        else if (page_name === "vendors"){

            $scope.query_data = {
                "sql":"where  user_id like :user_type",
                "sql_data": {
                    ":user_type": Models.id_type("vendor",$rootScope)+"%"
                },
            };
            $scope.page_title = "Vendors";
            $scope.collection_name = "customer_vendors";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let id_type = Models.id_type_name(data.user_id,$rootScope);
                        data.user_type = id_type;
                        let html = $(".customer-vendor-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [
                {
                    label:"company_name",
                    name:"Company name"
                },
                {
                    label:"name",
                    name:"Name"
                },
                {
                    label:"member_id",
                    name:"Member id"
                },
                {
                    label:"mobile",
                    name:"Mobile"
                },

                {
                    label:"time",
                    name:"Time"
                },

                {
                    label:null,
                    name:"Action"
                },

            ];
        }
        else if (page_name === "shifts"){

            $scope.query_data = {
            };
            $scope.page_title = "Shifts";
            $scope.collection_name = "shifts";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".shift-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [

                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"time",
                    name:"Time"
                },

                {
                    label:null,
                    name:"Action"
                },

            ];
        }
        else if (page_name === "devices"){

            $scope.query_data = {
            };
            $scope.page_title = "Devices";
            $scope.collection_name = "institute_devices";
            $scope.columnDefs = [
                {
                    targets:1,
                    render:function (data,type,row) {
                        let html = $(".institute-link").html();
                        let template = data;
                        if (html !== undefined){
                            template = Handlebars.compile(html)(row);
                        }
                        return template;
                    },
                }
            ];
            $scope.table_columns_format = [

                {
                    label:"serial_number",
                    name:"Serial number"
                },
                {
                    label:"institute_name",
                    name:"School name"
                },

                // {
                //     label:"time",
                //     name:"Time"
                // },


            ];
        }

        else if (page_name === "customers"){

            $scope.query_data = {
                "sql":"where user_id like :user_type",
                "sql_data": {
                    ":user_type": Models.id_type("customer",$rootScope)+"%"
                },
            };
            $scope.page_title = "Customers";
            $scope.collection_name = "customer_vendors";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let id_type = Models.id_type_name(data.user_id,$rootScope);
                        data.user_type = id_type;
                        let html = $(".customer-vendor-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                }
            ];
            $scope.table_columns_format = [
                {
                    label:"company_name",
                    name:"Company name"
                },
                {
                    label:"name",
                    name:"Name"
                },
                {
                    label:"member_id",
                    name:"Member id"
                },
                {
                    label:"mobile",
                    name:"Mobile"
                },

                {
                    label:"time",
                    name:"Time"
                },

                {
                    label:null,
                    name:"Action"
                },

            ];
        }


        else if (page_name === "attendance-logs"){
            let teacher_digit = Models.id_type("teacher",$rootScope);
            $scope.query_data = {
                "institute_id": $scope.institute.institute_id,
                "sql":"where attendance_logs.user_id like :user_type",
                "sql_data": {
                    ":user_type": teacher_digit+"%"
                },
            };
            $scope.page_title = "Teacher attendance logs";
            $scope.collection_name = "attendance_logs";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".raw-item-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // }
            ];
            $scope.table_columns_format = [
                {
                    label:"teacher_name",
                    name:"Teacher name"
                },
                {
                    label:"device_user_id",
                    name:"User id"
                },

                {
                    label:"attendance_type",
                    name:"Type"
                },
                {
                    label:"attendance_time",
                    name:"Attendance time"
                },

            ];
        }
        else if (page_name === "student-attendance-logs"){
            let student_digit = Models.id_type("student",$rootScope);
            $scope.query_data = {
                "institute_id": $scope.institute.institute_id,
                "sql":"where attendance_logs.user_id like :user_type",
                "sql_data": {
                    ":user_type": student_digit+"%"
                },
            };
            $scope.page_title = "Student attendance logs";
            $scope.collection_name = "attendance_logs";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".raw-item-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // }
            ];
            $scope.table_columns_format = [
                {
                    label:"teacher_name",
                    name:"Student name"
                },
                {
                    label:"device_user_id",
                    name:"User id"
                },

                {
                    label:"attendance_type",
                    name:"Type"
                },
                {
                    label:"attendance_time",
                    name:"Attendance time"
                },

            ];
        }

        else if (page_name === "admin-daily-attendance" || page_name === "admin-home"){
            $scope.query_data = {

            };
            $scope.page_title = "Daily attendance logs";
            $scope.collection_name = "attendance_logs";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".raw-item-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // }
            ];
            $scope.table_columns_format = [
                {
                    label:"teacher_name",
                    name:"Teacher name"
                },
                {
                    label:"device_user_id",
                    name:"User id"
                },

                {
                    label:"attendance_type",
                    name:"Type"
                },
                {
                    label:"attendance_time",
                    name:"Attendance time"
                },

            ];
        }
        else if (page_name === "teacher-list"){
            let teacher_type_digit = Models.id_type("teacher",$rootScope);
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                "sql":"where controllers.user_id like :user_id",
                "sql_data": {
                    ":user_id": teacher_type_digit+"%"
                },
                teacher_registration_report:true,
                designation:designation,
                institute_id:institute_id,
            };
            $scope.page_title = "Teacher registration report";
            $scope.collection_name = "controllers";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".bank-cash-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"designation",
                    name:"Designation"
                },
                {
                    label:"mobile",
                    name:"mobile"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },



                {
                    label:"time",
                    name:"Time"
                },
                // {
                //     label:null,
                //     name:"Action"
                // },

            ];
        }
        else if (page_name === "student-list"){
            let student_type_digit = Models.id_type("student",$rootScope);
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                "sql":"where controllers.user_id like :user_id",
                "sql_data": {
                    ":user_id": student_type_digit+"%"
                },
                student_registration_report:true,
                designation:designation,
                institute_id:institute_id,
            };
            $scope.page_title = "Student registration report";
            $scope.collection_name = "controllers";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".bank-cash-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"class_name",
                    name:"Class"
                },
                {
                    label:"department_name",
                    name:"Department"
                },

                {
                    label:"mobile",
                    name:"mobile"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

                {
                    label:"time",
                    name:"Time"
                },
                // {
                //     label:null,
                //     name:"Action"
                // },

            ];
        }
        else if (page_name === "institute-list"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                teacher_registration_report:true,
                designation:designation,
                institute_id:institute_id,
            };
            $scope.page_title = "Institute registration report";
            $scope.collection_name = "institutes";
            $scope.columnDefs = [
                // {
                //     targets:-1,
                //     render:function (data) {
                //         let html = $(".bank-cash-action").html();
                //         let template = Handlebars.compile(html)(data);
                //         return template;
                //     },
                //     orderable:false
                // },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"contact_number",
                    name:"Contact number"
                },
                {
                    label:"address",
                    name:"Address"
                },
                {
                    label:"time",
                    name:"Time"
                },
                // {
                //     label:null,
                //     name:"Action"
                // },

            ];
        }
        else if (page_name === "today-present-attendance"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                today_attendance:true,
                designation:designation,
                institute_id:institute_id,
                event_type:"present",
                user_type:$routeParams.user_type,
                user_id:$scope.source_user_id,
            };
            $scope.page_title = "Today present attendance";
            $scope.collection_name = "calendar";
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

            ];
        }
        else if (page_name === "today-training-attendance"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                today_attendance:true,
                designation:designation,
                institute_id:institute_id,
                event_type:"training",
                user_type:$routeParams.user_type,
                user_id:$scope.source_user_id,
            };
            $scope.page_title = "Today Training attendance";
            $scope.collection_name = "calendar";
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

            ];
        }
        else if (page_name === "today-leave-attendance"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                today_attendance:true,
                designation:designation,
                institute_id:institute_id,
                event_type:"leave",
                user_type:$routeParams.user_type,
                user_id:$scope.source_user_id,
            };
            $scope.page_title = "Today leave attendance";
            $scope.collection_name = "calendar";
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

            ];
        }
        else if (page_name === "today-late-attendance"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                today_attendance:true,
                designation:designation,
                institute_id:institute_id,
                event_type:"late",
                user_type:$routeParams.user_type,
                user_id:$scope.source_user_id,
            };
            $scope.page_title = "Today late attendance";
            $scope.collection_name = "calendar";
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

            ];
        }
        else if (page_name === "today-early-attendance"){
            let logged_user = Models.logged_user($rootScope);
            let designation = logged_user.designation;
            let institute_id = logged_user.institute_id;
            $scope.query_data = {
                today_attendance:true,
                designation:designation,
                institute_id:institute_id,
                event_type:"early",
                user_type:$routeParams.user_type,
                user_id:$scope.source_user_id,
            };
            $scope.page_title = "Today early attendance";
            $scope.collection_name = "calendar";
            $scope.columnDefs = [
                {
                    targets:-2,
                    render:function (data) {
                        let time_for_event = data;
                        if (time_for_event === "0000-00-00 00:00:00"){
                            time_for_event = moment().format("YYYY-MM-DD");
                        }
                        return time_for_event;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"source_name",
                    name:"Teacher name"
                },
                {
                    label:"time_for_event",
                    name:"Time for event"
                },
                {
                    label:"institute_name",
                    name:"Institute name"
                },

            ];
        }

        else if (page_name === "bank-reconciliations"){
            $scope.query_data = {};
            $scope.page_title = "Bank reconciliations";
            $scope.collection_name = "payment_requests";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".payment-request-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                },
                {
                    targets:-3,
                    render:function (data) {
                        let status = Number(data);
                        let result = "Pending";
                        if (status === 1){
                            result = "Honored";
                        }
                        if (status === 3){
                            result = "Canceled";
                        }
                        return data = result;

                    },
                    orderable:false
                },
                {
                    targets:0,
                    render:function (data,type,row) {
                        let html = $(".payment-request-sender").html();
                        let template = Handlebars.compile(html)(row);
                        return template;
                    }
                },



            ];
            $scope.table_columns_format = [
                {
                    label:"sender_name",
                    name:"To"
                },
                {
                    label:"bank_name",
                    name:"Bank name"
                },
                {
                    label:"amount",
                    name:"Amount."
                },

                {
                    label:"check_date",
                    name:"Check date"
                },
                {
                    label:"transaction_number",
                    name:"Transaction number"
                },
                {
                    label:"note",
                    name:"Note"
                },
                {
                    label:"active",
                    name:"Status"
                },
                {
                    label:"time",
                    name:"Time"
                },

                {
                    label:null,
                    name:"Action"
                },

            ];
        }


        else if (page_name === "bank-cashes"){
            $scope.query_data = {

            };
            $scope.page_title = "Bank cashes";
            $scope.collection_name = "bank_cashes";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".bank-cash-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"balance",
                    name:"Balance"
                },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:"Action"
                },

            ];
        }

        else if (page_name === "transports"){
            $scope.query_data = {
            };
            $scope.page_title = "Transports";
            $scope.collection_name = "transports";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".transport-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Vichle title"
                },

                {
                    label:"model",
                    name:"Model"
                },
                {
                    label:"number",
                    name:"Number"
                },


                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:"Action"
                },

            ];
        }
        else if (page_name === "branch-items"){
            $scope.query_data = {
                add_to_branch:true
            };
            $scope.page_title = "Branch items";
            $scope.collection_name = "items";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".item-add-action-template").html();
                            html = Models.compiler(html,data);
                        return html;
                    },
                    orderable:false
                },
                {
                    targets:1,
                    render:function (data,type,row) {
                        let html = $(".item-price-template").html();
                        html = Models.compiler(html,row);
                        return html;
                    },
                    orderable:false
                },
                {
                    targets:2,
                    render:function (data,type,row) {
                        let html = $(".item-amount-template").html();
                        return html;
                    },
                    orderable:false
                },

                {
                    targets:3,
                    render:function (data,type,row) {
                        let html = $(".item-unit-template").html();
                            html = Models.compiler(html,row);
                        return html;
                    },
                    orderable:false
                },

                {
                    targets:4,
                    render:function (data,type,row) {
                        let date = moment().format("YYYY-MM-DD");
                        let html = $(".item-date-template").html();
                        let template = Models.compiler(html,{date:date});
                        return template;
                    },
                    orderable:false
                },
                {
                    targets:5,
                    render:function (data,type,row) {
                        if (row.stock){
                            return "Added";
                        }

                    },
                    orderable:false
                },
                {
                    targets:6,
                    render:function (data,type,row) {
                        if (row.stock){
                            let stock = row.stock;
                            let unit_name = row.unit_name;
                            let bangla_switch = Models.bangla_switch();
                            if (bangla_switch){
                                stock = Models.number_conversion(stock,"e2b");
                            }
                            return stock +" "+ unit_name;
                        }
                    },
                    orderable:false
                },



            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Item name"
                },

                {
                    label:null,
                    name:"price"
                },
                {
                    label:null,
                    name:"Amount"
                },
                {
                    label:null,
                    name:"Unit"
                },
                {
                    label:null,
                    name:"Date"
                },
                {
                    label:null,
                    name:"Status"
                },
                {
                    label:null,
                    name:"Stock"
                },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:""
                },

            ];
        }
        else if (page_name === "branch-bank-cashes"){
            $scope.query_data = {
                branch_permission:true
            };
            $scope.page_title = "Branch bank cashes";
            $scope.collection_name = "bank_cashes";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".bank-cash-permission").html();
                            html = Models.compiler(html,data);
                        return html;
                    },
                    orderable:false
                },

            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Bank name"
                },
                {
                    label:"account_number",
                    name:"Account number"
                },


                {
                    label:"balance",
                    name:"Balance"
                },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:""
                },

            ];
        }

        else if (page_name === "special-items"){
            $scope.query_data = {
            };
            $scope.page_title = "Special items";
            $scope.collection_name = "special_items";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".special-item-action").html();
                            html = Models.compiler(html,data);
                        return html;
                    },
                    orderable:false
                },




            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:"Action"
                },

            ];
        }

        else if (page_name === "reports"){
            $scope.query_data = {};
            $scope.page_title = "Reports";
            $scope.collection_name = "reports";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".report-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                // {
                //     label:"source_name",
                //     name:"Source name"
                // },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:"Action"
                },

            ];
        }
        else if (page_name === "dynamic-report-templates"){
            $scope.query_data = {};
            $scope.page_title = "Dynamic report templates";
            $scope.collection_name = "dynamic_report_templates";
            $scope.columnDefs = [
                {
                    targets:-1,
                    render:function (data) {
                        let html = $(".dynamic-report-action").html();
                        let template = Handlebars.compile(html)(data);
                        return template;
                    },
                    orderable:false
                },
            ];
            $scope.table_columns_format = [
                {
                    label:"name",
                    name:"Name"
                },

                {
                    label:"time",
                    name:"Time"
                },
                {
                    label:null,
                    name:"Action"
                },

            ];
        }

        if ($scope.page_title_plus !== undefined){
            $scope.page_title += $scope.page_title_plus;
        }



        // make table required format
        $scope.column_format = [];
        let i = 0;
        for(let item of $scope.table_columns_format){
            let ob = {
                data:item.label
            };
            if (item.label === null){
                ob.defaultContent = "";
            }
            // if label is time sortable by time assign
            if (item.label === "time" || item.label === "attendance_time"){
                $scope.order = [[i,'desc']]
            }

            $scope.column_format.push(ob);
            i++;
        }
        let data = {
            "columns": "*",
            "sql":"",
            "sql_data": {},
            "table_name":$scope.collection_name,
            "data_table":true
        };

        Object.assign(data,$scope.query_data);
        let data_object = Models.retrive_request_object("request_data_list()");
        data_object['query_data']=JSON.stringify(data);
        // console.log(data_object);
        // let form_data = Models.form_data_maker("",data_object);
        $timeout(function () {
            let table = altair_datatables.dt_tableHeeshab({
                "processing": true,
                "serverSide": true,
                "ajax":{
                    url:"request",
                    data:data_object
                },
                "columns":$scope.column_format,
                "columnDefs":$scope.columnDefs,
                "order":$scope.order,
                pressExport:function (e, dt, node, config) {
                    let params = dt.ajax.params();
                    let options = {
                        table_name:$scope.collection_name,
                        limit:0,
                        skip:params.start,
                        data:{},
                    };
                    // if (params.order){
                    //     let order_column = params.order[0];
                    //     let order_column_name = params.columns[order_column.column].data;
                    //     let order_dir = order_column.dir;
                    //     options.data.order_column = order_column_name;
                    //     options.data.order_dir = order_dir;
                    // }

                    Models.export_data_from_data_table($http,options);
                }
            });
            if (table !== undefined){
                let container = table.table_container;
                table.on("draw",function (e,dt) {

                    // let params = table.ajax.params();

                    if ($(".item-unit:visible").length){
                        Models.custom_selectize(".item-unit");
                        Models.init_requirement();
                    }
                    else if ($(".bank-cash-permission-checkbox:visible").length){
                        Models.init_requirement();
                    }

                    if (page_name === "branch-items"){
                        let action_column = container.find("thead tr:first th:last");
                        let mark_template = $(".mark-action-template").html();
                        action_column.html(mark_template);
                        let footer_action_column = container.find("tfoot tr:first th:last");
                        let mark_save_button = $(".mark-save-button").html();
                        footer_action_column.html(mark_save_button);
                    }
                });
                container.on("click",".delete",function () {
                    let row = table.row( $(this).closest("tr") );
                    let data = row.data();
                    let id = data.id;
                    let table_name = $scope.collection_name;
                    Models.confirm(function () {
                        Models.request_sender($http,"other","deleter",function (data) {
                            if (data.status){
                                row.remove().draw();
                            }
                        },["'"+table_name+"'",id]);
                    });

                });
                container.on("click",".mark-all",function () {
                    container.find("tbody .add-item-to-branch").addClass("uk-hidden");
                    container.find("tbody .mark-checkbox").removeClass("uk-hidden");
                    container.find("tbody .mark-checkbox").each(function() {
                        let check_box = $(this).find("input");
                        check_box.iCheck('check');
                    });
                    container.find(".save-marked").removeClass("uk-hidden");
                    container.find(".unmark-all").removeClass("uk-hidden");

                });
                container.on("click",".unmark-all",function () {
                    container.find("tbody .add-item-to-branch").removeClass("uk-hidden");
                    container.find("tbody .mark-checkbox").addClass("uk-hidden");
                    container.find("tbody .mark-checkbox").each(function() {
                        let check_box = $(this).find("input");
                        check_box.iCheck('uncheck');
                    });
                    container.find(".save-marked").addClass("uk-hidden");
                    container.find(".unmark-all").addClass("uk-hidden");

                });
                container.on("click",".save-marked",function () {
                    let form_data = [];
                    container.find("tbody tr").each(function () {
                        let this_row = $(this);
                        let checkbox = this_row.find(".mark-checkbox input").prop("checked");
                        if (checkbox){
                            let price = this_row.find("[name='price']").val();
                            let quantity = this_row.find("[name='quantity']").val();
                            let date = this_row.find("[name='date']").val();
                            let unit = this_row.find(".item-unit").val();
                            let row_data = table.row( this_row ).data();
                            let item_id = row_data.item_id;
                            let default_unit_id = row_data.default_unit_id;
                            let assign = 0;
                            if (row_data.stock){
                                assign = 1;
                            }
                            form_data = form_data.concat([
                                {
                                    name:"price[]",
                                    value: price
                                },
                                {
                                    name:"quantity[]",
                                    value: quantity
                                },
                                {
                                    name:"date[]",
                                    value: date
                                },
                                {
                                    name:"unit[]",
                                    value: unit
                                },
                                {
                                    name:"item_id[]",
                                    value: item_id
                                },
                                {
                                    name:"assign[]",
                                    value: assign
                                },
                                {
                                    name:"default_unit_id[]",
                                    value: default_unit_id
                                },


                            ])
                        }

                    });
                    let other_options = {
                        data_list:form_data
                    }

                    if (!form_data.length){
                        Models.notify("At least 1 item required");
                        return;
                    }
                    Models.confirm(function () {
                        Models.request_sender($http,"post","add_item_to_branch_multiple",function (response) {
                            if (response.status){
                                Models.notify("Add to store done.");
                                table.draw();
                            }
                            else{
                                Models.notify("Data initiate failed");
                            }
                        },[],other_options);
                    })
                });


                container.on("click",".add-item-to-branch",function () {
                    let this_tr =$(this).closest("tr");
                    let row = table.row( this_tr );
                    Models.confirm(function () {
                        let data = row.data();
                        let item_id = data.item_id;
                        let quantity = this_tr.find("[name='quantity']").val();
                        let item_unit = this_tr.find("[name='item_unit']").val();
                        let date = this_tr.find("[name='date']").val();
                        let price = this_tr.find("[name='price']").val();
                        let assign = 0;
                        if (data.stock){
                            assign = 1;
                        }
                        let other_option= {
                            data:{
                                item_id:item_id,
                                quantity:quantity,
                                item_unit:item_unit,
                                date:date,
                                assign:assign,
                                price:price
                            }
                        };
                        Models.request_sender($http,"post","add_item_to_branch",function (data) {
                            if (data.status){
                                Models.notify("Add to store done.");
                                table.draw();
                            }
                        },[],other_option);
                    });

                });
                container.on("ifChanged",".bank-cash-permission-checkbox input",function () {
                    let check_box = $(this);
                    let status = check_box.prop("checked");
                    let this_tr =$(this).closest("tr");
                    let row = table.row( this_tr );
                    let data = row.data();
                    if (status){
                        status = 1;
                    }
                    else{
                        status = 0;
                    }
                    let bank_id = data.bank_id;
                    let assign = 0;
                    if (data.branch_id){
                        assign = 1;
                    }
                    let other_option= {
                        data:{
                            bank_id:bank_id,
                            status:status,
                            assign:assign
                        }
                    };

                    Models.request_sender($http,"post","bank_cash_permission",function (data) {
                        if (data.status){
                            Models.notify("Done.");
                            // table.draw();
                        }
                    },[],other_option);

                });



                container.on("click",".request-honor,.request-cancel",function () {
                    let activity_type = 1;
                    if ($(this).hasClass("request-cancel")){
                        activity_type = 3;
                    }
                    let row = table.row( $(this).closest("tr") );
                    let data = row.data();
                    Models.confirm(function () {
                        let tb_row = $(this).closest("tr");

                        let request_id = data.id;
                        let other = {
                            data:{
                                request_id:request_id,
                                activity:activity_type
                            }
                        };
                        Models.request_sender($http,"update","change_payment_request_status",function (data) {
                            if (data.status){
                                table.draw();
                            }

                        },[],other);
                    });



                });


            }

        });
    });

}]);








