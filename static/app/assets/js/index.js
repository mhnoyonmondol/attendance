window.onload = function(){
    let css_element = document.createElement("link");
        css_element.href =  "static/app/assets/css/compress.min.css";
        css_element.rel = "stylesheet";
        css_element.media = "all";
    let head = document.getElementsByTagName('head')[0];
        head.appendChild(css_element);

    let js_element = document.createElement("script");
        js_element.src =  "static/app/assets/js/compress.min.js";
    let body = document.getElementsByTagName('body')[0];
        body.appendChild(js_element);
};
