$(function() {
    if(isHighDensity()) {
        $.getScript( "assets/js/custom/dense.min.js", function(data) {
            // enable hires images
            altair_helpers.retina_images();
        });
    }
    if(Modernizr.touch) {
        // fastClick (touch devices)
        FastClick.attach(document.body);
    }
});
$window.load(function() {
    // ie fixes
    altair_helpers.ie_fix();
});