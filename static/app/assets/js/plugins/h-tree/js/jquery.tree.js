(function($) {
    $.fn.tree_structure = function(options) {
        var defaults = {
            'exchange_option': true,
            'add_option': true,
            'edit_option': true,
            'delete_option': true,
            'confirm_before_delete': true,
            'animate_option': [true, 5],
            'fullwidth_option': false,
            'align_option': 'center',
            'class_name': 'tree',
            'drag_disabled': false,
            'drag_start': function () {
            },
            'before_drag': function () {
            },
            'before_drop': function () {
            },
            'drop': function () {
            },
            'init': function () {
            },
            on_exchange:function () {
                
            },
            before_edit:function () {

            }

        };

        var getcode = document.URL;
        this.each(function() {
            let $this = this;
            var fns = {};
            if (options)
                $.extend(defaults, options);
            var exchange_option = defaults['exchange_option'];
            var add_option = defaults['add_option'];
            var edit_option = defaults['edit_option'];
            var delete_option = defaults['delete_option'];
            var confirm_before_delete = defaults['confirm_before_delete'];
            var animate_option = defaults['animate_option'];
            var fullwidth_option = defaults['fullwidth_option'];
            var align_option = defaults['align_option'];
            var draggable_option = defaults['draggable_option'];
            var vertical_line_text = '<span class="vertical"></span>';
            var horizontal_line_text = '<span class="horizontal"></span>';
            var add_action_text = add_option == true ? '<span class="add_action" title="Click for Add"></span>' : '';
            var exchange_action_text = exchange_option == true ? '<span class="exchange_action" title="Click for Exchange"></span>' : '';
            var edit_action_text = edit_option == true ? '<span class="edit_action" title="Click for Edit"></span>' : '';
            var delete_action_text = delete_option == true ? '<span class="delete_action" title="Click for Delete"></span>' : '';
            var highlight_text = '';
            var class_name = defaults.class_name;
            var event_name = 'pageload';

            if (align_option != 'center')
                $('.' + class_name + ' li').css({ 'text-align': align_option });
            if (fullwidth_option) {
                var i = 0;
                var prev_width;
                var get_element;
                $('.' + class_name + ' li li').each(function() {
                    var this_width = $(this).width();
                    if (i == 0 || this_width > prev_width) {
                        prev_width = $(this).width();
                        get_element = $(this);
                    }
                    i++;
                });
                var loop = get_element.closest('ul').children('li').eq(0).nextAll().length;
                var fullwidth = parseInt(0);
                for ($i = 0; $i <= loop; $i++) {
                    fullwidth += parseInt(get_element.closest('ul').children('li').eq($i).width());
                }
                $('.' + class_name + '').closest('div').width(fullwidth);
            }
            // $('.' + class_name + ' li.thide').each(function() {
            //     $(this).children('ul').hide();
            // });

            function prepend_data(target) {
                target.prepend(vertical_line_text + horizontal_line_text).children('div').prepend(add_action_text + delete_action_text + edit_action_text + exchange_action_text);
                // if (target.children('ul').length != 0)
                //     target.hasClass('thide') ? target.children('div').prepend('<b class="thide tshow"></b>') : target.children('div').prepend('<b class="thide"></b>');
                // target.children('div').prepend(highlight_text);
            }

            function draw_line(target) {
                var tree_offset_left = $('.' + class_name + '').offset().left;
                tree_offset_left = parseInt(tree_offset_left, 10);
                var child_width = target.children('div').outerWidth(true) / 2;
                var child_left = target.children('div').offset().left;
                if (target.parents('li').offset() != null)
                    var parent_child_height = target.parents('li').offset().top;
                vertical_height = (target.offset().top - parent_child_height) - target.parents('li').children('div').outerHeight(true) / 2;
                target.children('span.vertical').css({ 'height': vertical_height, 'margin-top': -vertical_height, 'margin-left': child_width, 'left': child_left - tree_offset_left });
                if (target.parents('li').offset() == null) {
                    var width = 0;
                } else {
                    var parents_width = target.parents('li').children('div').offset().left + (target.parents('li').children('div').width() / 2);
                    var current_width = child_left + (target.children('div').width() / 2);
                    var width = parents_width - current_width;
                }
                var horizontal_left_margin = width < 0 ? -Math.abs(width) + child_width : child_width;
                target.children('span.horizontal').css({ 'width': Math.abs(width), 'margin-top': -vertical_height, 'margin-left': horizontal_left_margin, 'left': child_left - tree_offset_left });
            }
            this.draw_line = function (target) {
                draw_line(target);

            };
            if (animate_option[0] == true) {
                function animate_call_structure() {
                    $timeout = setInterval(function() {
                        animate_li();
                    }, animate_option[1]);
                }
                var length = $('.' + class_name + ' li').length;
                var i = 0;

                function animate_li() {
                    prepend_data($('.' + class_name + ' li').eq(i));
                    draw_line($('.' + class_name + ' li').eq(i));
                    i++;
                    if (i == length) {
                        i = 0;
                        clearInterval($timeout);
                    }
                }
            }

            function call_structure() {
                $('.' + class_name + ' li').each(function() {
                    if (event_name == 'pageload')
                        prepend_data($(this));
                        let data = $(this).attr("data-info");
                        if (data !== undefined){
                            let obj = JSON.parse(data);
                            $(this).data(obj);
                            $(this).removeAttr("data-info");
                        }

                    draw_line($(this));

                });


            }
            this.call_structure = function(){
                call_structure();
            };
            animate_option[0] ? animate_call_structure() : call_structure();
            defaults.init(this);
            event_name = 'others';
            $(window).resize(function() {
                call_structure();
            });

            $(document).on("mouseenter mouseleave", '.' + class_name + ' li > div', function(event) {
                if (event.type == 'mouseenter' || event.type == 'mouseover') {
                    $('.' + class_name + ' li > div.current').removeClass('current');
                    $('.' + class_name + ' li > div.children').removeClass('children');
                    $('.' + class_name + ' li > div.parent').removeClass('parent');
                    $(this).addClass('current');
                    $(this).closest('li').children('ul').children('li').children('div').addClass('children');
                    $(this).closest('li').closest('ul').closest('li').children('div').addClass('parent');
                    $(this).children('span.highlight, span.add_action, span.delete_action, span.edit_action').show();
                } else {
                    $(this).children('span.highlight, span.add_action, span.delete_action, span.edit_action').hide();
                }
            });
            $(document).on("click", '.' + class_name + ' span.exchange_action', function(e) {
                e.stopImmediatePropagation();
                var this_parant=$(this).closest("li");
                var first_child=this_parant.children("ul").children("li:nth-of-type(1)");
                this_parant.children("ul").append(first_child);
                call_structure();
                defaults.on_exchange(this_parant)
            });

            function find_parent(_this) {
                if (_this.length > 0) {
                    _this.children('div').addClass('parent');
                    _this = _this.closest('li').closest('ul').closest('li');
                    return find_parent(_this);
                }
            }
            this.find_parent = function (_this) {
                find_parent(_this);
            };

            if (edit_option) {
                $(document).on("click", '.' + class_name + ' span.edit_action', function(e) {
                    if ($('form.add_data').length > 0)
                        $('form.add_data').remove();
                    if ($('form.edit_data').length > 0)
                        $('form.edit_data').remove();
                    var edit_string = $(this).closest('div').clone();
                    if (edit_string.children('span.highlight').length > 0)
                        edit_string.children('span.highlight').remove();
                    if (edit_string.children('span.delete_action').length > 0)
                        edit_string.children('span.delete_action').remove();
                    if (edit_string.children('span.add_action').length > 0)
                        edit_string.children('span.add_action').remove();
                    if (edit_string.children('span.edit_action').length > 0)
                        edit_string.children('span.edit_action').remove();
                    if (edit_string.children('b.thide').length > 0)
                        edit_string.children('b.thide').remove();
                    var checked_val = $(this).closest('li').hasClass('thide') ? 'checked' : '';
                    var edit_ele_id = $(this).closest("div").attr("id");
                    var _this = $(this);
                    var source_path = "/base/OmbazarBackEnd/static/OmbazarBackEnd/assets/js/plugins/h-tree/";
                    var editquery = '<form class="edit_data" method="post" action=""><img class="close" src="'+source_path+'images/close-button.png" /><select><option value="">Select position</option><option>Left</option><option>Right</option></select></form>';
                    if (_this.closest('div').children('form.edit_data').length == 0) {
                        _this.closest('div').append(editquery);
                        if ((_this.closest('div').children('form').offset().top + _this.closest('div').children('form').outerHeight()) > $(window).height()) {
                            _this.closest('div').children('form').css({ 'margin-top': -_this.closest('div').children('form').outerHeight() });
                        }
                        if ((_this.closest('div').children('form').offset().left + _this.closest('div').children('form').outerWidth()) > $(window).width()) {
                            _this.closest('div').children('form').css({ 'margin-left': -_this.closest('div').children('form').outerWidth() });
                        }
                        _this.closest('div').children('form').children('input.first_name').select();
                        _this.closest('div').closest('li').closest('ul').children('li').children('div').addClass('zindex');
                    }
                    defaults.before_edit(this,e);

                    $(document).on("click", "img.close", function() {
                        $(this).closest('form.edit_data').closest('div').children('span.highlight, span.add_action, span.delete_action, span.edit_action').hide();
                        $(this).closest('form.edit_data').remove();
                        $('li > div.zindex').removeClass('zindex');
                    });
                });
            }

            // if (draggable_option) {
                function draggable_event() {
                    droppable_event();

                    $('.' + class_name + ' li > div').draggable({
                        cursor: 'move',
                        distance: 40,
                        zIndex: 5,
                        revert: true,
                        revertDuration: 100,
                        snap: '.tree li div',
                        snapMode: 'inner',
                        disabled: defaults.drag_disabled,
                        start: function(event, ui) {
                            defaults.drag_start(this,$this,ui);
                            $('li.li_children').removeClass('li_children');
                            $(this).closest('li').addClass('li_children');
                        },
                        stop: function(event, ul) {
                            droppable_event();
                        }
                    });
                }
                this.draggable_event = function () {
                    draggable_event();
                };


                function droppable_event() {
                    $('.' + class_name + ' li > div').droppable({
                        accept: '.tree li div',
                        drop: function(event, ui) {
                            $('div.check_div').removeClass('check_div');
                            $('.li_children div').addClass('check_div');
                            var that =  this;
                            defaults.before_drop(this,$this,ui,function (action) {
                                var that_id = ui.draggable.closest('li').attr("id");
                                if (action) {
                                    var new_elem = $(that).next('ul').length == 0 ? $(that).after('<ul><li id="'+that_id+'">' + $(ui.draggable[0]).attr({ 'style': '' }).closest('li').html() + '</li></ul>') : $(that).next('ul').append('<li id="'+that_id+'">' + $(ui.draggable[0]).attr({ 'style': '' }).closest('li').html() + '</li>');
                                    $(ui.draggable[0]).closest('ul').children('li').length == 1 ? $(ui.draggable[0]).closest('ul').remove() : $(ui.draggable[0]).closest('li').remove();
                                    call_structure();
                                    draggable_event();
                                    defaults.drop(that,$this,ui);
                                }
                            });

                        }
                    });
                }
                this.droppable_event = function(){
                    droppable_event();
                };
                //$('.' + class_name + ' li > div').disableSelection();
                draggable_event();
            // }
            return this;
        });
        defaults.events = this;
        return this;
    };
})(jQuery);

