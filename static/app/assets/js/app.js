let base_path = '';
let template_dir = base_path+"templates/app/pages/";
let template_script_dir = base_path+"templates/app/scripts/";
let static_dir = "static/app/assets/";
let app = angular.module("heeshab",["ngRoute","ngSanitize"]);
/// url key for find location / keyword like /home and page is html file for which is content of this page
let urls = {
    home:{"url":"/","page":"home","controllers":["home"]},
    home2:{"url":"/home/:category?","page":"home","controllers":["home"]},
    home3:{"url":"/index","page":"home","controllers":["home"]},
    logout:{"url":"/logout","page":"logout"},
    not_found:{"url":"/404","page":"404"},
    undefined_branch:{"url":"/branch-error","page":"branch-error"},
    login:{"url":"/login","page":"login"},
    initiate:{"url":"/initiate","page":"initiate"},
    import_items:{"url":"/import-items","page":"import-items"},
    add_controller:{"url":"/add-controller/:controller_type","page":"add-controller"},
    profile:{"url":"/profile/:member_id","page":"profile"},
    edit_profile:{"url":"/edit-profile/:user_id","page":"edit-profile"},
    login_info_helper:{"url":"/login-info-helper","page":"login-info-helper"},
    controllers:{"url":"/handlers/:controller_type?","page":"handlers"},
    send_bulk_sms:{"url":"/send-bulk-sms","page":"send-bulk-sms",controllers:["send-bulk-sms","data-table"]},
    send_sms_test:{"url":"/send-sms-test","page":"send-sms-test",controllers:["send-sms-test","data-table"]},
    add_item:{"url":"/add-item","page":"add-item"},
    item:{"url":"/item/:item_id","page":"item"},
    edit_item:{"url":"/edit-item/:item_id","page":"edit-item"},
    settings:{"url":"/settings","page":"settings"},
    institute_settings:{"url":"/institute-settings/:institute_id","page":"institute-settings"},
    items:{"url":"/items","page":"data-table"},
    vendors:{"url":"/vendors","page":"data-table"},
    customers:{"url":"/customers","page":"data-table"},
    transports:{"url":"/transports","page":"data-table"},
    bank_cashes:{"url":"/bank-cashes","page":"data-table"},
    branch_items:{"url":"/branch-items","page":"data-table"},
    special_items:{"url":"/special-items","page":"data-table"},
    branch_bank_cashes:{"url":"/branch-bank-cashes","page":"data-table"},
    reports:{"url":"/reports","page":"data-table"},
    admin_daily_attendance:{"url":"/admin-daily-attendance","page":"data-table"},
    institute_list:{"url":"/institute-list","page":"data-table"},
    teacher_list:{"url":"/teacher-list","page":"data-table"},
    student_list:{"url":"/student-list","page":"data-table"},
    today_present_attendance:{"url":"/today-present-attendance/:user_type/:source?","page":"today-attendance","controllers":["today-attendance","data-table"]},
    today_absent_attendance:{"url":"/today-absent-attendance/:user_type/:source?","page":"today-absent-attendance","controllers":["today-absent-attendance","data-table"]},
    today_training_attendance:{"url":"/today-training-attendance/:user_type/:source?","page":"today-attendance","controllers":["today-attendance","data-table"]},
    today_leave_attendance:{"url":"/today-leave-attendance/:user_type/:source?","page":"today-attendance","controllers":["today-attendance","data-table"]},
    today_late_attendance:{"url":"/today-late-attendance/:user_type/:source?","page":"today-attendance","controllers":["today-attendance","data-table"]},
    today_early_attendance:{"url":"/today-early-attendance/:user_type/:source?","page":"today-attendance","controllers":["today-attendance","data-table"]},
    attendance_logs:{"url":"/attendance-logs/:institute_id","page":"attendance-logs","controllers":["attendance-logs","data-table"]},
    student_attendance_logs:{"url":"/student-attendance-logs/:institute_id","page":"attendance-logs","controllers":["attendance-logs","data-table"]},
    dynamic_report_templates:{"url":"/dynamic-report-templates","page":"data-table"},
    bank_reconciliations:{"url":"/bank-reconciliations","page":"data-table"},
    batch_maker:{"url":"/batch-maker","page":"data-table"},
    shifts:{"url":"/shifts","page":"data-table"},
    devices:{"url":"/devices","page":"data-table"},
    forgot_password:{"url":"/forgot-password","page":"forgot-password"},
    construction_mode:{"url":"/construction-mode","page":"construction-mode"},
    account_source:{"url":"/account-source","page":"account-source"},
    document_source:{"url":"/document-source","page":"document-source"},
    mapping:{"url":"/mapping","page":"mapping"},
    application:{"url":"/application/:document_sub_type?/:document_id?","page":"document"},
    pending_application:{"url":"/pending-application/:document_sub_type?/:document_id?","page":"document"},
    invoice:{"url":"/invoice/:document_sub_type?/:document_id?","page":"document"},
    bill:{"url":"/bill","page":"document"},
    receipt:{"url":"/receipt","page":"document"},
    add_vendor:{"url":"/add-vendor","page":"add-customer-vendor"},
    add_teacher:{"url":"/add-teacher/:institute_id","page":"add-teacher"},
    teacher:{"url":"/teacher/:member_id","page":"teacher"},
    add_student:{"url":"/add-student/:institute_id","page":"add-student"},
    student:{"url":"/student/:member_id","page":"student"},

    customer:{"url":"/customer/:member_id","page":"customer-vendor"},
    edit_teacher:{"url":"/edit-teacher/:user_id","page":"edit-teacher"},
    edit_student:{"url":"/edit-student/:user_id","page":"edit-student"},
    edit_customer:{"url":"/edit-customer/:user_id","page":"edit-customer-vendor"},
    tree:{"url":"/network/:user_id?","page":"tree"},
    activities:{"url":"/activities","page":"activities"},
    add_bank_cash:{"url":"/add-bank-cash","page":"add-bank-cash"},
    edit_bank_cash:{"url":"/edit-bank-cash/:bank_cash_id","page":"edit-bank-cash"},
    bank_cash:{"url":"/bank-cash/:bank_cash_id","page":"bank-cash"},
    report_generate:{"url":"/report-generate","page":"report-generate"},
    add_institute:{"url":"/add-institute","page":"add-institute"},
    edit_institute:{"url":"/edit-institute/:institute_id","page":"edit-institute"},
    institute:{"url":"/institute/:member_id","page":"institute"},
    institutes:{"url":"/institutes","page":"institutes"},
    teachers:{"url":"/teachers/:institute_id","page":"teachers"},
    students:{"url":"/students/:institute_id","page":"students"},
    add_transport:{"url":"/add-transport","page":"add-transport"},
    edit_transport:{"url":"/edit-transport/:transport_id","page":"edit-transport"},
    transport:{"url":"/transport/:member_id","page":"transport"},
    add_special_item:{"url":"/add-special-item","page":"add-special-item"},
    edit_special_item:{"url":"/edit-special-item/:special_item_id","page":"edit-special-item"},
    special_item:{"url":"/special-item/:member_id","page":"special-item"},
    dynamic_report_maker:{"url":"/dynamic-report-maker/","page":"dynamic-report-maker"},
    report:{"url":"/report/:report_id","page":"report"},
    edit_dynamic_report_template:{"url":"/edit-dynamic-report-template/:report_id","page":"edit-dynamic-report-template"},
    area_manager:{"url":"/area-manager","page":"area-manager"},
    global_calendar:{"url":"/global-calendar","page":"calendar"},
    institute_calendar:{"url":"/institute-calendar/:source","page":"calendar"},
    teacher_calendar:{"url":"/teacher-calendar/:source","page":"calendar"},
    student_calendar:{"url":"/student-calendar/:source","page":"calendar"},
    institute_device_manage:{"url":"/institute-device-manage/:institute_id","page":"institute-device-manage"},
    school:{"url":"/school","page":"school"},
    attendance:{"url":"/attendance","page":"attendance"},
    notice:{"url":"/notice","page":"notice"},
    notices:{"url":"/notices","page":"notices"},
    head_teacher_home:{"url":"/head-teacher-home/:source","page":"head-teacher-home","controllers":["head-teacher-home","data-table","calendar"]},
    head_teacher_home_2:{"url":"/head-teacher-home-2/:source","page":"head-teacher-home-2","controllers":["head-teacher-home-2","data-table","calendar"]},
    admin_home:{"url":"/admin-home","page":"admin-home","controllers":["admin-home","data-table","calendar"]},
    admin_home_2:{"url":"/admin-home-2","page":"admin-home-2","controllers":["admin-home-2","data-table","calendar"]},
    system_admin_home:{"url":"/system-admin-home","page":"system-admin-home","controllers":["system-admin-home","data-table","calendar"]},
    student_home:{"url":"/student-home/:source","page":"student-home","controllers":["student-home","data-table","calendar"]},
    teacher_home:{"url":"/teacher-home/:source","page":"teacher-home","controllers":["teacher-home","data-table","calendar"]},
    monthly_attendance_report:{"url":"/monthly-attendance-report/:category?","page":"monthly-attendance-report"},
    student_monthly_attendance_report:{"url":"/student-monthly-attendance-report/:source?","page":"monthly-attendance-report"},
    teacher_monthly_attendance_report:{"url":"/teacher-monthly-attendance-report/:source?","page":"monthly-attendance-report"},
    add_shift:{"url":"/add-shift","page":"add-shift"},
    edit_shift:{"url":"/edit-shift/:id","page":"edit-shift"},
    shift_settings:{"url":"/shift-settings/:id","page":"shift-settings"},
};


app.config(['$interpolateProvider','$routeProvider','$locationProvider','$httpProvider','$controllerProvider',function($interpolateProvider,$routeProvider,$locationProvider,$httpProvider,$controllerProvider) {
    $interpolateProvider.startSymbol('((');
    $interpolateProvider.endSymbol('))');

    $locationProvider.html5Mode(true);

    //provider for csft token security reason
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';

    app.register =
        {
            controller: $controllerProvider.register,
        };
    //single page development
    for (let url_info in urls){
        let url = urls[url_info].url;
        let page = urls[url_info].page;

        $routeProvider
        .when(url, {
            templateUrl : template_dir+page+".html",
            resolve:{
                script_load:function () {
                    return new Promise(function (resolve,reject) {
                        let list_of_script = [template_script_dir+page+".js"];
                        if (urls[url_info].controllers !== undefined){
                            list_of_script = [];
                            for (let controller_name of urls[url_info].controllers){
                                list_of_script.push(template_script_dir+controller_name+".js")
                            }
                        }

                        Models.multiple_script_loader(list_of_script,function () {
                            resolve();
                        });
                    })
                }
            }
        })
    }
    $routeProvider
    .otherwise(urls.not_found.url,{
        redirect: urls.not_found.page
    });
}]);
app.run(['$rootScope','$location','$route',function ($rootScope,$location,$route) {
   $rootScope.currency = "৳";
   $rootScope.site_logo = "/base/media/default/om-bazar-logo.png";
   $rootScope.rows = [];
   $rootScope.require = function(){

   };
   $rootScope.error_page = function(){
        let url = "/404";
        $location.path(url);
    };
   $rootScope.registration_complete = 0;
   $rootScope.rows_load = function(){};
   $rootScope.stores = [];
   $rootScope.basic_info = undefined;
   $rootScope.main_menus = undefined;
   $rootScope.previous_page = function () {
        window.history.back();
   };
   $rootScope.menu_called = 0;
   $rootScope.page_access = 1;
   $rootScope.logged = 1;
   $rootScope.denied_page_switch = 0;
   $rootScope.route_reload = function () {
       $route.reload();
   }
   $rootScope.self_profile_path = "profile";
   // basic info scopes
    // ***************************
    //**
    //**
    //**
    // ***************************
   // $rootScope.table_pagination_path = '/base/OmbazarBackEnd/templates/OmbazarBackEnd/resource/parts/table-pagination.html';

}]);
app.filter('range', [function() {
  return function(input, total) {
    total = parseInt(total);

    for (let i=0; i<total; i++) {
        if (i>0){
            input.push(i);
        }

    }

    return input;
  };
}]);
app.filter('if', [function() {
  return function(input, result, comparism='',else_result) {
      if (comparism !== ""){
          if (input===comparism) {
              return result;
          }
          else{
              return else_result;
          }
      }
      else{
          if(input){
              return result;
          }
          else{
              return else_result;
          }
      }
  };
}]);

app.filter('time', [function() {
  return function(input) {
      return moment(input).format("MMMM Do YYYY, h:mm:ss a");
  };
}]);

app.filter('activity_icon', ['$rootScope',function($rootScope) {
  return function(input) {
      let type_of_activities = $rootScope.type_of_activities;
      if (type_of_activities !== undefined){
          let filter = Models.custom_filter(type_of_activities,"label",input);
          if(filter !== undefined){
              return filter.icon;
          }
      }
  };
}]);

app.filter('activity_template', ['$rootScope',function($rootScope) {
  return function(input) {
        return Models.activity_template(input,$rootScope);
  };
}]);

app.filter('controller_type_name', ['$rootScope',function($rootScope) {
  return function(input) {
        let id_type_name = Models.id_type_name(input,$rootScope);
        let text = Models.readable_text(id_type_name);
        return text;
  };
}]);



app.filter('activate_worker', ['$rootScope',function($rootScope) {
  return function(input, result="",else_result="") {
      let activate_worker_id = Models.activate_worker_id($rootScope);
      if (activate_worker_id === input){
          return result;
      }
      else{
          return else_result;
      }
  };
}]);

app.filter('default', [function() {
  return function(input, result) {
      if (input === undefined){
          return result
      }
  };
}]);
app.filter('object_line_total', [function() {
  return function(input) {
      let item = input;
      let price = item.price;
      let sale = item.sale;
      let vat = item.vat;
      let quantity = item.quantity;

      //for gather rice grand total
      let line_total = 0;
      if (item.sale){
          line_total = item.sale*item.quantity;
      }
      else{
          line_total = item.price*item.quantity;
      }
      let line_vat = line_total*(vat/100);
      let line_total_with_vat = line_total+line_vat;
      return line_total_with_vat;

  };
}]);
app.filter('object_exist', [function() {
  return function(input, objects,key_name, result = 0) {
      let item = Models.custom_filter(objects,key_name,input);
      if (item !== undefined){
          if (result){
              return result;
          }
          else{
              return item;
          }
      }
      else{
          return "not-found";
      }

  };
}]);

app.filter('point', ['$rootScope',function($rootScope) {
  return function(input,quantity=0) {
      return  Models.point_maker($rootScope,input,quantity);
  };
}]);

app.filter('user_tooltip', function() {
    return function(input) {
        let html = $(".user-tooltip").html();
        let compile = Handlebars.compile(html)(input);
        return compile;
    };
});



app.directive("stalks",function () {
    let template  = ""+
        "<ul ng-if=\"stalk.stalk.length>0\">\n" +
        '<li data-info="((stalk))" ng-repeat="stalk in stalk.stalk">'+
        '<div class="tree-user">'+
        '<a class="" href="network/((stalk.user_id))" data-uk-tooltip="{pos:\'right\'}" title="((stalk | user_tooltip))">'+
        '<img src="((stalk.image))">'+
        '<span>((stalk.name))</span>'+
        '</a>'+
        '</div>'+
        '<!-- directive: stalks -->'+
        '</li>'+
        "</ul>\n";
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
app.service('service',['$rootScope',function ($rootScope) {
    this.basic_info = {};
    this.set_basic_info = function () {
        $rootScope.$emit("set_basic_info");
        this.basic_info = $rootScope.basic_info;
    };
    this.get_basic_info = function () {
        return this.basic_info;
    };

    this.main_menu_reload = function () {
        $rootScope.$emit("main_menu_reload");
    };
    this.container_load = function (callback = function () {}) {
        $rootScope.$emit("container_load");
        callback();
    }



}]);

app.directive("subMenus",function () {
    let template  = ''+
            '<ul ng-if="menu.menus.length>0">\n' +
            '<li title="Dashboard" ng-repeat="menu in menu.menus " >\n' +
            '<a href="((menu.link))">\n' +
            '<span class="menu_title">((menu.menu_name))</span>\n' +
            '</a>\n' +
            '\n' +
            '<!-- directive: sub-menus -->' +
            '</li>\n' +
            '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
app.directive("subCategories",function () {
    let template  = ''+
        '<ul ng-if="category.categories.length>0">\n' +
        '<li id="((category.id))" ng-repeat="category in category.categories " >\n' +
        '((category.name))\n' +
        '<!-- directive: sub-categories -->' +
        '</li>\n' +
        '</ul>\n';
    return {
        restrict : "M",
        replace : true,
        template : template
    };
});
/// sub document source
app.directive("subDocumentSource",function () {
    let template = '<ul class="md-list md-list-addon" data-uk-sortable="{group:\'connected-group\'}">\n' +
        '                        <li id="((source.id))" ng-repeat="source in source.children">\n' +
        '                            <div class="md-list-addon-element sortable-handler" ng-bind-html="source.icon">\n' +
        '\n' +
        '                            </div>\n' +
        '                            <div class="md-list-content">\n' +
        '                                <span class="md-list-heading">((source.name))</span>\n' +
        '                                <span class="uk-text-small uk-text-muted">((source.description))</span>\n' +
        '                            </div>\n' +
        '<!-- directive: sub-document-source -->' +
        '                        </li>\n' +
        '                    </ul>';

    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

//sub account source
app.directive("subAccountSource",function () {
    let template = '<ul class="md-list md-list-addon uk-sortable" data-uk-sortable="{group:\'connected-group\'}">\n' +
        '                        <li id="((source.id))" ng-repeat="source in source.children">\n' +
        '                            <div class="md-list-addon-element sortable-handler" ng-bind-html="source.icon">\n' +
        '\n' +
        '                            </div>\n' +
        '                            <div class="md-list-content">\n' +
        '                                <span class="md-list-heading">((source.name))</span>\n' +
        '                                <span class="uk-text-small uk-text-muted">((source.description))</span>\n' +
        '                            </div>\n' +
        '<!-- directive: sub-account-source -->' +
        '                        </li>\n' +
        '                    </ul>';

    return {
        restrict : "M",
        replace : true,
        template : template
    };
});

/// install or import collections
app.controller("install",['$scope','$http','$rootScope','service','$timeout',function ($scope,$http,$rootScope,service,$timeout) {
    let function_name = "installed_collections()";
    let extra_data = Models.retrive_request_object(function_name);
    let form_data = Models.form_data_maker("",extra_data);
    $scope.collections = [
    ];
    Models.$http($http,form_data,function (response,status) {
        let find_data = response.find_data;
        for(let collection in find_data){
            $scope.collections.push(find_data[collection]);

        }
    });

    ///upload new collection
    let  token = Models.csrftoken;
    let params = {
        request_type: "post",
        request_name: "upload_collection(request)",
        csrfmiddlewaretoken: token
    };
    Models.drug_drop_upload(params,function (response) {
        let new_data = response.new_collection;
        let collection_name = new_data.collection_name;
        let available = Models.find_index($scope.collections,"collection_name",collection_name);
        $scope.$apply(function () {
            if (available === -1) {
                $scope.collections.push(new_data);
            }else{
                $scope.collections[available] = new_data;
            }
        })
    });



}]);

// Page header to footer basic controller
app.controller("basic",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    let public_pages = [
        'profile',
        'login',
        'logout',
        "add-controller/system_designer",
        "forgot-password",
        "login-info-helper",
        "branch-error",
        "index",
        "",
        "home",
    ];

    let initial_pages = [
        "add-controller/system_designer",
        "settings",
        "account-source",
        "document-source",
        "mapping",
        "add-branch",
        "handlers/system_designer",
        "activities",
        "branches",
        "profile",
        "index",
        "",
        "home",
        "branch",
        "edit-branch",
        "edit-profile",
        "branch-error",
        "add-special-item",
        "edit-special-item",
        "special-items",
        "special-item",
        "add-item",
        "edit-item",
        "items",
        "item",
    ];

    $scope.public_pages = public_pages;
    $scope.initial_pages = initial_pages;

    // When nav template data loaded
    $scope.nav_init = function(){
        $scope.main_header_switch = 1;
    };
    // When main header / logo and branch selector template data loaded
    $scope.main_header_init = function(){
        $scope.main_menu_switch = 1;
    };
    // When main menu template data loaded
    $scope.main_menu_init = function(){
        // altair_main_header.init();
        altair_helpers.full_screen();
        $scope.basic_info_init();
        // console.log('menu init all ok')
    };
    $scope.login_init = function(){
        $location.path("/login");
    };
    $rootScope.document_switch = 0;
    $rootScope.shortcut_important_list_switch = 0;
    $scope.nav_switch = 1;
    $scope.main_header_switch = 0;
    $scope.main_menu_switch = 0;

    $scope.basic_info_init = function(){

        Models.get_basic_info($http,$rootScope,function (data) {
            // console.log(data);
            let basic_info = data;
            let logged_user_id = basic_info.logged_user_id;
            if (logged_user_id !== undefined){
                $(".sidebar_actions").removeClass("uk-hidden");
            }

            // let db_config = Models.db_init($rootScope.basic_info.tables_schema);
            $rootScope.logged_user_type = Models.logged_user_type($rootScope);
            // Get main menus data and initiate main menu
            $scope.menu_init = function(){
                if ($rootScope.basic_info!== undefined){
                    Models.user_menu_init($http,$rootScope,function (menu_list) {
                        $scope.menus = menu_list;
                        $timeout(function () {
                            Models.active_menu($location);
                            altair_main_sidebar.init();
                        });

                    });
                }
                else{
                    $scope.menus = [];
                }

            };
            $scope.menu_init();
            if ($rootScope.basic_info !== undefined) {
                if ($rootScope.basic_info.software_info !== undefined){
                    let software_info = $rootScope.basic_info.software_info;
                    $(".sidebar_logo img").attr('src',software_info.logo);
                    $rootScope.currency = software_info.currency;
                }
            }
            $rootScope.$on("main_menu_reload",function () {
                $scope.menu_init();
            });

            $scope.login_check();
        });

    };
    $scope.login_check = function(){
        ///public page
        let page_name = Models.page_name($location,[2]);
        let root_page_name = Models.page_name($location);

        for (let p of public_pages){
            if (page_name === p || p === root_page_name){
                $rootScope.logged = 1;
                return true;
            }
        }

        if(!$rootScope.basic_info.logged_user_id){
            // $location.path("/login");
            $rootScope.logged = 0;
            return false;
        }
        else{
            $rootScope.logged = 1;
            return true;
        }
    };

    $scope.page_controller = function(){

        Models.get_basic_info($http,$rootScope,function (basic_data) {
            let page = Models.page_name($location);
            let page_name = Models.page_name($location,[2]);
            let controller_type = Models.logged_user_type($rootScope);
            $scope.denied_page = function () {
                $rootScope.page_access = 0;
                $rootScope.denied_page_switch = 1;
                $rootScope.document_switch = 0;
                $rootScope.logged = 0;
                return false;
            };
            $scope.access_open = function () {
                $rootScope.page_access = 1;
                $rootScope.logged = 1;
                $rootScope.denied_page_switch = 0;
            };

            /// initiate document
            let logged_user_id = $rootScope.basic_info.logged_user_id;
            if (logged_user_id !== undefined){
                if (page === 'receipt' || page === 'application' || page === 'pending-application'){
                    $rootScope.document_switch = 1;
                }
                else{
                    $rootScope.document_switch = 0;
                }

                if (page_name === 'pos'){
                    $rootScope.shortcut_important_list_switch = 1;
                }
                else{
                    $rootScope.shortcut_important_list_switch = 0;
                }
            }
            let public_page = $scope.public_pages.indexOf(page_name);
            if (public_page !== -1){
                $scope.access_open();
                return false;
            }

            if (controller_type === "system_designer"){
                return ;
            }
            else{
                let software_info = basic_data.software_info;
                if (software_info !== undefined){
                    let construction_mode = software_info.construction_mode;
                    if (construction_mode !== undefined){
                        let status = construction_mode.active;
                        if (status){
                            $rootScope.construction_time = construction_mode.date +" "+ construction_mode.time;
                            // console.log($rootScope.construction_time);
                            if (Date.parse($rootScope.construction_time) > Date.now()) {
                                $location.path('/construction-mode');
                                let logged_user = $rootScope.basic_info.logged_user;
                                if (logged_user!==undefined){
                                    let access_menus = logged_user.access_menus;
                                    if (access_menus !== undefined){
                                        $rootScope.basic_info.logged_user.access_menus = [];
                                        service.main_menu_reload();
                                    }
                                }
                            }

                        }
                        else{
                            $rootScope.construction_time = null;
                        }
                    }
                }
            }
            if (page === "profile"){
                $rootScope.logged = 1;
                return ;
            }
            let user_access_menus = Models.user_access_menus($rootScope);
            let get_access_keys = [];

            if ($rootScope.basic_info !== undefined && user_access_menus !== undefined){
                for (let x of user_access_menus){
                    get_access_keys.push(Number(x.value));
                }
                Models.get_main_menus($http,$rootScope,function (main_menus) {
                    let menu_info = Models.custom_filter(main_menus,"link",page_name);
                    if (menu_info !== undefined){
                        let menu_id = menu_info.id;
                        let exist = get_access_keys.indexOf(menu_id);
                        if (exist === -1){
                            $scope.denied_page();

                        }
                        else{
                            $rootScope.page_access = 1;
                            $rootScope.logged = 1;
                            // $rootScope.denied_page_switch = 1;
                        }
                    }
                    // else if (menu_info === undefined) {
                    //     $scope.denied_page();
                    // }
                    else{
                        $scope.access_open();
                    }
                })
            }
            else if(user_access_menus === undefined){
                $scope.denied_page();
            }
        });


    };
    $scope.sidebar_manage = function(){
        let window_width = $(window).width();
        if (window_width <= 1220){
            altair_main_sidebar.hide_sidebar();
        }
    };

    $scope.$on('$routeChangeStart', function($event, next, current) {
        $scope.page_controller();
        $scope.sidebar_manage();
        Models.page_title_generator($location,$scope,$rootScope);
        /// destroy the before initiate data table for error handle of invoice/document page
        if ($( '#dt_tableHeeshab' ).dataTable !== undefined){
            let table = $( '#dt_tableHeeshab' ).dataTable().api();
            if (table.context.length){
                table.destroy();
            }
        }
        if (Models.intervals.length){
            for (let item of Models.intervals){
                clearInterval(item.id);
            }
        }

        Models.footer_remove();


    });
    $scope.$on('$routeChangeSuccess', function($event, next, current) {
        $("body").click();
        $rootScope.shortcut_important_list_switch = 0;
        $rootScope.document_switch = 0;
    });

}]);






// Shortcut important list like items,customer,invoices in secondary sidebar
app.controller("shortcut_important_list",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    $scope.shortcut_list_on = function () {
        altair_secondary_sidebar.init();
    }
}]);





// Help
app.controller("help",['$scope','$http','$rootScope','service','$location','$route','$timeout',function ($scope,$http,$rootScope,service,$location,$route,$timeout) {
    $timeout(function () {
        altair_md.video_player();
    });
}]);













































