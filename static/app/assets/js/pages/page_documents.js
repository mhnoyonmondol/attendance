let $document_card,
    $document_preview,
    $document_form,
    $documents_list_main,
    document_list_class, // main/sidebar list
    $invoice_add_btn;

altair_documents  = {
    init: function($http,$scope) {
        $document_card = $('#document'),
        $document_preview = $('#document_preview'),
        $document_form = $('#document_form'),
        $documents_list_main = $('#documents_list'),
        document_list_class = '.documents_list', // main/sidebar list
        $invoice_add_btn = $('#document_add');

        // add new invoice
        altair_documents.add_new($http,$scope);

        // open invoice
        let document_id = false;
        let url_document_id = $scope.route_params.document_id;
        if (url_document_id !== undefined){
            document_id = url_document_id;

        }
        altair_documents.open_document($http,$scope,document_id);

        // print invoice btn
        altair_documents.print_document();
        // search invoices
        // altair_documents.search_invoices();
    },
    add_new: function($http,$scope) {
        let invoice_calculation = null;
        if($invoice_add_btn) {

            let insert_form = function() {

                let $document_form_template = $('#document_form_template'),
                    card_height = $document_card.height(),
                    content_height = $document_card.find('.md-card-content').innerHeight(),
                    document_form_template_content = $document_form_template.html();
                    let context = {};
                    if ($scope.doc_type === "application" ){

                        context = {
                            currency:$scope.root.currency,
                            current_date:$scope.current_date,
                            start_date:$scope.current_date,
                            end_date:$scope.current_date,
                            document_type_option_name:$scope.document_sub_type_name,
                            document_type: $scope.doc_type,
                            logged_user_type:$scope.root.logged_user_type,
                        };
                    }
                    document_form_template_content = Handlebars.compile(document_form_template_content)(context);
                // remove "uk-active" class form invoices list
                $(document_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $document_card.height(card_height);

                $document_form
                    .hide()
                    // add form to card
                    .html(document_form_template_content)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // invoice due-date radio boxes
                altair_md.checkbox_radio();

            };


            // show invoice form on animation complete
            let show_form = function(call_back=function () {}) {
                $document_card.css({
                    'height': ''
                });
                $document_form.show();
                $document_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    call_back();
                },560); //2 x animation duration
            };

            // show invoice form on fab click event
            $invoice_add_btn.on('click', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
                altair_md.card_show_hide($document_card,insert_form,function () {
                    show_form(function () {
                        if ($scope.doc_type === "application"){
                            // institute selector
                            let designation = "";
                            let institute_id = "";
                            let logged_user = Models.logged_user($scope.root);
                            if (logged_user){
                                designation = logged_user.designation;
                                institute_id = logged_user.institute_id;
                            }
                            if ($scope.root.logged_user_type !== "teacher"){
                                Models.custom_selectize(".institute-selector",{
                                    searchField: ['label','value','member_id'],
                                    onInitialize:function () {
                                        let that = this;
                                        Models.get_document_save_institutes($scope.root,function (data) {
                                            let institutes = data;
                                            for (let institute_info of institutes){
                                                institute_info.label = institute_info.name;
                                                institute_info.value = institute_info.institute_id;
                                                that.addOption(institute_info);
                                            }
                                            if (!institutes.length){
                                                Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                                    // console.log(data);
                                                    if (data.find_data !== undefined){
                                                        for (let institute_info of data.find_data){
                                                            $scope.root.document_save_institutes.push(institute_info);
                                                            institute_info.label = institute_info.name;
                                                            institute_info.value = institute_info.institute_id;
                                                            that.addOption(institute_info);
                                                        }
                                                    }

                                                },["''","'"+designation+"'"]);
                                            }
                                        });
                                    },
                                    onType:function (value) {
                                        let that = this;
                                        if (!that.currentResults.total){
                                            Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                                // console.log(data);
                                                if (data.find_data !== undefined){
                                                    for (let institute_info of data.find_data){
                                                        $scope.root.document_save_institutes.push(institute_info);
                                                        institute_info.label = institute_info.name;
                                                        institute_info.value = institute_info.institute_id;
                                                        that.addOption(institute_info);
                                                    }
                                                    that.refreshOptions();
                                                }

                                            },["'"+value+"'","'"+designation+"'"]);
                                        }

                                    },
                                    onChange:function (value) {
                                        let user_selectize = $(".user-selector").get(0).selectize;
                                        user_selectize.clearOptions();
                                        Models.request_sender($http,"get","institute_teachers",function (data) {
                                            // console.log(data);
                                            if (data.find_data !== undefined){
                                                for (let user_info of data.find_data){
                                                    user_info.label = user_info.name;
                                                    user_info.value = user_info.user_id;
                                                    user_selectize.addOption(user_info);
                                                }
                                                user_selectize.refreshOptions();
                                            }

                                        },["'"+value+"'"]);
                                    }
                                });
                            }

                            // user selector
                            Models.custom_selectize(".user-selector",{
                                plugins: {
                                    'remove_button': {
                                        label: ''
                                    }
                                },
                                searchField: ['label','value','member_id'],
                                onInitialize:function () {
                                    let that = this;
                                    if (institute_id !== "" && institute_id !== null){
                                        let logged_user = Models.logged_user($scope.root);
                                        if (Models.is_logged_user_teacher($scope.root)){
                                            let user_info = {
                                                label:logged_user.name,
                                                value:logged_user.user_id,
                                            };
                                            that.addOption(user_info);
                                            that.setValue(logged_user.user_id);
                                        }
                                        else{
                                            Models.request_sender($http,"get","institute_teachers",function (data) {
                                                // console.log(data);
                                                if (data.find_data !== undefined){
                                                    for (let user_info of data.find_data){
                                                        user_info.label = user_info.name;
                                                        user_info.value = user_info.user_id;
                                                        that.addOption(user_info);
                                                    }
                                                    that.refreshOptions();
                                                }

                                            },["'"+institute_id+"'"]);
                                        }

                                    }


                                },
                            });
                            let options = {
                                type:$scope.document_sub_type+"_application_tag",

                            };
                            Models.tag_chooser($http,$scope,function (element) {

                            },options);
                        }

                    });
                },undefined);
                Models.init_requirement();

            });

        }
    },
    edit: function($http,$scope,invoice_id) {
        let invoice_calculation = null;
        Models.get_document($http,$scope,invoice_id,function (data) {
            let document_info = data;
            let insert_form = function() {


                let $document_form_template = $('#document_form_template'),
                    card_height = $document_card.height(),
                    content_height = $document_card.find('.md-card-content').innerHeight(),
                    document_form_template_content = $document_form_template.html();
                let context = {};
                if ($scope.doc_type === "application" ){

                    context = {
                        currency:$scope.root.currency,
                        current_date:$scope.current_date,
                        document_type_option_name:$scope.document_sub_type_name,
                        document_type: $scope.doc_type,
                        logged_user_type:$scope.root.logged_user_type,
                        edit:true

                    };
                    Object.assign(context,data);
                }
                document_form_template_content = Handlebars.compile(document_form_template_content)(context);
                // remove "uk-active" class form invoices list
                $(document_list_class).find('.md-list-item-active').removeClass('md-list-item-active');
                // set height for card-single
                $document_card.height(card_height);

                $document_form
                    .hide()
                    // add form to card
                    .html(document_form_template_content)
                    // set height for card content
                    .find('.md-card-content').innerHeight(content_height);

                // invoice due-date radio boxes
                altair_md.checkbox_radio();

            };


            // show invoice form on animation complete
            let show_form = function(call_back=function () {}) {
                $document_card.css({
                    'height': ''
                });
                $document_form.show();
                $document_preview.html('');
                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                    call_back();
                },560); //2 x animation duration
            };

            altair_md.card_show_hide($document_card,insert_form,function () {
                show_form(function () {
                    if ($scope.doc_type === "application"){
                        // institute selector
                        let designation = "";
                        let institute_id = "";
                        let logged_user = Models.logged_user($scope.root);
                        if (logged_user){
                            designation = logged_user.designation;
                            institute_id = logged_user.institute_id;
                        }
                        if ($scope.root.logged_user_type !== "teacher"){
                            Models.custom_selectize(".institute-selector",{
                                searchField: ['label','value','member_id'],
                                onInitialize:function () {
                                    let that = this;
                                    Models.get_document_save_institutes($scope.root,function (data) {
                                        let institutes = data;
                                        for (let institute_info of institutes){
                                            institute_info.label = institute_info.name;
                                            institute_info.value = institute_info.institute_id;
                                            that.addOption(institute_info);
                                        }
                                    });
                                    /// before institute id
                                    let before_institute_id = document_info.institute_id;
                                    Models.request_sender($http,"get","institute_info",function (institute_info) {
                                        if (institute_info.institute_id !== undefined){
                                            $scope.root.document_save_institutes.push(institute_info);
                                            institute_info.label = institute_info.name;
                                            institute_info.value = institute_info.institute_id;
                                            that.addOption(institute_info);
                                            that.setValue(before_institute_id);

                                        }
                                    },["'"+before_institute_id+"'"])
                                },
                                onType:function (value) {
                                    let that = this;
                                    if (!that.currentResults.total){
                                        Models.request_sender($http,"get","estimate_applicable_institutes",function (data) {
                                            // console.log(data);
                                            if (data.find_data !== undefined){
                                                for (let institute_info of data.find_data){
                                                    $scope.root.document_save_institutes.push(institute_info);
                                                    institute_info.label = institute_info.name;
                                                    institute_info.value = institute_info.institute_id;
                                                    that.addOption(institute_info);
                                                }
                                                that.refreshOptions();
                                            }

                                        },["'"+value+"'","'"+designation+"'"]);
                                    }

                                },
                                onChange:function (value) {
                                    let user_selectize = $(".user-selector").get(0).selectize;
                                    user_selectize.clearOptions();
                                    Models.request_sender($http,"get","institute_teachers",function (data) {
                                        // console.log(data);
                                        if (data.find_data !== undefined){
                                            for (let user_info of data.find_data){
                                                user_info.label = user_info.name;
                                                user_info.value = user_info.user_id;
                                                user_selectize.addOption(user_info);
                                            }
                                            user_selectize.refreshOptions();
                                            for(let id of document_info.teacher_ids){
                                                user_selectize.addItem(id);
                                            }
                                        }

                                    },["'"+value+"'"]);
                                }
                            });
                        }

                        // user selector
                        Models.custom_selectize(".user-selector",{
                            plugins: {
                                'remove_button': {
                                    label: ''
                                }
                            },
                            searchField: ['label','value','member_id'],
                            onInitialize:function () {
                                let that = this;
                                if (institute_id !== "" && institute_id !== null){
                                    let logged_user = Models.logged_user($scope.root);
                                    if (Models.is_logged_user_teacher($scope.root)){
                                        let user_info = {
                                            label:logged_user.name,
                                            value:logged_user.user_id,
                                        };
                                        that.addOption(user_info);
                                        that.setValue(logged_user.user_id);
                                    }
                                    else{
                                        Models.request_sender($http,"get","institute_teachers",function (data) {
                                            // console.log(data);
                                            if (data.find_data !== undefined){
                                                for (let user_info of data.find_data){
                                                    user_info.label = user_info.name;
                                                    user_info.value = user_info.user_id;
                                                    that.addOption(user_info);
                                                }
                                                that.refreshOptions();
                                                for(let id of document_info.teacher_ids){
                                                    that.addItem(id);
                                                }
                                            }

                                        },["'"+institute_id+"'"]);
                                    }

                                }


                            },
                        });
                        let options = {
                            type:$scope.document_sub_type+"_application_tag",
                            default_value:data.application_tag_id

                        };
                        Models.tag_chooser($http,$scope,function (element) {

                        },options);
                    }

                });
            },undefined);
            Models.init_requirement();
        });

    },


    open_document: function($http,$scope,document_id=false) {

        let show_document = function(element) {
            let $this = element;
            let invoice_id =$this.attr('data-document-id');
            Models.get_document($http,$scope,invoice_id,function (data) {

                // is the document corner dropdown menu is visible
                let total_drop_down_list = 0;
                data.delete = 1;
                if (data.delete){
                    total_drop_down_list++;
                }
                data.edit = 1;
                if (data.edit){
                    total_drop_down_list++;
                }

                // if document sub type leave or training
                if ($scope.document_sub_type === "leave" || $scope.document_sub_type === "training"){
                    data.authorized = Number(data.authorized);
                    if (data.authorize_permission){
                        total_drop_down_list++;
                        if (data.authorized){
                            total_drop_down_list--;
                        }
                    }

                }

                data.total_drop_down_list = total_drop_down_list;
                let document_type = "Application";
                data.document_name = document_type;
                data.invoice_date = moment(data.time).format("MMMM Do YYYY");
                data.start_date = moment(data.start_date).format("MMMM Do YYYY");
                data.end_date = moment(data.end_date).format("MMMM Do YYYY");
                // console.log(data);
                let $invoice_template = $('#document_template');
                let template_ht = $invoice_template.html();

                template_ht = Handlebars.compile(template_ht)(data);
                $document_preview.html(template_ht);
                $document_form.html('');

                $window.resize();

                setTimeout(function() {
                    // reinitialize uikit margin
                    altair_uikit.reinitialize_grid_margin();
                },290);

                $document_preview.data(data);
            });

        };

        $(document_list_class)
            .on('click','a', function(e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                // toggle card and show invoice
                altair_md.card_show_hide($document_card,undefined,show_document,$(this));
                // set active class
                $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');
            });


        if($(document_list_class).find('a').length) {
            // open first invoice
            if (document_id){
                $(document_list_class).find("a[data-document-id='"+document_id+"']").eq(0).click();
            }
            else{
                $(document_list_class).find('a').eq(0).click();
            }

        } else {
            if ($invoice_add_btn.length){
                // open form
                $invoice_add_btn.trigger('click');
            }
            else{
                altair_documents.open_empty_document($http,$scope);
            }

        }
        $("#document").on("click",".document-edit",function (e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            let document_id = $(this).attr("data-id");
            altair_documents.edit($http,$scope,document_id);
        });

    },
    open_empty_document: function($http,$scope) {
        let show_document = function(element) {
            let $this = element;
            let invoice_id =$this.attr('data-document-id');
            let $invoice_template = $('#empty_document');
            let template_ht = $invoice_template.html();

            template_ht = Handlebars.compile(template_ht)({});
            $document_preview.html("");
            $document_form.html(template_ht);

            $window.resize();

            setTimeout(function() {
                // reinitialize uikit margin
                altair_uikit.reinitialize_grid_margin();
            },290);
        };

        // toggle card and show invoice
        altair_md.card_show_hide($document_card,undefined,show_document,$(this));
        // set active class
        $(this).closest('li').siblings('li').removeClass('md-list-item-active').end().addClass('md-list-item-active');


    },

    print_document: function() {
        $body.on('click','#document_print',function(e) {
            e.preventDefault();
            e.stopImmediatePropagation();
            Models.confirm(function () {
                // hide sidebar
                altair_main_sidebar.hide_sidebar();
                // wait for dialog to fully hide
                setTimeout(function () {
                    window.print();
                }, 300)
            },"Do you want to print this?")
        })
    },
    copy_list_sidebar: function($scope,call_back=function () {}) {
        let $sidebar_secondary = $('#sidebar_secondary'),
            $sidebar_secondary_toggle = $('#sidebar_secondary_toggle');
        // hide secondary sidebar toggle btn for large screens
        $sidebar_secondary_toggle.addClass('uk-hidden-large');

        let documents_list_sidebar = $documents_list_main.clone();

        documents_list_sidebar.attr('id','documents_list_sidebar');

        $sidebar_secondary
            .find('.sidebar_secondary_wrapper').html(documents_list_sidebar)
            .end();
        let secondary_selectize = $sidebar_secondary.find("select.document-sub-types");
        secondary_selectize.removeClass("selectized");
        let default_option = secondary_selectize.val();
        secondary_selectize.next(".selectize-control").remove();
        $scope.document_type_selector($scope.document_sub_type);
        // $scope.$apply(function () {
        //     $scope.document_type_selector();
        // });
        altair_secondary_sidebar.init();
        $sidebar_secondary_toggle.attr("sidebar-active",true);
        call_back();

    },
};