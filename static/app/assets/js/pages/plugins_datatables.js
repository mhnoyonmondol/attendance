$(function() {
    // datatables
    // altair_datatables.dt_default();
    // altair_datatables.dt_scroll();
    // altair_datatables.dt_individual_search();
    // altair_datatables.dt_colVis();
    // altair_datatables.dt_tableExport();
});

altair_datatables = {
    dt_default: function() {
        var $dt_default = $('#dt_default');
        if($dt_default.length) {
            $dt_default.DataTable();
        }
    },
    dt_scroll: function() {
        var $dt_scroll = $('#dt_scroll');
        if($dt_scroll.length) {
            $dt_scroll.DataTable({
                "scrollY": "200px",
                "scrollCollapse": false,
                "paging": false
            });
        }
    },
    dt_individual_search: function() {
        var $dt_individual_search = $('#dt_individual_search');
        if($dt_individual_search.length) {

            // Setup - add a text input to each footer cell
            $dt_individual_search.find('tfoot th').each( function() {
                var title = $dt_individual_search.find('tfoot th').eq( $(this).index() ).text();
                $(this).html('<input type="text" class="md-input" placeholder="' + title + '" />');
            } );

            // reinitialize md inputs
            altair_md.inputs();

            // DataTable
            var individual_search_table = $dt_individual_search.DataTable();

            // Apply the search
            individual_search_table.columns().every(function() {
                var that = this;

                $('input', this.footer()).on('keyup change', function() {
                    that
                        .search( this.value )
                        .draw();
                } );
            });

        }
    },
    dt_colVis: function() {
        var $dt_colVis = $('.dt_colVis'),
            $dt_buttons = $dt_colVis.prev('.dt_colVis_buttons');

        if($dt_colVis.length) {

            // init datatables
            var colVis_table = $dt_colVis.DataTable({
                buttons: [
                    {
                        extend: 'colvis',
                        fade: 0
                    }
                ]
            });

            colVis_table.buttons().container().appendTo( $dt_buttons );

        }
    },
    dt_tableExport: function() {
        var $dt_tableExport = $('.dt_tableExport'),
            $dt_buttons = $dt_tableExport.prev('.dt_colVis_buttons');

        if($dt_tableExport.length) {
            var table_export = $dt_tableExport.DataTable({
                buttons: [
                    {
                        extend:    'copyHtml5',
                        text:      '<i class="uk-icon-files-o"></i> Copy',
                        titleAttr: 'Copy'
                    },
                    {
                        extend:    'print',
                        text:      '<i class="uk-icon-print"></i> Print',
                        titleAttr: 'Print'
                    },
                    {
                        extend:    'excelHtml5',
                        text:      '<i class="uk-icon-file-excel-o"></i> XLSX',
                        titleAttr: ''
                    },
                    {
                        extend:    'csvHtml5',
                        text:      '<i class="uk-icon-file-text-o"></i> CSV',
                        titleAttr: 'CSV'
                    },
                    {
                        extend:    'pdfHtml5',
                        text:      '<i class="uk-icon-file-pdf-o"></i> PDF',
                        titleAttr: 'PDF'
                    },

                ]
            });

            table_export.buttons().container().appendTo( $dt_buttons );

        }
    },
    dt_tableHeeshab: function(options={},table=null) {
        let printCounter = 0;
        var $dt_tableExport = $('#dt_tableHeeshab'),
            $dt_buttons = $dt_tableExport.prev('.dt_colVis_buttons');
        if (table){
            $dt_tableExport = $(table);
        }
        if($dt_tableExport.length) {
            let default_options = {
                pressExport:function(){},
                onTableInit:function(){},
                buttons: [
                    {
                        extend: 'colvis',
                        fade: 0,
                        text:      '<i class="material-icons" data-uk-tooltip="" title="Column selector">view_module</i>',
                    },
                    {
                        extend:    'copyHtml5',
                        text:      '<i class="uk-icon-files-o" data-uk-tooltip="" title="Copy"></i>',
                        exportOptions: {
                            columns: ':visible'
                        }

                    },
                    {
                        extend:    'print',
                        text:      '<i class="uk-icon-print" data-uk-tooltip="" title="Print"></i>',
                        titleAttr: 'Print',
                        exportOptions: {
                            columns: ':visible'
                        },
                        title:'',
                        customize:function (export_winow) {
                            let export_header = $(".export-header");
                            if (export_header.length){
                                export_header = export_header.removeClass("uk-hidden");
                                $(export_winow.document).find("div").html(export_header);
                            }
                            let export_footer = $(".export-footer");
                            if (export_footer.length){
                                export_footer = export_footer.removeClass("uk-hidden");
                                $(export_winow.document).find("table").after(export_footer);
                            }

                        },
                        // autoPrint:false
                    },
                    // {
                    //     text: '<i class="material-icons" data-uk-tooltip="" title="Export">save_alt</i>',
                    //     action: function ( e, dt, node, config ) {
                    //         default_options.pressExport(e, dt, node, config);
                    //     }
                    // }
                ],
                initComplete:function () {
                    let table = this.api();
                    let button_container = table.buttons().container();
                    button_container.appendTo( $dt_buttons );
                    default_options.tableReady(table);
                    default_options.onTableInit(table);

                },
                tableReady:function () {

                }
            };
            Object.assign(default_options,options);
            var table_export = $dt_tableExport.DataTable(default_options);
            table_export.table_container = $dt_tableExport;
            return table_export;

        }
    },



};