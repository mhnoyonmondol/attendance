let Models = {};
/// requests and responses for loading image
let requests = 0;
let responses = 0;

Models.intervals = [];

Models.custom_filter = function(obj,key_name,value,list=false,negetive=false){
    let find_item=obj.filter(function(item){
        if (negetive) {
            if (item[key_name] === undefined){
                return true;
            }
            else{
                return item[key_name]!==value;
            }

        }
        else{
            if (item[key_name] === undefined){
                return false;
            }
            else{
                return item[key_name]===value;
            }

        }

    });
    if(list){
        return find_item;
    }
    else{
        for(let x in find_item){
            return find_item[x];
        }
    }
};

Models.filter_in_array = function(filter_list,filter_keys,list=false,negative=false){
    let keys_list = Object.keys(filter_keys);
    let total_keys = keys_list.length;
    let find_item=filter_list.filter(function(item){
        let true_count = 0;
        if (negative) {
            for (let key in filter_keys){
                let value = filter_keys[key];
                if (item[key] === undefined){
                    true_count++;

                }
                else{
                    if (item[key]!==value){
                        true_count++;
                    }
                }
            }

        }
        else{
            for (let key in filter_keys){
                let value = filter_keys[key];
                if (item[key] !== undefined){
                    if (item[key]===value){
                        true_count++;
                    }
                }
            }

        }

        if (true_count === total_keys){
            return true;
        }
        else{
            return false;
        }

    });
    if(list){
        return find_item;
    }
    else{
        if (!find_item.length){
            return null;
        }
        else{
            return find_item[0];
        }
    }
};
Models.unique_checker = function (obj_list,checker_obj){
    let find_item=obj_list.filter(function(item){
        let result = true;
        for(let key in item){
            if (key !== "$$hashKey"){
                let value = item[key];
                let checker_value = checker_obj[key];
                if (checker_value === undefined){
                    result = false;
                }
                else{
                    if (checker_value !== value){
                        result =  false;
                    }
                }
            }
        }

        return result;
    });
    let unique = true;
    if (find_item !== undefined){
        if (find_item.length){
            unique = false;
        }
    }
    return unique;
};




Models.not_found_message = function(message="Not found"){
    var html="<div class='md-card'>";
        html+="<div class='md-card-content'>";
        html+="<h2 class='not-found-message'>"+message+"</h2>";
        html+="</div>";
        html+="</div>";
    return html;

};
Models.common_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Common request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"asset"
        };
    }
    return extra_data;

};
Models.retrive_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Retrieve request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"get"
        };
    }
    return extra_data;

};

Models.passed_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Passed request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"post"
        };
    }
    return extra_data;

};
Models.modify_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Modify request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"update"
        };
    }
    return extra_data;

};
Models.other_request_object = function(function_name=''){
    let extra_data = false;
    if(function_name === ''){
        console.log('Other request function name empty')
    }
    else{
        extra_data = {
            request_name: function_name,
            request_type:"other"
        };
    }
    return extra_data;

};


//processing image on
Models.loading_on = function(){
    if (requests < 1){
        altair_helpers.content_preloader_show('regular');
    }
    requests++;
    // When any form submit first disabled all button in form element
    $("form button").addClass("uk-disabled");
};
Models.loading_off = function (){
    responses++;
    if (requests === responses){
        altair_helpers.content_preloader_hide();
        requests = 0;
        responses = 0;
    }
    //remove disabled all button in form element
    $("form button").removeClass("uk-disabled");
};
Models.small_loading = function(elem,this_switch = 1,result="",size=24){
    if (this_switch){
        let html = "";
        html = "<img src='static/app/assets/img/spinners/spinner_small.gif' width='"+size+"' height='"+size+"'>";
        $(elem).html(html).addClass("uk-disabled")
    }
    else{
        $(elem).html(result).removeClass("uk-disabled");
    }
};

Models.clean = function(form_id) {
    document.getElementById(form_id).reset();
};
Models.modal_close = function () {
    $(".uk-modal-close").click();
};

Models.redirect = function (page_name) {
    window.location=path+page_name;
};
Models.notify = function (message='Changed has been saved',pos="top-center",timeout="5000") {
    UIkit.notify({
        message: message,
        status:  '',
        timeout: timeout,
        group: null,
        pos: pos,
        onClose: function() {

        }
    });

};

Models.have = function (element){
    if($(element).length>0){
        return true;
    }
    else{
        return false;
    }
};
Models.response = function (data) {
    UIkit.modal.info(data);
};
Models.get_form_data = function (form_name) {
    var form_data=new FormData($(form_name)[0]);
    return form_data;

};

Models.confirm = function(action, message = "Are you sure?"){
    Models.confirm_positive(message,function () {
        action();
    });
};

Models.confirm_positive = function(content="Are you sure?", onconfirm, oncancel) {
    let UI = UIkit;
    var options = arguments.length > 1 && arguments[arguments.length-1] ? arguments[arguments.length-1] : {};

    onconfirm = UI.$.isFunction(onconfirm) ? onconfirm : function(){};
    oncancel  = UI.$.isFunction(oncancel) ? oncancel : function(){};
    options   = UI.$.extend(true, {bgclose:false,stack:true, keyboard:true, modal:true, labels:UI.modal.labels}, UI.$.isFunction(options) ? {}:options);
    options.labels.Ok = "Ok";
    var modal = UI.modal.dialog(([
        '<div class="uk-margin uk-modal-content">'+String(content)+'</div>',
        '<div class="uk-modal-footer uk-text-right"><button class="uk-button js-modal-confirm">'+options.labels.Ok+'</button> <button class="uk-button uk-button-primary js-modal-confirm-cancel">'+options.labels.Cancel+'</button></div>'
    ]).join(""), options);

    modal.element.find(".js-modal-confirm, .js-modal-confirm-cancel").on("click", function(){
        UI.$(this).is('.js-modal-confirm') ? onconfirm() : oncancel();
        modal.hide();
    });
    // modal.on('show.uk.modal', function(){
    //     console.log("dkfjd");
    //     setTimeout(function(){
    //         modal.element.find('.js-modal-confirm').focus();
    //     }, 500);
    // });

    return modal.show();
};

// using jQuery
Models.getCookie = function (name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
};
Models.csrftoken = Models.getCookie('csrftoken');

Models.request = function (form_name,call_back,loading=true,extra_data=[],extra_images=[],fancy_tree=[]){
    var form_data=Models.get_form_data(form_name);
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }
    var tree_keys=Object.keys(fancy_tree);
    if(tree_keys.length>0){

        for(item in fancy_tree){    $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);

        }
        form_data=Models.get_form_data(form_name);
    }
    for(item in extra_data){
        var field_name=extra_data[item].name;
        var value=extra_data[item].value;
        form_data.append(field_name,value);

    }
    let token = form_data.get("csrfmiddlewaretoken");
    if (!token){
        if(form_name === ""){
            form_data.append("csrfmiddlewaretoken",Models.csrftoken);
        }
        else {
            // form_data.append("csrfmiddlewaretoken",document.getElementsByName('csrfmiddlewaretoken')[0].value);
        }

    }

    if(loading){
       Models.loading_on();
       }

    $.ajax({
        url:"/control/request/",
        data:form_data,
        type:"POST",
        processData: false,
        contentType: false,
        // dataType:"json",
        success:function(data,status){
            if (loading) {
                Models.loading_off();
            }
            call_back(data,status);
        },
        complete:function (xhr,status) {
            if(status != "success"){
                let error=xhr.responseText;
                console.log(error);
            }
        }

    });
};

Models.form_data_maker = function (form_element_name,extra_data={},extra_images=[],fancy_tree=[],data_list=[]) {
    // form name format like ("element")/ like jquery selector
    var form_data=Models.get_form_data(form_element_name);

    // Image an array []
    //format is array = [file_input_id_name]
    //example <input type='file' id='image'> array key is (image)
    for(item in extra_images){
        form_data.append(extra_images[item],document.getElementById(extra_images[item]).files[0]);
    }

    // tree make follow the fancytree rules
    //array format [{},{}]
    // tree = [
    //      {
    //         element_name:".categories.fancytree_radio" ,
    //         selected_name:"category" ,
    //         active_name:"parent"
    //      }
    //  ];
    //
    //
    for(item in fancy_tree){
        $(fancy_tree[item].element_name).fancytree("getTree").generateFormElements(fancy_tree[item].selected_name,fancy_tree[item].active_name);
    }
    form_data=Models.get_form_data(form_element_name);

    // extra data followed by object = {name:"xxxx",age:"xxx",other:"xxxx"}
    for(var item in extra_data){
        var field_name= item;
        var value=extra_data[item];
        form_data.append(field_name,value);
    }
    //csrf token for python security
    // form_data.append("csrfmiddlewaretoken",Models.csrftoken);
    // for(x of form_data.values()){
    //     console.log(x);
    // }


    //data list as form data
    for (let item of data_list){
        let name = item.name;
        let value = item.value;
        let file = item.file;
        if (file === undefined){
            form_data.append(name,value)
        }
        else{
            form_data.append(name,value,file)
        }

    }

    return form_data;

};

//// angular js support
Models.active_button = function (){
    return $("button[active='true']").attr('data-button-name');
};

Models.add_action = function(response,$location,url=""){
     status = response.status;
    if (status){
        ///First notify a message top bar
        let message = response.messages;
            message = Models.notify_message(message);
        let active_button = Models.active_button();
        if(active_button === 'save_and_new'){
            Models.clean("form");
            Models.update_inputs();
        }
        // If button name save, page redirect to profile page
        else{
            if (url !== ""){
                $location.path(url);
            }
            else{
                if ($("form").length){
                    if(active_button === 'save_and_new'){
                        Models.clean("form");
                        Models.update_inputs();
                    }
                }
            }

        }
    }
};

Models.informer = function(data){
    if(data.status !== undefined) {
        // if (!data.status) {
        //     let messages = data.messages;
        //     let errors = data.errors;
            let messages = Models.unique_array(data.messages);
            let errors = Models.unique_array(data.errors);

            let m_length = messages.length;
            let e_length = errors.length;
            let info_html = "";
            // If message available
            if(!data.status){
                if (m_length) {
                    let message_html = "<h3>Message</h3>";
                    for (let message in messages) {
                        message_html += "<p>" + messages[message] + "</p>";
                    }
                    info_html += message_html;
                }
            }
            // If error available
            if (e_length) {
                let error_html = "<h3>Error</h3>";
                for (let error in errors) {
                    error_html += "<p>" + errors[error] + "</p>";
                }
                info_html += error_html;
                console.log(data)
            }
            if(info_html){
                Models.smart_informer(1,info_html);
            }

        // }

    }
};
Models.server_response = function(data){
    let html = "<h3>Server response</h3>";
    html += data;
    Models.smart_informer(1,html);
};

Models.is_number = function(str){
    let result = false;
    let check = str.toString().search(/^[+-]?(?=.)(?:\d+,)*\d*(?:\.\d+)?$/);
    if (check === 0){
        result = true;
    }
    return result;
};
Models.is_multiple_form_key = function(str){
    let result = false;
    let check = str.toString().search(/^.+[\[][\]]$/);
    if (check === 0){
        result = true;
    }
    return result;
};

Models.$http = function($http,data,call_back,loading = true){
    if(loading){
        Models.loading_on();
    }
    if (Models.connection){
        //// if bangla language support all bangla number convert into english
        let bangla_switch = Models.bangla_switch();
        if (bangla_switch){
            let entries = data.entries();
            let multiple_entries = [];
            for (let entry of entries){
                let key = entry[0];
                let value = entry[1];
                let b2e = Models.number_conversion(value,"b2e");
                if (Models.is_number(b2e)){
                    if (Models.is_multiple_form_key(key)){
                        multiple_entries.push({
                            key: key,
                            value: b2e
                        });
                    }
                    else{
                        data.set(key,b2e);
                    }

                }
                else{
                    if (Models.is_multiple_form_key(key)){
                        multiple_entries.push({
                            key: key,
                            value: value
                        });
                    }
                }

            }
            if (multiple_entries.length){
                let unique_multiple_entries = Models.unique_array(multiple_entries);
                for (let entry of unique_multiple_entries){
                    data.delete(entry.key);
                }
                for (let entry of multiple_entries){
                    data.append(entry.key,entry.value);
                }

            }
        }
        // for (let entry of data.entries()) {
        //     let key = entry[0];
        //     let value = entry[1];
        //     console.log(key+": "+value);
        // }

        // When connection is on
        $http({
            method : "POST",
            url : "request",
            data : data,
            headers: { 'Content-Type': undefined}
        }).then(
            function (response) {
                let data = response.data;
                let status = response.status;
                if (loading){
                    Models.loading_off();
                }
                Models.informer(data);
                call_back(data,status);
                if (typeof data !== "object"){
                    Models.server_response(data);
                }
            },
            function(response) {
                let data = response.data;
                if (data === undefined){
                    data = response;
                }
                if (loading){
                    Models.loading_off();
                }
                if (typeof data !== "object"){
                    Models.server_response(data);
                }
            })
    }
    else{


    }
};

Models.notify_message = function(messages){
    let m_length = messages.length;
    let info_html = "";
    // If message available
    if(m_length){
        let message_html = '';
        for (let message in messages) {
            message_html += "<p>"+messages[message]+"</p>";
        }
        info_html += message_html;
        Models.notify(info_html,"top-center",5000)
    }
};
Models.update_inputs = function(){
    $("input,.md-input").each(function () {
        altair_md.update_input($(this));
    });

};
Models.init_requirement = function(){
    altair_md.inputs();
    altair_forms.switches();
    altair_md.checkbox_radio();
    altair_md.fab_toolbar();
    Models.update_inputs();
    // Models.dropify();
};

Models.dropify = function(options={},elem='.dropify'){
  $(elem).dropify(options);
};

Models.demo_countries_init = function(element = ".countries"){
    let country_selectize = Models.custom_selectize(element)
};

Models.drug_drop_upload = function(params,call_back,options={}) {
    var progressbar = $("#file_upload-progressbar"),
        bar         = progressbar.find('.uk-progress-bar'),
        settings    = {
            action: 'request', // Target url for the upload
            allow : '*.(json)', // File filter  format like jpg|jpeg|gif|png
            params : params,
            loadstart: function() {
                bar.css("width", "0%").text("0%");
                progressbar.removeClass("uk-hidden");

            },
            progress: function(percent) {
                percent = Math.ceil(percent);
                bar.css("width", percent+"%").text(percent+"%");
            },
            allcomplete: function(response,xhr) {

                bar.css("width", "100%").text("100%");
                setTimeout(function(){
                    progressbar.addClass("uk-hidden");
                }, 250);
                setTimeout(function() {
                    UIkit.notify({
                        message: "Upload Completed",
                        pos: 'top-right'
                    });
                },280);
                try {
                    let data = JSON.parse(response);
                    Models.informer(data);
                    call_back(data,status);
                }catch (e) {
                    Models.server_response(response);
                }


            }
        };
        $.extend(settings,options);

    var select = UIkit.uploadSelect($("#file_upload-select"), settings),
        drop   = UIkit.uploadDrop($("#file_upload-drop"), settings);
};

Models.find_index = function (docs,key,value) {
  let index_number=docs.findIndex(x => x[key] === value);
  return index_number;
};

Models.search_in_array = function(array,keyword){
    let result = array.filter(item => item.indexOf(keyword) > -1);
    return result;
};
Models.menu_maker = function (parent_id,main_menus,include=false){
    let menu_list = [];
    let menus = Models.custom_filter(main_menus,"parent_id",parent_id,true);
    for (let x in menus){
        let item = menus[x];
        let menu_id = item.id;
        let exist = true;
        if (include){
            let possible = include.indexOf(menu_id);

            if (possible !== -1 || item.menu_type === 'multiple'){
                exist = true;
            }
            else{
                exist = false;
            }
        }
        if (exist){
            item['menus'] = Models.menu_maker(menu_id,main_menus,include);
            menu_list.push(item);
        }

    }
    return menu_list;
};
Models.get_main_menus = function($http,$rootScope,call_back){
    if ($rootScope.main_menus !== undefined){
        call_back($rootScope.main_menus);
    } else{
        Models.retrieve_data($http,"main_menus",{},function (data,status) {
            if (data.find_data !== undefined){
                $rootScope.main_menus = data.find_data;
                call_back(data.find_data);

            }
            else{
                console.info('data not found');
            }
        },{},true);
    }
};
Models.user_menu_init = function($http,$rootScope,call_back){
    Models.get_basic_info($http,$rootScope,function () {
        Models.get_main_menus($http,$rootScope,function (data) {
            let controller_type = Models.logged_user_type($rootScope);
            let include = false;
            if (controller_type === "system_admin"){
                let access_menus = Models.user_access_menus($rootScope);
                let access_keys = [];
                for (access_menu of access_menus){
                    access_keys.push(Number(access_menu.value));
                }
                include = access_keys;
            }
            let main_menus = data;
            if (main_menus !== undefined && $rootScope.basic_info.logged_user_id !==undefined){
                let parent_menus = Models.custom_filter(main_menus,"parent_id",0,true);

                let menu_list = [];
                for (let x in parent_menus){
                    let item = parent_menus[x];
                    let menu_id = item.id;
                    let new_menus = Models.menu_maker(menu_id,main_menus,include);
                    if(new_menus.length || item.menu_type === 'single'){
                        item['menus'] = new_menus;
                        if (Models.logged_user_type($rootScope) === "system_admin"){
                            if (item.menu_type === 'single' && Models.main_menu_right(menu_id,$rootScope)){
                                menu_list.push(item);
                            }
                            else if (item.menu_type !== 'single') {
                                menu_list.push(item);
                            }
                        }
                        else{
                            menu_list.push(item);
                        }


                    }
                }
                call_back(menu_list);
            }

        });
    });

};

Models.tags_maker = function(element,options = {}){
    $(element).not("input.selectized,.multi.tags,.plugin-remove_button,.selectize-dropdown,.selectize-control").each(function () {
        let default_options = {
            delimiter: ',',
            persist: true,
            valueField: 'value',
            labelField: 'label',
            searchField: ['label','value','mobile'],
            create: function(input) {
                return {
                    value: input,
                    label: input
                }
            },
            onDropdownOpen: function($dropdown) {
                $dropdown
                    .hide()
                    .velocity('slideDown', {
                        begin: function() {
                            $dropdown.css({'margin-top':'0'})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    });

            },
            onDropdownClose: function($dropdown) {
                $dropdown
                    .show()
                    .velocity('slideUp', {
                        complete: function() {
                            $dropdown.css({'margin-top':''})
                        },
                        duration: 200,
                        easing: easing_swiftOut
                    })
            },
        };
        $.extend(default_options,options);
        $(this).selectize(default_options);
    });
};
Models.custom_selectize = function(element,options = {},element_type=false /* just name or html object*/){
    if(element_type){
        var available=Models.have(element,true);
    }
    else{
        var available=Models.have(element);
    }
    if(available){
        if(element_type){
            var this_elem=element;
        }
        else{
            var this_elem=$(element);
        }
        // console.log(this_elem.)
        this_elem.not("select.selectized,.plugin-remove_button,.selectize-dropdown,.selectize-control").each(function () {
            let default_options = {
                plugins: {
                    // 'remove_button': {
                    //     label: ''
                    // }
                },
                valueField: 'value',
                labelField: 'label',
                searchField: ['label','value','member_id','item_code','mobile'],
                hideSelected: true,
                onDropdownOpen: function($dropdown) {
                    $dropdown
                        .hide()
                        .velocity('slideDown', {
                            begin: function() {
                                $dropdown.css({'margin-top':'0'})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        });

                },
                onDropdownClose: function($dropdown) {
                    $dropdown
                        .show()
                        .velocity('slideUp', {
                            complete: function() {
                                $dropdown.css({'margin-top':''})
                            },
                            duration: 200,
                            easing: easing_swiftOut
                        })
                },
                heightAdjust:function (instance) {
                    let height_of_items = (instance.$control).height();
                    $(instance.$wrapper).height(height_of_items - 4);

                    $(window).resize(function () {
                        let height_of_items = (instance.$control).height();
                        $(instance.$wrapper).height(height_of_items - 4);
                    })
                }

            };
            $.extend(default_options,options);
            $(this).selectize(default_options);
        });
    }
    else{
        console.log("Not exist select element!");
    }
};
Models.selectize_height_reinitialized = function(instance){
    instance.settings.heightAdjust(instance);
};


Models.get_countries = function ($http,call_back) {
    if(Models.countries.length){
        let categories = Models.countries;
        call_back(categories);
    }
    else{
        let function_name = "countries()";
        let extra_data = Models.retrive_request_object(function_name);
        let form_data = Models.form_data_maker("",extra_data);

        Models.$http($http,form_data,function (response) {
            call_back(response.find_data);
        })
    }
};

Models.countries_init = function ($http,call_back=function () {}) {
    Models.get_countries($http,function (countries) {
        Models.countries = countries;
        Models.custom_selectize(".country-code-selector",{
            onInitialize:function () {
                for (let x in countries){
                    let item = countries[x];
                    let country_code = item.callingCodes[0];
                    let label = item.alpha2Code+" ("+country_code+")";
                    let value = country_code;
                    let opt_ob = {label: label, value: value};
                    this.addOption(opt_ob);
                    countries[x].calling_code = country_code;
                }
                call_back(this);
            },
            onChange: function (item) {
                let country = Models.custom_filter(countries,'calling_code',item);
                let country_name_field = $(".own-country-name");
                let nationality = {
                    country_name: country.name,
                    calling_code: country.calling_code,
                    region: country.region,
                    flag: country.flag
                };
                Models.selected_country = nationality;
                country_name_field.val(country.name);
                altair_md.update_input(country_name_field);
            }

        });
    });

};


Models.data_load = function ($http, $scope, $rootScope, $timeout,call_back) {
    ///function and query from parent controller
    let collection_name = $scope.collection_name;
    let query = $scope.data_query;
    let option = $scope.query_option;
        query = JSON.stringify(query);
        option = JSON.stringify(option);
        $rootScope.rows = [];

    let skip = ($scope.page-1)*$scope.limit;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "";
            if ($scope.custom_function !== undefined){
                method = $scope.custom_function.function_name;
                let params = $scope.custom_function.parameteres;
                let params_join = params.join();
                if (params.length){
                    params_join += ",";
                }
                method +=  "("+params_join+"'"+$scope.limit+"','"+skip+"')";
            }
            else{
                method = "request_collection_data('" + collection_name + "'," + query + "," + option + ",'" + $scope.limit + "','" + skip + "')";
            }

        let extra_data = Models.retrive_request_object(method);
            extra_data['query'] = query;
        let form_data = Models.form_data_maker("", extra_data);
        Models.$http($http, form_data, function (response, status) {
            // console.log(response);
            let finds = response.find_data;
            $rootScope.rows = finds;
            $timeout(function () {
                if (finds.length) {
                    $scope.filtered = $scope.limit * $scope.page;
                    $scope.start_row = $scope.filtered - $scope.limit;
                    altair_md.checkbox_radio();
                    $rootScope.require();
                    if (!$("#ts_pager_filter[role]").length) {
                        altair_tablesorter.pager_filter_example();
                        Models.custom_selectize(".ts_gotoPage",{
                            create: function(input) {
                                return {
                                    value: input,
                                    label: input
                                }
                            },
                        });
                        Models.custom_selectize(".pagesize");
                    }
                    $("#ts_pager_filter").trigger("update");

                    let page_instance = $(".ts_gotoPage").get(0).selectize;
                    page_instance.setValue($scope.page);
                    page_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.page = value;
                            // $scope.change();
                        })
                    });
                    let limit_instance = $(".pagesize").get(0).selectize;
                    limit_instance.setValue($scope.limit);
                    limit_instance.on("change", function (value) {
                        $scope.$apply(function () {
                            $scope.limit = value;
                            // $scope.change();
                        })
                    });
                }

            });
            call_back(response, status);
        });
    }
    else if(skip <= 0){
        $scope.prev_switch = 0;
        Models.custom_selectize(".ts_gotoPage");
        Models.custom_selectize(".pagesize");
    }
    else if(skip >= $scope.total){
        $scope.next_switch = 0;
        Models.custom_selectize(".ts_gotoPage");
        Models.custom_selectize(".pagesize");
    }

};

Models.export_data_from_data_table = function ($http,options={}, call_back=function () {}) {
    let default_options = {
        "columns": "*",
        "sql":"",
        "sql_data": {},
        "table_name":"",
        "limit":0,
        "skip":0,
        "data":{}
    };
    Object.assign(default_options,options);
    ///function and query from parent controller
    let default_data = {
        query_data:JSON.stringify({
            sql:default_options.sql,
            sql_data:default_options.sql_data,
            columns:default_options.columns
        }),
        export:true
    };
    Object.assign(default_data,default_options.data);
    let other = {
            data: default_data
        }
    ;
    Models.request_sender($http,"get","request_data_list",function (response) {
        if (response.status){
            let download_info = response.data;
            Models.file_downloader($http,download_info,function () {
                call_back(response);
            })
        }
    },["'"+default_options.table_name+"'",default_options.limit,default_options.skip],other);
};
Models.file_downloader = function($http,download_info,call_back){
    let pathoffile = download_info.file_path;
    let filename = download_info.file_name;
    $http.get(pathoffile, {
        responseType: "arraybuffer"
    }).then(function (response) {
        call_back(response);
        let headers = response.headers();
        headers['Content-Disposition'] = "attachment";
        let blob = new Blob([response.data], { type: "octet/stream" });
        let link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = filename;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    });
};



Models.total_counter = function($http, $scope,call_back){
    ///function and query from parent controller
    let data = {
        "columns": "*",
        "sql":"",
        "sql_data": {}
    };
    Object.assign(data,$scope.query_data);
    let collection_name = $scope.collection_name;
    let method = "request_data_list('" + collection_name + "',0,0,true)";
    let extra_data = Models.retrive_request_object(method);
    extra_data['query_data'] = JSON.stringify(data);
    if ($scope.more_data !== undefined) {
        Object.assign(extra_data,$scope.more_data);
    }
    let form_data = Models.form_data_maker("",extra_data);
    Models.$http($http,form_data,function (response,status) {
        // console.log(response);
        call_back(response,status);
    });
};

Models.request_data_load = function ($http, $scope, $rootScope, $timeout,call_back) {
    let data = {
        "columns": "*",
        "sql":"",
        "sql_data": {},
    };
    Object.assign(data,$scope.query_data);

    let collection_name = $scope.collection_name;
    let skip = $scope.skip;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let method = "request_data_list('" + collection_name + "','" + $scope.limit + "','" + skip + "')";

        let extra_data = Models.retrive_request_object(method);
        extra_data['query_data'] = JSON.stringify(data);
        if ($scope.more_data !== undefined) {
            Object.assign(extra_data,$scope.more_data);
        }
        let form_data = Models.form_data_maker("", extra_data);
        Models.$http($http, form_data, function (response, status) {
            let finds = response.find_data;
            if (finds !== undefined){
                if (finds.length) {
                    for (let x in finds) {
                        let item = finds[x];
                        /// check duplicate has been exist , but check first data length maximum 5000
                        if ($scope.rows.length <= 5000){
                            if (Models.unique_checker($scope.rows,item)){
                                $scope.rows.push(item);
                                $scope.skip += 1;
                            }
                        }
                        else{
                            $scope.rows.push(item);
                            $scope.skip += 1;
                        }
                    }
                }
            }


            $timeout(function () {
                call_back(response, status);
            });

        });
    }
};

Models.load_data_with_limit = function ($http, $scope, $timeout,call_back) {
    let function_name = $scope.function_name;
    let skip = $scope.skip;
    // console.log($scope.page);
    if(skip>=0 && skip<$scope.total) {
        $scope.prev_switch = 1;
        let other = {};
        Object.assign(other,$scope.other);
        Models.request_sender($http,"get",function_name,function (response) {
            let finds = response.find_data;
            if (finds.length) {
                for (let item of finds) {
                    /// check duplicate has been exist , but check first data length maximum 5000
                    if ($scope.rows.length <= 5000){
                        if (Models.unique_checker($scope.rows,item)){
                            $scope.rows.push(item);
                            $scope.skip += 1;
                        }
                    }
                    else{
                        $scope.rows.push(item);
                        $scope.skip += 1;
                    }

                }

            }


            $timeout(function () {
                call_back(response, status);
            });
        },["'" + $scope.limit + "'","'" + skip + "'"],other);
    }
};

Models.load_data_with_limit_total_counter = function ($http, $scope,call_back) {
    let function_name = $scope.function_name;
    let skip = $scope.skip;
    let other = {};
    Object.assign(other,$scope.other);
    Models.request_sender($http,"get",function_name,function (response) {
        if (response.status){
            call_back(response);
        }
    },["'" + $scope.limit + "'","'" + skip + "'",true],other);
};


Models.get_basic_info = function ($http,$rootScope,call_back) {
    if ($rootScope.basic_info !== undefined){
        call_back($rootScope.basic_info);
    } else{
        Models.retrieve_data($http,"basic_info",{},function (data,status) {
            if (data.status){
                $rootScope.basic_info = data.basic_info;
                call_back(data.basic_info);
            }
        },{},true);
    }
};

Models.retrieve_data = function ($http, collection_name, query,call_back,option={},function_name=false,parameteres = []) {
    query = JSON.stringify(query);
    option = JSON.stringify(option);
    let method = "";
    if (function_name) {
        method = collection_name;
        let params = parameteres;
        let params_join = params.join();
        method += "(" + params_join + ")";
    }
    else{
        method = "request_collection_data('" + collection_name + "'," + query + "," + option + ")";
    }
    // console.log(method);
    let extra_data = Models.retrive_request_object(method);
        extra_data['query'] = query;
    let form_data = Models.form_data_maker("", extra_data);
    Models.$http($http,form_data,function (response,status) {
        call_back(response,status);
    })
};

Models.request_sender = function ($http,type,function_name,call_back,parameteres = [],other_feature={}) {
    // let query = JSON.stringify(hidden_data);
    let options = {
        form_name:"",
        data:{},
        images: [],
        fancy_tree:[],
        data_list: []
    };
    Object.assign(options,other_feature);
    let method = function_name;
    let params = parameteres;
    let params_join = params.join();
    method += "(" + params_join + ")";
    let extra_data = {};
    if (type === "get"){
        extra_data = Models.retrive_request_object(method);
    }
    else if (type === "post"){
        extra_data = Models.passed_request_object(method);
    }
    else if (type === "update"){
        extra_data = Models.modify_request_object(method);
    }
    else if (type === "other"){
        extra_data = Models.other_request_object(method);
    }

    // extra_data['hidden_data'] = query;
    Object.assign(extra_data,options.data);
    let form_data = Models.form_data_maker(options.form_name, extra_data,options.images,options.fancy_tree,options.data_list);
    Models.$http($http,form_data,function (response,status) {
        call_back(response,status);
    })
};




Models.merge = function (target_object, new_object) {
    return Object.assign(target_object, new_object);
};
Models.index_number = function(obj,key_name,value){
    return obj.map(function(e) { return e[key_name]; }).indexOf(value);
};


Models.altair_init = function () {
    // page onload functions
        altair_page_onload.init();

        // main header
        altair_main_header.init();

        // main sidebar
        // altair_main_sidebar.init(true);
        // secondary sidebar
        altair_secondary_sidebar.init();

        // top bar
        altair_top_bar.init();

        // page heading
        altair_page_heading.init();

        // material design
        altair_md.init();

        // forms
        altair_forms.init();

        // truncate text helper
        altair_helpers.truncate_text($('.truncate-text'));

        // full screen
        altair_helpers.full_screen();

        // table check
        altair_helpers.table_check();

        // print page
        altair_helpers.print_page();
};
Models.id_info = function (user_id, info_name) {
    if (user_id !== undefined){
        let type = user_id.substr(0,3);
        let serial = user_id.substr(3,10);
        let user_row_id = user_id.substr(13,10);
        let device_row_id = user_id.substr(23,10);
        let option = user_id.substr(33,33);
        let self = type+serial+user_row_id+device_row_id;
        let arr = {"type": type, "serial": serial,user_row_id:user_row_id,device_row_id:device_row_id, "option": option,self:self};
        let result = arr[info_name];
        if (result === undefined){
            return false;
        }
        return result;
    }
    else{
        console.log("undefined user id for get id info")
    }
};

Models.id_type_name = function (user_id,$rootScope) {
    let id_type = Models.id_info(user_id, 'type');
    let reverse_id_types = Models.object_reverse($rootScope.basic_info.id_types);
    let type_name = reverse_id_types[id_type];
    return type_name;
};
Models.id_type = function (type,$rootScope) {
    return $rootScope.basic_info.id_types[type];
};

Models.object_reverse = function (object) {
    let reverse_object = {};
    for( let x in object){
        let key = x;
        let value = object[x];
        reverse_object[value] = key;
    }
    return reverse_object;
};

Models.modal = function ($interpolate,$scope,element,call_back) {
    let content = $(element).html();
        let result = $interpolate(content)($scope);
        Models.response(result);
        call_back();
};
Models.form_submit = function (form_name,call_back) {
  $(document).on("submit",form_name,function (e) {
      e.preventDefault();
      e.stopImmediatePropagation();
      call_back(this,e);
  });
};

Models.document_update = function ($http,collection_name,where,which,call_back) {
  let function_name = "document_update(request)";
    let extra_data = Models.modify_request_object(function_name);
    let update_object = {
        collection_name: collection_name,
        which:which,
        where:where
    };
    extra_data['update_document'] = JSON.stringify(update_object);
    let form_data = Models.form_data_maker("",extra_data);
    Models.$http($http,form_data,function (response,status) {
        call_back(response,status);
    });
};
// Models.sale_point_rule = function ($rootScope) {
//     let amount = 0
//     let point_rules = $rootScope.basic_info.point_rules;
//     if (point_rules !== undefined){
//         let sale_point_rule = Models.custom_filter()
//     }
//     return amount;
// };

Models.logged_user_type = function ($rootScope) {
    let user_type = null;
    let logged_user_id = $rootScope.basic_info.logged_user_id;
    if (logged_user_id !== undefined && logged_user_id){
        let id_type = Models.id_type_name(logged_user_id,$rootScope);
        user_type = id_type;
    }
    return user_type;
};

Models.logged_user = function ($rootScope) {
    let user = null;
    if ($rootScope.basic_info !== undefined){
        if ($rootScope.basic_info.logged_user !== undefined){
            user = $rootScope.basic_info.logged_user;
        }
    }
    return user;
};

Models.logged_user_id = function ($rootScope) {
    let user_id = null;
    let logged_user_id = $rootScope.basic_info.logged_user_id;
    if (logged_user_id !== undefined && logged_user_id){
        user_id = logged_user_id;
    }
    return user_id;
};
Models.logged_branch_id = function ($rootScope) {
    let user_id = null;
    let logged_branch_id = $rootScope.basic_info.logged_branch_id;
    if (logged_branch_id !== undefined && logged_branch_id){
        user_id = logged_branch_id;
    }
    return user_id;
};


Models.user_access_menus = function ($rootScope) {
    if ($rootScope.basic_info.logged_user !== undefined){
        return $rootScope.basic_info.logged_user.access_menus;
    }

};
Models.main_menu_right = function(menu_id,$rootScope){
    let permission = false;
    let user_access_menus = Models.user_access_menus($rootScope);
    let get_access_keys = [];
    if ($rootScope.basic_info !== undefined && user_access_menus !== undefined) {
        for (let x of user_access_menus) {
            get_access_keys.push(Number(x.value));
        }
        let exist = get_access_keys.indexOf(menu_id);
        if (exist === -1){
            permission = false;
        }
        else{
            permission = true;
        }
    }
    return permission;
};
Models.page_name = function ($location,part = []) {
    let path = $location.path();
    let path_part = path.split('/');
    let parts = [];
    parts.push(path_part[1]);
    if (part.length){
        for (let x of part){
            let r = path_part[x];
            if (r !== undefined){
                parts.push(r);
            }
        }
    }
    let result = parts.join("/");
    return result;
};
Models.smart_informer = function (switch_type=1,information="",content_hide=true) {
    // let default_options = {
    //     "element":".message-box",
    //     "close_icon":'<i class=\"material-icons \">close</i>',
    //     "width": "350px",
    //     "top": "0px",
    //     "information": ""
    // };
    // $.extend(default_options,options);
    // console.log(default_options);
    let element = $(".message-box");
    function box_max_height() {
        let body_height = $(document).height();
        let box_height = body_height-100+"px";
        element.children("div").css({
            "max-height": box_height
        });
    }
    box_max_height();
    $(window).resize(function () {
        box_max_height();
    });

    let body_width = $(document).width() + "px";
    let content_element = $("#page_content");

    if (switch_type){
        element.children("div").html(information);
        element.addClass("active");
        if (content_hide){
            content_element.addClass('overlay')
        }
    }
    else{
        element.removeClass("active");
        if (content_hide){
            content_element.removeClass('overlay')
        }
    }
    $(".message-box-close").click(function (e) {
        e.stopImmediatePropagation();
        Models.smart_informer(0);
    });
};
Models.estimate_storage = function (call_back) {
    navigator.storage.estimate().then(estimate => {
        // estimate.quota is the estimated quota
        // estimate.usage is the estimated number of bytes used
        let quota = estimate.quota;
        let usage = estimate.usage;
        let net_quota = quota - usage;
        let mb_quota = (net_quota/1000000)/2;
        call_back(mb_quota);
    });
};
Models.data_size = function (obj) {
    function size_of(data) {
        let l = encodeURI(data);
        console.log(l);
        return encodeURI(data).length;
    }
    return size_of(obj);
};

Models.db_init = function (schema) {
    let db_name ='tims';

    let db =  {
        name: db_name,
        tables: schema
    };
    return connection.isDbExist(db_name).then(function(isExist) {
        if (isExist) {
            connection.openDb(db_name);
        } else {
            connection.createDb(db);
        }
    }).catch(function(err) {
        console.error(err);
    });
};

Models.get_profile_content = function($http,$rootScope,$routeParams,call_back,member_id=false){
    let user_id = "";
    if (member_id){
        user_id = $routeParams.member_id;
    }
    else{
        user_id = $routeParams.user_id;
    }
    let logged_user_id = "";
    if ($rootScope.basic_info.logged_user_id !== undefined){
        if (member_id){
            logged_user_id = $rootScope.basic_info.logged_user.member_id;
        }
        else{
            logged_user_id = $rootScope.basic_info.logged_user.user_id;
        }

    }
    if (user_id === logged_user_id){
        let logged_user = $rootScope.basic_info.logged_user;
        call_back(logged_user);
    }
    else{

        Models.request_sender($http,"get","controller_info",function (data) {
            call_back(data);
        },["'"+user_id+"'",member_id]);
    }
};
Models.scroll_finish = function (element=window,call_back) {
    $(element).on("scroll",function(e){
        let scrollTop=$(this).scrollTop();
        let element_offset_top=$(document).height()-$(window).height();
            scrollTop = Math.round(scrollTop);
        if(scrollTop===element_offset_top){
            call_back();
        }
    });
};
Models.deleter = function ($http,table_name,id,call_back) {
    Models.request_sender($http,"other","deleter",function (data) {
        call_back(data);
    },["'"+table_name+"'","'"+id+"'"])
};

Models.get_selectize_instance = function(element){
    let result = null;
    element = $(element);
    if (element.length && element.get(0) !== undefined){
        if (element.get(0).selectize !== undefined){
            result = element.get(0).selectize;
        }
    }
    return result;

};


Models.tag_chooser = function ($http,$scope,call_back,options) {

    let default_options = {
        class_name:".tag-chooser",
        data_list:[],
        editable:true,
        selectize_options:{},
        default_value:0,
        type:null,
        multiple:false,
        allow:false,
        allow_compare_field:"",
        onAdd:function () {},
        onDelete:function () {},
        onChange:function () {}
    };
    Object.assign(default_options,options);
    let multiple = default_options.multiple;
    $(default_options.class_name).not(".active").each(function () {
        let tag_chooser = $(this);
        tag_chooser.addClass("active");
        let action = {};
        action.show_add_button = function() {
            tag_chooser.find(".add").removeClass("uk-hidden");
            tag_chooser.find(".delete").addClass("uk-hidden");
        };
        action.show_delete_button = function () {
            tag_chooser.find(".add").addClass("uk-hidden");
            tag_chooser.find(".delete").removeClass("uk-hidden");
        };
        action.hide_buttons = function () {
            tag_chooser.find(".add").addClass("uk-hidden");
            tag_chooser.find(".delete").addClass("uk-hidden");
        };
        let close_plugin = {};
        if (default_options.multiple){
            close_plugin.remove_button = {
                label: ''
            }
        }

        let selectize_options = {
            multiple:default_options.multiple,
            plugins:close_plugin,
            onInitialize:function () {
                if (default_options.default_value){
                    if (multiple){
                        for (let item of default_options.default_value){
                            this.addItem(item);
                        }
                    }
                    else{
                        this.setValue(default_options.default_value,"silent");
                    }



                }
                call_back(this);
                action.selectize = this;
            },
            onChange:function (value) {
                default_options.onChange(value,this);
                if (value){
                    let items = this.items;
                    let total_selected = items.length;
                    let total_have = 0;
                    for (let item of items){
                        let search = Models.index_number(list,"value",item);
                        if (search !== -1){
                            total_have++;
                        }
                    }
                    if (total_selected === total_have){
                        if (default_options.editable) {
                            action.show_delete_button();
                        }
                    }
                    else{
                        action.show_add_button();
                    }
                }
                else {
                    action.hide_buttons();
                }
            },


        };
        selectize_options.create = false;
        if (default_options.editable){
            selectize_options.create = function(input) {
                return {
                    value: input,
                    label: input,
                    new: true
                }
            };
        }
        Object.assign(selectize_options,default_options.selectize_options);
        let list = [];
        let element = tag_chooser.find("select");

        if (default_options.type){
            Models.request_sender($http,"get","tags",function (data) {
                if (data.status){
                    let data_list = data.find_data;
                    for (let item of data_list){
                        let obj = {
                            value:item.id,
                            label:item.tag
                        };
                        if (!default_options.allow){
                            list.push(obj);
                        }
                        else{

                            let compare_value = item[default_options.allow_compare_field];
                            let index = default_options.allow.indexOf(compare_value);
                            if (index !== -1){
                                list.push(obj);
                            }

                        }

                    }

                    selectize_options.options = list;
                    Models.custom_selectize(element,selectize_options);

                }
            },["'"+default_options.type+"'"])
        }
        else{
            list = default_options.data_list;
            selectize_options.options = list;
            Models.custom_selectize(element,selectize_options);
        }
        let parent = element;
        action.delete_tag = function () {
            let value = parent.val();
            if (value){
                let instance = action.selectize;
                let selected_items = instance.items;
                Models.confirm(function () {
                    let other = {
                        data: {
                            tags:selected_items
                        }
                    };
                    Models.request_sender($http,"other","tags_deleter",function (data) {
                        // console.log(data);
                        if (data.status){
                            let selectize = parent.get(0).selectize;
                            selectize.setValue("");
                            for ( let item of selected_items){
                                selectize.removeItem(item,"silent");
                                selectize.removeOption(item);
                            }
                            action.hide_buttons();
                            default_options.onDelete(parent);
                        }
                    },[],other);
                });

            }
        };
        action.add_tag = function () {
            let instance = action.selectize;
            let selected_items = instance.items;
            if (!default_options.type) {
                console.log("Type not found");
                return false;
            }
            let value = parent.val();
            if (value){
                // remove saved item in selected items
                let get_option_list = Models.get_list_from_selectize(instance);
                let unregister_tags = [];
                for (let item of selected_items){
                    let option_info = Models.custom_filter(get_option_list,"value",item);
                    if (option_info !== undefined){
                        let is_new = option_info.new;
                        if (is_new !== undefined){
                            unregister_tags.push(item);
                        }

                    }
                }
                let other = {
                    data:{
                        tags:unregister_tags
                    }
                };
                Models.request_sender($http,"post","add_tag",function (data) {
                    if (data.status){
                        let selectize = parent.get(0).selectize;
                        let update_data = data.data;
                        for (let info of update_data){
                            let update_tag = info.tag;
                            let update_id = info.id;
                            info.label = update_tag;
                            info.value = update_id;

                            selectize.updateOption(update_tag,info);
                            list.push(info);
                        }

                        selectize.refreshItems();
                        action.show_delete_button();
                        default_options.onAdd(parent);
                    }
                },["'"+default_options.type+"'"],other);
            }

        };
        tag_chooser.on("click",".add",function () {
            action.add_tag();
        });
        tag_chooser.on("click",".delete",function () {
            action.delete_tag();
        });
    });
};

Models.permission_part_id = function (part_name,$rootScope) {
    let permission_part_id = 0;
    let permission_parts = $rootScope.basic_info.permission_parts;
    if (permission_parts === undefined){
        Models.notify("Permission part not found");
    }
    else{
        let part_info = Models.custom_filter(permission_parts,"label",part_name);
        if (part_info === undefined){
            Models.notify("Permission part info undefined");
        }
        else{
            let part_id = part_info.id;
            if (part_id !== undefined){
                permission_part_id = part_id;
            }
            else{
                Models.notify("Undefined part id");
            }

        }
    }
    return permission_part_id;
};

Models.edit_item_permission = function (part_name,$rootScope) {
    let permission = false;
    let controller_type = Models.logged_user_type($rootScope);
    if (controller_type === "system_designer" || controller_type === "admin"){
        permission = true;
    }
    else if (controller_type === "system_admin") {
        let user_permission_parts = $rootScope.basic_info.logged_user.permission_parts;
        if (user_permission_parts !== undefined){
            let part_id = Models.permission_part_id(part_name,$rootScope);
            if (part_id){
                let filter = Models.custom_filter(user_permission_parts,"value",part_id.toString());
                if (filter !== undefined){
                    permission = true;
                }
            }
            else{
                Models.notify("Part id not found");
            }
        }
        else{
            Models.notify("User permission not defined");
        }
    }
    return permission;
};
Models.permission_of_part = function(part_name,$rootScope){
    return Models.edit_item_permission(part_name,$rootScope);
};
Models.timeConverter = function (UNIX_timestamp){
    let a = new Date(UNIX_timestamp * 1000);
    let months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    let year = a.getFullYear();
    let month = months[a.getMonth()];
    let date = a.getDate();
    let hour = a.getHours();
    let min = a.getMinutes();
    let sec = a.getSeconds();
    let time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
};
Models.readable_text = function(programing_text = "mahadi_hasan"){
    let clean = programing_text.replace(/([-]+|[_]+)/g, " ");
    let first_char = clean.substr(0,1).toUpperCase();
    let next_all_char = clean.substr(1,clean.length);
    let total = first_char+next_all_char;
    return total;

};
Models.page_title_generator = function ($location,$scope,$rootScope) {
    let page_name = Models.page_name($location,[2]);
    let basic_info = $rootScope.basic_info;
    let software_info = {};
    if (basic_info !== undefined){
        software_info = basic_info.software_info;
    }
    let title = software_info.title;
    if (page_name !== ""){
        if (page_name !== "home" && page_name !== "index" ){
            let splits = page_name.split("/");
            let refine_parts = [];
            for (let part of splits){
                let clean = part.replace(/([-]+|[_]+)/g, " ");
                let first_char = clean.substr(0,1).toUpperCase();
                let next_all_char = clean.substr(1,clean.length);
                let total = first_char+next_all_char;
                refine_parts.push(total);
            }

            title = refine_parts.join(" | ");
        }
    }


    $("title").html(title);
};

Models.tree = function (element,options={}) {
    let defaults = {
        'exchange_option': false,
        'add_option': true,
        'edit_option': false,
        'delete_option': true,
        'confirm_before_delete': false,
        'animate_option': false,
        'fullwidth_option': false,
        'align_option': 'center',
        'drag_disabled': false,
    };
    $.extend(defaults,options);
    return $(element).tree_structure(defaults);
};

Models.user_tree = function (user_id,$http,call_back) {
    let demo_user = {
        "user_id":"000000000000",
        "image":"static/app/assets/img/default/profile.png",
        "name":"Company"
    };
    let tree = [];
    if (user_id === undefined){
        Models.get_root_workers($http,"ZSM",function (data) {
            demo_user['stalk'] = data;
            tree.push(demo_user);
            call_back(tree);

        });
    }

};
Models.round = function(amount){
        amount = Number(amount);
    let round = Math.floor(amount);
    let round_amount = Models.amount(amount-round);
    let obj = {
        amount: Models.amount(round),
        round_amount:round_amount
    };
    return obj;
};
Models.amount = function(amount){
    let number = Number(amount);
        number = number.toFixed(2);
        number = Number(number);
    return number;
};

Models.get_date_in_timestamp = function (time_date) {
    let split = time_date.split(" ");
    let date = split[0];
    return date;
};

Models.is_divisional = function(divisional_amount, division_by){
    let result = false;
    if (Number(division_by)){
        result = true;
    }
    return result;
};

Models.convertNumberToWords = function(amount) {
    let words = new Array();
    words[0] = '';
    words[1] = 'One';
    words[2] = 'Two';
    words[3] = 'Three';
    words[4] = 'Four';
    words[5] = 'Five';
    words[6] = 'Six';
    words[7] = 'Seven';
    words[8] = 'Eight';
    words[9] = 'Nine';
    words[10] = 'Ten';
    words[11] = 'Eleven';
    words[12] = 'Twelve';
    words[13] = 'Thirteen';
    words[14] = 'Fourteen';
    words[15] = 'Fifteen';
    words[16] = 'Sixteen';
    words[17] = 'Seventeen';
    words[18] = 'Eighteen';
    words[19] = 'Nineteen';
    words[20] = 'Twenty';
    words[30] = 'Thirty';
    words[40] = 'Forty';
    words[50] = 'Fifty';
    words[60] = 'Sixty';
    words[70] = 'Seventy';
    words[80] = 'Eighty';
    words[90] = 'Ninety';
    amount = amount.toString();
    let atemp = amount.split(".");
    let number = atemp[0].split(",").join("");
    let n_length = number.length;
    let words_string = "";
    if (n_length <= 9) {
        let n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        let received_n_array = new Array();
        for (let i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (let i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (let i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (let i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
};

Models.get_list_from_selectize = function (instance) {
    let list = [];
    // refine options as list
    let options = instance.options;
    if (options !== undefined){
        for (let x in options){
            let item = options[x];
            list.push(item);
        }
    }
    return list;

};
Models.get_documents = function (document_type,type_option) {

};

Models.get_document = function ($http,$scope,document_id,call_back) {
    Models.request_sender($http,"get","get_document",function (data) {
        call_back(data);
    },["'"+document_id+"'","'"+$scope.document_sub_type+"'","'"+$scope.doc_type+"'"])
};



Models.get_invoice_last_info = function ($http,$rootScope,call_back) {
    if ($rootScope.invoice_last_update !== undefined){
        call_back($rootScope.invoice_last_update);
    }
    else{
        Models.request_sender($http,"get","invoice_last_update",function (data) {
            $rootScope.invoice_last_update = data;
            call_back(data);
        });
    }
};
Models.last_invoice_value = function ($rootScope,type) {
  let result = "";
    if($rootScope.invoice_last_update !== undefined){
        result = $rootScope.invoice_last_update[type];
    }
  return result;
};

Models.get_type_of_activities = function ($http,$rootScope,call_back) {
    if ($rootScope.type_of_activities !== undefined){
        call_back($rootScope.type_of_activities);
    }
    else{
        Models.request_sender($http,"get","type_of_activities",function (data) {
            $rootScope.type_of_activities = data;
            call_back(data);
        });
    }
};

Models.activity_template = function (data,$rootScope) {
    let input = data;
    let type_of_activities = $rootScope.type_of_activities;
    let action = input.action;
    if (type_of_activities !== undefined){
        let filter = Models.custom_filter(type_of_activities,"label",action);
        if(filter !== undefined){
            let action_name = filter.name;
            let template = $(".common-template").html();
            if (action === "edit_item" || action === "add_item"){
                template = $(".item-template").html();
            }
            else if (action === "add_chemist" || action === "edit_chemist" || action === "delete_chemist"){
                template = $(".chemist-template").html();
            }
            else if (action === "sale"){
                template = $(".document-template").html();
            }
            else if (action === "change_software_title" || action === "change_software_currency"
                || action === "change_software_time_zone"  || action === "change_software_logo"
                || action === "change_software_favicon" || action === "change_software_construction_mode"
                || action === "change_software_email"  || action === "change_software_company_name"
                || action === "invoice_authorize"  || action === "batch_increment" || action === "add_bank_cash"
                || action === "bank_cash_initial_balance" || action === "edit_bank_cash" || action === "delete_bank_cash"  ){
                template = $(".simple-template").html();
            }
            else if (action === "add_account_source" || action === "add_document_source"){
                template = $(".simple-template").html();
            }
            else if (action === "accounts_mapping"){
                template = $(".simple-template").html();
            }
            else if (action === "add_batch" || action === "delete_batch"){
                template = $(".simple-template").html();
            }
            else if (action === "delete_raw_item"){
                template = $(".common-template").html();
            }
            else if (action === "invoice_payment" || action === "cancel_bank_reconciliation" || action === "accept_bank_reconciliation"){
                template = $(".invoice-payment").html();
                input.currency = $rootScope.currency;
            }







            let context = input;
            context.action_name = action_name;
            let compile = Handlebars.compile(template)(context);
            return compile;
        }
    }
};
Models.compiler = function (html,data) {
    let template_name = Handlebars.compile(html)(data);
    return template_name;
};

Models.unique_array = function (a) {
    for(let i=0; i<a.length; ++i) {
        for(let j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }

    if (typeof a === 'object'){
        let na = [];
        for (let [key,value] of Object.entries(a)){
            na.push(value);
        }
        a = na;
    }
    return a;
};


Models.document_helper_suggester = function (type,input_field,$scope,$http,document_type=false,document_sub_type=false) {
    $(input_field).suggest([],{
        onKeyDown:function (element,before_source,e) {
            let that = this;
            if (e.which !== 8){
                let value = $(element).val();
                let search = Models.search_in_array(before_source,value);
                let value_of_document_type;
                let value_of_document_sub_type;
                if (document_type){
                    value_of_document_type = "'"+document_type+"'";
                }
                else{
                    value_of_document_type = "'"+$scope.doc_type+"'";
                    if ($scope.doc_type === undefined){
                        value_of_document_type = "''";
                    }
                }
                if (document_sub_type){
                    value_of_document_sub_type = "'"+document_sub_type+"'";
                }
                else{
                    value_of_document_sub_type = "'"+$scope.document_sub_type+"'";
                    if ($scope.document_sub_type === undefined){
                        value_of_document_sub_type = "''";
                    }
                }

                if (!search.length){
                    let parameters = ["'"+type+"'","''",value_of_document_type,value_of_document_sub_type];

                    Models.request_sender($http,"get","document_helpers",function (data) {

                        if (data.status){
                            let find_data = data.find_data;
                            if (find_data !== undefined){
                                let suggetions =  [];
                                for (let item of find_data){
                                    suggetions.push(item.value);
                                }
                                that.addOptions(suggetions);
                            }
                        }
                    },parameters,{data:{value:value}});
                }

            }
            /// hide delete button
            element = $(element);
            let parent = element.closest(".suggest-input");
            let delete_button = parent.find(".delete");
            delete_button.addClass("uk-hidden");

        },
        onAccept:function (element,source) {
            let that = this;
            autosize.update($(element));
            element = $(element);
            let value = element.val();
            let parent = element.closest(".suggest-input");
            let before_delete_button = parent.find(".delete");
            if (!before_delete_button.length){
                parent.append("<i class='material-icons delete' data-uk-tooltip='' title='Remove from template'>delete_forever</i>");
            }
            let delete_button = parent.find(".delete");
            delete_button.removeClass("uk-hidden");
            delete_button.click(function (e) {
                Models.confirm(function () {
                    let other = {
                        data:{
                            document_type:$scope.doc_type,
                            document_type_option:$scope.document_sub_type,
                            type:type,
                            value:value
                        }
                    };
                    Models.request_sender($http,"other","document_helper_delete",function (data) {
                        if (data.status){
                            // Models.notify("Deleted");
                        }else{
                            Models.notify("Delete failed");
                        }
                    },[],other);
                    that.deleteOption(value);
                    element.val("");
                    delete_button.addClass("uk-hidden");
                    autosize.update(element);
                })
            });

        }
    });
};


Models.active_menu = function ($location) {
    let page_name = Models.page_name($location,[2]);
    $(".menu_section li").each(function () {
        let elem = $(this);
        let link = elem.children("a").attr("href");
        if (page_name === link && page_name !== ""){
            elem.addClass("act_item");
        }

    });
    let $sidebar_main = $('#sidebar_main');
    $sidebar_main.find("a").click(function () {
        let parent = $(this).parent(".submenu_trigger");
        if (!parent.length){
            $sidebar_main.find(".current_section").removeClass("current_section");
            $sidebar_main.find(".act_item").removeClass("act_item");
            $(this).closest("li").addClass("act_item");
            altair_main_sidebar.active_menu();
        }

        if (parent.length === 0){
            $sidebar_main.find(".act_item").removeClass("act_item");
            $(this).parent("li").addClass("act_item");
        }

    });

};


Models.document_payment_status = function (info={}) {
    let status = "unpaid";
    let default_info = {
        net_payable_amount:0,
        total_paid_amount: 0
    };
    Object.assign(default_info,info);
    let net_payable_amount = Number(default_info.net_payable_amount);
    let total_paid_amount = Number(default_info.total_paid_amount);

    if (total_paid_amount < net_payable_amount && total_paid_amount !== 0){
        status = "partial_paid";
    }
    else if(total_paid_amount === net_payable_amount){
        status = "paid";
    }
    else if (total_paid_amount === 0){
        status = "unpaid";
    }

    return status;
};
Models.payment_status_name = function(status_label){
    let result = "Unpaid";
    if (status_label === "paid"){
        result = "Paid";
    }
    else if(status_label === "partial_paid"){
        result = "Partial paid";
    }
    return result;
};
Models.payment_status_class = function(status_label){
    let result = "uk-badge-danger";
    if (status_label === "paid"){
        result = "uk-badge-success";
    }
    else if(status_label === "partial_paid"){
        result = "uk-badge-warning";
    }
    return result;
};


Models.css_load = function (url,call_back) {
    let script = document.createElement('link');
    script.onload = function () {
        call_back();
    };
    script.rel = "stylesheet";
    script.media = "all";
    script.href = url;

    document.head.appendChild(script);
};
Models.script_load = function (url,call_back) {
    let script = document.createElement('script');
    script.onload = function () {
        call_back();
    };
    script.src = url;

    document.body.appendChild(script);
};
Models.working_schedule_calendar_init = function () {
    let schedule_calender = {
        script_load:function (call_back) {
            Models.css_load("static/app/assets/js/plugins/Dynamic-Weekly-Scheduler-jQuery-Schedule/dist/jquery.schedule.css",function () {
                Models.script_load("static/app/assets/js/plugins/Dynamic-Weekly-Scheduler-jQuery-Schedule/dist/jquery.schedule.js",function () {
                    call_back();
                });
            });
        },
        init:function (element,options) {
            let default_options = {
                days: [
                    "Sun",
                    "Mon",
                    "Tue",
                    "Wed",
                    "Thu",
                    "Fri",
                    "Sat",

                ],
                periodDuration: 30,
                hour:12,
                onInit:function () {
                    let heading_offset_maintain = {
                        init:function () {
                            let fixed = false;
                            // $(".jqs").scroll(function () {
                            //     if (!fixed){
                            //         console.log(fixed);
                            //         let this_calender = $(this);
                            //         let heading = this_calender.find(".jqs-grid-head");
                            //         let position = heading.offset();
                            //         heading.css({
                            //             position: "fixed",
                            //             left:position.left+"px",
                            //             top:position.top+"px",
                            //         });
                            //         console.log({
                            //             position: "fixed",
                            //             left:position.left+"px",
                            //             top:position.top+"px",
                            //         });
                            //         fixed = true;
                            //     }
                            // });
                            let this_element = $(element);
                            let heading = this_element.find(".jqs-grid-head");
                            heading.wrap("<div class='jqs-head-wrap' />")
                        }
                    };
                    heading_offset_maintain.init();

                }
            };
            this.script_load(function () {
                Object.assign(default_options,options);
                // assign weekly calendar
                $(element).jqs(default_options);

            });
            return default_options;

        }

    };
    return schedule_calender;
};

Models.event_types = function($rootScope){
    let events = [];
    if ($rootScope.basic_info !== undefined){
        if ($rootScope.basic_info.event_types !== undefined){
            events = $rootScope.basic_info.event_types;
        }
    }
    else{
        Models.notify("Basic info undefined for event types");
    }
    return events;
};
Models.middate = function(start_date='0000-00-00',end_date='0000-00-00'){
    let date1 = new Date(start_date);
    let date2 = new Date(end_date);
    var middate = new Date((date1.getTime() + date2.getTime()) / 2);
    return moment(middate).format("YYYY-MM-DD");
};
Models.calendar_init = function ($rootScope,$http) {
    let calender = {
        script_load:function (call_back) {
            Models.css_load("static/app/bower_components/fullcalendar/dist/fullcalendar.min.css",function () {
                Models.script_load("static/app/bower_components/fullcalendar/dist/fullcalendar.min.js",function () {
                    call_back();
                });
            });
        },
        init:function (options) {
            let default_options = {
                element:".calendar",
                type:"default",
                source_id:"",
                absent:false,
                institute_id:"",
                header: {
                    left: 'title today',
                    center: '',
                    right: 'month prev,next'
                },
                buttonIcons: {
                    prev: 'md-left-single-arrow',
                    next: 'md-right-single-arrow',
                    prevYear: 'md-left-double-arrow',
                    nextYear: 'md-right-double-arrow'
                },
                buttonText: {
                    today: ' ',
                    month: ' ',
                    week: ' ',
                    day: ' '
                },
                aspectRatio: 2.1,
                defaultDate: moment(),
                selectable: true,
                selectHelper: true,
                eventLimit: true,
                editable:false,
                calender_editable:true,
                timeFormat: '(HH)(:mm)',
                events: [


                ],
                layoutButton:0
            };

            this.script_load(function () {
                Object.assign(default_options,options);
                // assign  calendar
                let $calendar_selectable = $(default_options.element),
                    calendarColorsWrapper = $('<div id="calendar_colors_wrapper"></div>');

                let calendarColorPicker = altair_helpers.color_picker(calendarColorsWrapper).prop('outerHTML');

                if($calendar_selectable.length) {
                    let institute_id = default_options.institute_id;
                    let absent = default_options.absent;
                    let event_type_template = $(".event-type-template").html();
                    Object.assign(default_options,{
                        select: function(start, end) {
                            if (default_options.calender_editable){
                                UIkit.modal.prompt('' +
                                    '<h3 class="heading_b uk-margin-bottom">New Event</h3><div class="uk-margin-medium-bottom" id="calendar_colors">' +
                                    '</div>' +
                                    ''+event_type_template +
                                    'Event Title:',
                                    '', function(newvalue){
                                        let event_selectize = $(".event-selectize").get(0).selectize;
                                        let event_type = event_selectize.getValue();
                                        let event_info = Models.filter_in_array(Models.event_types($rootScope),{label:event_type});
                                        let color = "";
                                        if (event_info){
                                            color = event_info.color;
                                        }
                                        if($.trim( newvalue ) !== '') {
                                            let eventData,
                                                eventColor = $('#calendar_colors_wrapper').find('input').val();
                                            eventData = {
                                                // title: newvalue,
                                                // start: start,
                                                // end: end,
                                                // color: color
                                            };
                                            let calendar_type = default_options.type;
                                            let other_option = {
                                                data:{
                                                    title:newvalue,
                                                    start_time:moment(start).format('YYYY-MM-DD HH:mm'),
                                                    end_time:moment(end).format('YYYY-MM-DD HH:mm'),
                                                    color:color,
                                                    event_type:event_type,
                                                    type:calendar_type,
                                                    source_id: default_options.source_id,
                                                }
                                            };
                                            // console.log(other_option);
                                            Models.request_sender($http,"post","add_calendar_event",function (response) {
                                                if (response.status){
                                                    eventData = response.data;

                                                    $calendar_selectable.fullCalendar('renderEvent', eventData, true); // stick? = true
                                                    $calendar_selectable.fullCalendar('unselect');

                                                }
                                                else{
                                                    Models.notify("Failed");
                                                }

                                            },[],other_option);

                                        }
                                    }, {
                                        labels: {
                                            Ok: 'Add Event'
                                        }
                                    },function () {
                                        Models.custom_selectize(".event-selectize",{
                                            onInitialize:function () {
                                                let instance = this;
                                                let event_types = Models.event_types($rootScope);
                                                for (let item of event_types){
                                                    let new_object = {};
                                                    new_object.label = item.name;
                                                    new_object.value = item.label;
                                                    new_object.color = item.color;
                                                    if (item.calendar){
                                                        instance.addOption(new_object);
                                                    }

                                                }
                                            }
                                        });
                                    });
                            }

                        },
                        eventAfterRender:function (event,element) {
                            if (default_options.calender_editable){
                                /// if the calendar owner is institute then global event not editable

                                if ((default_options.source_id !== "" && event.type !== "default") || default_options.source_id === "" ){
                                    let customize_template = $(".event-customize-template").html();
                                    let content = $(element).find(".fc-content");
                                    content.append(customize_template);
                                    $(element).attr("data-uk-tooltip","").attr("title","Double click for edit");

                                }
                            }
                        },
                        viewRender:function (view) {
                            // delete before rendered events
                            let before_events = $calendar_selectable.fullCalendar('clientEvents');
                            for (let event of before_events){
                                $calendar_selectable.fullCalendar('removeEvents', event.id);
                            }
                            // new rendered
                            let start_date = moment(view.start).format('YYYY-MM-DD');
                            let end_date = moment(view.end).format('YYYY-MM-DD');
                            Models.calendar_events($http,default_options.type,default_options.source_id,function (data) {
                                // console.log(data);
                                if (data.find_data !== undefined){
                                    for(let eventData of data.find_data){
                                        $calendar_selectable.fullCalendar('renderEvent', eventData, true); // stick? = true
                                        $calendar_selectable.fullCalendar('unselect');

                                    }

                                }
                            },start_date,end_date,absent,institute_id);
                            if (!default_options.calender_editable){
                                let middate = Models.middate(start_date,end_date);

                                let current_year = moment().format("YYYY");
                                let current_month = moment().format("MM");
                                let year_of_middate = moment(middate).format("YYYY");
                                let month_of_middate = moment(middate).format("MM");

                                let currect_second = new Date(current_year+"-"+current_month+"-01").getTime();
                                let middate_second = new Date(year_of_middate+"-"+month_of_middate+"-01").getTime();

                                if (currect_second <= middate_second){
                                    $(".fc-next-button").addClass("uk-hidden");
                                }
                                else{
                                    $(".fc-next-button").removeClass("uk-hidden");
                                }

                            }

                            if (!default_options.layoutButton){
                                $(".fc-month-button").addClass("uk-hidden");
                            }


                        },

                        // eventDrop:function (info,more,sdfd) {
                        //
                        // }
                    });
                    let cal = $calendar_selectable.fullCalendar(default_options);
                    if (default_options.calender_editable){
                        cal.on("click",".fc-more",function () {
                            let popover = $(".fc-popover");
                            popover.find(".fc-event").each(function () {
                                let element = this;
                                let event = $(element).data().fcSeg.event;
                                /// if the calendar owner is institute then global event not editable

                                if ((default_options.source_id !== "" && event.type !== "default") || default_options.source_id === "") {
                                    if (event.event_type === "holiday"){
                                        let customize_template = $(".event-customize-template").html();
                                        let content = $(element).find(".fc-content");
                                        content.append(customize_template);
                                        $(element).attr("data-uk-tooltip", "").attr("title", "Double click for edit");

                                    }

                                }
                            });

                        });
                        cal.on("dblclick",".fc-event",function () {
                            let element = $(this);
                            let data = element.data().fcSeg;
                            let calendar_event_info = data.event;
                            /// if the calendar owner is institute then global event not editable
                            if ((default_options.source_id !== "" && calendar_event_info.type !== "default") || default_options.source_id === "" ) {
                                if (calendar_event_info.event_type === "holiday") {
                                    UIkit.modal.prompt('' +
                                        '<h3 class="heading_b uk-margin-bottom">Update Event</h3><div class="uk-margin-medium-bottom" id="calendar_colors">' +
                                        '</div>' +
                                        '' + event_type_template +
                                        'Event Title:',
                                        '', function (newvalue) {
                                            let event_selectize = $(".event-selectize").get(0).selectize;
                                            let event_type = event_selectize.getValue();
                                            let event_info = Models.filter_in_array(Models.event_types($rootScope), {label: event_type});
                                            let color = "";
                                            if (event_info) {
                                                color = event_info.color;
                                            }
                                            if ($.trim(newvalue) !== '') {
                                                let eventData,
                                                    eventColor = $('#calendar_colors_wrapper').find('input').val();
                                                eventData = {
                                                    // title: newvalue,
                                                    // start: start,
                                                    // end: end,
                                                    // color: color
                                                };
                                                let calendar_type = default_options.type;
                                                let other_option = {
                                                    data: {
                                                        title: newvalue,
                                                        start_time: calendar_event_info.start_time,
                                                        end_time: calendar_event_info.end_time,
                                                        color: color,
                                                        event_type: event_type,
                                                        type: calendar_type,
                                                        source_id: calendar_event_info.source_id,
                                                        edit: true,
                                                        id: calendar_event_info.id,
                                                    }
                                                };
                                                console.log(other_option);
                                                Models.request_sender($http, "update", "edit_calendar_event", function (response) {
                                                    if (response.status) {
                                                        eventData = calendar_event_info;
                                                        eventData.title = newvalue;
                                                        eventData.event_type = event_type;

                                                        $calendar_selectable.fullCalendar('updateEvent', eventData);
                                                        $calendar_selectable.fullCalendar('unselect');

                                                    } else {
                                                        Models.notify("Failed");
                                                    }

                                                }, [], other_option);

                                            }
                                        }, {
                                            labels: {
                                                Ok: 'Update Event'
                                            }
                                        }, function (modal) {
                                            Models.custom_selectize(".event-selectize", {
                                                onInitialize: function () {
                                                    let instance = this;
                                                    let event_types = Models.event_types($rootScope);
                                                    for (let item of event_types) {
                                                        let new_object = {};
                                                        new_object.label = item.name;
                                                        new_object.value = item.label;
                                                        new_object.color = item.color;
                                                        if (item.calendar) {
                                                            instance.addOption(new_object);
                                                        }
                                                    }
                                                    instance.setValue(calendar_event_info.event_type);
                                                }
                                            });
                                            $(modal.dialog).find("p input").val(calendar_event_info.title);

                                        });
                                }
                            }

                        });
                        cal.on("click",".event-delete",function () {

                            let element = $(this).closest(".fc-event");
                            Models.confirm(function () {
                                let data = element.data().fcSeg;
                                let calendar_event_info = data.event;
                                Models.request_sender($http,"other","deleter",function (data) {
                                    if (data.status){
                                        $calendar_selectable.fullCalendar('removeEvents', calendar_event_info.id);
                                        $calendar_selectable.fullCalendar('unselect');
                                    }
                                    else{
                                        Models.notify("Delete failed");
                                    }
                                },["'calendar'","'"+calendar_event_info.id+"'"]);

                            });


                        });
                    }

                }

            });
            return default_options;

        }

    };
    return calender;
};


Models.number_conversion = function(number,type="e2b"){
    let e2b={'0':'০','1':'১','2':'২','3':'৩','4':'৪','5':'৫','6':'৬','7':'৭','8':'৮','9':'৯','%':'%'};
    let b2e={};
    for(let x in e2b){
        b2e[e2b[x]]=x;
    }

    number=String(number);
    let exchange_type;
    if(type==="e2b"){
        exchange_type=e2b;
    }
    else if(type==="b2e"){
        exchange_type=b2e;
    }


     function getexchange(number) {
        let retStr = number;
        for (let x in exchange_type) {
            retStr = retStr.replace(new RegExp(x, 'g'), exchange_type[x]);
        }
        return retStr;
    }

    let converted_number=getexchange(number);
    if(type==="b2e"){
        if (Models.is_number(converted_number)){
            converted_number = Number(converted_number);
        }
    }
    return converted_number
};

Models.bangla_switch = function () {
    return 1;
};
Models.document_type_option_name = function (doc_type,doc_type_option,$rootScope) {
    let result = "";
    let basic_info = $rootScope.basic_info;
    if (basic_info !== undefined){
        let document_types = basic_info.document_types;
        if (document_types !== undefined){
            let doc_type_info = Models.custom_filter(document_types,"label",doc_type);
            if (doc_type_info !== undefined){
                let doc_type_options = doc_type_info.options;
                if (doc_type_options !== undefined){
                    let i = 0;
                    for (let x of doc_type_options){

                        let select_object = {
                            label:x.name,
                            value:x.label
                        };
                        if (x.label === doc_type_option){
                            result = x.name;
                        }
                        i++;
                    }
                }
                else{
                    Models.notify("Document type options undefined");
                }
            }
            else{
                Models.notify("Document type undefined");
            }
        }
    }
    return result;
};

Models.get_document_save_users = function ($rootScope,call_back) {
    if ($rootScope.document_save_users !== undefined){
        call_back($rootScope.document_save_users);
    }
    else{
        $rootScope.document_save_users = [];
        call_back([]);
    }
};


Models.get_document_save_items = function ($rootScope,call_back) {
    if ($rootScope.document_save_items !== undefined){
        call_back($rootScope.document_save_items);
    }
    else{
        $rootScope.document_save_items = [];
        call_back([]);
    }
};

Models.get_document_save_institutes = function ($rootScope,call_back) {
    if ($rootScope.document_save_institutes !== undefined){
        call_back($rootScope.document_save_institutes);
    }
    else{
        $rootScope.document_save_institutes = [];
        call_back([]);
    }
};


Models.negative_number = function(number){
    number = Number(number);
    return number - (number + number);
};

Models.admin_designations = function () {
    let data = ["ATEO","TEO","ADPEO","DPEO","DD","CMO"];
    return data;
};

Models.area_parent_name = function ($rootScope,label) {
    let result = false;
    let parent = Models.area_parents($rootScope,label);
    if (parent){
        result = parent['name'];
    }
    return result;
};

Models.child_areas = function ($http,instance,parent,instiute=false) {
    Models.request_sender($http,"get","child_areas",function (response) {

        if (response.find_data !== undefined){
            for(let item of response.find_data){
                item.label = item.name;
                item.value = item.id;

                instance.addOption(item);
                instance.addItem(item.id);


            }
        }
    },["'"+parent+"'",false,instiute]);
};

Models.sql_in = function (items = []) {
    let items_with_quatation = [];
    for(let item of items){
        items_with_quatation.push("'"+item+"'");
    }
    let join = items_with_quatation.join(",");
    return join;

};
Models.calendar_events = function ($http,calendar_type,source,callback,start_date=0,end_date=0,absent=false,institute_id="") {
    Models.request_sender($http,"get","calendar_events",function (response) {
        callback(response);

    },["'"+calendar_type+"'","'"+source+"'","'"+start_date+"'","'"+end_date+"'",absent,"'"+institute_id+"'"]);

};
Models.calendar_source_id = function($http,calendar_type,source_member_id,call_back){
    if (calendar_type === "institute"){
        Models.request_sender($http,"get","institute_info",function (data) {
            if (data.institute_id !== undefined){
                call_back(data.institute_id,data.institute_id);
                /// assign footer
                let footer_data = {
                    source:data.name
                };
                Models.generate_footer(footer_data);
            }
        },["'"+source_member_id+"'",1]);
    }
    else if(calendar_type === "teacher"){
        Models.request_sender($http,"get","teacher_info",function (data) {
            if (data.user_id !== undefined){
                call_back(data.user_id,data.institute_id,data);
                /// assign footer
                let footer_data = {
                    source:data.institute_name
                };
                Models.generate_footer(footer_data);
            }
        },["'"+source_member_id+"'",1]);
    }
    else if(calendar_type === "student"){
        Models.request_sender($http,"get","student_info",function (data) {
            // console.log(data);
            if (data.user_id !== undefined){
                call_back(data.user_id,data.institute_id,data);
                /// assign footer
                let footer_data = {
                    source:data.institute_name
                };
                Models.generate_footer(footer_data);
            }
        },["'"+source_member_id+"'",1]);
    }

    else{
        call_back("");
    }

};
Models.generate_footer = function (data) {
    let template = $(".footer-template").html();
    if (template !== undefined){
        template = Models.compiler(template,data);
        $("body").append(template);
    }
};
Models.footer_remove = function () {
    $("footer").remove();
};
Models.area_parents = function ($rootScope,label="",all=false) {
    let data = [
        {
            name: "Country",
            label: "country",
            child: "division",
            designation: "CMO",
            allow:["ATEO","TEO","ADPEO","DPEO","DD"],
        },
        {
            name: "Division",
            label: "division",
            child: "zilla",
            designation: "DD",
            allow:["ATEO","TEO","ADPEO","DPEO"]
        },
        {
            name: "Zilla",
            label: "zilla",
            child: "upazilla",
            designation: "DPEO",
            allow:["ATEO","TEO","ADPEO"]
        },
        {
            name: "Upazilla",
            label: "upazilla",
            child: "thana",
            designation: "ADPEO",
            allow:["ATEO","TEO"]
        },
        {
            name: "Thana",
            label: "thana",
            child: "cluster",
            designation: "TEO",
            allow:["ATEO"]
        },
        {
            name: "Cluster",
            label: "cluster",
            child: "school",
            designation: "ATEO",
            allow:["Head master","Master"]
        },
    ];
    if (all){
        return data;
    }

    if (label !== ""){
        let filter = Models.filter_in_array(data,{label:label});
        if (filter){
            return filter;
        }
        else{
            return null;
        }
    }
    return false;

};

Models.admin_area_manager = function ($rootScope,$http,designation,default_area=null,user_id=null) {
    Models.custom_selectize(".area",{
        onInitialize:function () {
            let instance = this;
            let area_parents = Models.area_parents($rootScope,"",true);
            let parent_info = Models.filter_in_array(area_parents,{designation:designation});

            if (parent_info){
                let area_label = parent_info.label;
                Models.request_sender($http,"get","child_areas",function (data) {
                    // console.log(data);
                    if (data.find_data !== undefined){
                        for (let item of data.find_data){
                            item.label = item.name;
                            item.value = item.id;
                            instance.addOption(item);
                        }
                        if (default_area){
                            instance.setValue(default_area);
                        }
                    }
                },["'"+area_label+"'",1,0,"'"+user_id+"'",true]);

            }
        },
        onChange:function (value) {

        }
    });

};

Models.gant_chart_init = function () {
    let gant_chart = {
        script_load:function (call_back) {
            Models.script_load("static/app/assets/js/custom/gantt_chart.js",function () {
                call_back();
            });
        },
        init:function (options) {
            let default_options = {
                element:"#gantt_chart",
                resizable:false,
                draggable:false,
                data: [], // gantt data
                startDate: moment().startOf("month").format("MM/DD/YYYY"),
                endDate: moment().endOf("month").add(15, 'days').format("MM/DD/YYYY"),
                // showToday: true, // highlight today
                // startToday: true, // scroll to today
                behavior: {
                    // onClick: function (data) {
                    //     console.log("You clicked on an event: "+"\n", data);
                    // },
                    // onResize: function (data) {
                    //     console.log('You resized an event: '+"\n", data);
                    // },
                    // onDrag: function (data) {
                    //     console.log("You dragged an event: "+"\n", data);
                    // }
                }

            };
            this.script_load(function () {
                Object.assign(default_options,options);
                // console.log(default_options);
                // assign weekly calendar
                let $gantt_chart = $(default_options.element);

                if($gantt_chart.length) {
                    // console.log(default_options);
                    $gantt_chart.ganttView(default_options);

                    $gantt_chart.find('[title]').each(function() {
                        $(this).attr('data-uk-tooltip',"{offset:4}");
                    })

                }

            });
            return default_options;

        }

    };
    return gant_chart;
};

Models.peity_charts_init = function () {
    let peity_chart = {
        script_load:function (call_back) {
            Models.script_load("static/app/bower_components/peity/jquery.peity.min.js",function () {
                Models.script_load("static/app/bower_components/countUp.js/dist/countUp.min.js",function () {
                    call_back();
                });
            });
        },
        init:function (options) {
            let default_options = {
                element:"#gantt_chart",

            };
            this.script_load(function () {
                Object.assign(default_options,options);
                $(".peity_orders").peity("donut", {
                    height: 24,
                    width: 24,
                    fill: ["#8bc34a", "#eee"]
                });
                $('.countUpMe').each(function () {
                    var target = this,
                        countTo = $(target).text();
                    theAnimation = new CountUp(target, 0, countTo, 0, 2);
                    theAnimation.start();
                });
                $(".peity_visitors").peity("bar", {
                    height: 28,
                    width: 48,
                    fill: ["#d84315"],
                    padding: 0.2
                });

            });
            return default_options;

        }

    };
    return peity_chart;
};

Models.chartist_init = function () {
    let chartist = {
        script_load:function (call_back) {
            Models.css_load("static/app/bower_components/chartist/dist/chartist.min.css",function () {
                Models.css_load("static/app/bower_components/c3js-chart/c3.min.css",function () {
                    Models.script_load("static/app/bower_components/c3js-chart/c3.js",function () {
                        Models.script_load("static/app/bower_components/d3/d3.min.js",function () {
                            Models.script_load("static/app/bower_components/chartist/dist/chartist.min.js",function () {
                                call_back();
                            });
                        });
                    });
                });
            });

        },
        init:function (options,call_back = function () {}) {
            let default_options = {
                element:"#c3_chart_donut",
                columns: [
                    ['Absent', 30],
                    ['Present', 120],
                    ['Training', 10],
                    ['Leave', 30],
                ],

            };
            this.script_load(function () {
                Object.assign(default_options,options);
                // donut chart
                var c3chart_donut_id = '#c3_chart_donut';

                if ( $(c3chart_donut_id).length ) {

                    var c3chart_donut = c3.generate({
                        bindto: c3chart_donut_id,
                        data: {
                            columns: default_options.columns,
                            type : 'donut',
                            onclick: function (d, i) {  },
                            onmouseover: function (d, i) { },
                            onmouseout: function (d, i) {  }
                        },
                        donut: {
                            title: "Attendance",
                            width: 40
                        },
                        color: {
                            pattern: ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
                        }
                    });
                    call_back(c3chart_donut);


                }

            });
            return default_options;

        }

    };
    return chartist;
};


Models.events_by_institute_ids = function ($http,institute_ids,start_date,end_date,callback) {
    Models.request_sender($http,"get","events_of_institutes",function (data) {
        if (data.find_data){
            callback(data.find_data);
        }
    },["'"+institute_ids+"'","'"+start_date+"'","'"+end_date+"'"]);
};

Models.my_institutes = function ($http,designation,institute_id=null,callback=function () {}) {
    Models.request_sender($http,"get","my_institutes",function (data) {
        if (data.find_data){
            callback(data.find_data);
        }
    },["'"+designation+"'","'"+institute_id+"'"]);
};
Models.institutes_teachers = function ($http,institute_ids,callback) {
    Models.request_sender($http,"get","institutes_teachers",function (data) {
        if (data.find_data){
            callback(data.find_data);
        }
    },["'"+institute_ids+"'"]);
};

Models.is_logged_user_teacher = function ($rootScope) {
    let result = false;
    let logged_user = Models.logged_user($rootScope);
    if (logged_user){
        let designation = logged_user.designation;
        if (Models.logged_user_type($rootScope) === "teacher" && designation.toLowerCase() !== "head teacher") {
            result = true;
        }
    }
    return result;
};
Models.event_short_name = function (event) {
    let result = null;
    let events = {
      late:"L",
      present:"P",
      early:"E",
      holiday:"H",
      training:"T",
      absent:"A",
      leave:"V",
    };
    if (events[event] !== undefined){
        result = events[event];
    }
    return result;

};


Models.mailbox_init = function () {
    let mailbox = {
        script_load:function (call_back) {
            Models.script_load("static/app/assets/js/pages/page_mailbox.js",function () {
                call_back();
            });
        },
        init:function (options) {
            this.script_load(function () {
                altair_helpers.hierarchical_slide();
                Models.init_requirement();
                altair_mailbox.init(options);

            });
        }

    };
    return mailbox;
};

Models.getTwentyFourHourTime = function (amPmString) {
    let current_date = moment().format("MM/DD/YYYY");
    var d = new Date(current_date + " " + amPmString);
    return d.getHours().toString().padStart(2,'0') + ':' + d.getMinutes().toString().padStart(2,'0');
};

Models.uri_source_info = function($http,$routeParams,call_back){
    let student_member_id = $routeParams.source;
    if (student_member_id !== undefined){
        Models.request_sender($http,"get","user_info",function (source_info) {
            if (source_info.user_id !== undefined){
                call_back(source_info.user_id,source_info);
            }
        },["'"+student_member_id+"'",true])
    }
    else{
        call_back(0);
    }
};

Models.multiple_script_loader = function(script_list,call_back){
    let fns = {
        loader:function (script_list,call_back) {
            let instance = this;
            if (script_list.length){
                let current_script = script_list[0];
                Models.script_load(current_script,function () {
                    script_list.shift();
                    instance.loader(script_list,function () {
                        call_back();
                    })
                })
            }
            else{
                call_back();
            }

        },
        init:function (script_list,call_back) {
            this.loader(script_list,function () {
                call_back();
            })

        }
    };
    fns.init(script_list,function () {
        call_back();
    });
    return fns;
};

Models.tree_script_loader = function (call_back) {
    Models.css_load("static/app/assets/skins/jquery-fancytree/ui.fancytree.min.css",function () {
        Models.script_load("static/app/bower_components/jquery.fancytree/dist/jquery.fancytree-all.min.js",function () {
            Models.script_load("static/app/assets/js/pages/plugins_tree.js",function () {
                call_back();
            });
        });
    });
    return true;
};

Models.data_table_script_load = function (call_back) {
    let script_list = [
        "static/app/bower_components/datatables/media/js/jquery.dataTables.min.js",
        "static/app/bower_components/datatables-buttons/js/dataTables.buttons.js",
        "static/app/assets/js/custom/datatables/buttons.uikit.js",
        "static/app/bower_components/datatables-buttons/js/buttons.colVis.js",
        "static/app/bower_components/datatables-buttons/js/buttons.html5.js",
        "static/app/bower_components/datatables-buttons/js/buttons.print.js",
        "static/app/assets/js/custom/datatables/datatables.uikit.min.js",
        "static/app/assets/js/pages/plugins_datatables.js",
    ];
    Models.multiple_script_loader(script_list,function () {
        call_back();
    });
    return true;
};