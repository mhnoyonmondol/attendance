let Model = {};
Model.response = function (data) {
    UIkit.modal.info(data);
};
Model.informer = function(data){
    if(data.status !== undefined) {
        // if (!data.status) {
        let messages = data.messages;
        let errors = data.errors;
        let m_length = messages.length;
        let e_length = errors.length;
        let info_html = "";
        // If message available
        if(!data.status){
            if (m_length) {
                let message_html = "<h3>Message</h3>";
                for (let message in messages) {
                    message_html += "<p>" + messages[message] + "</p>";
                }
                info_html += message_html;
            }
        }
        // If error available
        if (e_length) {
            let error_html = "<h3>Error</h3>";
            for (let error in errors) {
                error_html += "<p>" + errors[error] + "</p>";
            }
            info_html += error_html;
            console.log(data)
        }
        return info_html;

        // }

    }
    else{
        return "data not found";
    }
};

$(document).ready(function () {
    altair_md.init();
    $(".install").submit(function (e) {
        e.preventDefault();
        $(".install-button").html("Installing...");
        let form_data = new FormData($(this)[0]);
        form_data.append("request_name","install()");
        form_data.append("request_type","post");
        $.ajax({
            url: "./request",
            type:"POST",
            processData: false,
            contentType: false,
            data:form_data,
            dataType:"json",
            success:function (data) {
                let inform = Model.informer(data);
                if (data.status){
                    $(".install-button").html("Done");
                    window.location = "./";
                }
                else{
                    $(".error").html("Sorry! something gonna be wrong.");
                    $(".install-button").html("Try again");
                    $('.error-box').addClass('active');
                    $('.error-box .md-card-content').html(inform);
                }
                console.log(data);
            },
            error:function (data) {
                $(".install-button").html("Failed");
                console.log(data);
            }
        });
        $("form input").change(function () {
            $(".error").html("");
            $('.error-box').removeClass('active');
        });
    });
});