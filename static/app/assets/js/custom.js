
$(document).ready(function () {
    $("body").on("click","form button",function () {
        $("form button").removeAttr("active");
        $(this).attr("active",true)
    });

    $("body").on("click",":reset",function () {
        setTimeout(function () {
            Models.update_inputs();
        },100)

    });
    // When all resource loaded done. then <style> tag remove because sidebar menu dynamic change css is active
    $('head style').remove();
    // manage password field
    $(document).on("click",".password-show-button",function () {

        let password_field = $(this).closest(".password-relative").find("input");
        let current_type = password_field.attr("type");
        if (current_type === "password"){
            password_field.attr("type","text");
        } else{
            password_field.attr("type","password");
        }
    });
    $("body").on("click",".block-delete-button i",function () {
        let service = $(this).closest(".service-item");
        let transport_item = $(this).closest(".transport-item");
        let special_service_item = $(this).closest(".special-service-item");
        if (service.length){
            let hr = service.prev("hr");
            hr.remove();
            service.remove();
        }
        else if(transport_item.length){
            let service = $(this).closest(".transport-item");
            let hr = service.prev("hr");
            hr.remove();
            service.remove();
        }
        else if(special_service_item.length){
            let service = $(this).closest(".special-service-item");
            let hr = service.prev("hr");
            hr.remove();
            service.remove();
        }


    });
});

window.addEventListener("beforeprint", function(event) {
    if ($('body').find('.print-area').length){
        $("body").addClass('print-layout');
    }
});
// window.addEventListener("afterprint", function(event) {
//     $("body").removeClass('print-layout');
// });


// window.onbeforeprint = function () {
//     //
// };
// Set connection status
Models.connection = 1;
// function setConnected(isConnected){
//     // var buttons = 'input[type="submit"], button';
//     if( isConnected ){
//         Models.connection = 1;
//         // $(buttons).removeAttr('disabled', 'disabled');
//     } else {
//         Models.connection = 0;
//         // $(buttons).attr('disabled', 'disabled');
//     }
// }
//
// if(navigator.onLine){
//     // set the connection status to true
//     setConnected(true);
// } else {
//     // Not online, set the connection status to false
//     setConnected(false);
// }
//
// $(window).bind('online', function(){
//    // set the connection status to true
//    setConnected(true);
// });
// $(window).bind('offline', function(){
//    // set the connection status to false
//    setConnected(false);
// });
//
// //initiate service worker for db indexdb
// let connection = new JsStore.Instance(new Worker('static/app/assets/js/plugins/JsStore-master/dist/jsstore.worker.min.js'));





// if("serviceWorker" in navigator){
//     // navigator.serviceWorker.register("sw.js")
//     //     .then(function () {
//     //         console.log("Service worker registered");
//     //     });
//     $("body").on("click",'.save-home',function () {
//         if (navigator.serviceWorker.controller){
//             console.log('new data action');
//             let updates = [
//                 '/favicon.png',
//             ];
//             for (let i=1;i<=49;i++){
//                 updates.push("/static/app/assets/img/gallery/img ("+i+").jpg")
//             }
//             console.log(updates);
//             navigator.serviceWorker.controller.postMessage(updates);
//             $('.after-image').attr('src','/static/app/assets/img/gallery/img (1).jpg');
//         }
//         else{
//             alert("service worker not ready");
//         }
//     });
//
//
//     console.log("its have");
//
// }
// else{
//     console.log("not fond");
// }

