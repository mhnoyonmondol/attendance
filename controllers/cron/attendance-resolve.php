<?php
    header('Access-Control-Allow-Origin: *');

    //// require define for accurate path
    $document_root = $_SERVER['DOCUMENT_ROOT'] ?? '';
    $explode_document_root = explode("/",$document_root);
    $last_directory_name = $explode_document_root[count($explode_document_root ) - 1] ?? '';
    $define_file_name = "../../define.php";
    if ($last_directory_name != "htdocs"){
        $define_file_name = '/home/admin/web/edukitgps.com/public_html/define.php';
    }

    require_once $define_file_name;
    require_once(project_root."controllers/packages/vendor/autoload.php");
    $return_object = array(
        "status" => 0,
        "errors" => array(),
        "messages" => array()
    );

    set_time_limit(0);

    $fas_api_common = new common();
    $common = new common();
    try {

        if ($common->is_online_host()){
            $pdo = new PDO("mysql:host=localhost;dbname=admin_fas_system", "admin_fas_system", "70557082");
        }
        else{
            $pdo = new PDO("mysql:host=localhost;dbname=fas_api", "root", "");
        }


        $fas_api_common->connection = $pdo;

    } catch (PDOException $e) {
        echo "Fas api connection faild";
        die();
    }



    $institutes_sql = "left join institute_devices on institute_devices.institute_id = institutes.institute_id 
    where institutes.active != :delete";
    $institutes = $common->retriever("institutes","institute_devices.serial_number",$institutes_sql,array(
        ":delete" => 2
    ),true);

    $execution_limit = 500;

    $total_logs = 0 ;

    $delete_attendance_logs = $common->connection->exec('TRUNCATE `attendance_logs`;');

    $join_event_types = $common->sql_in_maker(array("present","early","late"));
    $delete_attendance_calendar_events = $common->deleter("calendar","where event_type in($join_event_types)");

    foreach ($institutes as $institute){
        $device_info =  $fas_api_common->retriever("devices","id","where sn = :sn ",array(
            ":sn" => $institute['serial_number']
        ));

        $offset = 0;
        while(true){
            $attendance_logs = $fas_api_common->retriever("attendance_logs","*","where device_id = :device_id limit $offset,$execution_limit",array(
                ":device_id" => $device_info['id'] ?? ''
            ),true);
            if ($attendance_logs){
                $total_logs += count($attendance_logs);
                $attendance_entries_for_edukit_api = array();

                foreach ($attendance_logs as $attendance_log){
                    $attendance_entries_for_edukit_api[] = array(
                        "user_id" => $attendance_log['enrollid'],
                        "check_type" => $attendance_log['in_or_out'],
                        "check_time" => $attendance_log['time'],
                    );
                }

                /// get reply using edukit attendance api by cURL
                $post_fields = http_build_query(array(
                    "request_type" => "attendance",
                    "machine_sn" => $institute['serial_number'],
                    "data" => json_encode($attendance_entries_for_edukit_api),
                ));
                $domain = "localhost/attendance";
                if ($common->is_online_host()){
                    $domain = "edukitgps.com";
                }
                $ch = curl_init();
                curl_setopt($ch,CURLOPT_URL,"http://$domain/controllers/api/attendance.php");
                curl_setopt($ch,CURLOPT_POST,true);
                curl_setopt($ch,CURLOPT_POSTFIELDS,$post_fields);
                curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
                $raw_result = curl_exec($ch);
                $result = json_decode($raw_result,true);

                curl_close($ch);

            }

            if (!$attendance_logs){
                break;
            }
            else{
                $offset += $execution_limit;
            }
        }

    }

    echo 'total logs: '.$total_logs;




?>