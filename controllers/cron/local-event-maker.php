<?php
    header('Access-Control-Allow-Origin: *');

    //// require define for accurate path
    $document_root = $_SERVER['DOCUMENT_ROOT'];
    $explode_document_root = explode("/",$document_root);
    $last_directory_name = $explode_document_root[count($explode_document_root ) - 1];
    $define_file_name = "../../define.php";
    if ($last_directory_name != "htdocs"){
        $define_file_name = '/home/admin/web/edukitgps.com/public_html/define.php';
    }

    require_once $define_file_name;
    require_once(project_root."controllers/packages/vendor/autoload.php");
    $return_object = array(
        "status" => 0,
        "errors" => array(),
        "messages" => array()
    );

    set_time_limit(0);

    $common = new common();
    $time = date("Y-m-d");
    //before date
    $previous_date = $common->next_time(-1,0,0,"Y-m-d",$time." 00:00:00");
    $search_date = $previous_date;
    $join_global_types = $common->sql_in_maker(array("default","institute"));
    /// find default events from calender
    $default_event_sql = "where active != :delete and DATE(calendar.start_time)>=:start_date and DATE(calendar.end_time)<=:end_date and 
     calendar.type in($join_global_types)";
    $default_event_columns = "*";
    $default_events = $common->retriever("calendar",$default_event_columns,$default_event_sql,array(
        ":delete" => 2,
        ":start_date" => $search_date,
        ":end_date" => $search_date,
    ),true);


    $current_default_event_types = array();
    foreach ($default_events as $e){
        $current_default_event_types[] = $e['event_type'];
    }
    $join_current_event_types = $common->sql_in_maker($current_default_event_types);

    $holiday_type_info = $common->initial_data->event_types("holiday");
    $events_info = array(
        "holiday" => $holiday_type_info
    );
//    $late_type_info = $common->initial_data->event_types("late");
//    $early_type_info = $common->initial_data->event_types("early");

    $institute_per_loop = 100;
    $loaded_institute = 0;
    $loop_count = 0;
    while(true){
        $institutes = $common->retriever("institutes","institute_id","where active != :delete limit $loaded_institute, $institute_per_loop",array(
            ":delete" => 2
        ),true);
        $institute_ids = array();
        foreach ($institutes as $institute){
            $institute_ids[] = $institute['institute_id'];
        }
        $teacher_digit = $common->id_type("teacher");
        $student_digit = $common->id_type("student");
        $join_institute_ids = $common->sql_in_maker($institute_ids);
        $user_sql = "
        where controllers.active != :delete and (controllers.user_id like :teacher_id or controllers.user_id like :student_id) and 
        controllers.institute_id in($join_institute_ids)";
        $user_sql_columns = "controllers.user_id,controllers.institute_id";
        $institutes_users = $common->retriever("controllers",$user_sql_columns,$user_sql,array(
            ":delete" => 2,
            ":teacher_id" => $teacher_digit."%",
            ":student_id" => $student_digit."%",
        ),true);

        $institutes_user_ids = array();
        foreach ($institutes_users as $user){
            $institutes_user_ids[] = $user['user_id'];
        }

        $join_institutes_user_ids = $common->sql_in_maker($institutes_user_ids);
        $before_sql = "left join controllers on controllers.user_id = calendar.source_id 
         where calendar.active != :delete and calendar.source_id in($join_institutes_user_ids) and calendar.event_type in($join_current_event_types) 
        and DATE(calendar.start_time)>=:start_date and DATE(calendar.end_time)<=:end_date ";
        $before_sql_columns = "calendar.source_id,controllers.institute_id,calendar.event_type,date(calendar.start_time) as start_date,
        date(calendar.end_time) as end_date";
        $before_events_today = $common->retriever("calendar",$before_sql_columns,$before_sql,array(
            ":delete" => 2,
            ":start_date" => $search_date,
            ":end_date" => $search_date,
        ),true);


        $calender_event_entries = array();
        foreach ($institute_ids as $institute_id){
            $institute_users = $common->filter_in_array($institutes_users,array(
                "institute_id" => $institute_id
            ));

            foreach ($institute_users as $user){
                foreach ($default_events as $event){
                    $possible = false;
                    if ($event['type'] == "default"){
                        $possible = true;
                    }
                    elseif ($event['type'] == "institute"){
                        if ($institute_id == $event['source_id'])
                        $possible = true;
                    }

                    if ($possible){
                        $check_before = $common->filter_in_array($before_events_today,array(
                            "start_date" => $search_date,
                            "end_date" => $search_date,
                            "source_id" => $user['user_id'],
                            "institute_id" => $user['institute_id'],
                            "event_type" => $event['event_type'],
                        ),false);
                        if (!$check_before){
                            $event_info = $events_info[$event['event_type']];
                            $attendance_status = $event['event_type'];
                            $event_title = $event['title'];
                            $event_color = $event_info['color'];
                            $user_type = $common->id_type_name($user['user_id']);
                            $calender_event_entries[] = array(
                                ":type" => $user_type,
                                ":source_id" => $user['user_id'],
                                ":event_type" => $attendance_status,
                                ":title" => $event_title,
                                ":color" => $event_color,
                                ":time" => $time,
                                ":active" => 1,
                                ":start_time" => $search_date . " " . "00:00:00",
                                ":end_time" => $search_date . " " . "00:00:00",
                                ":creator_id" => "cron",
                                ":time_for_event" => $search_date
                            );
                        }
                    }



                }

            }
        }

        if ($calender_event_entries){
            try{
                $columns = "title,type,source_id,event_type,start_time,end_time,color,active,time,creator_id,time_for_event";
                $values = ":title,:type,:source_id,:event_type,:start_time,:end_time,:color,:active,:time,:creator_id,:time_for_event";
                $send = $common->creator("calendar",$columns,$values,$calender_event_entries,true);
                if (!$send){
                    $return_object['errors'][] = "New events data send failed";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
        }
        $loaded_institute += count($institute_ids);
        $loop_count++;
        if (count($institutes) == $institute_per_loop){

        }
        else{
            break;
        }
    }

    if (!$common->error_in_object($return_object)){
        $return_object['status'] = 1;
    }
    $return_object['institute_per_loop'] = $institute_per_loop;
    $return_object['loaded_institute'] = $loaded_institute;
    $return_object['loop_count'] = $loop_count;
    echo "<pre>";
    print_r($return_object);

?>