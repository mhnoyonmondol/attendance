<?php
header('Access-Control-Allow-Origin: *');

    //// require define for accurate path
    $document_root = $_SERVER['DOCUMENT_ROOT'];
    $explode_document_root = explode("/",$document_root);
    $last_directory_name = $explode_document_root[count($explode_document_root ) - 1];
    $define_file_name = "../../define.php";
    if ($last_directory_name != "htdocs"){
        $define_file_name = '/home/admin/web/edukitgps.com/public_html/define.php';
    }


    require_once $define_file_name;
    require_once(project_root."controllers/packages/vendor/autoload.php");
    $return_object = array(
        "status" => 3,
        "errors" => array(),
        "messages" => array()
    );

//    set_time_limit(0);

    $common = new common();
    try{
        $columns = "time";
        $values = ":time";
        $send = $common->creator("test",$columns,$values,array(
            ":time" => date("Y-m-d H:i:s")
        ));
        if (!$send){
            $return_object['errors'][] = "New events data send failed";
        }
        else{
            $return_object['messages'][] = 'send';
        }
    }catch (Exception $exception){
        $return_object['errors'][] = $exception->getMessage();
    }
    echo "<pre>";
    print_r($return_object);

?>