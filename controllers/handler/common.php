<?php
    require_once(project_root."/controllers/installer/db-config.php");
    class common extends db
    {
        protected $host_of_db;
        protected $name_of_db;
        protected $name_of_db_user;
        protected $password_of_user;
        public $connection;
        public $connection_status = 1;
        public $initial_data;

        function __construct()
        {
//            global $db_host, $db_name, $db_password, $db_user_name;
            $this->host_of_db = $this->db_host;
            $this->name_of_db = $this->db_name;
            $this->name_of_db_user = $this->db_user_name;
            $this->password_of_user = $this->db_password;
            if ($this->is_localhost()){
                $this->host_of_db = "localhost";
//                $this->name_of_db = "march_edukit";
                $this->name_of_db = "edukit_online";
                $this->name_of_db_user = "root";
                $this->password_of_user = "";
            }

            $this->initial_data = new initial_data();
            try {
                $pdo = new PDO("mysql:host=$this->host_of_db;dbname=$this->name_of_db", $this->name_of_db_user, $this->password_of_user);
//                $pdo->exec("set names utf8");

                $this->connection = $pdo;

            } catch (PDOException $e) {
                $this->connection_status = 0;
//                die("connection off");
            }
        }


        function date_difference($start_date,$end_date,$format="%a"){
            $start_date = new DateTime($start_date);
            $end_date = new DateTime($end_date);
            $day_difference = $end_date->diff($start_date)->format($format);
            return $day_difference;
        }
        function am_pm_time_format($time){
            $result = 0;
            $explode = explode(" ",$time);
            if (count($explode) == 2){
                $result = $time;
            }
            else{
                $default_format = date("Y-m-d ".$time.":00");
                $result = $this->text_date_time("h:i A",$default_format);
            }
            return  $result;
        }

        function is_localhost(){
            $result = false;
            $server_address = $_SERVER['SERVER_ADDR'] ?? '123.123.0.0';
            if ($server_address == "127.0.0.1" or $server_address == "::1" ){
                $result = true;
            }

            return $result;
        }

        function is_online_host(){
            $result = false;
            if (!$this->is_localhost()){
                $result = true;
            }
            return $result;
        }

        function main_domain(){
            $server_name = "localhost";
            if (isset($_SERVER['SERVER_NAME'])){
                $server_name = $_SERVER['SERVER_NAME'];
            }
            /// remove (www.) if available of this name string
            $server_name = preg_replace("/www\./","",$server_name,1);
            /// devide name string
            $explode = explode(".",$server_name);
            /// is sub domain available ? if explode count = 3 then its true or false
            if (count($explode) == 3){
                $sub_domain = $explode[0];
                // remove sub domain part
                $server_name = preg_replace("/".$sub_domain."\./","",$server_name,1);
            }

            if($this->site_port() != 80 && $this->site_port() != 443){
                $server_name .= ":".$this->site_port();
            }
            return $server_name;
        }
        function site_protocol(){
            $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
            return $protocol;
        }
        function site_port(){
            $port = 80;
            if (isset($_SERVER['SERVER_PORT'])){
                $port = $_SERVER['SERVER_PORT'];
            }

            return $port;
        }

        function db_info(){
            return array(
                "db_host" => $this->host_of_db,
                "db_name" => $this->name_of_db,
                "db_user_name" => $this->name_of_db_user,
                "db_password" => $this->password_of_user

            );
        }

        function get_db_tables_schema($connection=null)
        {
            if (!$connection){
                $connection = $this->connection;
            }
            $prepare = $connection->prepare("show tables");
            $prepare->execute();
            $tables = $prepare->fetchAll(PDO::FETCH_COLUMN);
            $table_schema = array();
            foreach ($tables as $table) {
                $prepare = $connection->prepare("SHOW INDEXES FROM $table");
                $prepare->execute();
                $indexes = $prepare->fetchAll(PDO::FETCH_ASSOC);

                //get table columns
                $prepare = $connection->prepare("SHOW COLUMNS FROM $table");
                $prepare->execute();
                $columns = $prepare->fetchAll(PDO::FETCH_ASSOC);
                $table_schema[] = array(
                    "table_name" => $table,
                    "columns" => $columns,
                    "indexes" => $indexes
                );
            }
            return $table_schema;
        }
        function db_schema_update($materials){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            // latest db is new changes
            // and old db is updatable
            $requirement = array(
                "latest_db" => array(
                    "db_host" => "",
                    "db_name" => "",
                    "db_user_name" => "",
                    "db_password" => "",

                ),
                "old_db" => array(
                    "db_host" => "",
                    "db_name" => "",
                    "db_user_name" => "",
                    "db_password" => "",
                ),
            );
            $requirement = array_merge($requirement,$materials);
            $latest_connection = $old_connection = null;
            if ($materials){
                // check material is valid
                if (!isset($materials['latest_db'])){
                    $return_object['errors'][] = "Latest database information not set";
                }
                else{
                    foreach ($requirement['latest_db'] as $key => $value){
                        if (!$value and $key != "db_password"){
                            $return_object['errors'][] = "Latest database ($key) not set";
                        }
                    }
                }
                if (!isset($materials['old_db'])){
                    $return_object['errors'][] = "Old database information not set";
                }
                else{
                    foreach ($requirement['old_db'] as $key => $value){
                        if (!$value and $key != "db_password"){
                            $return_object['errors'][] = "Old database ($key) not set";
                        }
                    }
                }
                if (!$this->error_in_object($return_object)){
                    $latest_db_schema = array();
                    $old_db_schema = array();
                    $old_db_auto_increment_data = array();
                    // get latest database schema
                    try {
                        $latest_db_host = $requirement['latest_db']['db_host'];
                        $latest_db_name = $requirement['latest_db']['db_name'];
                        $latest_db_user_name = $requirement['latest_db']['db_user_name'];
                        $latest_db_password = $requirement['latest_db']['db_password'];
                        $latest_connection = new PDO("mysql:host=$latest_db_host;dbname=$latest_db_name", $latest_db_user_name, $latest_db_password);
                        $latest_db_schema = $this->get_db_tables_schema($latest_connection);
                    } catch (PDOException $e) {
                        $return_object['errors'][] = $e->getMessage();
                    }
                    // get old database schema
                    try {
                        $old_db_host = $requirement['old_db']['db_host'];
                        $old_db_name = $requirement['old_db']['db_name'];
                        $old_db_user_name = $requirement['old_db']['db_user_name'];
                        $old_db_password = $requirement['old_db']['db_password'];
                        $old_connection = new PDO("mysql:host=$old_db_host;dbname=$old_db_name", $old_db_user_name, $old_db_password);
                        $old_db_schema = $this->get_db_tables_schema($old_connection);
//                        $old_db_auto_increment_data = $this->tables_auto_increment_schema($old_db_name,$old_connection);

                    } catch (PDOException $e) {
                        $return_object['errors'][] = $e->getMessage();
                    }
                    if (!$latest_db_schema){
                        $return_object['errors'][] = "Latest database schema empty";
                    }
//                    if (!$old_db_schema){
//                        $return_object['errors'][] = "Old database schema empty";
//                    }
                    //                print_r($latest_db_schema);
                    if ($this->error_in_object($return_object)){
                        return $return_object;
                    }
                    $update_query = "";
                    // compare together and make expected query
                    foreach ($latest_db_schema as $latest_table_schema){
                        $table_name = $latest_table_schema['table_name'];
                        $filter_keys = array(
                            "table_name" => $table_name
                        );
                        $old_table_schema = $this->filter_in_array($old_db_schema,$filter_keys,false);
                        // if old table not found as current table name
                        if (!$old_table_schema){

                            $create_params = array();
                            $auto_increment = "";
                            foreach ($latest_table_schema['columns'] as $column){
                                $column_name = $column['Field'];
                                $column_type = $column['Type'];
                                $type_explode = explode("(",rtrim($column_type,")"));
                                $type_name = $type_explode[0];
                                $type_length = "";
                                if (isset($type_explode[1])){
                                    $type_length = $type_explode[1];
                                }

                                $column_null = $column['Null'];
                                $column_key = $column['Key'];
                                $column_default = $column['Default'];
                                $column_extra = $column['Extra'];
                                $column_line_sql = "\t $column_name $type_name";
                                if ($type_length){
                                    $column_line_sql .= "($type_length) ";
                                }

                                $default_value = " DEFAULT ";
                                if ($column_null == "NO"){
                                    $default_value = " NOT ";
                                }
                                $column_line_sql .= $default_value." NULL";
                                if ($column_default){
                                    $column_line_sql .= " DEFAULT '$column_default'";
                                }
                                if ($column_extra == "auto_increment"){
                                    $auto_increment .= "-- \n";
                                    $auto_increment .= "-- Auto increment for table ($table_name) \n";
                                    $auto_increment .= "-- \n";
                                    $auto_increment .= " ALTER TABLE $table_name \n";
                                    $auto_increment .= "\t MODIFY $column_line_sql AUTO_INCREMENT;\n";
                                }
                                $create_params[] = $column_line_sql;


                            }
                            $join_create_params = join(",\n",$create_params);
                            if ($join_create_params){
                                $update_query .= "-- \n";
                                $update_query .= "-- Add new table ($table_name) schema \n";
                                $update_query .= "-- \n";
                                $update_query .= "CREATE TABLE $table_name (\n";
                                $update_query .= $join_create_params;
                                $update_query .= "\n)";
                                $update_query .= ";\n";
                            }

                            // make index query
                            $index_data_obj = array();
                            foreach ($latest_table_schema['indexes'] as $index_data){
                                $index_table_name = $index_data['Table'];
                                $index_key = $index_data['Key_name'];
                                $index_column = $index_data['Column_name'];
                                $index_primary = "";
                                if ($index_key == "PRIMARY"){
                                    $index_primary = " PRIMARY";
                                    $index_key = "";
                                }
                                else{
                                    $index_key = " $index_key";
                                }

                                $index_line = "\t ADD $index_primary KEY $index_key ($index_column)";

                                $index_data_obj[$index_table_name][] = $index_line;

                            }
                            foreach ($index_data_obj as $table_name => $lines){
                                $update_query .= "-- \n";
                                $update_query .= "-- Indexes for  ($table_name) \n";
                                $update_query .= "-- \n ";
                                $update_query .= "ALTER TABLE $table_name \n";
                                $update_query .= join(",\n",$lines);
                            }

                            if ($index_data_obj){
                                $update_query .= ";\n";
                            }
                            // add auto increment line if available
                            if ($auto_increment){
                                $update_query .= $auto_increment;
                            }
                            else{
                                $auto_increment = "";
                            }
                        }
                        // if any change into old and latest table schema
                        elseif($latest_table_schema != $old_table_schema){
//                            print_r($old_table_schema);
                            $update_params = array();
                            $auto_increment = "";
                            foreach ($latest_table_schema['columns'] as $column){
                                $column_name = $column['Field'];
                                $column_type = $column['Type'];
                                $type_explode = explode("(",rtrim($column_type,")"));
                                $type_name = $type_explode[0];
                                $type_length = "";
                                if (isset($type_explode[1])){
                                    $type_length = $type_explode[1];
                                }

                                $column_null = $column['Null'];
                                $column_key = $column['Key'];
                                $column_default = $column['Default'];
                                $column_extra = $column['Extra'];
                                $column_line_sql = " $column_name $type_name";
                                if ($type_length){
                                    $column_line_sql .= "($type_length) ";
                                }

                                $default_value = " DEFAULT ";
                                if ($column_null == "NO"){
                                    $default_value = " NOT ";
                                }
                                $column_line_sql .= $default_value." NULL";
                                if ($column_default){
                                    $column_line_sql .= " DEFAULT '$column_default'";
                                }
                                if ($column_extra == "auto_increment"){
                                    $auto_increment .= "-- \n";
                                    $auto_increment .= "-- Auto increment for table ($table_name) \n";
                                    $auto_increment .= "-- \n";
                                    $auto_increment .= " ALTER TABLE $table_name \n";
                                    $auto_increment .= "\t MODIFY $column_line_sql AUTO_INCREMENT;\n";
                                }
                                // get old column data by this column name
                                $old_column_filter_keys = array(
                                    "Field" => $column_name
                                );
                                $old_column = $this->filter_in_array($old_table_schema['columns'],$old_column_filter_keys,false);
                                // if latest  column not exist. then add this column at old table
                                if (!$old_column){
                                    $update_params[] = "\t ADD ".$column_line_sql;
                                }
                                // if column exist into latest version but something changed. then update this column at old table
                                elseif($old_column != $column){
                                    $update_params[] = "\t MODIFY ".$column_line_sql;
                                }


                            }
                            $join_update_params = join(",\n",$update_params);
                            if ($join_update_params){
                                $update_query .= "-- \n";
                                $update_query .= "-- Update table ($table_name) schema \n";
                                $update_query .= "-- \n";
                                $update_query .= "ALTER TABLE $table_name \n";
                                $update_query .= $join_update_params;
                                $update_query .= ";\n";
                            }
                            // add auto increment line if available
                            if ($auto_increment){
                                //check auto increment column in old table
                                $increment_filter_keys = array(
                                    "Extra" => "auto_increment"
                                );
                                $check_increment = $this->filter_in_array($old_table_schema['columns'],$increment_filter_keys,false);
                                if (!$check_increment){
                                    $update_query .= $auto_increment;
                                }

                            }
                            else{
                                $auto_increment = "";
                            }

                            // make index query
                            $index_data_obj = array();
                            foreach ($latest_table_schema['indexes'] as $index_data){
                                $index_table_name = $index_data['Table'];
                                $index_key = $index_data['Key_name'];
                                $index_column = $index_data['Column_name'];
                                $index_primary = "";
                                if ($index_key == "PRIMARY"){
                                    $index_primary = "PRIMARY";
                                    $index_key = "";
                                }
                                // check this index key is exist in old database table
                                $old_index_filter_keys = array(
                                    "Column_name" => $index_column
                                );
                                $old_index_data = $this->filter_in_array($old_table_schema['indexes'],$old_index_filter_keys,false);
                                /// remove cardinality eky from both object // and solution will be next time
                                if ($old_index_data){
                                    unset($old_index_data['Cardinality']);
                                    unset($index_data['Cardinality']);
                                }
                                if (!$old_index_data){
                                    $index_line = "\t ADD $index_primary KEY $index_key ($index_column)";
                                    $index_data_obj[$index_table_name][] = $index_line;
                                }
                                elseif( $index_data != $old_index_data){
                                    /// delete old index
                                    if (isset($old_index_data['Key_name'])){
                                        $old_index_key = $old_index_data['Key_name'];
                                        $index_line = "\t DROP INDEX $old_index_key";
                                        $index_data_obj[$index_table_name][] = $index_line;
                                    }
                                    // add new index
                                    $index_line = "\t ADD $index_primary KEY $index_key ($index_column)";
                                    $index_data_obj[$index_table_name][] = $index_line;

                                }

                            }
                            foreach ($index_data_obj as $table_name => $lines){
                                if ($lines){
                                    $update_query .= "--\n";
                                    $update_query .= "-- Indexes for  ($table_name) \n";
                                    $update_query .= "--\n";
                                    $update_query .= "ALTER TABLE $table_name \n";
                                    $update_query .= join(",\n",$lines);
                                }

                            }
                            if ($index_data_obj){
                                $update_query .= ";\n";
                            }
                        }

                    }
                    if ($update_query){
                        try{
                            $old_connection->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
                            $update_status = $old_connection->exec("START TRANSACTION; ".$update_query." COMMIT;");

                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }


                }

            }
            else{
                $return_object['errors'][] = "Material empty";
            }
            if (!$this->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }

        function custom_time(){
            return date("Y-m-d H:i:s");
        }
        function number_from_text($str,$multiple=false){
            $d = preg_match_all("/-?\d+\.?\:?\d*/",$str,$matches);
            $data = $matches[0];
            if (!$multiple){
                if ($data){
                    return $data[0];
                }
                else{
                    return false;
                }
            }
            return $data;
        }

        function string_from_text($str,$multiple=false){
            $d = preg_match_all("/[^0-9]+(?<!:)(?<!\.)[^0-9]?/",$str,$matches);
            $data = $matches[0];
            if (!$multiple){
                if ($data){
                    return $data[0];
                }
                else{
                    return false;
                }
            }
            return $data;
        }
        function flat_percent_amount_info($amount){
            $bangla_switch = $this->bangla_switch();
            if ($bangla_switch){
                $amount = $this->number_conversion($amount,"b2e");

            }
            $result = array(
                "amount" => 0,
                "type" => "flat",
                "error" => ""
            );
            if ($amount){
                $number = $this->number_from_text($amount);
                $string = $this->string_from_text($amount);
                if ($number){
                    $error = "";
                    $type = "flat";
                    if ($string){
                        if ($string != "p"){
                            $error = "Unknown format";
                        }
                        else{
                            $type = "percent";
                        }
                    }


                    $result = array(
                        "amount" => $number,
                        "type" => $type,
                        "error" => $error
                    );
                }
            }
            return $result;
        }

//        function new_time($hour=0,$minute=0,$second=0,$day=0,$month=0,$year=0){
//            if ($day){
//
//            }
//            return mktime($hour,$minute,$second,$month,$day,$year);
//        }

        function db_tables_schema(){
            $prepare = $this->connection->prepare("show tables");
            $prepare->execute();
            $tables = $prepare->fetchAll(PDO::FETCH_COLUMN);
            $table_schema = array();
            $i = 0;
            foreach ($tables as $table){
                //get table columns
                $prepare = $this->connection->prepare("SHOW COLUMNS FROM $this->name_of_db.$table");
                $prepare->execute();
                $columns = $prepare->fetchAll(PDO::FETCH_ASSOC);
                $column_fields = array();
                foreach ($columns as $info){
                    $column_name = $info['Field'];
                    $column_type = $info['Type'];
                    $column_key = $info['Key'];
                    $column_extra = $info['Extra'];
                    $type = explode('(',$column_type);
                    $type_name = $type[0];
                    //check data type
                    $data_type = "";
                    if ($type_name == "int"){
                        $data_type = "number";
                    }
                    elseif($type_name == 'varchar'){
                        $data_type = "string";
                    }
                    /// check auto increment
                    $auto_increment = false;
                    if ($column_extra == 'auto_increment'){
                        $auto_increment = true;
                    }
                    // check primary key
                    $primary_key = false;
                    if ($column_key == "PRI"){
                        $primary_key = true;
                    }
                    $data = array(
                        "name" => $column_name,
                        "primaryKey" => $primary_key,
                        "autoIncrement" => $auto_increment,
                        "dataType" => $data_type
                    );
                    if ($auto_increment){
                        unset($data['dataType']);
                    }
                    $column_fields[] = $data;

                }
                $table_schema[] = array(
                    "name" => $table,
                    "columns" => $column_fields
                );
            }
            return $table_schema;
        }

        function request()
        {
            extract($_REQUEST);

        }

        function getBrowser()
        {
            $u_agent = $_SERVER['HTTP_USER_AGENT'] ?? '';
            $bname = 'Unknown';
            $platform = 'Unknown';
            $version = "";
            $ub = "";

            //First get the platform?
            if (preg_match('/linux/i', $u_agent)) {
                $platform = 'linux';
            } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
                $platform = 'mac';
            } elseif (preg_match('/windows|win32/i', $u_agent)) {
                $platform = 'windows';
            }

            // Next get the name of the useragent yes seperately and for good reason
            if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
                $bname = 'Internet Explorer';
                $ub = "MSIE";
            } elseif (preg_match('/Firefox/i', $u_agent)) {
                $bname = 'Mozilla Firefox';
                $ub = "Firefox";
            } elseif (preg_match('/Chrome/i', $u_agent)) {
                $bname = 'Google Chrome';
                $ub = "Chrome";
            } elseif (preg_match('/Safari/i', $u_agent)) {
                $bname = 'Apple Safari';
                $ub = "Safari";
            } elseif (preg_match('/Opera/i', $u_agent)) {
                $bname = 'Opera';
                $ub = "Opera";
            } elseif (preg_match('/Netscape/i', $u_agent)) {
                $bname = 'Netscape';
                $ub = "Netscape";
            }

            // finally get the correct version number
            $known = array('Version', $ub, 'other');
            $pattern = '#(?<browser>' . join('|', $known) .
                ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
            if (!preg_match_all($pattern, $u_agent, $matches)) {
                // we have no matching number just continue
            }

            // see how many we have
            $i = count($matches['browser']);
            if ($i != 1) {
                //we will have two since we are not using 'other' argument yet
                //see if version is before or after the name
                if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                    $version = $matches['version'][0];
                } else {
                    $version = $matches['version'][1];
                }
            } else {
                $version = $matches['version'][0];
            }

            // check if we have a number
            if ($version == null || $version == "") {
                $version = "?";
            }

            return array(
                'userAgent' => $u_agent,
                'name' => $bname,
                'version' => $version,
                'platform' => $platform,
                'pattern' => $pattern
            );
        }

        function default_image($type,$all_data=false){
            $data = array(
                "profile" => "static/app/assets/img/default/profile.png",
                "item" => "static/app/assets/img/default/item.png",
                "logo" => "static/app/assets/img/default/logo.png",
                "banner" => "static/app/assets/img/default/banner.png",
                "long_banner" => "static/app/assets/img/default/banner-long.jpg",
                "challan" => "static/app/assets/img/default/challan.png",
                "no_image" => "static/app/assets/img/default/no-image.png",
            );
            if ($all_data){
                return $data;
            }
            elseif (isset($data[$type])){
                return $data[$type];
            }

            return 0;
        }
        function folder_maker($directory){
            $result = "";
            $date_root = project_root.$directory;
            if (!file_exists($date_root)){
                $status = mkdir($date_root,0777,true);
                if (!$status){
                    return "";
                }
            }
            return $directory."/";
        }
        function image_uploder($input_name,$prefix,$default=''){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            /// if expected file missing or empty
            if (!isset($_FILES[$input_name]['name']) or empty($_FILES[$input_name]['name'])){
                $default_image = $this->default_image($default);
                if ($default_image){
                    $data = array(
                        "file_path" => $default_image
                    );
                    $return_object['status'] = 1;
                    $return_object['upload_data'] = $data;
                }
                return $return_object;
            }

            $directory = "media/images/".date("Y/m/d");
            $date_root = project_root.$directory;
            if (!file_exists($date_root)){
                $status = mkdir($date_root,0777,true);
                if (!$status){
                    $return_object['errors'][] = "Folder not created";
                    return $return_object;
                }
            }
            $storage = new \Upload\Storage\FileSystem($date_root);
            $file = new \Upload\File($input_name, $storage);

            // Optionally you can rename the file on upload
            $new_filename = uniqid();
            $file->setName($prefix.$new_filename);

            // Validate file upload
            // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
            $file->addValidations(array(
                // Ensure file is of type "image/png"
                new \Upload\Validation\Mimetype(array('image/png', 'image/gif','image/jpg','image/jpeg')),

                //You can also add multi mimetype validation
                //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

                // Ensure file is no larger than 5M (use "B", "K", M", or "G")
                new \Upload\Validation\Size('100M')
            ));

            // Access data about the file that has been uploaded
            $data = array(
                'file_path'  => $directory."/".$file->getNameWithExtension(),
            );

            // Try to upload file
            try {
                // Success!
                $file->upload();
            } catch (\Exception $e) {
                // Fail!
                $errors = $file->getErrors();
                $return_object['errors'][] = $errors;
            }
            if (!$return_object['errors']){
                $return_object['status'] = 1;
                $return_object['upload_data'] = $data;
            }
            return $return_object;
        }
        function retriever($table_name,$columns,$sql,$data=array(),$multiple_data=false){
            $query = $this->connection->prepare("select $columns from $table_name $sql");
            $query->execute($data);
            $find_data = array();
            if ($multiple_data){
                $result = $query->fetchAll(PDO::FETCH_ASSOC);
                $find_data = $result;
            }
            else{
                $result = $query->fetch(PDO::FETCH_ASSOC);
                if (isset($result['active'])){
                    $result['active'] = intval($result['active']);
                }
                $find_data = $result;
            }
            return $find_data;
        }
        function data_counter($table_name,$columns,$sql,$data=array()){
            $query = $this->connection->prepare("select $columns from $table_name $sql");
            $query->execute($data);
            $count = $query->rowCount();
            return $count;
        }

        function modifier($table_name,$sql,$data=array(),$multiple=false){
            $query = $this->connection->prepare("update $table_name set $sql");
            if ($multiple){
                try{
                    $total_data = count($data);
                    $total_response = 0;
                    $this->connection->beginTransaction();
                    foreach ($data as $item){
                        $send = $query->execute($item);
                        if (!$send){
                            $error_info = "Error in $table_name";
                            throw new Exception($error_info);
                        }
                        else{
                            $total_response++;
                        }
                    }
                    $this->connection->commit();
                    if ($total_data == $total_response){
                        return true;
                    }

                }catch (Exception $exception){
                    throw new Exception("Bulk data not update for ".$exception->getMessage());
                }
            }
            $action = $query->execute($data);
            if ($action){
                return true;
            }
            else{
                $error_info = "Error in $table_name";
                throw new Exception($error_info);
            }
        }
        function deleter($table_name,$sql,$data=array(),$multiple=false){
            $query = $this->connection->prepare("delete from $table_name $sql");
            if ($multiple){
                try{
                    $total_data = count($data);
                    $total_response = 0;
                    $this->connection->beginTransaction();
                    foreach ($data as $item){
                        $send = $query->execute($item);
                        if (!$send){
                            $error_info = "Error in $table_name";
                            throw new Exception($error_info);
                        }
                        else{
                            $total_response++;
                        }
                    }
                    $this->connection->commit();
                    if ($total_data == $total_response){
                        return true;
                    }

                }catch (Exception $exception){
                    throw new Exception("Bulk data not delete for ".$exception->getMessage());
                }
            }
            $action = $query->execute($data);
            if ($action){
                return true;
            }
            else{
                $error_info = "Error in $table_name";
                throw new Exception($error_info);
            }
        }
        function creator($table_name,$columns,$values,$data=array(),$multiple = false){
//            echo "insert into $table_name ($columns) values ($values)";
            $query = $this->connection->prepare("insert into $table_name ($columns) values ($values)");
            if ($multiple){
                try{
                    $total_data = count($data);
                    $total_response = 0;
                    $this->connection->beginTransaction();
                    foreach ($data as $item){
                        $send = $query->execute($item);
                        if (!$send){
                            $error_info = "Error in $table_name";
                            throw new Exception($error_info);
                        }
                        else{
                            $total_response++;
                        }
                    }
                    $this->connection->commit();
                    if ($total_data == $total_response){
                        return true;
                    }

                }catch (Exception $exception){
                    throw new Exception("Bulk data not pass for ".$exception->getMessage());
                }
            }
            $action = $query->execute($data);
            if ($action){
                return true;
            }
            else{
                $error_info = "Error in $table_name";
                throw new Exception($error_info);
            }
        }
        function id_type($type=''){
            //for ensure last update type is available
            $data=array(
                "system_designer" => "000",
                "system_admin" =>"001",
                "admin" =>"002",
                "teacher" =>"003",
                "institute" =>"004",
                "student" =>"005",
            );
            if ($type){
                if (isset($data[$type])){
                    return $data[$type];
                }else{
                    return false;
                }
            }
            else{
                return $data;
            }

            /// option types maybe below
            //.. member_id,any user_id,
        }

        function member_id_prefix($type){
            $data = array(
                "system_designer" => "S",
                "system_admin" => 'SA',
                "admin" => 'A',
                "teacher" => 'T',
                "institute" => 'I',
                "leave" => 'L',
                "training" => 'TA',
                "student" => 'ST',

            );
            if ($type){
                if (isset($data[$type])){
                    $result = $data[$type];
                    return $result;
                }
                else{
                    return false;
                }
            }
            else{
                return $data;
            }
        }
        function id_type_option($type=''){
            //for ensure last update type is available
            $data=array(
                "invoice" => "1",
                "bill" => "2",
                "receipt" => "3",
                "branch_item" => "4",
                "bank_cash" => "5",
                "application" => "6",
            );
            if ($type){
                if (isset($data[$type])){
                    return $data[$type];
                }else{
                    return false;
                }
            }
            else{
                return $data;
            }

            /// option types maybe below
            //.. member_id,any user_id,
        }


        function currency_symbol_name($symbol,$language="en"){
            $result = "";
            $symbol_list = array(
                "৳" => array(
                    "bn" => "টাকা",
                    "en" => "taka"
                ),
                "$" => array(
                    "bn" => "ডলার",
                    "en" => "dollar"
                ),

            );
            if (isset($symbol_list[$symbol])){
                $result = $symbol_list[$symbol][$language];
            }
            return $result;
        }
        function get_last_update($type,$option="",$user_row_id="",$device="",$table_name="last_updates",$source=""){
            $condition = " where type=:type and type_option=:option and user_row_id=:user_row_id and device=:device and source=:source ";
            $data = array(
                ":type" => $type,
                ":option" => $option,
                ":user_row_id" => $user_row_id,
                ":device" => $device,
                ":source" => $source,
            );
            $last_update = $this->retriever($table_name,"*",$condition,$data);
            return $last_update;
        }

        function get_last_update_value($type,$option="",$default_user_info=false,$user_row_id="",$device_id=""){
            if ($default_user_info){
                $user_cookies = $this->user_cookies_data();
                $user_row_id = $user_cookies['logged_user_row_id'];
                $device_id = $user_cookies['device'];
            }
            $info = $this->get_last_update($type,$option,$user_row_id,$device_id);
            if ($info){
                return $info['value'];
            }
            return "";
        }

        function get_last_amount($type,$option="",$user_row_id="",$device=""){
            $info = $this->get_last_update($type,$option,$user_row_id,$device);
            $amount = 0;
            if ($info){
                $amount=$info['value'];
            }
            return $amount;
        }
        function getRealIpAddr()
        {
            if (!empty($_SERVER['HTTP_CLIENT_IP'] ?? ''))   //check ip from share internet
            {
                $ip=$_SERVER['HTTP_CLIENT_IP'] ?? '';
            }
            elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'] ?? ''))   //to check ip is pass from proxy
            {
                $ip=$_SERVER['HTTP_X_FORWARDED_FOR'] ?? '';
            }
            else
            {
                $ip=$_SERVER['REMOTE_ADDR'] ?? '';
            }
            return $ip;
        }
        function get_ip_id(){
            return 0;
        }
        function id_info($id,$info_type){
            $id_type = substr($id,0,3);
            $serial = substr($id,3,10);
            $user_row_id = substr($id,13,10);
            $device_row_id = substr($id,23,10);
            $option = substr($id,33,33);
            $self = $id_type.$serial.$user_row_id.$device_row_id;
            if ($info_type == "type"){
                return $id_type;
            }
            elseif ($info_type == "serial"){
                return $serial;
            }
            elseif ($info_type == "user_row_id"){
                return $user_row_id;
            }
            elseif ($info_type == "device_row_id"){
                return $device_row_id;
            }
            elseif ($info_type == "option"){
                return $option;
            }
            elseif ($info_type == "self"){
                return $self;
            }
            else{
                throw new Exception("Id info not found");
            }
        }

        function type_name($type){
            $data=array_flip($this->id_type());
            return $data[$type] ?? '';
        }

        function id_type_name($user_id){
            try{
                $id_info=$this->id_info($user_id,"type");
            }catch (Exception $exception){
                return false;
            }
            return $type_name=$this->type_name($id_info);
        }
        function password_generator(){
            return rand(10000000,99999999);
        }
        function mail_sender($from_info, $to_info, $subject, $message){
            $result = 0;
            try{
                if (!isset($from_info)){
                    throw new Exception("From info not set");
                }
                else{
                    if (!isset($from_info['email'])){
                        throw new Exception("From email not set");
                    }
                    if (!isset($from_info['name'])){
                        throw new Exception("From name not set");
                    }
                }
                if (!isset($to_info)){
                    throw new Exception("To info not set");
                }
                else{
                    if (!isset($to_info['email'])){
                        throw new Exception("To email not set");
                    }
                    if (!isset($to_info['name'])){
                        throw new Exception("To name not set");
                    }
                }
                if (!$message){
                    throw new Exception("Message not set");
                }
                try{
                    $response_code = $this->third_party_mail_sender($from_info,$to_info,$subject,$message);
                }catch (Exception $exception){
                    throw new Exception("Mail send failed of third party for ".$exception->getMessage());
                }
                if ($response_code == 200 or $response_code == 202){
                    $result = 1;
                }
                else{
                    $to = $to_info['email'];
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

                    // More headers
                    $from_email = $from_info['email'];
                    $headers .= "From: <$from_email>" . "\r\n";

                    $status = mail($to,$subject,$message,$headers);
                    if ($status){
                        $result = 1;
                    }
                }
            }catch (Exception $exception){
                throw new Exception("Mail not send for ".$exception->getMessage());
            }
            return $result;
        }
        function third_party_mail_sender($from_info, $to_info, $subject, $message){
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($from_info['email'], $from_info['name']);
            $email->setSubject($subject);
            $email->addTo($to_info['email'], $to_info['name']);
            $email->addContent(
                "text/html", $message
            );
            $sendgrid = new \SendGrid("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");
            try {
                $response = $sendgrid->send($email);
                return $response->statusCode();
            } catch (Exception $e) {
                throw new Exception("Send grid mail send fail for ". $e->getMessage());
            }
        }

        function make_proper_mobile_number($mobile_number){
            $mobile = substr($mobile_number,-10);
            return "880".$mobile;
        }
        function sms_sender($sms,$numbers){
            $result = array(
                "insertedSmsIds" => 0000,
                "isError" => false,
                "message" => "Success",
                "errorCode" => 0,
            );

            $user_id = "March.Admin";
            $password = "March.Admin";
//            if (true){
            if ($this->is_online_host()){
                $url = "https://panel.inboxbd.net/API/SendSMSv2?userId=$user_id&password=$password&commaSeperatedReceiverNumbers=$numbers&smsText=$sms";
                $server_response=@file_get_contents($url);
                $server_response=json_decode($server_response,true);
                if ($server_response){
                    $status = $server_response['status'] ?? '';
                    if ($status == 'Submitted successfully'){
                        $result['insertedSmsIds'] = $server_response['MSISDN_ID_List'] ?? array();
                    }
                }
            }
            else{
                $result['insertedSmsIds'] = "1";
            }
            return $result;

        }
    function send_sms($message,$requirement,$time,$connection=null,$institute_info=array()){

        $common = $this;
        $space_per_sms = ceil(strlen($message) / 160);
        if ($connection){
            $common->connection = $connection;
        }

        if (!$institute_info){
            $institute_info = $this->retriever("institutes","sms_balance,name,contact_number","where institute_id = :institute_id",array(
                ":institute_id" => $requirement['institute_id']
            ));
        }


        $sms_balance = $institute_info['sms_balance'] ?? 0;
        if ($sms_balance >= $space_per_sms){
            $send_sms = $common->sms_sender($message,$common->make_proper_mobile_number($requirement['mobile']));
            if (!$send_sms['insertedSmsIds']){
                throw new Exception("SMS send failed! error code is ".$send_sms['errorCode']);
            }
            else{
                $common->modifier('institutes','sms_balance = sms_balance - :amount where institute_id = :institute_id',array(
                    ":amount" => $space_per_sms,
                    ":institute_id" => $requirement['institute_id']
                ));
                $columns = "receiver_id,receiver_type,time,message,amount,active,mobile,institute_id";
                $values = ":receiver_id,:receiver_type,:time,:message,:amount,:active,:mobile,:institute_id";

                return $save = $common->creator("sms_records",$columns,$values,array(
                    ":receiver_id" => $requirement['receiver_id'],
                    ":receiver_type" => $requirement['receiver_type'],
                    ":institute_id" => $requirement['institute_id'],
                    ":mobile" => $requirement['mobile'],
                    ":time" => $time,
                    ":active" => 1,
                    ":amount" => $space_per_sms,
                    ":message" => $message,
                ));
            }
        }

        return true;


    }

        function get_api_sms_balance(){
            $user_id = "March Edukit Ltd";
            $password = "m@rch321";

            $balance = 0;

            $url = "https://panel.inboxbd.net/API/Inquiry?userId=$user_id&password=$password ";
            $server_response=@file_get_contents($url);
            $server_response=json_decode($server_response,true);
            if ($server_response){
                $balance = $server_response['balance'] ?? 0;
            }
            return $balance;

        }


        function get_int($string){
            return preg_replace('/\D/', '', $string);
        }
        function get_int_without_row_id($string){
            // $logged_cookies
            $logged_cookies = $this->user_cookies_data();
            $device_row_id = $logged_cookies['device'];
            $logged_user_row_id = $logged_cookies['logged_user_row_id'];
            $replace_str = $logged_user_row_id.$device_row_id;
            $int = $this->get_int($string);
            return preg_replace("/$replace_str/", '', $int,1);
        }

        function file_deleter($file_name){
            $result = true;
            $default_images = $this->default_image("",true);
            foreach ($default_images as $image){
                $image_name = project_root.$image;
                if($file_name == $image_name){
                    return true;
                }
            }
            if (file_exists($file_name)){
                $status = unlink($file_name);
                if (!$status){
                    $result = false;
                }
            }
            return $result;
        }
        function user_cookies_data(){
            $return_object = array(
                "logged_user_id" => "",
                "device" => "",
                "logged_user_row_id" => "",
            );
            if (isset($_SESSION['logged_user_id'])){
                $return_object['logged_user_id'] = $_SESSION['logged_user_id'];
            }
            if (isset($_SESSION['device'])){
                $return_object['device'] = $_SESSION['device'];
            }
            if (isset($_SESSION['logged_user_row_id'])){
                $return_object['logged_user_row_id'] = $_SESSION['logged_user_row_id'];
            }

            return $return_object;
        }
        function logged_user_type(){
            $user_cookies = $this->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            return $this->id_type_name($logged_user_id);
        }
        function logged_user_id(){
            $user_cookies = $this->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            return $logged_user_id;
        }

        function assign_device(){
            $result = "";
            if (isset($_SESSION['logged_user_id'])){
                $user_id = $_SESSION['logged_user_id'];
                $ip = $this->getRealIpAddr();
                //check if exist
                $check_sql = "where user_id=:user_id and ip=:ip";
                $check_sql_data = array(
                    ":user_id" => $user_id,
                    ":ip" => $ip
                );
                $get_data = $this->retriever("devices","id",$check_sql,$check_sql_data);
                if (!$get_data){
                    $columns = "user_id,ip,device_info,time,active";
                    $values = ":user_id,:ip,:device_info,:time,:active";
                    $data = array(
                        ":user_id" => $user_id,
                        ":ip" => $ip,
                        ":device_info" => "",
                        ":time" => $this->custom_time(),
                        ":active" => 1
                    );
                    try{
                        $creater = $this->creator("devices",$columns,$values,$data);
                        if ($creater){
                            $get_data = $this->retriever("devices","id",$check_sql,$check_sql_data);
                            if ($get_data){
                                $id = $get_data['id'];
                                $result = $id;
                            }
                        }
                    }catch (Exception $exception){
                    }
                }
                else{
                    $result = $get_data['id'];
                }
            }
            return $result;

        }
        function error_in_object($object_data){
            $result = false;
            if ($object_data['errors'] or $object_data['messages']){
                $result = true;
            }
            return $result;
        }
        function next_time($day=0,$hour=0,$minute=0,$format="Y-m-d H:i:s",$timestamp=null){
            if ($timestamp){
                $explode = explode(" ",$timestamp);
                $date = $explode[0];
                $time = $explode[1];
                $explode_date = explode("-",$date);
                $year = $explode_date[0];
                $month = $explode_date[1];
                $day = $explode_date[2] + $day;
                $explode_time = explode(":",$time);
                $hour = $explode_time[0] + $hour;
                $minute = $explode_time[1] + $minute;
                $next_time = date($format,mktime($hour,$minute,0,$month,$day,$year));
                return $next_time;
            }


            $next_time=date($format,strtotime("+$day days  $hour hours $minute minutes"));
            return $next_time;
        }
        function due_date($issue_date,$due_day){
            $iss_date=explode("-",$issue_date);
            $d=$iss_date[2];
            $m=$iss_date[1];
            $y=$iss_date[0];
            $time=mktime(0,0,0,$m,$d,$y);
            $due_date=date("Y-m-d",strtotime("+$due_day days",$time));
            return $due_date;
        }
        function amount($amount, $separator=""){
            if (!$amount){
                $amount = 0;
            }
            return number_format($amount,2,".",$separator);
        }
        function number($amount){
            $number = floatval($amount);
            return $number;
        }
        function is_divisional($divisional_amount, $division_by){
            $result = false;
            if ($division_by){
                $result = true;
            }
            return $result;
        }

        function get_flat_rate($item_info){
            $trade_price = $this->number($item_info['trade_price']);
            $vat = $this->number($item_info['vat']);
            $vat_type = $item_info['vat_type'];
            $discount = $this->number($item_info['discount']);
            $discount_type = $item_info['discount_type'];
            if ($vat_type == "percent"){
                $vat = ($trade_price*$vat)/100;
            }
            if ($discount_type == "percent"){
                $discount = ($trade_price*$discount)/100;
            }
            $calc_rate = ($trade_price - $discount) + $vat;
            $flat_rate = $this->amount($calc_rate);
            return $flat_rate;
        }
        function logged_branch_id(){
            $branch_id = "";
            if (isset($_SESSION['logged_branch_id'])){
                $branch_id = $_SESSION['logged_branch_id'];
            }
            return $branch_id;
        }

        function document_id_maker($type,$type_option="",$last_amount="",$branch_id=""){
            $result = false;
            $id_maker = $member_id_maker = "";
            $user_cookies = $this->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $logged_branch_id = $this->logged_branch_id();
            if ($branch_id){
                $logged_branch_id = $branch_id;
            }
            if (!$logged_branch_id){
                throw new Exception("Branch required");
            }
            /// check the type is exist
            $document_info = $this->initial_data->document_types($type);
            if ($document_info){
                /// check the type option in this document type
                if ($document_info['options']){
                    $options = $document_info['options'];
                    $filter_keys = array(
                        "label" => $type_option
                    );
                    $filter = $this->initial_data->filter_in_array($options,$filter_keys,false);
                    if (!$filter){
                        throw new Exception("Document type option is invalid");
                    }
                }
                else{
                    throw new  Exception("Document type option undefined");
                }

            }
            try{
                /// make document id
                $id_maker = new id_maker("document",$logged_user_row_id,$device,$type,$last_amount);
                if ($id_maker->generate_id){
                    $document_id = $id_maker->generate_id;
                    $result = $document_id;
                }
                else{
                    throw new  Exception("Document id not generated");
                }

            }catch (Exception $exception){
                throw new Exception("Document id creator".$exception->getMessage());
            }
            return $result;

        }
        function document_member_id_maker($type,$type_option="",$last_amount="",$branch_id=""){
            $result = false;
            $id_maker = $member_id_maker = "";
            $user_cookies = $this->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $logged_branch_id = $this->logged_branch_id();
            if ($branch_id){
                $logged_branch_id = $branch_id;
            }
            if (!$logged_branch_id){
                throw new Exception("Branch required");
            }
            /// check the type is exist
            $document_info = $this->initial_data->document_types($type);
            if ($document_info){
                /// check the type option in this document type
                if ($document_info['options']){
                    $options = $document_info['options'];
                    $filter_keys = array(
                        "label" => $type_option
                    );
                    $filter = $this->initial_data->filter_in_array($options,$filter_keys,false);
                    if (!$filter){
                        throw new Exception("Document type option is invalid");
                    }
                }
                else{
                    throw new  Exception("Document type option undefined");
                }

            }
            //// make document member id
            //:: if you know member id allow only document type ? but you should be able to maked by document types option key;
            //:: but make sure, you have member id prefix as option type key, which is required for (member id prefixer in common)
            try{
                $member_id_maker = new member_id_maker($type_option,$logged_user_row_id,$device,$last_amount);

                if ($member_id_maker->generate_id){
                    $member_id = $member_id_maker->generate_id.date("-Y");
                    $result = $member_id;
                }
                else{
                    throw new Exception("Member id not generated for document ");
                }

            }catch (Exception $exception){
                throw new Exception($exception->getMessage());
            }
            return $result;

        }
        function document_creator($type,$type_option=""){
            $result = false;
            $id_maker = $member_id_maker = "";
            $user_cookies = $this->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $logged_branch_id = $this->logged_branch_id();
//            if (!$logged_branch_id){
//                throw new Exception("Branch required");
//            }
            /// check the type is exist
            $document_info = $this->initial_data->document_types($type);
            if ($document_info){
                /// check the type option in this document type
                if ($document_info['options']){
                    $options = $document_info['options'];
                    $filter_keys = array(
                        "label" => $type_option
                    );
                    $filter = $this->initial_data->filter_in_array($options,$filter_keys,false);
                    if (!$filter){
                        throw new Exception("Document type option is invalid");
                    }
                }
                else{
                    throw new  Exception("Document type option undefined");
                }

            }
            try{
                /// make document id
                $id_maker = new id_maker("document",$logged_user_row_id,$device,$type);
                if ($id_maker->generate_id){
                    $document_id = $id_maker->generate_id;
                    $member_id = "";
                    $sql_columns = "branch_id,document_id,type,type_option,member_id,active,time,authorization_status,payment_status,creator_id";
                    $sql_values = ":branch_id,:document_id,:type,:type_option,:member_id,:active,:time,:authorization_status,:payment_status,:creator_id";
                    $sql_data = array(
                        ":document_id" => $document_id,
                        ":branch_id" => $logged_branch_id,
                        ":type" => $type,
                        ":type_option" => $type_option,
                        ":member_id" => "",
                        ":time" => $this->custom_time(),
                        ":active" => 1,
                        ":authorization_status" => 0,
                        ":payment_status" => "unpaid",
                        ":creator_id" => $this->logged_user_id(),
                    );
                    //// make document member id
                    //:: if you know member id allow only document type ? but you should be able to maked by document types option key;
                    //:: but make sure, you have member id prefix as option type key, which is required for (member id prefixer in common)
                    try{
                        $member_id_maker = new member_id_maker($type_option,$logged_user_row_id,$device);

                        if ($member_id_maker->generate_id){
                            $member_id = $member_id_maker->generate_id.date("-Y");
                            $sql_data[':member_id'] = $member_id;
                        }
                        else{
                            throw new Exception("Member id not generated for document ");
                        }

                    }catch (Exception $exception){
                        throw new Exception($exception->getMessage());
                    }

                    try{
                        $save_document = $this->creator("documents",$sql_columns,$sql_values,$sql_data);
                        if ($save_document){
                            $row_id = $this->connection->lastInsertId();
                            $document_info = array(
                                "row_id" => $row_id,
                                "document_id" => $document_id,
                                "member_id" => $member_id
                            );
                            $result = $document_info;
                            //update last value of document id maker
                            try{
                                $update = $id_maker->update();
                                if (!$update->status){
                                    $result = false;
                                }
                            }catch (Exception $exception){
                                throw new Exception($exception->getMessage());
                            }
                            //update last value of document member id
                            try{
                                $update = $member_id_maker->update();
                                if (!$update->status){
                                    $result = false;
                                }
                            }catch (Exception $exception){
                                throw new Exception($exception->getMessage());
                            }

                        }
                    }catch (Exception $exception){
                        throw new  Exception($exception->getMessage());
                    }

                }
                else{
                    throw new  Exception("Document id not generated");
                }

            }catch (Exception $exception){
                throw new Exception("Document id creator".$exception->getMessage());
            }

            return $result;

        }

        function custom_filter($array, $column, $key){
            return (array_search($key, array_column($array, $column)));
        }
        function index_number_in_array($array, $column, $key){
            return $this->custom_filter($array,$column,$key);
        }
        function index_number_in_list($array, $key){
            return $a = array_search($key, $array);
        }

        function negative_number($number){
            return $number - ($number + $number);
        }
        function document_source_option_name($document_type,$option_label){
            $label_name = "";
            $document_type_info = $this->initial_data->document_source($document_type);
            $options = $document_type_info['options'];
            $keys = array(
                "label"=> $option_label
            );
            $option_info = $this->initial_data->filter_in_array($options,$keys,false);
            if ($option_info){
                $label_name = $option_info['name'];
            }
            return $label_name;
        }


        function change_last_update($type,$value,$option="",$default_user_info=false,$user_row_id="",$device_id=""){
            $result = false;
            try{
                if ($default_user_info){
                    $user_cookies = $this->user_cookies_data();
                    $user_row_id = $user_cookies['logged_user_row_id'];
                    $device_id = $user_cookies['device'];
                }

                $last_update = new assign_last_update($type,$option,$user_row_id,$device_id,$value);
                $status = $last_update->status;
                if ($status){
                    return $status;
                }
            }catch (Exception $exception){
                throw new Exception("Last update change failed for ".$exception->getMessage());
            }
            return $result;
        }

        function filter_in_array($list,$filter_keys,$list_data=true,$negative=false,$original_index=false){
            return $this->initial_data->filter_in_array($list,$filter_keys,$list_data,$negative,$original_index);
        }


        function bangla_switch(){
            return 1;
        }
        function number_conversion ($number_str,$type){
            $EnglishToBanglaNumber= array("1" =>"১","2" =>"২","3" =>"৩","4" =>"৪","5" =>"৫","6" =>"৬","7" =>"৭","8" =>"৮","9" =>"৯","0" =>"০","%"=>"%");
            $BanglaToEnglishNumber=array_flip($EnglishToBanglaNumber);
            $array = array();
            if($type=="b2e"){
                $array= $BanglaToEnglishNumber;
            }
            elseif($type=="e2b"){
                $array= $EnglishToBanglaNumber;
            }
            elseif($type=="e2e"){
                $array=array();
            }
            $data=$number_str;
            foreach($array as $key=>$number){
                $data=str_replace($key,$number,$data);
            }
            return $data;
        }



        function round($amount){
            $round = floor($amount);
            $round_amount = $this->amount($amount - $round);
            $round_info = array(
                "amount" => $this->amount($round),
                "round_amount" => $round_amount,
            );
            return $round_info;
        }

        function text_date_time($format="",$date_time=""){
            if (!$date_time){
                $date_time = date("Y-m-d H:i:s");
            }
            if (!$format){
                $format = "M j Y, g:i A";
            }
            return date($format, strtotime($date_time));
        }

        function convertNumberToWordEn($num = false)
        {
            $num = str_replace(array(',', ' '), '' , trim($num));
            if(! $num) {
                return false;
            }
            $num = (int) $num;
            $words = array();
            $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
                'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
            );
            $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
            $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
                'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
                'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
            );
            $num_length = strlen($num);
            $levels = (int) (($num_length + 2) / 3);
            $max_length = $levels * 3;
            $num = substr('00' . $num, -$max_length);
            $num_levels = str_split($num, 3);
            for ($i = 0; $i < count($num_levels); $i++) {
                $levels--;
                $hundreds = (int) ($num_levels[$i] / 100);
                $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
                $tens = (int) ($num_levels[$i] % 100);
                $singles = '';
                if ( $tens < 20 ) {
                    $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
                } else {
                    $tens = (int)($tens / 10);
                    $tens = ' ' . $list2[$tens] . ' ';
                    $singles = (int) ($num_levels[$i] % 10);
                    $singles = ' ' . $list1[$singles] . ' ';
                }
                $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
            } //end for loop
            $commas = count($words);
            if ($commas > 1) {
                $commas = $commas - 1;
            }
            return implode(' ', $words);
        }

        function convertNumberToWord($number){
            $result = $number;
            $bangla_switch = $this->bangla_switch();
            $retrieve = new retrieve();
            $software_info = $retrieve->software_info();
            $currency = $software_info['currency'];
            if ($bangla_switch){
                $bangla_word_converter = new BanglaNumberToWord();
                $amount_as_words = $bangla_word_converter->numToWord($number);
                $result = $amount_as_words." ".$this->currency_symbol_name($currency,"bn");
            }
            else{
                $amount_as_words = $this->convertNumberToWordEn($number);
                $result = trim($amount_as_words.$this->currency_symbol_name($currency,"en"));
            }
            return $result;

        }
        function add_activities($action,$affected_id){
            $result = false;
            $user_cookies = $this->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $device_id = $user_cookies['device'];
            /// check first this action is exist in type of activities
            $action_info = $this->initial_data->type_of_activities($action);
            if ($action_info){
                try{
                    $columns = "user_id,affected_id,action,device,time";
                    $values = ":user_id,:affected_id,:action,:device,:time";
                    $data = array(
                        ":user_id" => $logged_user_id,
                        ":affected_id" => $affected_id,
                        ":action" => $action,
                        ":device" => $device_id,
                        ":time" => $this->custom_time()
                    );
                    $send = $this->creator("activities",$columns,$values,$data);
                    if ($send){
                        $result = true;
                    }
            }catch (Exception $exception){
                    throw new Exception("Activities add failed");
                }
            }
            else{
                throw new Exception("Invalid activities action");
            }
            return $result;
        }
        function readable_text($string){
            $clean = preg_replace("/([-]+|[_]+)/"," ",$string);
            $first_character_upper = ucfirst($clean);
            return $first_character_upper;
        }
        function excel_file_maker($data,$type=""){
            $result = array();
            $user_cookies = $this->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device_id = $user_cookies['device'];
            try{
                if ($data){
                    $header = [];
                    $first_row = $data[0];
                    foreach ($first_row as $key => $value){
                        $heading_text = $this->readable_text($key);
                        $format = "string";
                        if ($key == "id"){
                            $format = "integer";
                        }
                        elseif($key == "time"){
                            $format = "datetime";
                        }
                        elseif($key == "active"){
                            $format = "integer";
                        }
                        $header[$heading_text] = $format;
                    }
                    $wExcel = new Ellumilel\ExcelWriter();
                    $wExcel->writeSheetHeader('Sheet1', $header);
                    $wExcel->setAuthor('Page 71.org');
                    foreach ($data as $column){
                        $column_data = [];
                        foreach ($column as $key => $value){
                            $column_data[] = $value;
                        }
                        $wExcel->writeSheetRow('Sheet1', $column_data);
                    }
                    $file_name = $type." ".$logged_user_row_id." ".$device_id."_excel_data_".date('d-m-Y_').".xlsx";
                    $folder = "media/export/".date("d/m/Y");
                    $folder_maker = $this->folder_maker($folder);
                    if ($folder_maker){
                        $full_file_path = $folder_maker.$file_name;
                        $wExcel->writeToFile(project_root.$full_file_path);
                        $result['file_name'] = $file_name;
                        $result['file_path'] = $full_file_path;
                    }
                    else{
                        throw new Exception("Folder maker failed");
                    }
                }
                else{
                    throw new Exception("Empty data for excel file");
                }


            }catch (Exception $exception){
                throw new Exception("Excel file failed ".$exception->getMessage());
            }
            return $result;

        }

        function update_document_helpers($helpers=array(),$document_type,$document_type_option){
            $result = false;
            if ($helpers){
                $user_cookies = $this->user_cookies_data();
                $logged_user_id = $user_cookies['logged_user_id'];
                $join_text_list = array();
                $join_type_list = array();
                /// get values and prepare for check existence
                foreach ($helpers as $key => $value){
                    $join_text_value = "'".$value."'";
                    $join_type_value = "'".$key."'";
                    $join_text_list[] = $join_text_value;
                    $join_type_list[] = $join_type_value;
                }
                $join_text = join(",",$join_text_list);
                $join_type = join(",",$join_type_list);
                $sql = "where value in($join_text) and type in($join_type) and document_type=:document_type and document_type_option=:document_type_option";
                $sql_data = array(
                    ":document_type" => $document_type,
                    ":document_type_option" => $document_type_option,
                );

                try{
                    $find_becomes = $this->retriever("document_helpers","*",$sql,$sql_data,true);
                    /// make new entries
                    $new_entries = array();
                    foreach ($helpers as $key => $value){
                        $filter_keys = array(
                            "type" => $key,
                            "value" => $value
                        );
                        $filter = $this->filter_in_array($find_becomes,$filter_keys,false);
                        if (!$filter){
                            $entry_object = array(
                                ":user_id" => $logged_user_id,
                                ":document_type" => $document_type,
                                ":document_type_option" => $document_type_option,
                                ":type" => $key,
                                ":value" => $value,
                                ":active" => 1,
                                ":time" => $this->custom_time(),
                            );
                            $new_entries[] = $entry_object;
                        }

                    }
                    /// save new entries if available
                    if ($new_entries){
                        try{
                            $columns = "user_id,document_type,document_type_option,type,value,active,time";
                            $values = ":user_id,:document_type,:document_type_option,:type,:value,:active,:time";
                            $send = $this->creator("document_helpers",$columns,$values,$new_entries,true);
                            if (!$send){
                                throw new Exception("Document helpers data not send");
                            }
                            else{
                                $result = true;
                            }
                        }catch (Exception $exception){
                            throw new Exception($exception->getMessage());
                        }

                    }
                    else{
                        $result = true;
                    }
                }catch (Exception $exception){
                    throw new Exception( $exception->getMessage());
                }
            }
            else{
                throw new Exception("Helpers not found");
            }
            return $result;

        }
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        function barcode_generator($str,$type="TYPE_CODE_128"){
            if($type=="TYPE_CODE_128"){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                return base64_encode($generator->getBarcode($str, $generator::TYPE_CODE_128));
            }
            elseif($type=="TYPE_CODE_39"){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                return base64_encode($generator->getBarcode($str, $generator::TYPE_CODE_39));
            }
            elseif($type=="TYPE_INTERLEAVED_2_5"){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                return base64_encode($generator->getBarcode($str, $generator::TYPE_INTERLEAVED_2_5));
            }
            elseif($type=="TYPE_EAN_2"){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                return base64_encode($generator->getBarcode($str, $generator::TYPE_EAN_2));
            }
            elseif($type=="TYPE_UPC_A"){
                $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
                return base64_encode($generator->getBarcode($str, $generator::TYPE_UPC_A));
            }

        }
        function first_date_of_date($date){
            $d = new DateTime($date);
            $d->modify('first day of this month');
            return $d->format('Y-m-d');
        }
        function last_date_of_date($date){
            $d = new DateTime($date);
            $d->modify('last day of this month');
            return $d->format('Y-m-d');
        }


        function sql_in_maker($list){
            $ids_for_join = array();
            foreach ($list as $item){
                $ids_for_join[] = "'".$item."'";
            }
            $join_ids = join(",",$ids_for_join);
            return $join_ids;
        }

        function admin_institutes_join_sql($designation){
            $join_sql = "";
            $admin_type_info = $this->initial_data->admin_types($designation);
            if ($admin_type_info){
                $available_area = $admin_type_info['area'];
                $parent = "institute";
                $join_sql .= " left join areas as $parent on $parent.name = institutes.institute_id and $parent.active = '1'";

                foreach ($available_area as $index => $area){
                    $join_sql .= " left join areas as $area on $area.id = $parent.parent and $area.active = '1'";
                    $parent = $area;
                }
                $join_sql .= " left join controllers on controllers.area = $parent.id ";

            }
            return $join_sql;
        }






}

    class assign_last_update{
        public $common;
        public $status;
        function __construct($type,$option,$user_row_id,$device,$value,$table_name="last_updates",$source="")
        {
            $common = new common();
            $this->common = $common;
            // last update info
            $last_update_info = $common->get_last_update($type,$option,$user_row_id,$device,$table_name,$source);

            if ($last_update_info){
                try{
                    $sql = " value=:value where type=:type and type_option=:option and user_row_id=:user_row_id and device=:device and source=:source ";
                    $data = array(
                        ":value" => $value,
                        ":type" => $type,
                        ":option" => $option,
                        ":user_row_id" => $user_row_id,
                        ":device" => $device,
                        ":source" => $source,
                    );
                    $status = $common->modifier($table_name,$sql,$data);
                    if ($status){
                        $this->status = 1;
                    }
                    else{
                        $this->status = 0;
                    }
                }catch (Exception $exception){
                    throw new Exception("Last value not updated for ".$type.$option.$user_row_id.$device." in modify sql");
                }
            }else{
                try{
                    $columns = "type,type_option,user_row_id,device,value,time,source";
                    $values = ":type,:option,:user_row_id,:device,:value,:time,:source";
                    $data = array(
                        ":value" => $value,
                        ":type" => $type,
                        ":option" => $option,
                        ":user_row_id" => $user_row_id,
                        ":device" => $device,
                        ":source" => $source,
                        ":time" => $common->custom_time(),
                    );
                    $status = $common->creator($table_name,$columns,$values,$data);
                    if ($status){
                        $this->status = 1;
                    }
                    else{
                        $this->status = 0;
                    }
                }catch (Exception $exception){
                    throw new Exception("Last value not updated for ".$type.$option.$user_row_id.$device." in insert sql");
                }
            }
        }

    }
    class id_maker{
        public $common;
        public $generate_id;
        public $type;
        public $option;
        public $user_row_id;
        public $device;
        public $value;
        public $last_amount;
        function __construct($type,$user_row_id,$device,$option="",$last_amount="")
        {
            $common = new common();
            $this->common = $common;
            if ($type == "system_designer"){
                $user_row_id = 0;
                $device = 0;
            }

            $this->type = $type;
            $this->option = "user_id";
            $this->user_row_id = $user_row_id;
            $this->device = $device;
            // last update info
            try{
                if (!strlen($last_amount)){
                    $last_amount = $common->get_last_amount($type,$this->option,$user_row_id,$device);
                }

                $this->last_amount = $last_amount;
                $next_amount = $last_amount + 1;
                $this->value = $next_amount;
                //id type digit
                $id_type_digit_part = $common->id_type($type);
                $serial_part = str_pad($next_amount,10,0,STR_PAD_LEFT);

                if ($type != "system_designer" and !$user_row_id){
                    throw new Exception("User row id missing");
                }
                if ($type != "system_designer" and !$device){
                    throw new Exception("Device is missing");
                }

                $user_id_part = str_pad($user_row_id,10,0,STR_PAD_LEFT);
                $device_part = str_pad($device,10,0,STR_PAD_LEFT);
                $type_option_id= "";
                if ($option){
                    $type_option_id = $common->id_type_option($option);
                    if (!$type_option_id){
                        throw new Exception("Type option undefined");
                    }
                }

                if (strlen($option) == 66){
                    //get parent id of option
                    try{
                        $option = $common->id_info($option,"self");
                    }catch (Exception $exception){
                        throw new Exception($exception->getMessage());
                    }

                }
                elseif($type_option_id){
                    $option = $type_option_id;
                }
                elseif(strlen($option) > 33){
                    throw new Exception("Option is too long.");
                }
                $option_part = str_pad($option,33,0,STR_PAD_LEFT);
                $id = $id_type_digit_part.$serial_part.$user_id_part.$device_part.$option_part;
                $this->generate_id = $id;
            }
            catch (Exception $exception){
                throw new Exception("Id maker failed. The error is ".$exception->getMessage());
            }
        }
        function update(){
            return $assigner = new assign_last_update($this->type,$this->option,$this->user_row_id,$this->device,$this->value);
        }
    }
    class member_id_maker{
        public $common;
        public $generate_id;
        public $type;
        public $option;
        public $user_row_id;
        public $device;
        public $value;
        public $last_amount;
        function __construct($type,$user_row_id,$device_row_id,$last_amount="")
        {
            $common = new common();
            $this->common = $common;
            if ($type == "system_designer"){
                $user_row_id = 0;
                $device_row_id = 0;
            }
            $this->type = $type;
            $this->option = "member_id";
            $this->user_row_id = $user_row_id;
            $this->device = $device_row_id;
            // last update info
            try{
                if (!strlen($last_amount)){
                    $last_amount = $common->get_last_amount($type,$this->option,$user_row_id,$device_row_id);
                }

                $this->last_amount = $last_amount;
                $next_amount = $last_amount + 1;
                $this->value = $next_amount;
                $prefix = $common->member_id_prefix($type);
                if (!$prefix){
                    throw new Exception("Member id prefix undefined");
                }
                if ($type != "system_designer" and !$user_row_id){
                    throw new Exception("User row id missing");
                }
                if ($type != "system_designer" and !$device_row_id){
                    throw new Exception("Device is missing");
                }
                $member_id = $prefix.$user_row_id.$device_row_id.$next_amount;
                $this->generate_id = $member_id;
            }
            catch (Exception $exception){
                throw new Exception("Member Id maker failed. The error is ".$exception->getMessage());
            }

        }
        function update($value=""){
            if ($value){
                $this->value = $value;
            }
            return $assigner = new assign_last_update($this->type,$this->option,$this->user_row_id,$this->device,$this->value);
        }

    }

    class multiple_document_creator{
        /*$list = array(
                array(
                    "type" => "bill",
                    "type_option" => "distribution_bill",
                    "branch_id" => ""
                ),
                ....
            );*/
        public $status = 0;
        public $errors = array();
        public $messages = array();
        public $new_documents_entries = array();
        public $update_entries_last_updates = array();
        public $new_entries_last_updates = array();
        public $document_ids_for_join = array();
        function __construct($list)
        {
            if (!$list){
                $this->errors[] = "List empty";
            }
            $common = new common();
            $time = $common->custom_time();
            $user_cookies = $common->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $last_amount_of_types = array();
            $types = array();
            // get unique types make by list
            foreach ($list as $sl => $item) {

                $type = $item['type_option'];
                $index = $common->index_number_in_list($types,$type);
                if (!strlen($index)){
                    $types[] = $type;
                }
            }
            $types[] = "document";
            $types_for_join = array();
            foreach ($types as $type){
                $types_for_join[] = "'".$type."'";
            }
            $join_types = join(",",$types_for_join);
            /// find enlisted types
            $types_sql = "where type in ($join_types) and type_option in ('user_id','member_id') and 
            user_row_id=:user_row_id and device=:device";
            $types_sql_data = array(
                ":user_row_id" => $logged_user_row_id,
                ":device" => $device
            );
            $types_sql_columns = "*";
            $type_list = $common->retriever("last_updates",$types_sql_columns,$types_sql,$types_sql_data,true);

            // make new entries of last updates which is not existence
            $new_entries_last_updates = array();
            foreach ($types as $index => $type){
                $last_amount = 0;
                if ($type == "document"){
                    $filter_keys = array(
                        "type" => $type,
                        "type_option" => "user_id"
                    );
                    $filter = $common->filter_in_array($type_list,$filter_keys,false);
                    if (!$filter){
                        $new_entries_last_updates[] = array(
                            ":type" => "document",
                            ":type_option" => "user_id",
                            ":value" => 0,
                            ":user_row_id" => $logged_user_row_id,
                            ":device" => $device,
                            ":time" => $time
                        );
                    }
                    else{
                        $last_amount = $filter['value'];
                    }



                }
                else{
                    $filter_keys = array(
                        "type" => $type,
                        "type_option" => "member_id"
                    );
                    $filter = $common->filter_in_array($type_list,$filter_keys,false);
                    if (!$filter){
                        $new_entries_last_updates[] = array(
                            ":type" => $type,
                            ":type_option" => "member_id",
                            ":value" => 0,
                            ":user_row_id" => $logged_user_row_id,
                            ":device" => $device,
                            ":time" => $time
                        );
                    }
                    else{
                        $last_amount = $filter['value'];
                    }
                }
                $last_amount_of_types[$type] = $last_amount;

            }

            $new_documents_entries = array();
            $document_ids_for_join = array();
            foreach ($list as $sl => $item){
                /// make document id
                $type = $item['type'];
                $type_option = $item['type_option'];
                $branch_id = $item['branch_id'];
                $document_entry = array(
                    ":type" => $type,
                    ":type_option" => $type_option,
                    ":branch_id" => $branch_id,
                    ":time" => $time,
                    ":active" => 1,
                    ":authorization_status" => 0,
                    ":payment_status" => "unpaid",
                );

                try{
                    $last_document_amount = $last_amount_of_types['document'];
                    $document_id = $common->document_id_maker($type,$type_option,$last_document_amount,$branch_id);
                    if ($document_id){
                        $document_entry[':document_id'] = $document_id;
                        $document_ids_for_join[] = "'".$document_id."'";
                        $last_amount_of_types['document'] = $last_document_amount + 1;
                    }
                    else{
                        $this->errors = "$sl no. document id not generated";
                    }
                    $last_amount_of_types['document'] = $last_document_amount+1;
                }catch (Exception $exception){
                    $this->errors = $exception->getMessage();
                }
                /// make document member id
                try{
                    $last_member_id_amount = $last_amount_of_types[$type_option];
                    $document_member_id = $common->document_member_id_maker($type,$type_option,$last_member_id_amount,$branch_id);
                    if ($document_member_id){
                        $document_entry[':member_id'] = $document_member_id;
                        $last_amount_of_types[$type_option] = $last_member_id_amount+1;
                    }
                    else{
                        $this->errors = "$sl no. document member id not generated";
                    }

                }catch (Exception $exception){
                    $this->errors = $exception->getMessage();
                }
                $new_documents_entries[] = $document_entry;

            }
            $update_entries_last_updates = array();
            foreach ($last_amount_of_types as $type => $amount){
                $update_entry = array(
                    ":type" => $type,
                    ":value" => $amount,
                    ":user_row_id" => $logged_user_row_id,
                    ":device" => $device,
                );
                if ($type == "document"){
                    $update_entry[":type_option"] = "user_id";
                }
                else{
                    $update_entry[":type_option"] = "member_id";
                }
                $update_entries_last_updates[] = $update_entry;
            }
            $this->new_documents_entries = $new_documents_entries;
            $this->document_ids_for_join = $document_ids_for_join;
            $this->new_entries_last_updates = $new_entries_last_updates;
            $this->update_entries_last_updates = $update_entries_last_updates;


        }
        function action(){
            $common = new common();
            $documents = array();
            if (!$this->errors){
                /// send new entries for last updates
                $new_last_updates_columns = "type,type_option,value,user_row_id,device,time";
                $new_last_updates_values = ":type,:type_option,:value,:user_row_id,:device,:time";
                try{
                    $send_new_last_updates = $common->creator("last_updates",$new_last_updates_columns,$new_last_updates_values,$this->new_entries_last_updates,true);
                    if ($send_new_last_updates){
                        $update_last_updates_sql = "value = :value where type=:type and type_option=:type_option and 
                        user_row_id=:user_row_id and device=:device";
                        try{
                            $update_last_updates = $common->modifier("last_updates",$update_last_updates_sql,$this->update_entries_last_updates,true);
                            if ($update_last_updates){
                                $new_documents_columns = "branch_id,document_id,type,type_option,member_id,time,active,authorization_status,payment_status";
                                $new_documents_values = ":branch_id,:document_id,:type,:type_option,:member_id,:time,:active,:authorization_status,:payment_status";
                                try{
                                    $send_new_documents = $common->creator("documents",$new_documents_columns,$new_documents_values,$this->new_documents_entries,true);
                                    if ($send_new_documents){

                                    }
                                    else{
                                        $this->errors[] = "New document send failed";
                                    }
                                }catch (Exception $exception){
                                    $this->errors[] = $exception->getMessage();
                                }

                            }
                            else{
                                $this->errors[] = "Update failed for last updates";
                            }
                        }catch (Exception $exception){
                            $this->errors[] = "Error for update last updates. ".$exception->getMessage();
                        }
                    }
                    else{
                        $this->errors[] = "New last updates data send failed";
                    }
                }catch (Exception $exception){
                    $this->errors[] = "Error for new last updates entry . ".$exception->getMessage();
                }
            }
            if (!$this->errors){
                $join_document_ids = join(",",$this->document_ids_for_join);
                $documents_sql = "where document_id in ($join_document_ids)";
                $documents_sql_data = array();
                $documents_columns = "*";
                $documents = $common->retriever("documents",$documents_columns,$documents_sql,$documents_sql_data,true);
            }
            return $documents;
        }
    }

