<?php
    require_once(project_root."controllers/packages/vendor/autoload.php");
    class modify{
        protected $connection;
        public $common;
        public $validator;
        public $retriever;
        function __construct()
        {
            $db = new common();
            $this->retriever = new retrieve();
            $this->validator = new data_validator();
            $this->common = $db;
            $this->connection = $db->connection;
        }
        function edit_profile(){

            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            require_once project_root."controllers/handler/security.php";
            $user_id = $_REQUEST['user_id'];
            $before_member_id = $_REQUEST['before_member_id'];
            $before_password = $_REQUEST['before_password'];
            $before_image = $_REQUEST['before_image'];
            $before_nid = $_REQUEST['before_nid'];
            $before_signature = $_REQUEST['before_signature'];
            $before_image_delete_path = "";
            $before_nid_delete_path = "";
            $before_signature_delete_path = "";

//            print_r($before_nid);
            $common = $this->common;
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $controller_type = $common->id_type_name($user_id);
                /// define some variable
            $id_maker = $member_id_maker = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->controller_info_validator($controller_type);
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];

                if (!$data){
                    $return_object['errors'][] = "Controller data not set";
                }

                //if member id changed
                if ($member_id !=$before_member_id){
                    // check the member id is duplicate? and update the last value
                    try{
                        $member_id_maker = new member_id_maker($controller_type,$logged_user_row_id,$device);
                        $sql = " where member_id = :member_id ";
                        $sql_data = array(
                            ":member_id" => $member_id
                        );
                        try{
                            $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($checker){
                                $return_object['errors'][] = "Member id already exist";
                            }
                            else{
                                //update the member id
                                $input_last_amount = $common->get_int_without_row_id($member_id);
                                $last_amount = $member_id_maker->last_amount;
                                if ($input_last_amount>$last_amount){
                                    $update_amount = $input_last_amount;
                                }
                                else{
                                    $update_amount = $last_amount;
                                }
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                    }
                }

                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload nid image
                    $nid_file_name = "";
                    if(isset($_FILES['nid']['name']) and  !empty($_FILES['nid']['name'])){
                        $upload_nid = $common->image_uploder("nid",$controller_type."nid");
                        if ($upload_nid['status']){
                            $upload_data = $upload_nid['upload_data'];
                            $nid_file_name = $upload_data['file_path'];
                            $before_nid_delete_path = project_root.$before_nid;
                        }
                    }else{
                        $nid_file_name = $before_nid;
                    }

                    //upload profile image
                    $profile_file_name = "";
                    if (isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
                        $upload_profile_image = $common->image_uploder("image",$controller_type."profile",'profile');
                        if ($upload_profile_image['status']){
                            $upload_data = $upload_profile_image['upload_data'];
                            $profile_file_name = $upload_data['file_path'];
                            $before_image_delete_path = project_root.$before_image;
                        }
                    }
                    else{
                        $profile_file_name = $before_image;
                    }
                    
                    //upload signature image
                    $signature_file_name = "";
                    if (isset($_FILES['signature']['name']) and !empty($_FILES['signature']['name'])){
                        $upload_signature_image = $common->image_uploder("signature",$controller_type."signature",'no_image');
                        if ($upload_signature_image['status']){
                            $upload_data = $upload_signature_image['upload_data'];
                            $signature_file_name = $upload_data['file_path'];
                            $before_signature_delete_path = project_root.$before_signature;
                        }
                    }
                    else{
                        $signature_file_name = $before_signature;
                    }

                    //first assign last update for member id maker
                    //member id maker
                    if ($member_id_maker){
                        try{
                            $status = $member_id_maker->update($update_amount);
                            if (!$status->status){
                                $return_object['errors'][] = "Member id maker not update";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                        }
                    }



                    // send the user data
                    $more_sql = ",password=:password";
                    if (!$_REQUEST['password']){
                        $more_sql = "";
                        unset($data[":password"]);
                    }
                    $area = "";
                    if (isset($_REQUEST['area'])){
                        $area = ",area=:area";
                    }

                    $sql = "member_id=:member_id,name=:name,mobile=:mobile,email=:email$more_sql,
                    active=:active,joining_date=:joining_date,nid=:nid,basic_salary=:basic_salary,designation=:designation,
                    second_contact=:second_contact,blood_group=:blood_group,time=:time,image=:image,department=:department,address=:address,
                    login_schedule_switch=:login_schedule_switch,login_schedule=:login_schedule,signature=:signature,
                    signature_switch=:signature_switch$area 
                    where user_id=:user_id";
                    $data[':nid'] = $nid_file_name;
                    $data[':image'] = $profile_file_name;
                    $data[':signature'] = $signature_file_name;
                    $data[":user_id"] = $user_id;
                    // set before active for edit same admin
                    if ($_REQUEST['user_id'] == $_SESSION['logged_user_id']){
                        if (isset($_REQUEST['before_active'])){
                            $data[":active"] = $_REQUEST['before_active'];
                        }
                    }
                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
//                                print_r($data);
                                $send = $common->modifier("controllers",$sql,$data);
                                if ($send){
                                    // delete before files
                                    if ($before_image_delete_path){
                                        $delete = $common->file_deleter($before_image_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before image not deleted";
                                        }
                                    }
                                    if ($before_nid_delete_path){
                                        $delete = $common->file_deleter($before_nid_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before nid not deleted";
                                        }
                                    }

                                    if ($before_signature_delete_path){
                                        $delete = $common->file_deleter($before_signature_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before signature not deleted";
                                        }
                                    }

                                    /// if logged user id and editable user id is be same
                                    $logged_user_id = $_SESSION['logged_user_id'];
                                    if ($logged_user_id == $user_id){
                                        $this_profile_info = $this->retriever->controller_info($user_id);
                                        if ($this_profile_info){
                                            $return_object['profile_info'] = $this_profile_info;
                                        }
                                        else{
                                            $return_object['messages'][] = "Profile information failed";
                                        }

                                    }
                                    /// if profile type is mpo then worker territory entry enabled
                                    if (isset($_REQUEST['territory'])){
                                        $territories = $_REQUEST['territory'];
                                        try{
                                            $add_territories = $common->add_worker_territories($user_id,$territories);
                                            if (!$add_territories){
                                                $return_object['errors'][] = "Territory add failed";
                                            }
                                        }catch (Exception $exception){
                                            $return_object['errors'][] = $exception->getMessage();
                                        }
                                    }
                                    $return_object['member_id'] = $member_id;
                                    $return_object['messages'][] = "Data successfully updated.";
                                    $return_object['status'] = 1;
                                    /// record for activities this task
                                    $activity_type = "edit_".$controller_type;

                                    try{
                                        $record = $common->add_activities($activity_type,$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }
                                    /// access menu and permission part access data entry
                                    if ($_REQUEST['user_id'] !=$_SESSION['logged_user_id']){
                                        if ($controller_type == "system_admin"){
                                            //access menus
                                            if (isset($validation['estimate_access_menus'])){
                                                $menus_data = $validation['estimate_access_menus'];
                                                // set the user id in estimate access menus
                                                $i = 0;
                                                $selected_ids = array();
                                                foreach ($menus_data as $item){
                                                    $selected_ids[] = "'".$item[':value']."'";
                                                    $menus_data[$i][":user_id"] = $user_id;
                                                    $i++;
                                                }
                                                /// check all menu id exist/assigned in accesses
                                                /// send new data for not existence accesses
                                                $join = join(",",$selected_ids);
                                                $check_sql = "where user_id=:user_id and type=:type and type_option=:type_option and value in ($join)";
                                                $check_sql_data = array(
                                                    ":user_id" => $user_id,
                                                    ":type" => "access_menu",
                                                    ":type_option" => "system_admin"
                                                );
                                                $check_finder = $this->common->retriever("accesses","value",$check_sql,$check_sql_data,true);
                                                $new_entries = array();
                                                foreach ($menus_data as $menu_info){
                                                    $menu_id = $menu_info[":value"];
                                                    $filter_keys = array(
                                                        "value" => $menu_id
                                                    );
                                                    $check_in_finder = $common->filter_in_array($check_finder,$filter_keys,false);
                                                    if (!$check_in_finder){
                                                        $new_object = array(
                                                            ":user_id" => $user_id,
                                                            ":type" => "access_menu",
                                                            ":type_option" => "system_admin",
                                                            ":value" => $menu_id,
                                                            ":active" => 0,
                                                            ":time" => $common->custom_time()
                                                        );
                                                        $new_entries[] = $new_object;
                                                    }
                                                }

                                                if ($new_entries){
                                                    $columns = "user_id,type,type_option,value,time,active";
                                                    $values = ":user_id,:type,:type_option,:value,:time,:active";
                                                    try{
                                                        $send = $common->creator("accesses",$columns,$values,$new_entries,true);
                                                        if (!$send){
                                                            $return_object['errors'][] = "New entry failed for access menus";
                                                        }
                                                    }catch (Exception $exception){
                                                        $return_object['errors'][] = "Access menus new entry failed for ".$exception->getMessage();
                                                    }

                                                }
//
                                                $sql = "active=:active,time=:time where user_id=:user_id and type=:type and 
                                                type_option=:type_option and value=:value";
                                                try{
                                                    $send = $common->modifier("accesses",$sql,$menus_data,true);
                                                    if (!$send){
                                                        $return_object['errors'][] = "Access menus data update fail!";
                                                    }
                                                }catch (Exception $exception){
                                                    $return_object['errors'][] = "Access menus error for ".$exception->getMessage();
                                                }
                                            }
                                            else{
                                                $return_object['errors'][] = "Access menus not set";
                                            }
                                            /// permission parts
                                            if (isset($validation['permission_parts'])){
                                                $parts_data = $validation['permission_parts'];
                                                // set the user id in permission parts
                                                $i = 0;
                                                $selected_ids = array();
                                                foreach ($parts_data as $item){
                                                    $selected_ids[] = "'".$item[':value']."'";
                                                    $parts_data[$i][":user_id"] = $user_id;
                                                    $i++;
                                                }

                                                /// check all menu id exist/assigned in permission parts
                                                /// send new data for not existence accesses
                                                $join = join(",",$selected_ids);
                                                $check_sql = "where user_id=:user_id and type=:type and type_option=:type_option and value in ($join)";
                                                $check_sql_data = array(
                                                    ":user_id" => $user_id,
                                                    ":type" => "permission_part",
                                                    ":type_option" => "system_admin"
                                                );
                                                $check_finder = $this->common->retriever("accesses","value",$check_sql,$check_sql_data,true);
                                                $new_entries = array();
                                                foreach ($parts_data as $menu_info){
                                                    $menu_id = $menu_info[":value"];
                                                    $filter_keys = array(
                                                        "value" => $menu_id
                                                    );
                                                    $check_in_finder = $common->filter_in_array($check_finder,$filter_keys,false);
                                                    if (!$check_in_finder){

                                                        $new_object = array(
                                                            ":user_id" => $user_id,
                                                            ":type" => "permission_part",
                                                            ":type_option" => "system_admin",
                                                            ":value" => $menu_id,
                                                            ":active" => 0,
                                                            ":time" => $common->custom_time()
                                                        );
                                                        $new_entries[] = $new_object;
                                                    }
                                                }
                                                if ($new_entries){
                                                    $columns = "user_id,type,type_option,value,time,active";
                                                    $values = ":user_id,:type,:type_option,:value,:time,:active";
                                                    try{
                                                        $send = $common->creator("accesses",$columns,$values,$new_entries,true);
                                                        if (!$send){
                                                            $return_object['errors'][] = "New entry failed for access menus";
                                                        }
                                                    }catch (Exception $exception){
                                                        $return_object['errors'][] = "Access menus new entry failed for ".$exception->getMessage();
                                                    }

                                                }
                                                $sql = "active=:active,time=:time where user_id=:user_id and type=:type and type_option=:type_option 
                                            and value=:value";
                                                try{
                                                    $send = $common->modifier("accesses",$sql,$parts_data,true);
                                                    if (!$send){
                                                        $return_object['errors'][] = "Permission parts data send fail!";
                                                    }
                                                }catch (Exception $exception){
                                                    $return_object['errors'][] = "Permission parts error for ".$exception->getMessage();
                                                }

                                            }
                                            else{
                                                /// if permission parts empty then all access deactive of permission parts
                                                $sql = "active=:active where user_id=:user_id and type=:type and type_option=:type_option 
                                                ";
                                                $sql_data = array(
                                                    ":active" => 0,
                                                    ":user_id" => $user_id,
                                                    ":type" => "permission_part",
                                                    ":type_option" => "system_admin",
                                                );
                                                try{
                                                    $send = $common->modifier("accesses",$sql,$sql_data);
                                                    if (!$send){
                                                        $return_object['errors'][] = "Permission parts data update fail!";
                                                    }
                                                }catch (Exception $exception){
                                                    $return_object['errors'][] = "Permission parts error for ".$exception->getMessage();
                                                }
//                                                $return_object['errors'][] = "Permission parts not set";
                                            }

                                        }
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "Controller data update fail of ".$controller_type;
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        function edit_teacher(){

            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            require_once project_root."controllers/handler/security.php";
            $user_id = $_REQUEST['user_id'];
            $before_password = $_REQUEST['before_password'];

            $before_member_id = $_REQUEST['before_member_id'];
            $before_image = $_REQUEST['before_image'];
            $before_nid = $_REQUEST['before_nid'];
            $before_image_delete_path = "";
            $before_nid_delete_path = "";
            $before_trade_license_delete_path = "";

//            print_r($before_nid);
            $common = $this->common;
            $user_type = $common->id_type_name($user_id);
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $controller_type = $common->id_type_name($user_id);
            /// define some variable
            $id_maker = $member_id_maker = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->teacher_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];

                if (!$data){
                    $return_object['errors'][] = "User data not set";
                }

                //if member id changed
                if ($member_id !=$before_member_id){
                    // check the member id is duplicate? and update the last value
                    try{
                        $member_id_maker = new member_id_maker($controller_type,$logged_user_row_id,$device);
                        $sql = " where member_id = :member_id ";
                        $sql_data = array(
                            ":member_id" => $member_id
                        );
                        try{
                            $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($checker){
                                $return_object['errors'][] = "Member id already exist";
                            }
                            else{
                                //update the member id
                                $input_last_amount = $common->get_int_without_row_id($member_id);
                                $last_amount = $member_id_maker->last_amount;
                                if ($input_last_amount>$last_amount){
                                    $update_amount = $input_last_amount;
                                }
                                else{
                                    $update_amount = $last_amount;
                                }
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                    }
                }

                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){

                    //upload profile image
                    $profile_file_name = "";
                    if (isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
                        $upload_profile_image = $common->image_uploder("image",$controller_type."profile",'profile');
                        if ($upload_profile_image['status']){
                            $upload_data = $upload_profile_image['upload_data'];
                            $profile_file_name = $upload_data['file_path'];
                            $before_image_delete_path = project_root.$before_image;
                        }
                    }
                    else{
                        $profile_file_name = $before_image;
                    }
                    
                    //upload nid image
                    $nid_file_name = "";
                    if (isset($_FILES['nid']['name']) and !empty($_FILES['nid']['name'])){
                        $upload_nid_image = $common->image_uploder("nid",$controller_type."nid");
                        if ($upload_nid_image['status']){
                            $upload_data = $upload_nid_image['upload_data'];
                            $nid_file_name = $upload_data['file_path'];
                            if ($before_nid){
                                $before_nid_delete_path = project_root.$before_nid;
                            }

                        }
                    }
                    else{
                        $nid_file_name = $before_nid;
                    }

                    //first assign last update for member id maker
                    //member id maker
                    if ($member_id_maker){
                        try{
                            $status = $member_id_maker->update($update_amount);
                            if (!$status->status){
                                $return_object['errors'][] = "Member id maker not update";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                        }
                    }

                    // if password changed
                    $more_sql = ",password=:password";
                    if (!$_REQUEST['password']){
                        $more_sql = "";
                        unset($data[":password"]);
                    }

                    // send the user data
                    $sql = "member_id=:member_id,name=:name,mobile=:mobile,email=:email$more_sql,joining_date=:joining_date,nid=:nid,
                    designation=:designation,address=:permanent_address,birth_date=:birth_date,educational_qualification=:educational_qualification,
                    first_date_of_job=:first_date_of_job,active=:active,image=:image,trainings=:trainings,subject_wise_trainings=:subject_wise_trainings,
                    device_user_id=:device_user_id,shift=:shift     
                      where user_id = :user_id";
                    $data[':image'] = $profile_file_name;
                    $data[':nid'] = $nid_file_name;
                    $data[":user_id"] = $user_id;

                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
//                                print_r($data);
                                $send = $common->modifier("controllers",$sql,$data);
                                if ($send){
                                    // delete before files
                                    if ($before_image_delete_path){
                                        $delete = $common->file_deleter($before_image_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before image not deleted";
                                        }
                                    }
                                    if ($before_nid_delete_path){
                                        $delete = $common->file_deleter($before_nid_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before nid not deleted";
                                        }
                                    }



                                    $return_object['member_id'] = $member_id;
                                    $return_object['messages'][] = "Data successfully updated.";
                                    $return_object['status'] = 1;
                                    /// record for activities this task
                                    try{
                                        $record = $common->add_activities("edit_$user_type",$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "User data update fail of ".$controller_type;
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        function edit_student(){

            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            require_once project_root."controllers/handler/security.php";
            $user_id = $_REQUEST['user_id'];
            $before_password = $_REQUEST['before_password'];

            $before_member_id = $_REQUEST['before_member_id'];
            $before_image = $_REQUEST['before_image'];
            $before_image_delete_path = "";
            $before_nid_delete_path = "";

//            print_r($before_nid);
            $common = $this->common;
            $user_type = $common->id_type_name($user_id);
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $controller_type = $common->id_type_name($user_id);
            /// define some variable
            $id_maker = $member_id_maker = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->student_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];

                if (!$data){
                    $return_object['errors'][] = "User data not set";
                }

                //if member id changed
                if ($member_id !=$before_member_id){
                    // check the member id is duplicate? and update the last value
                    try{
                        $member_id_maker = new member_id_maker($controller_type,$logged_user_row_id,$device);
                        $sql = " where member_id = :member_id ";
                        $sql_data = array(
                            ":member_id" => $member_id
                        );
                        try{
                            $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($checker){
                                $return_object['errors'][] = "Member id already exist";
                            }
                            else{
                                //update the member id
                                $input_last_amount = $common->get_int_without_row_id($member_id);
                                $last_amount = $member_id_maker->last_amount;
                                if ($input_last_amount>$last_amount){
                                    $update_amount = $input_last_amount;
                                }
                                else{
                                    $update_amount = $last_amount;
                                }
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                    }
                }

                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){

                    //upload profile image
                    $profile_file_name = "";
                    if (isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
                        $upload_profile_image = $common->image_uploder("image",$controller_type."profile",'profile');
                        if ($upload_profile_image['status']){
                            $upload_data = $upload_profile_image['upload_data'];
                            $profile_file_name = $upload_data['file_path'];
                            $before_image_delete_path = project_root.$before_image;
                        }
                    }
                    else{
                        $profile_file_name = $before_image;
                    }

                    //first assign last update for member id maker
                    //member id maker
                    if ($member_id_maker){
                        try{
                            $status = $member_id_maker->update($update_amount);
                            if (!$status->status){
                                $return_object['errors'][] = "Member id maker not update";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                        }
                    }

                    // if password changed
                    $more_sql = ",password=:password";
                    if (!$_REQUEST['password']){
                        $more_sql = "";
                        unset($data[":password"]);
                    }

                    // send the user data
                    $sql = "member_id=:member_id,name=:name,mobile=:mobile,email=:email$more_sql,joining_date=:joining_date,address=:address,birth_date=:birth_date,
                    session=:session,gender=:gender,religion=:religion,branch=:branch,
                    father_name=:father_name,mother_name=:mother_name,roll_number=:roll_number,nid_number=:nid_number,
                    active=:active,image=:image,class=:class,department=:department,shift=:shift,device_user_id=:device_user_id  
                      where user_id = :user_id";
                    $data[':image'] = $profile_file_name;
                    $data[":user_id"] = $user_id;

                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
//                                print_r($data);
                                $send = $common->modifier("controllers",$sql,$data);
                                if ($send){
                                    // delete before files
                                    if ($before_image_delete_path){
                                        $delete = $common->file_deleter($before_image_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before image not deleted";
                                        }
                                    }
                                    if ($before_nid_delete_path){
                                        $delete = $common->file_deleter($before_nid_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before nid not deleted";
                                        }
                                    }



                                    $return_object['member_id'] = $member_id;
                                    $return_object['messages'][] = "Data successfully updated.";
                                    $return_object['status'] = 1;
                                    /// record for activities this task
                                    try{
                                        $record = $common->add_activities("edit_$user_type",$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "User data update fail of ".$controller_type;
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        
        function software_info_update(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            if (isset($_REQUEST['type']) and isset($_REQUEST['value'])){
                $type = $_REQUEST['type'];
                $value = $_REQUEST['value'];
                if ($type == "software_favicon" or $type == "software_logo"  or $type == "software_login_banner" ){
                    $before_file = "";
                    if (isset($_REQUEST['before_file'])){
                        $before_file = $_REQUEST['before_file'];
                        /// first upload new file
                        if (isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])){
                            $upload = $common->image_uploder("image","software ".$type);
                            if ($upload['status']){
                                $upload_data = $upload['upload_data'];
                                $value = $upload_data['file_path'];
                                $return_object[$type] = $value;

                                $delete_path = project_root.$before_file;
                                if ($delete_path){
                                    $delete = $common->file_deleter($delete_path);
                                    if (!$delete){
                                        $return_object['errors'][] = "Before image not deleted";
                                    }
                                }

                            }
                        }
                        else{
                            $return_object['errors'][] = "File empty";
                        }

                    }
                    else{
                        $return_object['errors'][] = "Before file not found";
                    }
                }
                if (!$common->error_in_object($return_object)){
                    try{
                        $last_update = new assign_last_update($type,"","","",$value,"settings");
                        if ($last_update->status){
                            $return_object['status'] = 1;
                            /// record for activities this task
                            $activity = "change_".$type;

                            try{
                                $record = $common->add_activities($activity,"");
                                if (!$record){
                                    $return_object['errors'][] = "Activity record failed";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                            }

                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }
                }


            }
            else{
                $return_object['errors'][] = "Data not found";
            }
            return $return_object;
        }
        function institute_settings_update(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            if (isset($_REQUEST['type']) and isset($_REQUEST['value'])  and isset($_REQUEST['institute_id']) ){
                $type = $_REQUEST['type'];
                $value = $_REQUEST['value'];
                $institute_id = $_REQUEST['institute_id'];
                if (!$common->error_in_object($return_object)){
                    if ($type == "in_time_scan_range"){
                        try{
                            $value = $_REQUEST['in_range_from'];
                            $last_update = new assign_last_update("in_range_from","institute_settings","","",$value,"settings",$institute_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        try{
                            $value = $_REQUEST['in_range_to'];
                            $last_update = new assign_last_update("in_range_to","institute_settings","","",$value,"settings",$institute_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        if (!$common->error_in_object($return_object)){
                            $return_object['status'] = 1;
                        }


                    }
                    elseif ($type == "out_time_scan_range"){
                        try{
                            $value = $_REQUEST['out_range_from'];
                            $last_update = new assign_last_update("out_range_from","institute_settings","","",$value,"settings",$institute_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        try{
                            $value = $_REQUEST['out_range_to'];
                            $last_update = new assign_last_update("out_range_to","institute_settings","","",$value,"settings",$institute_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        if (!$common->error_in_object($return_object)){
                            $return_object['status'] = 1;
                        }


                    }

                    else{
                        try{
                            $last_update = new assign_last_update($type,"institute_settings","","",$value,"settings",$institute_id);
                            if ($last_update->status){
                                $return_object['status'] = 1;
                                /// record for activities this task
                                $activity = "change_".$type;

                                try{
                                    $record = $common->add_activities($activity,"");
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }

                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }

                }


            }
            else{
                $return_object['errors'][] = "Data not found";
            }
            return $return_object;
        }
        function shift_settings_update(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            if (isset($_REQUEST['type']) and isset($_REQUEST['value'])  and isset($_REQUEST['shift_id'])  ){
                $type = $_REQUEST['type'];
                $value = $_REQUEST['value'];
                $shift_id = $_REQUEST['shift_id'];
                if (!$common->error_in_object($return_object)){
                    if ($type == "in_time_scan_range"){
                        try{
                            $value = $_REQUEST['in_range_from'];
                            $last_update = new assign_last_update("in_range_from","shift_settings","","",$value,"shift_settings",$shift_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        try{
                            $value = $_REQUEST['in_range_to'];
                            $last_update = new assign_last_update("in_range_to","shift_settings","","",$value,"shift_settings",$shift_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        if (!$common->error_in_object($return_object)){
                            $return_object['status'] = 1;
                        }


                    }
                    elseif ($type == "out_time_scan_range"){
                        try{
                            $value = $_REQUEST['out_range_from'];
                            $last_update = new assign_last_update("out_range_from","shift_settings","","",$value,"shift_settings",$shift_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        try{
                            $value = $_REQUEST['out_range_to'];
                            $last_update = new assign_last_update("out_range_to","shift_settings","","",$value,"shift_settings",$shift_id);
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                        if (!$common->error_in_object($return_object)){
                            $return_object['status'] = 1;
                        }


                    }

                    else{
                        try{
                            $last_update = new assign_last_update($type,"shift_settings","","",$value,"shift_settings",$shift_id);
                            if ($last_update->status){
                                $return_object['status'] = 1;
                                /// record for activities this task
                                $activity = "change_".$type;

                                try{
                                    $record = $common->add_activities($activity,"");
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }

                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }

                }


            }
            else{
                $return_object['errors'][] = "Data not found";
            }
            return $return_object;
        }

        function new_password(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $validation = $this->validator->new_password_validation();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                if (isset($_SESSION['confirmation'])){
                    $confirmation_info = $_SESSION['confirmation'];
                    $user_id = $confirmation_info['user_id'];
                    $repeat_password = $_REQUEST['repeat_password'];
                    $new_password = $_REQUEST['new_password'];
                    if ($new_password != $new_password){
                        $return_object['messages'][] = "Password not matched";
                    }
                    else{
                        $md5_password = md5($new_password);
                        $sql = "password=:password where user_id = :user_id";
                        $sql_data = array(
                            ":password" => $md5_password,
                            ":user_id" => $user_id
                        );

                        try{
                            $update = $this->common->modifier("controllers",$sql,$sql_data);
                            if ($update){
                                $return_object['status'] = 1;
                                unset($_SESSION['confirmation']);
                            }
                            else{
                                $return_object['errors'][] = "Password recovery fail";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Update fail for ".$exception->getMessage();
                        }
                    }

                }
                else{
                    $return_object['messages'][] = "Token deleted";
                }
            }
            return $return_object;
        }

        function edit_institute(){

            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            require_once project_root."controllers/handler/security.php";
            $institute_id = $_REQUEST['institute_id'];
            $before_member_id = $_REQUEST['member_id'];
            $before_logo = $_REQUEST['before_logo'];
            $before_banner = $_REQUEST['before_banner'];
            $before_logo_delete_path = "";
            $before_banner_delete_path = "";

//            print_r($before_banner);
            $common = $this->common;
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            $controller_type = $common->id_type_name($institute_id);
            /// define some variable
            $id_maker = $member_id_maker = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->institute_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                if (!$data){
                    $return_object['errors'][] = "Controller data not set";
                }
                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload banner image
                    $banner_file_name = "";
                    if(isset($_FILES['banner']['name']) and  !empty($_FILES['banner']['name'])){
                        $upload_banner = $common->image_uploder("banner","institute banner");
                        if ($upload_banner['status']){
                            $upload_data = $upload_banner['upload_data'];
                            $banner_file_name = $upload_data['file_path'];
                            $before_banner_delete_path = project_root.$before_banner;
                        }
                    }else{
                        $banner_file_name = $before_banner;
                    }

                    //upload logo image
                    $logo_file_name = "";
                    if (isset($_FILES['logo']['name']) and !empty($_FILES['logo']['name'])){
                        $upload_logo_image = $common->image_uploder("logo","institute logo");
                        if ($upload_logo_image['status']){
                            $upload_data = $upload_logo_image['upload_data'];
                            $logo_file_name = $upload_data['file_path'];
                            $before_logo_delete_path = project_root.$before_logo;
                        }
                    }
                    else{
                        $logo_file_name = $before_logo;
                    }


                    $sql = "name=:name,contact_number=:contact_number,address=:address,code=:code,
                    in_sms_switch=:in_sms_switch,out_sms_switch=:out_sms_switch,
                    banner=:banner,logo=:logo,active=:active  
                    where institute_id=:institute_id";

                    $data[':banner'] = $banner_file_name;
                    $data[':logo'] = $logo_file_name;
                    $data[":institute_id"] = $institute_id;

                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
//
                                $send = $common->modifier("institutes",$sql,$data);
                                if ($send){
                                    // delete before files
                                    if ($before_logo_delete_path){
                                        $delete = $common->file_deleter($before_logo_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before image not deleted";
                                        }
                                    }
                                    if ($before_banner_delete_path){
                                        $delete = $common->file_deleter($before_banner_delete_path);
                                        if (!$delete){
                                            $return_object['errors'][] = "Before banner not deleted";
                                        }
                                    }

                                    //// shifts update
                                    if (!isset($_REQUEST['shifts'])){
                                        $_REQUEST['shifts'] = array();
                                    }

                                    if (isset($_REQUEST['shifts'])){
                                        $before_shifts = $common->retriever("institute_shifts","*","where institute_id = :institute_id",array(
                                            ":institute_id" => $institute_id
                                        ),true);

                                        $new_shifts = array();
                                        $delete_shifts = array();
                                        $time = $common->custom_time();
                                        foreach ($_REQUEST['shifts'] as $shift){
                                            $filter = $common->filter_in_array($before_shifts,array(
                                                "shift" => $shift
                                            ),false);
                                            if (!$filter){
                                                $new_shifts[] = array(
                                                    ":shift" => $shift,
                                                    ":institute_id" => $institute_id,
                                                    ":time" => $time,
                                                    ":active" => 1,
                                                );
                                            }
                                        }
                                        foreach ($before_shifts as $shift){
                                            $shift_index = $common->index_number_in_list($_REQUEST['shifts'],$shift['shift']);
                                            if (!strlen($shift_index)){
                                                $delete_shifts[] = array(
                                                    ":shift" => $shift['shift'],
                                                    ":institute_id" => $institute_id
                                                );
                                            }
                                        }
                                        if ($new_shifts){
                                            try{
                                                $columns = "shift,institute_id,time,active";
                                                $values = ":shift,:institute_id,:time,:active";
                                                $new_shifts = $common->creator("institute_shifts",$columns,$values,$new_shifts,true);
                                                if (!$new_shifts){
                                                    $return_object['errors'][] = "New shift not saved";
                                                }
                                            }catch (Exception $exception){
                                                $return_object['errors'][] = $exception->getMessage();
                                            }
                                        }
                                        if ($delete_shifts){
                                            try{
                                                $sql = "where shift = :shift and institute_id = :institute_id";
                                                $new_shifts = $common->deleter("institute_shifts",$sql,$delete_shifts,true);
                                                if (!$new_shifts){
                                                    $return_object['errors'][] = "Canceled shift not deleted";
                                                }
                                            }catch (Exception $exception){
                                                $return_object['errors'][] = $exception->getMessage();
                                            }
                                        }

                                    }


                                    
                                    $return_object['member_id'] = $before_member_id;
                                    $return_object['messages'][] = "Data successfully updated.";
                                    $return_object['status'] = 1;
                                    /// record for activities this task
                                    $activity_type = "edit_institute";

                                    try{
                                        $record = $common->add_activities($activity_type,$institute_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }
                                }
                                else{
                                    $return_object['errors'][] = "Institute data update fail of ".$controller_type;
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "Institute Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        

        function edit_calendar_event(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $validation = $this->validator->calendar_event_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $event_id = $_REQUEST['id'];
                if (isset($validation['data'])){
                    $event_data = $validation['data'];
                    $event_data[':id'] = $event_id;
                    $event_sql = "title = :title,event_type = :event_type,color = :color where id = :id";
                    try{
//
                        $update = $common->modifier("calendar",$event_sql,$event_data);
                        if ($update){
                            $return_object['status'] = 1;
                        }
                        else{
                            $return_object['errors'][] = "Event info update failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                else{
                    $return_object['messages'][] = " Event data  empty";
                }
            }

            return $return_object;

        }
        function resend_bulk_sms($id){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;

            try{
                if ($common->get_api_sms_balance() < 100){
                    $return_object['messages'][] = "Sorry try later";
                    $common->sms_sender("SMS Quota finished purchase some sms for (sms api)","01756018979");

                }
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }

            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            $sql = "where bulk_sms_id = :bulk_sms_id and active = :pending";
            $columns = "*";
            $pending_sms_list = $common->retriever("sms_records",$columns,$sql,array(
                ":bulk_sms_id" => $id,
                ":pending" => 0
            ),true);
            if ($pending_sms_list){
                $sql = "where id = :bulk_sms_id";
                $columns = "*";
                $bulk_sms_info = $common->retriever("bulk_sms",$columns,$sql,array(
                    ":bulk_sms_id" => $id,
                ));
                $sender_type = '';
                $sender_id = $bulk_sms_info['user_id'] ?? '';
                if ($sender_id){
                    $sender_type = $common->id_type_name($sender_id);
                }
                if (!$sender_type){
                    $return_object['errors'][] = "Sender type undefiend";
                    return $return_object;
                }


                foreach ($pending_sms_list as $index => $item){
                    if ($sender_type == "teacher"){
                        $institute_info = $this->retriever->simple_institute_info($item['institute_id']);
                    }
                    else{
                        $institute_info = $this->retriever->simple_controller_info($sender_id);
                    }

                    $sms_balance = $institute_info['sms_balance'] ?? 0;

                    if ($sms_balance >= $item['amount']){
                        $send_sms = $common->sms_sender($item['message'],$common->make_proper_mobile_number($item['mobile']));
                        if (!$send_sms['insertedSmsIds']){
                            $return_object['messages'][] = "SMS send failed! error code is ".$send_sms['errorCode'];
                        }
                        else {

                            try{
                                if ($sender_type == "teacher"){
                                    $common->modifier('institutes', 'sms_balance = sms_balance - :amount where institute_id = :institute_id', array(
                                        ":amount" => $item['amount'],
                                        ":institute_id" => $item['institute_id']
                                    ));
                                }
                                else{
                                    $common->modifier('controllers', 'sms_balance = sms_balance - :amount where user_id = :institute_id', array(
                                        ":amount" => $item['amount'],
                                        ":institute_id" => $sender_id
                                    ));
                                }


                                $common->modifier('sms_records', 'active = :active where id = :id', array(
                                    ":active" => 1,
                                    ":id" => $item['id']
                                ));

                                $common->modifier('bulk_sms', 'delivered = delivered + :amount where id = :id', array(
                                    ":id" => $item['bulk_sms_id'],
                                    ":amount" => $item['amount']
                                ));
                            }catch (Exception $exception){
                                $return_object['messages'][] = $exception->getMessage();
                            }


                        }
                    }
                    else{
                        $return_object['messages'][] = "Insufficient sms of ".$institute_info['name']." and contact number ".$institute_info['contact_number'];
                    }

                }
                if (!$common->error_in_object($return_object)){
                    $return_object['status'] = 1;
                }
            }
            else{
                $return_object['status'] = 1;
            }

            return $return_object;

        }

        function edit_shift(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $validation = $this->validator->shift_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $shift_id = $_REQUEST['id'];
                if (isset($validation['data'])){
                    $shift_data = $validation['data'];
                    $shift_data[':id'] = $shift_id;
                    $shift_sql = "name = :name where id = :id";
                    try{
//
                        $update = $common->modifier("shifts",$shift_sql,$shift_data);
                        if ($update){
                            $return_object['status'] = 1;


                            /// record for activities this task
                            $activity_type = "edit_shift";

                            try{
                                $record = $common->add_activities($activity_type,$shift_id);
                                if (!$record){
                                    $return_object['errors'][] = "Activity record failed";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                            }
                        }
                        else{
                            $return_object['errors'][] = "shift info update failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                else{
                    $return_object['messages'][] = " shift data  empty";
                }
            }

            return $return_object;

        }
        function shift_initiate(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $validation = $this->validator->shift_initiate_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $user_type = $_REQUEST['user_type'];
                $shift = $_REQUEST['shift'];
                $shift_initiate_sql = "shift = :shift where user_id like :user_id";
                $shift_initiate_data = array(
                    ":shift" => $shift,
                    ":user_id" => $common->id_type($user_type)."%"
                );
                try{
//
                    $update = $common->modifier("controllers",$shift_initiate_sql,$shift_initiate_data);
                    if ($update){
                        $return_object['status'] = 1;
                        $return_object['messages'][] = $common->readable_text($user_type)." shift initiate done";
                        /// record for activities this task
                        $activity_type = "shift_initiate";

                        try{
                            $record = $common->add_activities($activity_type,$user_type);
                            if (!$record){
                                $return_object['errors'][] = "Activity record failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                        }
                    }
                    else{
                        $return_object['errors'][] = "shift_initiate info update failed";
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
            }

            return $return_object;

        }

        function edit_application(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $validation = $this->validator->application_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $document_row_id = $_REQUEST['document_row_id'];
                if (isset($validation['data'])){
                    $application_data = $validation['data'];
                    /// first delete before event of calendar
                    $delete_sql = "where document_row_id = :document_row_id";
                    $delete_sql_data = array(
                        ":document_row_id" => $document_row_id
                    );
                    try{
                        $delete = $common->deleter("calendar",$delete_sql,$delete_sql_data);
                        if (!$delete){
                            $return_object['errors'][] = "Events not deleted";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }
                    /// second delete before application items/users
                    $delete_sql = "where document_row_id = :document_row_id";
                    $delete_sql_data = array(
                        ":document_row_id" => $document_row_id
                    );
                    try{
                        $delete = $common->deleter("application_items",$delete_sql,$delete_sql_data);
                        if (!$delete){
                            $return_object['errors'][] = "Before application items not deleted";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                    /// third delete before authorization
                    $delete_sql = "where document_row_id = :document_row_id";
                    $delete_sql_data = array(
                        ":document_row_id" => $document_row_id
                    );
                    try{
                        $delete = $common->deleter("authorized_documents",$delete_sql,$delete_sql_data);
                        if (!$delete){
                            $return_object['errors'][] = "Before authorized_documents  not deleted";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }


                    $update_sql = "institute_id=:institute_id,start_date=:start_date,end_date=:end_date,message=:message,application_tag=:application_tag 
                    where document_row_id = :document_row_id";

                    $application_data[":document_row_id"] = $document_row_id;

                    try{
                        $update = $common->modifier("applications",$update_sql,$application_data);
                        if ($update){

                            // save application items
                            $application_items = $validation['items'];

                            if ($application_items){
                                $i=0;
                                foreach ($application_items as $item){
                                    $application_items[$i][':document_row_id'] = $document_row_id;
                                    $i++;
                                };
                                $columns = "document_row_id,user_id,active,time";
                                $values = ":document_row_id,:user_id,:active,:time";
                                try{
                                    $pass = $common->creator("application_items",$columns,$values,$application_items,true);
                                    if ($pass){
                                        //// auto authorization
                                        $passed = new passed();
                                        $authorize_info = $passed->document_authorize($document_row_id);
                                        if ($authorize_info['status']){
                                            $return_object['status'] = 1;
                                        }
                                        else{
                                            $return_object['errors'][] = "Event authorize failed";
                                        }


                                    }
                                    else{
                                        $return_object['errors'][] = "Application items not saved";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = $exception->getMessage();
                                }
                            }else{
                                $return_object['errors'][] = "Application items empty";
                            }
                        }
                        else{
                            $return_object['errors'][] = "Application info update failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                else{
                    $return_object['messages'][] = " Application data  empty";
                }
            }

            return $return_object;

        }





    }