<?php
    require_once(project_root."controllers/packages/vendor/autoload.php");
    class retrieve{
        protected $connection;
        public $connection_status;
        public $common;
        public $initial_data;
        function __construct()
        {
            $common = new common();
            $initial = new initial_data();
            $this->common = $common;
            $this->initial_data = $initial;
            $this->connection = $common->connection;
            $this->connection_status = $common->connection_status;
        }
        function basic_info(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $logged_user_id = "";

            $all_info = array();
            // table schema for pwa
            $table_schema = $this->common->db_tables_schema();
            $all_info['tables_schema'] = $table_schema;
            // id types
            $id_types = $this->common->id_type();
            $all_info['id_types'] = $id_types;
            //permission parts
            $all_info['permission_parts'] = $this->initial_data->admin_permission_parts();

            //software information
            $all_info['software_info'] = $this->software_info();
            //document types
            $all_info['document_types'] = $this->initial_data->document_types();
            // estimated workers types
//            $all_info['estimated_worker_types'] = $this->initial_data->estimate_worker_types();

            $all_info['event_types'] = $this->initial_data->event_types();

            // if logged exist get the logged user information
            if (isset($_SESSION['logged_user_id'])){
                $logged_user_id = $_SESSION['logged_user_id'];
                $controller_info = $this->controller_info($logged_user_id);
                if ($controller_info){
                    $all_info['logged_user'] = $controller_info;
                    $all_info['logged_user_id'] = $logged_user_id;
                }
                if (isset($_SESSION['activate_user_id'])){
                    $activate_user_id = $_SESSION['activate_user_id'];
                    $user_info = $this->controller_info($activate_user_id);
                    if ($user_info){
                        $all_info['activate_worker'] = $user_info;
                    }
                }

            }


            $return_object['basic_info'] = $all_info;
            $return_object['status'] = 1;
            return $return_object;

        }
        function training_names($training_ids=array()){
            $join_ids = $this->common->sql_in_maker($training_ids);
            $sql = "where id in ($join_ids) and active != :active";
            $sql_data = array(
                ":active" => 2
            );
            $trainings = $this->common->retriever("tags","tag",$sql,$sql_data,true);
            $names = array();
            foreach ($trainings as $train){
                $names[] = $train['tag'];
            }
            $join = join(", ",$names);
            return $join;
        }
        function controller_info($user_id,$member_id = false){
            $sql = "left join tags as des on des.id=controllers.designation 
            left join tags as blood on blood.id=controllers.blood_group 
            left join tags as department on department.id=controllers.department 
            left join tags as class on class.id=controllers.class  
            
            left join tags as branch on branch.id=controllers.branch  
            left join tags as session on session.id=controllers.session  
            left join tags as gender on gender.id=controllers.gender  
            left join tags as religion on religion.id=controllers.religion  
            
            left join institutes on institutes.institute_id = controllers.institute_id 
            left join shifts on shifts.id = controllers.shift 
            
              where controllers.id > :id 
              ";
            $data = array(
                ":id" => 0,
            );
            if ($member_id){
                $sql .= " and controllers.member_id = :member_id";
                $data[':member_id'] = $user_id;
            }
            else{
                $sql .= " and controllers.user_id = :user_id";
                $data[':user_id'] = $user_id;
            }
            $columns = "controllers.*,
            des.tag as designation,
            des.id as designation_id,
            blood.tag as blood_group,
            blood.id as blood_group_id,
            department.tag as department,
            department.id as department_id,
            
            religion.tag as religion,
            religion.id as religion_id,
            
            gender.tag as gender,
            gender.id as gender_id,
            
            session.tag as session,
            session.id as session_id,
            
            branch.tag as branch,
            branch.id as branch_id,
            
            
            
         
            class.tag as class_name,
            institutes.name as institute_name,
            institutes.member_id as institute_member_id,
            shifts.name as shift_name";
            $user_info = $this->common->retriever("controllers",$columns,$sql,$data);
            if ($user_info){
                $trainings = (array)json_decode($user_info['trainings'],true);
                $subject_wise_trainings = (array)json_decode($user_info['subject_wise_trainings'],true);
//
                $user_info['subject_wise_trainings'] = $subject_wise_trainings;
                $user_info['trainings'] = $trainings;
                $user_info['training_names'] = $this->training_names($trainings);
                $user_info['subject_wise_training_names'] = $this->training_names($subject_wise_trainings);

                unset($user_info['password']);
                $user_id = $user_info['user_id'];
                /// check the user type admin possibility. if its true , then get access menus and permission part rights

                ///get access menus
                $sql = "where user_id=:user_id and type=:type and type_option=:type_option and active=:active";
                $sql_data = array(
                    ":user_id" => $user_id,
                    ":type" => "access_menu",
                    ":type_option" => "system_admin",
                    ":active" => 1
                );
                $access_menus = $this->common->retriever("accesses","value",$sql,$sql_data,true);
                $user_info["access_menus"] = $access_menus;

                $user_id_type_name = $this->common->id_type_name($user_id);
                if ($user_id_type_name){
                    if ($user_id_type_name != "system_admin" and $user_id_type_name != "system_designer"){
                        $estimate_menus = $this->initial_data->estimated_access_menus($user_id_type_name);
                        $access = array();
                        foreach ($estimate_menus as $menu){
                            $menu_id = $menu['id'];
                            $access[] = array(
                                "value" => $menu_id
                            );
                        }
                        $user_info["access_menus"] = $access;
                    }
                }
                ///get permission part
                $sql = "where user_id=:user_id and type=:type and type_option=:type_option and active=:active";
                $sql_data = array(
                    ":user_id" => $user_id,
                    ":type" => "permission_part",
                    ":type_option" => "system_admin",
                    ":active" => 1
                );
                $permission_parts = $this->common->retriever("accesses","value",$sql,$sql_data,true);
                $user_info["permission_parts"] = $permission_parts;


                return $user_info;
            }
            return array();
        }



        function teacher_info($user_id,$member_id = false){
            return $this->user_info($user_id,$member_id);
        }
        function student_info($user_id,$member_id = false){
            return $this->user_info($user_id,$member_id);
        }
        function user_info($user_id,$member_id = false){
            $sql = "left join tags on tags.id = controllers.designation 
             left join institutes on institutes.institute_id = controllers.institute_id 
             where controllers.id > :id ";
            $data = array(
                ":id" => 0,
            );
            if ($member_id){
                $sql .= " and controllers.member_id = :member_id";
                $data[':member_id'] = $user_id;
            }
            else{
                $sql .= " and controllers.user_id = :user_id";
                $data[':user_id'] = $user_id;
            }
            $columns = "controllers.*,institutes.name as institute_name,tags.tag as designation,
            institutes.address as institute_address";
            $user_info = $this->common->retriever("controllers",$columns,$sql,$data);
            if ($user_info){
                return $user_info;
            }
            return array();
        }




        function initiate(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            // compress css and js
            $other = new other();
            $result = $other->compress();
            if (!$result['status']){
                $return_object['errors'][] = "Compress failed!";
            }
            if ($return_object['errors'] or $return_object['messages']){
                $return_object['errors'][] = "Fail";
            }
            else{
                $return_object['status'] = 1;
                $return_object['messages'][] = "Done";
            }

//            $config_file = "/controllers/installer/config.txt";
//            $new_data = "";
//            $open_file = fopen($config_file,'r+');
//            while (!feof($open_file)){
//
//            }
            return $return_object;
            // initiate database table schema
        }
        function simple_institute_info($institute_id){
            $result = $this->common->retriever("institutes","sms_balance,name,contact_number","where institute_id = :institute_id",array(
                ":institute_id" => $institute_id
            ));
            return $result;
        }

        function simple_controller_info($user_id){
            $result = $this->common->retriever("controllers","sms_balance,name,mobile as contact_number","where user_id = :user_id",array(
                ":user_id" => $user_id
            ));
            return $result;
        }

        function main_menus(){
            $menus = array();
            if (isset($_SESSION['logged_user_id'])){
                $logged_user_id = $_SESSION['logged_user_id'];
                if ($logged_user_id){
                    $id_type = $this->common->id_type_name($logged_user_id);
                    $menus = $this->initial_data->estimated_menus($id_type);
                }
            }
            else{
                $menus = $this->initial_data->menus();
            }
            $return_object = array(
                "find_data" => $menus
            );
            return $return_object;
        }
        function next_member_id($type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $user_cookies = $this->common->user_cookies_data();
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            try{
                $member_id_maker = new member_id_maker($type,$logged_user_row_id,$device);
                if ($member_id_maker->generate_id){
                    $return_object['member_id'] = $member_id_maker->generate_id;
                    $return_object['status'] = 1;
                }
                else{
                    $return_object['errors'][] = "Next member id not generated!";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Fail next id maker for, ".$exception->getMessage();
            }
            return $return_object;
        }
        function controller_enrol_basic_info($controller_type){
            $return_object = array(
                "status" => 1,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $next_member_id_info = $this->next_member_id($controller_type);
                if ($next_member_id_info['status']){
                    $return_object['member_id'] = $next_member_id_info['member_id'];
                }
                else{
                    $return_object['errors'][]  = "Member id not generated";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Member id failed for ".$exception->getMessage();
            }

            $estimate_access_menus = $this->initial_data->estimated_access_menus($controller_type);
            $return_object['access_menus'] = $estimate_access_menus;
            $return_object['permission_parts'] = $this->initial_data->admin_permission_parts();
            return $return_object;
        }

        function teacher_enrol_basic_info($user_type="teacher"){
            $return_object = array(
                "status" => 1,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $next_member_id_info = $this->next_member_id($user_type);
                if ($next_member_id_info['status']){
                    $return_object['member_id'] = $next_member_id_info['member_id'];
                }
                else{
                    $return_object['errors'][]  = "Member id not generated";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Member id failed for ".$exception->getMessage();
            }
            return $return_object;
        }
        function student_enrol_basic_info($user_type="student"){
            $return_object = array(
                "status" => 1,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $next_member_id_info = $this->next_member_id($user_type);
                if ($next_member_id_info['status']){
                    $return_object['member_id'] = $next_member_id_info['member_id'];
                }
                else{
                    $return_object['errors'][]  = "Member id not generated";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Member id failed for ".$exception->getMessage();
            }
            return $return_object;
        }


        function login(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rule = [
                "mobile" => "required|numeric|min:10",
                "password" => "required|min:8"
            ];
            $common = $this->common;
            $validator = new data_validator();
            $validation = $validator->validate($rule);
            if (!$validation->status){
                $return_object['messages'] = $validation->messages;
            }
            else{
                $mobile = $_REQUEST['mobile'];
                $password = md5($_REQUEST['password']);
                $min_mobile = substr($mobile,-10);
                $sql = " where mobile like :mobile and password = :password and active != :delete";
                $data = array(
                    ":mobile" => "%".$min_mobile,
                    ":password" => $password,
                    ":delete" => 2,
                );
                $find = $this->common->retriever("controllers","active,user_id,id,login_schedule,login_schedule_switch",$sql,$data);
//                $return_object['errors'][] = "just check";

                if ($find){

                    $active = $find['active'];
                    $user_id = $find['user_id'];
                    $user_row_id = $find['id'];
                    if ($active){
                        /// check login schedule
                        if ($find['login_schedule_switch']){
                            $schedules = json_decode($find['login_schedule'],true);
                            $current_day = date("w");
                            $filter_keys = array(
                                "day" => $current_day
                            );
                            $filter_schedule = $this->common->filter_in_array($schedules, $filter_keys,false);
                            $login_permission_at_this_time = 0;
                            if ($filter_schedule){
                                $periods = $filter_schedule['periods'];
                                foreach ($periods as $period){
                                    if ($period){
                                        $start_hour_minute = $period[0];
                                        $end_hour_minute = $period[1];
                                        /// make start time
                                        $start_hour_text = $common->number_from_text($start_hour_minute);
                                        $start_label = $common->string_from_text($start_hour_minute);
                                        $start_hour_text_split = explode(":",$start_hour_text);
                                        $start_hour = $start_hour_text_split[0];
                                        $start_minute = 0;
                                        if (count($start_hour_text_split)== 2){
                                            $start_minute = $start_hour_text_split[1];
                                        }
                                        if ($start_label){
                                            $start_hour = $start_hour + 12;
                                        }
                                        elseif($start_hour == 12){
                                            $start_hour = 0;
                                        }
                                        $start_time = mktime(floatval($start_hour),floatval($start_minute),0);

                                        /// make end time
                                        $end_hour_text = $common->number_from_text($end_hour_minute);
                                        $end_label = $common->string_from_text($end_hour_minute);
                                        $end_hour_text_split = explode(":",$end_hour_text);
                                        $end_hour = $end_hour_text_split[0];
                                        $end_minute = 0;
                                        if (count($end_hour_text_split) == 2){
                                            $end_minute = $end_hour_text_split[1];
                                        }
                                        if ($end_label){
                                            $end_hour = $end_hour + 12;
                                        }
                                        elseif($end_hour == 12){
                                            if ($end_minute == 0)
                                                $end_hour = 24;
                                            else{
                                                $end_hour = 0;
                                            }
                                        }

                                        $end_time = mktime(floatval($end_hour),floatval($end_minute),0);
                                        $current_time = time();
                                        if ($start_time <= $current_time and $current_time <= $end_time){
                                            $login_permission_at_this_time = 1;
                                            break;
                                        }
                                    }

                                }

                            }

                            if (!$login_permission_at_this_time){
                                $return_object['messages'][] = "Sorry you are not permitted at this time!";
                                return $return_object;
                            }
                        }




                        $_SESSION['logged_user_id'] = $user_id;
                        //assign device ip address
                        $device_id = $this->common->assign_device();
                        if ($device_id){
                            $return_object['status'] = 1;

                            $_SESSION['logged_user_row_id'] = $user_row_id;
                            $_SESSION['device'] = $device_id;
                            $get_basic_info = $this->basic_info();
                            $return_object['basic_info'] = $get_basic_info['basic_info'];
                        }
                        else{
                            unset($_SESSION['logged_user_id']);
                            $return_object['errors'][] = "Device id not set";
                        }



                    }else{
                        $return_object['messages'][] = "You are suspended now!";
                    }
                }
                else{
                    $return_object['messages'][] = "Mobile or password not correct!";
                }

            }
            return $return_object;
        }

        function request_documents_counter($table_name,$param_column = "",$param_sql="",$param_sql_data=array()){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['query_data'])){
                $get_query_data = json_decode($_REQUEST['query_data'],true);
                if (isset($get_query_data['columns']) and isset($get_query_data['sql']) and isset($get_query_data['sql_data'])){
                    $columns = "id";
                    $sql = $get_query_data["sql"];
                    $sql_data = $get_query_data["sql_data"];
                    /// query add without deleted items
                    if ($sql){
                        $sql .= " and $table_name.active!=:delete_active ";
                    }
                    else{
                        $sql .= " where $table_name.active!=:delete_active ";
                    }
                    $sql_data[":delete_active"] = 2;
                    if ($param_sql){
                        $sql = $param_sql;
                        $sql_data = $param_sql_data;
                        $columns = $param_column;
                    }
                    try{
                        $get_data = $this->common->data_counter($table_name,$columns,$sql,$sql_data);
                        $return_object['total'] = $get_data;
                        $return_object['sql'] = $sql;
                        $return_object['sql_data'] = $sql_data;
                        $return_object['columns'] = $columns;
                        $return_object['table_name'] = $table_name;
                        $return_object['status'] = 1;
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Count fail for ".$exception->getMessage();
                    }

                }
                else{
                    $return_object['errors'][] = "Decode data empty";
                }
            }
            else{
                $return_object['errors'][] = "Query info empty!";
            }
            return $return_object;
        }
        function request_data_list($table_name="",$limit=0,$skip=0,$count=false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['query_data'])){
                $get_query_data = json_decode($_REQUEST['query_data'],true);
                if (isset($get_query_data['columns']) and isset($get_query_data['sql']) and isset($get_query_data['sql_data'])){
                    $columns = $get_query_data["columns"];
                    if ($table_name == "controllers"){
                        if ($columns){
                            $columns .= ",concat('') as password ";
                        }
                    }
                    $sql_data = $get_query_data["sql_data"];
                    $sql = $get_query_data["sql"];
                    if (isset($get_query_data['data_table'])) {
                        $table_name = $get_query_data['table_name'];
                    }
                    /// query add without deleted items
                    $self_table_name = "";
                    if ($table_name){
                        $self_table_name = $table_name.".";
                    }
                    if ($sql){
                        $sql .= " and $self_table_name"."active!=:delete_active ";
                    }
                    else{
                        $sql .= " where $self_table_name"."active!=:delete_active ";
                    }
                    $sql_data[":delete_active"] = 2;
                    // if data table on
                    if (isset($get_query_data['data_table'])){
                        $skip = $_REQUEST['start'];
                        $limit = $_REQUEST['length'];
                        $search_key = $_REQUEST['search']['value'];
                        $columns_fields = $_REQUEST['columns'];
                        /// query by search keyword
                        if ($search_key){

                            $search_sql = " and ( ";
                            $search_sql_list = array();
                            foreach ($columns_fields as $column){
                                $column_name = $column['data'];
                                if ($column_name){
                                    $search_table = $table_name;
                                    $search_column = $column_name;
                                    // change the table name and column name for join table column

                                    if ($table_name == "payment_requests" and $column_name == "sender_name"){
                                        $search_table = "controllers";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "payment_requests" and $column_name == "bank_name"){
                                        $search_table = "bank_cashes";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "reports" and $column_name == "source_name"){
                                        $search_table = "source";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "attendance_logs" and $column_name == "teacher_name"){
                                        $search_table = "controllers";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "institute_name"){
                                        $search_table = "institutes";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "designation"){
                                        $search_table = "tags";
                                        $search_column = "tag";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "class_name"){
                                        $search_table = "class";
                                        $search_column = "tag";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "department_name"){
                                        $search_table = "department";
                                        $search_column = "tag";
                                    }
                                    elseif ($table_name == "calendar" and $column_name == "institute_name"){
                                        $search_table = "institutes";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "calendar" and $column_name == "source_name"){
                                        $search_table = "controllers";
                                        $search_column = "name";
                                    }
                                    elseif ($table_name == "institute_devices" and $column_name == "institute_name"){
                                        $search_table = "institutes";
                                        $search_column = "name";
                                    }





                                    $sql_data_field = ":".$search_table.$search_column."_search_field";
                                    $sql_data_field_value = "%".$search_key."%";
                                    $sql_text = $search_table.".".$search_column." like ".$sql_data_field." ";
                                    $sql_data[$sql_data_field] = $sql_data_field_value;
                                    $search_sql_list[] = $sql_text;
                                }
                            }
                            $search_list_join = join(" or ",$search_sql_list);
                            $search_sql .= $search_list_join;
                            $search_sql .= " ) ";
                            $sql .= $search_sql;
                            /// sql plus for bank reconciliation

                        }
                        if ($table_name == "payment_requests"){
                            $sql .= " and payment_requests.check_date != :not_date";
                            $sql_data[':not_date'] = "0000-00-00";
                        }
                        elseif ($table_name == "bulk_sms"){
                            $sql .= " and bulk_sms.user_id = :user_id";
                            $sql_data[':user_id'] = $this->common->logged_user_id();
                        }

                        elseif ($table_name == "attendance_logs"){
                            $institute_id = "";
                            if (isset($get_query_data["institute_id"])){
                                $institute_id = $get_query_data["institute_id"];
                            }

                            $sql .= " and attendance_logs.institute_id = :institute_id ";
                            $sql_data[':institute_id'] = $institute_id;
                        }
                        elseif (isset($get_query_data["teacher_registration_report"]) or isset($get_query_data["student_registration_report"]) or isset($get_query_data["teacher_registration_report"])){
                            $sql .= " and institutes.active != :institute_active";
                            $sql_data[':institute_active'] = 2;
                            $logged_user_type = $this->common->logged_user_type();
                            if ($logged_user_type == "admin" or $logged_user_type == "teacher"){
                                $my_institutes = $this->my_institutes($get_query_data["designation"],$get_query_data["institute_id"],$this->common->logged_user_id());

                                $institute_ids = array();
                                foreach ($my_institutes['find_data'] as $institute){
                                    $institute_ids[] = $institute['institute_id'];
                                }
                                $institute_ids = array_unique($institute_ids);
                                $join_institute_ids = $this->common->sql_in_maker($institute_ids);
                                $sql .= " and institutes.institute_id in($join_institute_ids) ";
                            }

                        }
                        elseif (isset($get_query_data["today_attendance"]) and $table_name == "calendar"){
                            $sql .= " and institutes.active != :institute_active";
                            $sql_data[':institute_active'] = 2;
                            $logged_user_type = $this->common->logged_user_type();
                            if ($logged_user_type == "admin" or $logged_user_type == "teacher"){
                                $my_institutes = $this->my_institutes($get_query_data["designation"],$get_query_data["institute_id"],$this->common->logged_user_id());

                                $institute_ids = array();
                                foreach ($my_institutes['find_data'] as $institute){
                                    $institute_ids[] = $institute['institute_id'];
                                }
                                $institute_ids = array_unique($institute_ids);
                                $join_institute_ids = $this->common->sql_in_maker($institute_ids);
                                $sql .= " and institutes.institute_id in($join_institute_ids) ";

                            }
                            $sql .= " and calendar.event_type = :event ";
                            $sql_data[':event'] = $get_query_data["event_type"];

                            $event_user_type = $get_query_data["user_type"];
                            $sql .= " and calendar.source_id like :user_type_id ";
                            $sql_data[':user_type_id'] = $this->common->id_type($event_user_type)."%";

                            if ($get_query_data["user_id"]){
                                $sql .= " and calendar.source_id = :source_user_id ";
                                $sql_data[':source_user_id'] = $get_query_data["user_id"];

                            }


                            $sql .= " and :date BETWEEN DATE(calendar.start_time) and DATE(calendar.end_time) ";
                            $sql_data[':date'] = date("Y-m-d");


                        }


                        /// if order by fired and its must be query of before limit
                        if (isset($_REQUEST['order'])){
                            $order_by_list = array();
                            $i= 0;
                            foreach ($_REQUEST['order'] as $column){
                                if (count($columns_fields) == count($_REQUEST['order'])){
                                    $column_name = $columns_fields[$i]['data'];
                                }
                                else{
                                    $j = $column['column'];
                                    $column_name = $columns_fields[$j]['data'];
                                }

                                if ($column_name){
                                    $direction = $column['dir'];
                                    $dir_table = $table_name;
                                    $dir_column = $column_name;
                                    // change the table name for join table column direction

                                    if ($table_name == "payment_requests" and $column_name == "sender_name"){
                                        $dir_table = "controllers";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "payment_requests" and $column_name == "bank_name"){
                                        $dir_table = "bank_cashes";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "reports" and $column_name == "source_name"){
                                        $dir_table = "source";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "attendance_logs" and $column_name == "teacher_name"){
                                        $dir_table = "attendance_logs";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "institute_name"){
                                        $dir_table = "institutes";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "designation"){
                                        $dir_table = "tags";
                                        $dir_column = "tag";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "class_name"){
                                        $dir_table = "class";
                                        $dir_column = "tag";
                                    }
                                    elseif ($table_name == "controllers" and $column_name == "department_name"){
                                        $dir_table = "department";
                                        $dir_column = "tag";
                                    }
                                    elseif ($table_name == "calendar" and $column_name == "institute_name"){
                                        $dir_table = "institutes";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "calendar" and $column_name == "source_name"){
                                        $dir_table = "controllers";
                                        $dir_column = "name";
                                    }
                                    elseif ($table_name == "institute_devices" and $column_name == "institute_name"){
                                        $dir_table = "institutes";
                                        $dir_column = "name";
                                    }





                                    $text = $dir_table.".".$dir_column." ".$direction;
                                    $order_by_list[] = $text;
                                }
                                $i++;
                            }
                            $join_sort = join(",",$order_by_list);
                            if ($join_sort){
                                $sql .= " order by ".$join_sort." ";
                            }

                        }

                    }
                    if (isset($get_query_data['area']) and $table_name == 'controllers') {
                        if ($get_query_data['area']){
                            $child_areas = $this->child_areas($get_query_data['area']);
                            $child_area_ids = array();
                            foreach ($child_areas['find_data'] as $area){
                                $child_area_ids[] = $area['id'];
                            }
                            $join_area_ids = $this->common->sql_in_maker($child_area_ids);
                            $sql .= " and controllers.area in($join_area_ids) ";
                        }


                    }



                    if ($table_name == "institutes" and !isset($get_query_data["teacher_registration_report"])){
                        $sql .= " group by institutes.institute_id ";
                    }


                    if ($table_name == "notices"){
                        $sql .= " order by notices.time desc ";
                    }
                    else{
                        if (!isset($get_query_data['data_table'])){
                            $sql .= " order by $table_name.id desc ";
                        }

                    }

                    $count_sql =  $sql;

                    if ($limit){
                        $sql .= " limit $skip,$limit ";
                    }

                    try{
                        $table_name_with_join = $table_name;

                        if ($table_name == "payment_requests"){
                            $table_name_with_join .= " left join controllers on controllers.user_id= payment_requests.user_id 
                            left join bank_cashes on bank_cashes.bank_id = payment_requests.bank_id";
                            $columns = "payment_requests.*,controllers.name as sender_name,controllers.image as sender_image,
                            bank_cashes.name as bank_name";
                        }
                        elseif ($table_name == "reports"){
                            $table_name_with_join .= " left join  customer_vendors as source on source.user_id = reports.source";
                            $columns = "reports.*,source.name as source_name";
                        }
                        elseif ($table_name == "controllers"){
                            $table_name_with_join .= " left join  tags on tags.id = controllers.designation 
                            left join tags as class on class.id = controllers.class 
                            left join tags as department on department.id = controllers.department 
                            left join institutes on institutes.institute_id = controllers.institute_id ";
                            $columns = "controllers.*,tags.tag as designation,institutes.name as institute_name,
                            class.tag as class_name,department.tag as department_name";

                        }
                        elseif ($table_name == "institutes" and !isset($get_query_data["teacher_registration_report"])){
                            if ($this->common->logged_user_type() === "admin" or $this->common->logged_user_type() === "teacher" ){
                                $table_name_with_join .= $this->common->admin_institutes_join_sql($_REQUEST['user_designation']);
                            }
                            $columns = "institutes.*";
                            $table_name_with_join .= " left join controllers as head_teacher on head_teacher.institute_id = institutes.institute_id 
                             ";
                        }
                        elseif ($table_name == "attendance_logs"){
                            $table_name_with_join .= " left join controllers on controllers.user_id = attendance_logs.user_id ";
                            $columns = "attendance_logs.*,controllers.name as teacher_name";

                        }
                        elseif ($table_name == "institute_devices"){
                            $table_name_with_join .= " left join institutes on institutes.institute_id = institute_devices.institute_id ";
                            $columns = "institute_devices.*,institutes.name as institute_name,institutes.member_id";

                        }

                        elseif ($table_name == "calendar"){
                            $table_name_with_join .= " left join controllers on controllers.user_id = calendar.source_id 
                            left join institutes on institutes.institute_id = controllers.institute_id ";
                            $columns = "calendar.*,controllers.name as source_name,institutes.name as institute_name";

                        }


                        if (isset($get_query_data["add_to_branch"])){
                            $branch_id = "";
                            $table_name_with_join .= "
                            left join item_units on items.default_unit_id = item_units.id 
                            left join branch_items on items.item_id= branch_items.item_id and 
                            branch_items.branch_id = :branch_id 
                            
                            ";
                            $columns = "branch_items.*,items.name,items.item_id,items.time,item_units.unit_name,items.default_unit_id 
                            ";
                            if (isset($_SESSION['logged_branch_id'])){
                                $branch_id = $_SESSION["logged_branch_id"];
                            }
                            $sql_data[":branch_id"] =  $branch_id;
                        }
                        elseif (isset($get_query_data["branch_permission"])){
                            $branch_id = "";
                            $table_name_with_join .= "
                            left join branch_bank_cashes on bank_cashes.bank_id = branch_bank_cashes.bank_id and 
                            branch_bank_cashes.branch_id = :branch_id 
                            
                            ";
                            $columns = "bank_cashes.*,branch_bank_cashes.branch_id,case when branch_bank_cashes.id is null then '0' else branch_bank_cashes.active end  as status 
                            ";
                            if (isset($_SESSION['logged_branch_id'])){
                                $branch_id = $_SESSION["logged_branch_id"];
                            }
                            $sql_data[":branch_id"] =  $branch_id;
                        }
//                        return $return_object;
                        if ($count){
                            $total_data = $this->request_documents_counter($table_name_with_join,"$table_name.id",$count_sql,$sql_data);
                            return $total_data;
                        }
                        else{

                            $get_data = $this->common->retriever($table_name_with_join,$columns,$sql,$sql_data,true);
                            foreach ($get_data as $index => $item){
                                $get_data[$index]['time'] = $this->common->text_date_time("",$item['time']);
                            }
                            if (isset($get_query_data["add_to_branch"])){
                                $items_ids = array();
                                foreach ($get_data as $item){
                                    $item_id = $item['item_id'];
                                    $items_ids[] = $item_id;

                                }
                                $join_item_ids = array();
                                foreach($items_ids as $item_id){
                                    $join_item_ids[] = "'".$item_id."'";
                                }
                                $text_join_ids = join(",",$join_item_ids);
                                $unit_sql = "where item_id in($text_join_ids) and active != :active";
                                $unit_sql_data = array(
                                    ":active" => 2
                                );
                                $unit_data = $this->common->retriever("item_units","*",$unit_sql,$unit_sql_data,true);
                                $b_i = 0;
                                foreach($items_ids as $item_id){
                                    $filter_keys = array(
                                        "item_id" => $item_id
                                    );
                                    $filter = $this->common->filter_in_array($unit_data,$filter_keys);
                                    $get_data[$b_i]["units"] = $filter;
                                    $b_i++;
                                }
                            }

//                        $return_object['sql'] = $sql;
//                        $return_object['sql_data'] = $sql_data;
                            if (!isset($_REQUEST['export'])){
                                $return_object['find_data'] = $get_data;
//                                $return_object['table_name'] = $table_name_with_join;
//                                $return_object['sql'] = $sql;
//                                $return_object['sql_data'] = $sql_data;
                            }

                            $return_object['status'] = 1;
                            // if data table on

                            if (isset($get_query_data['data_table'])){
                                $total_data = $this->request_documents_counter($table_name_with_join,"$table_name.id",$count_sql,$sql_data);
                                $total = $total_data['total'];
                                $table_format_data = array(
                                    "draw"=>intval($_REQUEST['draw']),
                                    "recordsTotal"=>$total,
                                    "recordsFiltered"=>$total,
                                    "data"=>$get_data,
                                    "sql" => $sql,
                                    "table_name" => $table_name_with_join,
                                    "sql_data" => $sql_data,
                                    "columns" => $columns,
                                );
                                return $table_format_data;
                            }
                            else{
                                if (isset($_REQUEST['export'])){
                                    try{
                                        $prefix = $table_name;
                                        if (isset($_REQUEST['prefix'])){
                                            $prefix .= " ".$_REQUEST['prefix'];
                                        }
                                        $file_info = $this->common->excel_file_maker($get_data,$prefix);
                                        if ($file_info){
                                            $return_object['status'] = 1;
                                            $return_object['data'] = $file_info;
                                        }
                                        else{
                                            $return_object['errors'][] = "Excel file failed";
                                            $return_object['status'] = 0;
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = $exception->getMessage();
                                    }
                                }
                            }
                        }



                    }catch (Exception $exception){
                        $return_object['errors'][] = "Retrieve fail for ".$exception->getMessage();
                    }

                }
                else{
                    $return_object['errors'][] = "Decode data empty";
                }
            }
            else{
                $return_object['errors'][] = "Query info empty!";
            }
            return $return_object;
        }


        function tags($type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $sql = " where type=:type and active=:active";
                $sql_data = array(
                    ":type" => $type,
                    ":active" => 1
                );
                $data = $this->common->retriever("tags","*",$sql,$sql_data,true);
                $return_object['find_data'] = $data;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = "Get data error for ".$exception->getMessage();
            }
            return $return_object;
        }
        function shifts($with_settings=false,$institute_id=null){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $sql = "left join institute_shifts on institute_shifts.shift = shifts.id 
                 where shifts.active=:active";
                $sql_data = array(
                    ":active" => 1
                );
                if ($institute_id){
                    $sql .= " and institute_shifts.institute_id = :institute_id ";
                    $sql_data[':institute_id'] = $institute_id;
                }

                $sql .= " group by shifts.id ";
                $data = $this->common->retriever("shifts","shifts.*",$sql,$sql_data,true);

                if ($data){
                    if ($with_settings){
                        $shift_settings = $this->time_schedule_format();
                        $sql = "where type_option = :type_option";
                        $sql_data = array(
                            ":type_option" => "shift_settings",
                        );
                        $find = $this->common->retriever("shift_settings","*",$sql,$sql_data,true);
                        foreach ($data as $index => $shift){
                            $shift_id = $shift['id'];
                            $filter_current_settings = $this->common->filter_in_array($find,array(
                                "source" => $shift_id
                            ));
                            /// insert default settings
                            $data[$index]['settings'] = $shift_settings;
                            /// update settings by changes
                            foreach ($shift_settings as $key => $value){
                                $filter_keys = array(
                                    "type" => $key,
                                );
                                $filter = $this->common->filter_in_array($filter_current_settings,$filter_keys,false);
                                if($filter){
                                    $data[$index]['settings'][$key] = $filter['value'];
                                }

                            }
                        }

                    }

                }
                $return_object['find_data'] = $data;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = "Get data error for ".$exception->getMessage();
            }
            return $return_object;
        }



        function software_info($without_error=false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $software_info = array(
                "title" => "Erp from page71.org",
                "currency" => "$",
                "time_zone" => "Asia/Dhaka",
                "logo" => "static/app/assets/img/default/no-image.png",
                "login_banner" => "static/app/assets/img/default/no-image.png",
                "favicon" => "favicon.ico",
                "email" => "info@page71.org",
                "company_name" => "page71.org",
                "construction_mode" => array(
                    "active" => 0,
                    "date" => date("Y-m-d"),
                    "time" => date("H:i"),
                ),
                "https" => 0,
            );
            $sql = "where type_option != :type_option";
            $sql_data = array(
                ":type_option" => "institute_settings"
            );
            $find = $common->retriever("settings","*",$sql,$sql_data,true);
            if ($find){
                //for title
                $filter_keys = array(
                    "type" => "software_title"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['title'] = $filter['value'];
                }
                //for currency
                $filter_keys = array(
                    "type" => "software_currency"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['currency'] = $filter['value'];
                }
                //for time_zone
                $filter_keys = array(
                    "type" => "software_time_zone"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['time_zone'] = $filter['value'];
                }
                //for logo
                $filter_keys = array(
                    "type" => "software_logo"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['logo'] = $filter['value'];
                }
                //for favicon
                $filter_keys = array(
                    "type" => "software_favicon"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['favicon'] = $filter['value'];
                }
                //for login banner
                $filter_keys = array(
                    "type" => "software_login_banner"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['login_banner'] = $filter['value'];
                }

                //for email
                $filter_keys = array(
                    "type" => "software_email"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['email'] = $filter['value'];
                }
                //for company_name
                $filter_keys = array(
                    "type" => "software_company_name"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['company_name'] = $filter['value'];
                }
                //for https force
                $filter_keys = array(
                    "type" => "software_https"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['https'] = floatval($filter['value']);
                }

                //for construction_mode
                $filter_keys = array(
                    "type" => "software_construction_mode"
                );
                $filter = $common->filter_in_array($find,$filter_keys,false);
                if ($filter){
                    $software_info['construction_mode'] = json_decode($filter['value'],true);
                }

            }
            if ($without_error){
                return $software_info;
            }
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            else{
                return $software_info;
            }


        }
        function institute_settings($institute_id,$default_object = false,$am_pm_format = false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $software_info = array(
                "absence_person_a_day" => "3",
            );
            if ($default_object){
                return $software_info;
            }
            $sql = "where type_option = :type_option and source=:institute_id";
            $sql_data = array(
                ":type_option" => "institute_settings",
                ":institute_id" => $institute_id,
            );
            $find = $common->retriever("settings","*",$sql,$sql_data,true);
            if ($find){
                foreach ($software_info as $key => $value){
                    $filter_keys = array(
                        "type" => $key
                    );
                    $filter = $common->filter_in_array($find,$filter_keys,false);
                    if($filter){
                        $software_info[$key] = $filter['value'];
                    }

                }


            }

            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            else{
                return $software_info;
            }


        }
        function time_schedule_format($am_pm_format = false){
            $format = array(
                "in_time" => "09:00",
                "out_time" => "17:00",
                "late_time" => "15",
                "early_time" => "15",
                "in_range_from" => "08:00",
                "in_range_to" => "10:00",
                "out_range_from" => "16:00",
                "out_range_to" => "18:00",
            );
            if ($am_pm_format){
                $time_format_keys = array("in_time","out_time","in_range_from","out_range_from","in_range_to","out_range_to");
                foreach ($time_format_keys as $key){
                    $value = $format[$key];
                    $format[$key] = $this->common->am_pm_time_format($value);
                }
            }
            return $format;
        }
        function shift_info($id,$settings = false,$am_pm_format = false){
            $sql = " where id = :id ";
            $data = array(
                ":id" => $id,
            );
            $columns = "*";
            $shift_info = $this->common->retriever("shifts",$columns,$sql,$data);
            if ($shift_info){
                if ($settings){
                    $shift_settings = $this->time_schedule_format($am_pm_format);
                    $sql = "where type_option = :type_option and source=:source_id";
                    $sql_data = array(
                        ":type_option" => "shift_settings",
                        ":source_id" => $id,
                    );
                    $find = $this->common->retriever("shift_settings","*",$sql,$sql_data,true);
                    if ($find){
                        foreach ($shift_settings as $key => $value){
                            $filter_keys = array(
                                "type" => $key
                            );
                            $filter = $this->common->filter_in_array($find,$filter_keys,false);
                            if($filter){
                                $shift_settings[$key] = $filter['value'];
                            }

                        }


                    }

                    if ($am_pm_format){
                        $time_format_keys = array("in_time","out_time","in_range_from","out_range_from","in_range_to","out_range_to");
                        foreach ($time_format_keys as $key){
                            $value = $shift_settings[$key];
                            $shift_settings[$key] = $this->common->am_pm_time_format($value);
                        }
                    }
                    
                    $shift_info['settings'] = $shift_settings;
                }


                return $shift_info;
            }
            return array();
        }
        function find_users(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $validator = new data_validator();
            $validation = $validator->find_mobile_validation();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $mobile = $_REQUEST['mobile'];
                try{
                    $sql = "where mobile like :mobile and active=:active";
                    $columns = "id,user_id,email,mobile,name,member_id,image";
                    $sql_data = array(
                        ":mobile" => "%".$mobile."%",
                        ":active" => 1
                    );
                    $find = $common->retriever("controllers",$columns,$sql,$sql_data,true);
                    $return_object['find_data'] = $find;
                    $return_object['status'] = 1;
                }catch (Exception $exception){
                    $return_object['errors'][] = "Find fail for ".$exception->getMessage();
                }
            }
            return $return_object;
        }


        function documents($limit,$skip,$count=false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $document_type = "";
            $type_option = "";
            if(isset($_REQUEST['document_type'])){
                $document_type = $_REQUEST['document_type'];
            }
            else{
                $return_object['errors'][] = "Document type undefined";
            }
            if(isset($_REQUEST['type_option'])){
                $type_option = $_REQUEST['type_option'];
            }
            else{
                $return_object['errors'][] = "Document type option undefined";
            }
            if (!$common->error_in_object($return_object)){
                $sql = " ";

                if ($type_option == "cash_sale" or $type_option == "cash_bill"){
                    $sql .= "  inner join invoice_bills on invoice_bills.document_row_id=documents.id 
                    left join customer_vendors on  customer_vendors.user_id = invoice_bills.receiver_id 
                    ";
                }
                elseif ($type_option == "gdn"){
                    $sql .= " inner join gdns on gdns.document_row_id = documents.id ";
                }
                elseif ($type_option == "cash_memo" or $type_option == "bill_memo" or $type_option == "return_cash_memo" or $type_option == "return_bill_memo" ){
                    $sql .= " inner join payments on payments.document_row_id = documents.id ";
                }
                elseif ($type_option == "leave" or $type_option == "training" ){
                    $sql .= " inner join applications on applications.document_row_id = documents.id ";
                }

                $logged_branch_id = $common->logged_branch_id();
                $logged_user_type = $common->logged_user_type();
                $sql .= "where documents.type=:type and documents.type_option=:type_option and documents.active!=:active ";
                $sql_data = array(
                    ":type" => $document_type,
                    ":type_option" => $type_option,
                    ":active" => 2,
                );

                if($logged_user_type == "teacher"){
                    $sql .= " and applications.active = :permitted ";
                    $sql_data[':permitted'] = 1;
                }
                if (isset($_REQUEST['pending_application'])){
                    $sql .= " and documents.authorization_status = :pending ";
                    $sql_data[':pending'] = 0;
                }
                if (isset($_REQUEST['keyword'])){
                    $keyword = $_REQUEST['keyword'];
                    $like_plus = "";
                    if ($type_option == "cash_sale" or $type_option == "cash_bill"){
                        $like_plus .= " or customer_vendors.name like :receiver_name  ";
                    }

                    $sql .= " and ( 
                    documents.member_id like :document_member_id $like_plus 
                     ) ";
                    $sql_data[":document_member_id"] = "%".$keyword."%";
                    if ($type_option == "cash_sale" or $type_option == "cash_bill" ){
                        $sql_data[":receiver_name"] = "%".$keyword."%";
                    }

                }
                if (isset($_REQUEST['designation'])){
                    $designation = $_REQUEST['designation'];
                    $institute_id = $_REQUEST['institute_id'];
                    $my_institutes = $this->my_institutes($designation,$institute_id);
                    if (isset($my_institutes['find_data'])){
                        $institutes = $my_institutes['find_data'];
                        $institute_ids = array();
                        foreach ($institutes as $institute){
                            $institute_ids[] = $institute['institute_id'];
                        }
                        $join_institute_ids = $common->sql_in_maker($institute_ids);
                        $sql .= " and applications.institute_id in($join_institute_ids) ";
                    }
                }
                $sql .= " order by documents.time desc ";
                if (!$count){
                    $sql .= " limit $skip,$limit";
                }

                $columns = "documents.*,
                concat(extract(day from documents.time),' ',MONTHNAME(documents.time)) as doc_time,
                authorization_status as authorized
                ";
                if ($type_option == "cash_sale" or $type_option == "cash_bill" ){
                    $columns .= ",customer_vendors.name as receiver_name,invoice_bills.distribution_status";
                }

                if ($count){
                    try{
                        $count_data = $common->data_counter("documents",$columns,$sql,$sql_data);
                        $return_object['total'] = $count_data;
                        $return_object['status'] = 1;
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Data count failed for ".$exception->getMessage();
                    }
                }
                else{
                    try{
                        $find = $common->retriever("documents",$columns,$sql,$sql_data,true);
                        $return_object['find_data'] = $find;
                        $return_object['status'] = 1;
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Data find failed for ".$exception->getMessage();
                    }
                }
            }

            return $return_object;
        }


        function invoice_last_update(){
            $data = array(
                "note" => "",
                "remarks" => "",
            );
            $user_cookies = $this->common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            /// get last note by this user
            try{
                $note = $this->common->get_last_update_value("invoice_note",$logged_user_id,true);
                $data['note'] = $note;
            }catch (Exception $exception){

            }
            /// get last remarks by this user
            try{
                $note = $this->common->get_last_update_value("invoice_remarks",$logged_user_id,true);
                $data['remarks'] = $note;
            }catch (Exception $exception){

            }

            return $data;
        }
        function type_of_activities($type=""){
            return $this->initial_data->type_of_activities($type);
        }

        function activities($limit,$skip,$count=false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            if (!$common->error_in_object($return_object)){
                $sql = " left join controllers on controllers.user_id = activities.user_id  
                left join chemists on chemists.user_id = activities.affected_id 
                left join tags as market on market.id = chemists.market 
                left join items on items.item_id = activities.affected_id 
                left join documents on documents.document_id = activities.affected_id 
                left join invoice_bills on invoice_bills.document_row_id = documents.id 
                left join controllers as profile on profile.user_id = activities.affected_id 
                left join vendors on vendors.user_id = activities.affected_id 
                left join map_sources on map_sources.id = activities.affected_id 
                left join batches on batches.batch_id = activities.affected_id 
                left join raw_items on raw_items.item_id = activities.affected_id 
                left join payment_requests on payment_requests.id = activities.affected_id 
                left join invoice_bills as payment_of_invoice on payment_of_invoice.document_row_id = payment_requests.document_row_id 
                left join documents as  document_of_payment on document_of_payment.id = payment_of_invoice.document_row_id 
                left join bank_cashes on bank_cashes.bank_id = activities.affected_id 
                
                ";
                $sql_data = array(
                );
                /// if logged user is admin . then activities will be without system designer log
                $user_cookies = $common->user_cookies_data();
                $logged_user_id = $user_cookies['logged_user_id'];
                $id_type_name = $common->id_type_name($logged_user_id);
                if ($id_type_name){
                    if ($id_type_name != "system_designer"){
                        $id_type = $common->id_type("system_designer");
                        $sql .= " where activities.user_id not like :system_designer ";
                        $sql_data[":system_designer"] = $id_type."%";
                    }
                }
                $sql .= " order by activities.time desc ";
                if (!$count){
                    $sql .= " limit $skip,$limit";
                }

                $columns = "activities.*,controllers.name as controller_name,controllers.member_id as controller_member_id,controllers.image as controller_image, 
                case 
                    when document_of_payment.member_id is not null then document_of_payment.member_id 
                    when chemists.name is not null then chemists.name 
                    when items.item_name is not null then items.item_name 
                    when documents.member_id is not null then documents.member_id 
                    when profile.name is not null then profile.name 
                    when vendors.name is not null then vendors.name 
                    when map_sources.name is not null then map_sources.name 
                    when batches.name is not null then batches.name 
                    when raw_items.item_name is not null then raw_items.item_name 
                    when bank_cashes.name is not null then bank_cashes.name 
                    
                    else 'Unknown'
                end
                as affected_name,
                case 
                    when items.image is not null then items.image 
                    when profile.image is not null then profile.image 
                    when vendors.image is not null then vendors.image 
                    when raw_items.image is not null then raw_items.image 
                    else 'Unknown'
                end
                as affected_image,
                
                items.item_code,items.generic_name,
                market.tag as market_name,
                payment_requests.amount as payment_amount
                
                ";

                if ($count){
                    try{
                        $count_data = $common->data_counter("activities",$columns,$sql,$sql_data);
                        $return_object['total'] = $count_data;
                        $return_object['status'] = 1;
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Data count failed for ".$exception->getMessage();
                    }
                }
                else{
                    try{
                        $find = $common->retriever("activities",$columns,$sql,$sql_data,true);
                        $return_object['find_data'] = $find;
                        $return_object['status'] = 1;
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Data find failed for ".$exception->getMessage();
                    }
                }
            }

            return $return_object;
        }
        function document_helpers($type,$value,$document_type,$document_sub_type,$limit=true){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['value'])){
                $value = $_REQUEST['value'];
            }
            $user_cookies = $this->common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];

            $sql = "where type=:type and value like :value and document_type=:document_type and 
            document_type_option=:document_type_option and user_id=:user_id";
            $sql_data = array(
                ":type" => $type,
                ":value" => "".$value."%",
                ":document_type" => $document_type,
                ":document_type_option" => $document_sub_type,
                ":user_id" => $logged_user_id,
            );
            if ($limit){
                $sql .= " limit 2";
            }
            try{
                $find = $this->common->retriever("document_helpers","value",$sql,$sql_data,true);
                $return_object['find_data'] = $find;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }


        function application_info($document_id){
            $application_data = array();
            $sql = "left join documents on applications.document_row_id = documents.id 
            left join institutes  on applications.institute_id=institutes.institute_id  
            left join tags on tags.id = applications.application_tag 
            left join authorized_documents on authorized_documents.document_row_id=applications.document_row_id 
            left join controllers as authorize on  authorized_documents.user_id=authorize.user_id 
            left join controllers as prepare on documents.creator_id=prepare.user_id 
            where documents.document_id = :document_id";
            $sql_data = array(
                ":document_id" => $document_id,
            );
            $columns = "applications.*,documents.type as document_type,documents.member_id,prepare.name as sender_name,
            institutes.name as institute_name,institutes.address as institute_address,documents.authorization_status,
            documents.type_option,tags.tag as application_tag,applications.application_tag as application_tag_id,
            case when prepare.id is null then '' else prepare.name end as prepare_user_name,
            documents.authorization_status as authorized,documents.document_id as document_id,
            documents.payment_status,date(documents.time) as issue_date,
            case when authorize.id is null then '' else authorize.name end as authorize_user_name";
            $find = $this->common->retriever("applications",$columns,$sql,$sql_data);
            if ($find){
                $application_data = $find;
                $document_time = $find['issue_date'];
                $document_type = $find['document_type'];
                $print_date = $this->common->text_date_time();
                $document_date = $this->common->text_date_time("Y-m-d",$document_time);
                $application_data['print_date'] = $print_date;
                $application_data['document_date'] = $document_date;
                $application_data['document_id'] = $document_id;
                $logged_user_type = $this->common->logged_user_type();
                $permission_switch = 1;
                if ($logged_user_type == "teacher" or $logged_user_type == "system_designer" or $logged_user_type == "system_admin" ){
                    $permission_switch = 0;
                }
                $application_data['permission_switch'] = $permission_switch;

                $start_date = $application_data['start_date'];
                $start_date = $this->common->next_time(0,0,0,"Y-m-d",$start_date);
                $application_data['start_date'] = $start_date;

                $start_date = new DateTime($start_date);
                $end_date = $application_data['end_date'];
                $end_date = $this->common->next_time(0,0,0,"Y-m-d",$end_date);
                $application_data['end_date'] = $end_date;
                $end_date = new DateTime($end_date);
                $day_difference = $end_date->diff($start_date)->format("%a");
                $logged_user_id = $this->common->logged_user_id();
                $logged_user_type = $this->common->logged_user_type();
                $authorize_permission = false;
//                if ($logged_user_type == "teacher"){
//                    $teacher_info = $this->teacher_info($logged_user_id);
//                    if ($teacher_info){
//                        $designation = $teacher_info['designation'];
//                        if ($designation == "Head teacher"){
//                            if ($day_difference <= 2){
//
//
//                            }
//
//                        }
//                    }
//                }
//                else{
//                    $authorize_permission = true;
//                }
                $authorize_permission = true;
                $application_data['authorize_permission'] = $authorize_permission;
                $document_row_id = $application_data['document_row_id'];
                $sql = " left join controllers on controllers.user_id=application_items.user_id 
                left join tags on tags.id = controllers.designation 
                where application_items.document_row_id = :document_row_id";
                $sql_data = array(
                    ":document_row_id" => $document_row_id
                );
                $columns = "application_items.*,controllers.name,tags.tag as designation,controllers.device_user_id";
                $find_items = $this->common->retriever("application_items",$columns,$sql,$sql_data,true);
                /// get invoice type option name
                $invoice_type_info = $this->initial_data->document_types($document_type);
                if ($invoice_type_info){
                    $invoice_option_name = $application_data['type_option'];
                    $invoice_type_name = $invoice_type_info['name'];
                    $application_data['document_type'] = $invoice_type_name;
                    $options = $invoice_type_info['options'];
                    $keys = array(
                        "label"=> $invoice_option_name
                    );
                    $option_info = $this->initial_data->filter_in_array($options,$keys,false);
                    if ($option_info){
                        $application_data['type_option_name'] = $option_info['name'];
                    }
                }
                $teacher_names_array = array();
                $teacher_ids = array();
                if ($find_items) {
                    $i = 0;
                    foreach ($find_items as $item) {
                        $teacher_names_array[] = $item['name'];
                        $teacher_ids[] = $item['user_id'];
                        $find_items[$i]['sl'] = $i + 1;
                        $i++;
                    }
                    $application_data['items'] = $find_items;
                }
                $application_data['teacher_names'] = join(", ",$teacher_names_array);
                $application_data['teacher_ids'] = $teacher_ids;
            }
            return $application_data;
        }



        function get_document($document_id,$document_type_option,$document_type=""){
            $document_info = array();
            if ($document_type_option == "training" or $document_type_option == "leave" ){
                $document_info = $this->application_info($document_id);
            }
            return $document_info;
        }

        function institute_info($institute_id,$member_id = false,$teacher_count=false,$student_count=false){

            $sql = " 
            where institutes.id > :id ";
            $data = array(
                ":id" => 0,
            );

            if ($member_id){
                $sql .= " and member_id = :member_id";
                $data[':member_id'] = $institute_id;
            }
            else{
                $sql .= " and institute_id = :institute_id";
                $data[':institute_id'] = $institute_id;
            }
            $columns = "institutes.*";

            $institute_info = $this->common->retriever("institutes",$columns,$sql,$data);
            if ($institute_info){
                $institute_info['total_teacher'] = 0;
                $institute_info['total_student'] = 0;
                $institute_id = $institute_info['institute_id'];
                if ($teacher_count){
                    $teacher_id_digit = $this->common->id_type("teacher");
                    $total_teachers_sql = "where active != :active and user_id like :user_id and institute_id = :institute_id";
                    $total_teachers = $this->common->data_counter("controllers","id",$total_teachers_sql,array(
                        ":active" => 2,
                        ":user_id" => $teacher_id_digit."%",
                        ":institute_id" => $institute_id,
                    ));
                    $institute_info['total_teacher'] = $total_teachers;
                }

                if ($student_count){
                    $student_id_digit = $this->common->id_type("student");
                    $total_students_sql = "where active != :active and user_id like :user_id and institute_id = :institute_id";
                    $total_students = $this->common->data_counter("controllers","id",$total_students_sql,array(
                        ":active" => 2,
                        ":user_id" => $student_id_digit."%",
                        ":institute_id" => $institute_id,
                    ));
                    $institute_info['total_student'] = $total_students;
                }
                /// institute shifts
                $institute_info['shift_data'] = $this->common->retriever("institute_shifts","*","where institute_id = :institute_id",array(
                    ":institute_id" => $institute_id
                ),true);


                
                return $institute_info;
            }
            return array();
        }


        function institute_device_info($institute_id){
            $sql = " left join institute_devices on institute_devices.institute_id = institutes.institute_id 
            left join controllers on controllers.user_id = institute_devices.creator_id 
              where institutes.institute_id = :institute_id ";
            $data = array(
                ":institute_id" => $institute_id,
            );
            $columns = "institute_devices.*,institute_devices.name as device_name,institutes.name,institutes.institute_id,
            controllers.name as creator_name";
            $institute_info = $this->common->retriever("institutes",$columns,$sql,$data);
            if ($institute_info){
                return $institute_info;
            }
            return array();
        }


        function child_areas($parent,$type=false,$institute=false,$unique_by_admin=null,$allow_by_logged_user = false){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $column = "parent";
            if ($type){
                $column = "type";
            }
            try{
                $join = "";
                if ($type){
                    $join = "left join areas as grand_parent on  grand_parent.id = areas.parent
                    ";
                }
                else{
                    $join = "left join areas as grand_parent on  grand_parent.id = areas.parent
                    ";
                }
                if ($institute){
                    $join .= " left join institutes on institutes.institute_id = areas.name ";
                }
                if ($unique_by_admin){
                    $join .= " left join controllers on controllers.area = areas.id  ";
                }
                $sql = "$join where areas.$column = :parent and areas.active != :active";
                $sql_data = array(
                    ":active" => 2,
                    ":parent" => $parent,
                );

                $sub_sql = "";
                if ($unique_by_admin){
                    $sub_sql = "(select count(id) as total from controllers as active_area where active_area.area = areas.id and 
                    active_area.active = :area_active)";
                    $sql .= " and (controllers.id is null or controllers.active = :delete or controllers.user_id = :user_id) ";
                    $sql_data[':delete'] = 2;
                    $sql_data[':user_id'] = $unique_by_admin;
//                    $sql_data[':area_active'] = 1;
                }
                if ($type){
                    if ($parent == "country"){
                        $sql .= " and (grand_parent.active = :grand_active or grand_parent.id is null )";
                        $sql_data[':grand_active'] = 1;
                    }
                    else{
                        $sql .= " and (grand_parent.active = :grand_active )";
                        $sql_data[':grand_active'] = 1;
                    }

                }
                if ($allow_by_logged_user){
                    if ($this->common->logged_user_type() == "admin"){
                        $logged_user = $this->user_info($this->common->logged_user_id());
                        $my_areas = $this->my_area_by_type($logged_user['designation'],$parent,$logged_user['user_id']);
                        $my_area_ids = array();
                        foreach ($my_areas['find_data'] as $this_area){
                            $my_area_ids[] = $this_area['id'];
                        }
                        $join_my_ids = $this->common->sql_in_maker($my_area_ids);
                        $sql .= " and areas.id in($join_my_ids) ";
                    }

                }
                $columns = "areas.*";
                if ($institute){
                    $columns .= ",institutes.name as name";
                    $sql .= " order by institutes.name asc ";
                }
                else{
                    $sql .= " order by areas.name asc ";
                }
                
                if ($unique_by_admin){
//                    $columns .= ",$sub_sql as total,controllers.id as c_id";
                }

                $finds = $this->common->retriever("areas",$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function estimate_institutes(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $sub_sql = "(select count(id) as total from areas as active_area where active_area.name = areas.name and 
                active_area.active = :area_active)";
                $sql = "left join areas on areas.name = institutes.institute_id 
                 where institutes.active != :active and (areas.id is null or areas.active = :delete) having $sub_sql = 0";
                $sql_data = array(
                    ":active" => 2,
                    ":area_active" => 1,
                    ":delete" => 2,
                );

                $columns = "institutes.*";
                $finds = $this->common->retriever("institutes",$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function get_sms_receivers(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array(),
                "find_data" => array()
            );

            try{
                $to_type = $_REQUEST['to_type'] ?? '';
                $session = $_REQUEST['session'] ?? '';
                $gender = $_REQUEST['gender'] ?? '';
                $institute_id = $_REQUEST['institute_id'] ?? '';
                $religion = $_REQUEST['religion'] ?? '';
                $branch = $_REQUEST['branch'] ?? '';
                $department = $_REQUEST['department'] ?? '';
                $institute = $_REQUEST['institute'] ?? '';
                $class = $_REQUEST['class'] ?? '';
                $designation = $_REQUEST['designation'] ?? '';

                if (!$to_type){
                    $return_object['messages'][] = "To type required";
                    return $return_object;
                }
                $id_type = $this->common->id_type($to_type);
                $sql = " where  controllers.user_id like :user_id and controllers.institute_id != :empty and controllers.active != :delete";
                $sql_data = array(
                    ':user_id' => $id_type."%",
                    ':empty' => "",
                    ':delete' => 2,
                );


                if ($session){
                    if (gettype($session) != 'array'){
                        $session = explode(",",$session);
                    }
                    $join_session = $this->common->sql_in_maker($session);
                    $sql .= " and controllers.session in($join_session)";
                }

                if ($department){
                    if (gettype($department) != 'array'){
                        $department = explode(",",$department);
                    }
                    $join_department = $this->common->sql_in_maker($department);
                    $sql .= " and controllers.department in($join_department)";
                }
                if ($gender){
                    if (gettype($gender) != 'array'){
                        $gender = explode(",",$gender);
                    }
                    $join_gender = $this->common->sql_in_maker($gender);
                    $sql .= " and controllers.gender in($join_gender)";
                }
                if ($religion){
                    if (gettype($religion) != 'array'){
                        $religion = explode(",",$religion);
                    }
                    $join_religion = $this->common->sql_in_maker($religion);
                    $sql .= " and controllers.religion in($join_religion)";
                }
                if ($branch){
                    if (gettype($branch) != 'array'){
                        $branch = explode(",",$branch);
                    }
                    $join_branch = $this->common->sql_in_maker($branch);
                    $sql .= " and controllers.branch in($join_branch)";
                }
                if ($class){
                    if (gettype($class) != 'array'){
                        $class = explode(",",$class);
                    }
                    $join_class = $this->common->sql_in_maker($class);
                    $sql .= " and controllers.class in($join_class)";
                }

                if (gettype($institute) != 'array'){
                    $institute = explode(",",$institute);
                }

                if (strlen($this->common->index_number_in_list($institute,'all'))){
                    $institute = array();
                    $my_institute_data = $this->my_institutes($designation,$institute_id);
                    $institutes = $my_institute_data['find_data'] ?? array();
                    foreach ($institutes as $item){
                        $institute[] = $item['institute_id'];
                    }
                }
                $join_institute = $this->common->sql_in_maker($institute);
                $sql .= " and controllers.institute_id in($join_institute)";
                

                $columns = "controllers.name as label,controllers.user_id as value,controllers.mobile,controllers.institute_id";
                $return_object['find_data'] = $this->common->retriever("controllers",$columns,$sql,$sql_data,true);;



                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }



        function calendar_event_info($id){
            $sql = " 
             where calendar.id = :id";
            $data = array(
                ":id" => $id,
            );
            $columns = "calendar.*,calendar.start_time as start,calendar.end_time as end";
            $calendar_event_info = $this->common->retriever("calendar",$columns,$sql,$data);
            if ($calendar_event_info){
                return $calendar_event_info;
            }
            return array();
        }

        function calendar_events($type,$source,$start_date=0,$end_date=0,$absent=false,$institute_id=null){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            try{
                $common = $this->common;
                $types = array("default");
                $sources = array("");
                $join = "";

                if ($type == "institute"){
                    $types[] = "institute";
                    $sources[] = $source;
                }
                elseif ($type == "teacher"){
                    $types[] = "institute";
                    $types[] = "teacher";
                    $teacher_info = $this->teacher_info($source);
                    if ($teacher_info){
                        $sources[] = $teacher_info['institute_id'];
                    }
                    $sources[] = $source;
                }
                elseif ($type == "student"){
                    $types[] = "institute";
                    $types[] = "student";
                    $teacher_info = $this->student_info($source);
                    if ($teacher_info){
                        $sources[] = $teacher_info['institute_id'];
                    }
                    $sources[] = $source;
                }


                $join_types = $this->common->sql_in_maker($types);
                $join_sources = $this->common->sql_in_maker($sources);
                $sql = "$join where type in($join_types)  and source_id in($join_sources) and calendar.active != :active and 
                creator_id != :cron
                   ";
                $sql_data = array(
                    ":active" => 2,
                    ":cron" => "cron",
                );
                $current_date = date("Y-m-d");
                if (!$end_date){
                    $end_date = $current_date;
                }

                $joining_date = '0000-00-00';
                if (!$start_date){
                    if ($type == "student" or $type == "teacher"){
                        /// get first start date from user joining date
                        $user_sql_data = array(
                            ":user_id" => $common->logged_user_id()
                        );
                        if ($source){
                            $user_sql_data[':user_id'] = $source;
                        }
                        $user_info = $common->retriever("controllers","date(time) as joining_date","where user_id = :user_id",$user_sql_data);
                        if ($user_info){
                            $joining_date = $user_info['joining_date'];
                        }
                        else{
                            $return_object['errors'][] = "User info not found";
                            return $return_object;
                        }
                    }

                }
                elseif ($type == "student" or $type == "teacher"){
                    /// get first start date from user joining date
                    $user_sql_data = array(
                        ":user_id" => $common->logged_user_id()
                    );
                    if ($source){
                        $user_sql_data[':user_id'] = $source;
                    }
                    $user_info = $common->retriever("controllers","date(time) as joining_date","where user_id = :user_id",$user_sql_data);

                    if ($user_info){
                        $joining_date = $user_info['joining_date'];
                    }
                    else{
                        $return_object['errors'][] = "User info not found";
                    }

                }

                if ($start_date == $end_date){
                    $sql .= " and :date BETWEEN DATE(calendar.start_time) and DATE(calendar.end_time) ";
                    $sql_data[':date'] = $start_date;
                }
                else{
                    if ($start_date){
                        $sql .= " and date(calendar.start_time)>=:start_date ";
                        $sql_data[':start_date'] = $start_date;
                    }
                    if ($end_date){
                        $sql .= " and date(calendar.end_time)<=:end_date ";
                        $sql_data[':end_date'] = $end_date;
                    }
                }




                $columns = "calendar.*,calendar.start_time as start,calendar.end_time as end,date(calendar.start_time) as start_date";
                $finds = $this->common->retriever("calendar",$columns,$sql,$sql_data,true);
                $absent = true;
                if ($end_date < $current_date){
                    $current_date = $end_date;
                }

                if ($joining_date != '0000-00-00'){
                    if ($joining_date > $start_date){
                        $start_date = $joining_date;
                    }
                }



                $days = $this->common->date_difference($start_date,$current_date);
                if ($absent and ($type == "teacher" or $type == "student")){
                    $institute_users = $this->institute_users($institute_id,$type,$source);
                    $institute_users = $institute_users['find_data'];

                    foreach ($institute_users as $user){
                        $shift_id = $user['shift'];
                        $shift_info = $this->shift_info($shift_id,true);
                        if ($shift_info){
                            if (isset($shift_info['settings'])){
                                $settings = $shift_info['settings'];
                                $in_time = $current_date." ".$settings['in_time'].":00";
                                $late_time = $this->common->next_time(0,0,$settings['late_time'],"Y-m-d H:i:s",$in_time);
                                $absent_event_info = $this->initial_data->event_types("absent");
                                for ($i=0;$i<=$days;$i++){
                                    $next_date = $this->common->next_time($i,0,0,"Y-m-d",$start_date." "."00:00:00");

                                    // absent event by every user with not exist check
                                    $source_id = $user['user_id'];
                                    $user_type = $common->id_type_name($source_id);
                                    $filter_keys = array(
                                        "start_date" => $next_date,
                                        "source_id" => $source_id
                                    );
                                    $filter = $this->common->filter_in_array($finds,$filter_keys,false);
                                    if (!$filter){
                                        $source_name = $user['name'];
                                        $institute_name = $user['institute_name'];

                                        $new_object = array(
                                            "type" => $user_type,
                                            "source_id" => $source_id,
                                            "source_name" => $source_name,
                                            "institute_name" => $institute_name,
                                            "title" => $absent_event_info['name'],
                                            "color" => $absent_event_info['color'],
                                            "event_type" => $absent_event_info['label'],
                                            "start" => $next_date." "."00:00:00",
                                            "end" => $next_date." "."00:00:00",
                                            "time_for_event" => $next_date." "."00:00:00",
                                        );
                                        $new_object['start_date'] = $common->text_date_time("Y-m-d",$new_object['start']);
                                        if ($next_date == $current_date){
                                            $current_time = date("Y-m-d H:i:s");
                                            if ($current_time > $late_time){
                                                $finds[] = $new_object;
                                            }
                                        }
                                        else{
                                            $finds[] = $new_object;
                                        }

                                    }

                                }
                            }
                            else{
                                $return_object['errors'][] = "Undefined shift settings of ".$user['name'];
                            }
                        }
                        else{
                            $return_object['errors'][] = "Shift not set of ".$user['name'];
                        }

                    }

                }
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function make_peity_visitor_data($rows_data){
            $result = array(
                "total" => 0,
                "data" => ""
            );
            $dates = array();
            foreach ($rows_data as $item){
                $dates[] = $item['date'];
            }
            $dates = array_unique($dates);
            $order_data = array();
            foreach ($dates as $date){
                $filter_by_date = $this->common->filter_in_array($rows_data,array(
                    "date" => $date
                ));
                $order_data[] = count($filter_by_date);
            }
            if ($rows_data){
                $result['total'] = count($rows_data);
                $result['data'] = join(",",$order_data);
            }

            return $result;
        }
        function dashboard_info($dashboard_type,$designation=null,$institute_id=null,$user_id=null){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            $logged_user_id = $this->common->logged_user_id();
            if (!$designation){
                $logged_controller = $this->controller_info($logged_user_id);
                if ($logged_controller){
                    $designation = $logged_controller['designation'];
                }
            }
            $output = array(
                "total_admin" => array(
                    "total"=> 0,
                    "data" => "0,0"
                ),
                "total_teacher" => array(
                    "total"=> 0,
                    "data" => "0,0"
                ),
                "total_student" => array(
                    "total"=> 0,
                    "data" => "0,0"
                ),
                "total_institute" => array(
                    "total"=> 0,
                    "data" => "0,0"
                ),
                "total_absent" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_present" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_leave" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_training" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_late" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_early" => array(
                    "total" => 0,
                    "current" => 0
                ),
                "total_holiday" => array(
                    "total" => 0,
                    "current" => 0
                ),

                "today_attendance_status" => "",
                "pending_application" => 0,
                "sms_balance" => 0,



            );

            if ($dashboard_type == "system_admin" or $dashboard_type == "system_designer"){
                $teacher_id_digit = $common->id_type("teacher");
                $sql = "where active != :active and user_id like :user_id";
                $total_teacher = $common->retriever("controllers","id,date(time) as date",$sql,array(
                    ":active" => 2,
                    ":user_id" => $teacher_id_digit."%",
                ),true);
                $output['total_teacher'] = $this->make_peity_visitor_data($total_teacher);

                $admin_id_digit = $common->id_type("admin");
                $sql = "where active != :active and user_id like :user_id";
                $total_admin = $common->retriever("controllers","id,date(time) as date",$sql,array(
                    ":active" => 2,
                    ":user_id" => $admin_id_digit."%",
                ),true);
                $output['total_admin'] = $this->make_peity_visitor_data($total_admin);
                
                $student_id_digit = $common->id_type("student");
                $sql = "where active != :active and user_id like :user_id";
                $total_student = $common->retriever("controllers","id,date(time) as date",$sql,array(
                    ":active" => 2,
                    ":user_id" => $student_id_digit."%",
                ),true);
                $output['total_student'] = $this->make_peity_visitor_data($total_student);
                
                
                $sql = "where active != :active";
                $total_institute = $common->retriever("institutes","id,date(time) as date",$sql,array(
                    ":active" => 2,
                ),true);
                $output['total_institute'] = $this->make_peity_visitor_data($total_institute);

                /// total pending application
                $sql = "left join documents on documents.id = applications.document_row_id 
                where documents.active != :active and documents.authorization_status = :pending";
                $total_institute = $common->retriever("applications","documents.id",$sql,array(
                    ":active" => 2,
                    ":pending" => 0,
                ),true);

                $output['pending_application'] = count($total_institute);



            }
            elseif ($dashboard_type == "admin" or $dashboard_type == "head_teacher" or $dashboard_type == "head_teacher_2" or $dashboard_type == "admin_2"){
                $events_user_type = "teacher";
                if ($common->logged_user_type() == "student" or $dashboard_type == "head_teacher_2" or $dashboard_type == "admin_2"){
                    $events_user_type = "student";
                }

                $my_institutes = $this->my_institutes($designation,$institute_id);
                $institute_ids = array();
                foreach ($my_institutes['find_data'] as $institute){
                    $institute_ids[] = $institute['institute_id'];
                }
                $institute_ids = array_unique($institute_ids);
                $join_institute_ids = $common->sql_in_maker($institute_ids);
                $teacher_id_digit = $common->id_type("teacher");
                $sql = "where active != :active and user_id like :user_id and institute_id in ($join_institute_ids)";
                $total_teacher = $common->retriever("controllers","id,date(time) as date",$sql,array(
                    ":active" => 2,
                    ":user_id" => $teacher_id_digit."%",
                ),true);
                $output['total_teacher'] = $this->make_peity_visitor_data($total_teacher);

                $student_id_digit = $common->id_type("student");
                $sql = "where active != :active and user_id like :user_id and institute_id in($join_institute_ids)";
                $total_student = $common->retriever("controllers","id,date(time) as date",$sql,array(
                    ":active" => 2,
                    ":user_id" => $student_id_digit."%",
                ),true);
                $output['total_student'] = $this->make_peity_visitor_data($total_student);

                $sql = "where active != :active and institute_id in($join_institute_ids)";
                $total_institute = $common->retriever("institutes","id,date(time) as date",$sql,array(
                    ":active" => 2,
                ),true);
                $output['total_institute'] = $this->make_peity_visitor_data($total_institute);

                $current_date = date("Y-m-d");
                $total_teacher = count($total_teacher);

                $events_of_institutes = $this->events_of_institutes($institute_ids,$current_date,$current_date,array(),$events_user_type);
                $events_of_institutes = $events_of_institutes['find_data'];
                /// present info
                $filter_present = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "present"
                ));

                $output['total_present']['total'] = $total_teacher;
                $output['total_present']['current'] = count($filter_present);
                
                /// absent info
                $filter_absent = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "absent"
                ));
                $output['total_absent']['total'] = $total_teacher;
                $output['total_absent']['current'] = count($filter_absent);
                
                /// training info
                $filter_training = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "training"
                ));
                $output['total_training']['total'] = $total_teacher;
                $output['total_training']['current'] = count($filter_training);

                /// leave info
                $filter_leave = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "leave"
                ));
                $output['total_leave']['total'] = $total_teacher;
                $output['total_leave']['current'] = count($filter_leave);
                
                /// late info
                $filter_late = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "late"
                ));
                $output['total_late']['total'] = $total_teacher;
                $output['total_late']['current'] = count($filter_late);

                /// early info
                $filter_early = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "early"
                ));
                $output['total_early']['total'] = $total_teacher;
                $output['total_early']['current'] = count($filter_early);

                /// total pending application
                $sql = "left join documents on documents.id = applications.document_row_id 
                where documents.active != :active and documents.authorization_status = :pending and 
                applications.institute_id in ($join_institute_ids)";
                $total_institute = $common->retriever("applications","documents.id",$sql,array(
                    ":active" => 2,
                    ":pending" => 0,
                ),true);

                $output['pending_application'] = count($total_institute);


                if ($dashboard_type == "head_teacher"  or $dashboard_type == "head_teacher_2" ){
                    $logged_controller = $common->retriever('controllers','institute_id',"where user_id = :user_id",array(
                        ":user_id" => $common->logged_user_id()
                    ));

                    if (!$institute_id){
                        $institute_id = $logged_controller['institute_id'] ?? '';
                    }
                    $institute = $common->retriever('institutes','sms_balance',"where institute_id = :institute_id",array(
                        ":institute_id" => $institute_id
                    ));
                    $output['sms_balance'] = $institute['sms_balance'] ?? 0;
                }
                if ($dashboard_type == "admin"  or $dashboard_type == "admin_2" ){
                    $logged_controller = $common->retriever('controllers','sms_balance',"where user_id = :user_id",array(
                        ":user_id" => $common->logged_user_id()
                    ));


                    $output['sms_balance'] = $logged_controller['sms_balance'] ?? 0;
                }

                

            }
            elseif ($dashboard_type == "student" or $dashboard_type == "teacher"){
                $my_institutes = $this->my_institutes($designation,$institute_id,$user_id);

                $institute_ids = array();
                foreach ($my_institutes['find_data'] as $institute){
                    $institute_ids[] = $institute['institute_id'];
                }
                $institute_ids = array_unique($institute_ids);
                $join_institute_ids = $common->sql_in_maker($institute_ids);

                $current_date = date("Y-m-d");
                $start_date = date("Y")."-01-01";
                $events_of_institutes = $this->events_of_institutes($institute_ids,$start_date,$current_date,null,$dashboard_type,$user_id);
                $events_of_institutes = $events_of_institutes['find_data'];
                /// today attendance status
                $today_filter = $common->filter_in_array($events_of_institutes,array(
                    "source_id" => $user_id,
                    "event_type" => "present",
                    "start_date" => $current_date
                ),false);
                if ($today_filter){
                    $present_status_info = $this->initial_data->event_types("present");
                    $output['today_attendance_status'] = $present_status_info['name'];
                }
                else{
                    $absent_status_info = $this->initial_data->event_types("absent");
                    $output['today_attendance_status'] = $absent_status_info['name'];
                }

                /// present info
                $filter_absent = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "present"
                ));
                $output['total_present']['total'] = 0;
                $output['total_present']['current'] = count($filter_absent);

                /// absent info
                $filter_absent = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "absent"
                ));
                $output['total_absent']['total'] = 0;
                $output['total_absent']['current'] = count($filter_absent);

                /// late info
                $filter_late = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "late"
                ));
                $output['total_late']['total'] = 0;
                $output['total_late']['current'] = count($filter_late);

                /// early info
                $filter_early = $common->filter_in_array($events_of_institutes,array(
                    "event_type" => "early"
                ));
                $output['total_early']['total'] = 0;
                $output['total_early']['current'] = count($filter_early);
                
                /// holiday info
//                $filter_holiday = $common->filter_in_array($events_of_institutes,array(
//                    "event_type" => "holiday"
//                ));
//                $output['total_holiday']['total'] = 0;
//                $output['total_holiday']['current'] = count($filter_holiday);
                


            }


            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
                $return_object['output'] = $output;
            }

            return $return_object;

        }

        function events_of_institutes($institute_ids,$start_date="",$end_date="",$exclude_event_types=null,$events_user_type = "teacher",$user_id = null,$data_by_every_date=true){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            try{
                if (gettype($institute_ids) != "array"){
                    $institute_ids = explode(",",$institute_ids);
                }
                if (gettype($exclude_event_types) != "array"){
                    $exclude_event_types = explode(",",$exclude_event_types);
                }


                $user_ids = array();
                $join_institute_ids = $this->common->sql_in_maker($institute_ids);
                $institutes_users = array();

                if ($events_user_type == "teacher"){
                    if ($common->logged_user_type() == "teacher"){
                        if (!$user_id){
                            $user_id = $common->logged_user_id();
                        }
                        $teacher_info = $this->teacher_info($user_id);
                        if ($teacher_info){
                            if (strtolower($teacher_info['designation']) == "head teacher"){
                                $user_id = null;
                            }
                        }
                        else{
                            $return_object['errors'][] = "Teacher info not found";
                            return $return_object;
                        }
                    }
                    $institutes_users = $this->institutes_teachers($institute_ids,$user_id);
                }
                else if ($events_user_type == "student"){

                    if ($common->logged_user_type() == "student"){
                        $user_id = $common->logged_user_id();
                    }

                    $institutes_users = $this->institutes_students($institute_ids,$user_id);
                }
                else{
                    $return_object['errors'][] = "Unknown user type";
                    return $return_object;
                }

                $institutes_users = $institutes_users['find_data'];
                $institutes = $this->common->retriever("institutes","*","where institute_id in($join_institute_ids)",array(),true);

                foreach ($institutes_users as $user){
                    $user_ids[] = $user['user_id'];
                }
                $join_sources = $this->common->sql_in_maker(array_unique(array_merge($institute_ids,$user_ids)));


                $sql = "left join controllers on controllers.user_id = calendar.source_id 
                left join institutes on institutes.institute_id = controllers.institute_id 
                where (calendar.type = :default_type or calendar.source_id in($join_sources)) and  calendar.active != :active 
                   ";

                $sql_data = array(
                    ":active" => 2,
                    ":default_type" => "default",
                );
                $current_date = date("Y-m-d");
                if (!$end_date){
                    $end_date = $current_date;
                }
                if (!$start_date){
                    if ($events_user_type == "student" or $events_user_type == "teacher"){
                        /// get first start date from user joining date
                        $user_info_sql_data = array(
                            ":user_id" => $common->logged_user_id()
                        );
                        if ($user_id){
                            $user_info_sql_data[':user_id'] = $user_id;
                        }
                        $user_info = $common->retriever("controllers","date(time) as joining_date","where user_id = :user_id",$user_info_sql_data);

                        if ($user_info){
                            $start_date = $user_info['joining_date'];
                        }
                        else{
                            $return_object['errors'][] = "Student info not found";
                            return $return_object;
                        }
                    }
                    else{
                        $start_date = $current_date;
                    }

                }

                if ($start_date == $end_date){
                    $sql .= " and :date BETWEEN DATE(calendar.start_time) and DATE(calendar.end_time) ";
                    $sql_data[':date'] = $start_date;
                }
                else{
                    if ($start_date){
                        $sql .= " and date(calendar.start_time)>=:start_date ";
                        $sql_data[':start_date'] = $start_date;
                    }
                    if ($end_date){
                        $sql .= " and date(calendar.end_time)<=:end_date ";
                        $sql_data[':end_date'] = $end_date;
                    }
                }


                if ($exclude_event_types){
                    $join_exclude_event_types = $common->sql_in_maker($exclude_event_types);
                    $sql .= " and calendar.event_type not in ($join_exclude_event_types) ";
                }

                $columns = "calendar.title,calendar.color,calendar.type,calendar.event_type,calendar.start_time as start,
                calendar.end_time as end,date(calendar.start_time) as start_date,date(calendar.end_time) as end_date,calendar.source_id,
                controllers.name as source_name,institutes.name as institute_name,calendar.time_for_event";
                $finds = $this->common->retriever("calendar",$columns,$sql,$sql_data,true);

//

                $absent = true;
                if ($end_date < $current_date){
                    $current_date = $end_date;
                }

                $days_diff = $this->common->date_difference($start_date,$current_date);

                if ($data_by_every_date){
                    /// make data by indivisual date
                    $student_data = $common->filter_in_array($finds,array(
                        "type" => "student"
                    ),true,false,true);

                    foreach ($student_data as $index => $event_info) {
                        $event_start_date = $event_info['start_date'];
                        $event_end_date = $event_info['end_date'];
                        $days = $this->common->date_difference($event_start_date, $event_end_date);
                        for ($i = 0; $i <= $days; $i++) {
                            $next_date = $this->common->next_time($i, 0, 0, "Y-m-d", $event_start_date . " " . "00:00:00");
                            if ($next_date <= $end_date and $next_date >= $start_date){
                                $event_info['start_date'] = $next_date;
                                $event_info['end_date'] = $next_date;
                                $finds[] = $event_info;
                            }

                        }
                        unset($finds[$index]);
                    }

                    $teacher_data = $common->filter_in_array($finds,array(
                        "type" => "teacher"
                    ),true,false,true);

                    foreach ($teacher_data as $index => $event_info) {
                        $event_start_date = $event_info['start_date'];
                        $event_end_date = $event_info['end_date'];
                        $days = $this->common->date_difference($event_start_date, $event_end_date);
                        for ($i = 0; $i <= $days; $i++) {
                            $next_date = $this->common->next_time($i, 0, 0, "Y-m-d", $event_start_date . " " . "00:00:00");
                            if ($next_date <= $end_date and $next_date >= $start_date){
                                $event_info['start_date'] = $next_date;
                                $event_info['end_date'] = $next_date;
                                $finds[] = $event_info;
                            }
                        }
                        unset($finds[$index]);
                    }


                    //// remove default/institute type event because default event assign by cron file
                    $filter_default_data = $common->filter_in_array($finds,array(
                        "type" => "default"
                    ),true,false,true);

                    foreach ($filter_default_data as $index => $event_info){
                        unset($finds[$index]);
                    }

                    $filter_institute_data = $common->filter_in_array($finds,array(
                        "type" => "institute"
                    ),true,false,true);

                    foreach ($filter_institute_data as $index => $event_info){
                        unset($finds[$index]);
                    }

                    $finds = array_values($finds);

                }


                if ($absent){
                    $shifts = $this->shifts(true);
                    $shifts = $shifts['find_data'];

                    foreach ($institute_ids as $institute_id){

                        /// get this institute users
                        $institute_users_filter_keys = array(
                            "institute_id" => $institute_id
                        );
                        $institute_users = $common->filter_in_array($institutes_users,$institute_users_filter_keys);

                        foreach ($institute_users as $user){
                            $shift_id = $user['shift'];
                            $shift_info = $common->filter_in_array($shifts,array(
                                "id" => $shift_id
                            ),false);
                            if ($shift_info){
                                if (isset($shift_info['settings'])){
                                    $settings = $shift_info['settings'];
                                    $in_time = $current_date." ".$settings['in_time'].":00";
                                    $late_time = $this->common->next_time(0,0,$settings['late_time'],"Y-m-d H:i:s",$in_time);
                                    $absent_event_info = $this->initial_data->event_types("absent");

                                    for ($i=0;$i<=$days_diff;$i++){
                                        $next_date = $this->common->next_time($i,0,0,"Y-m-d",$start_date." "."00:00:00");
                                        if ($next_date <= $end_date and $next_date >= $start_date){
//                                            echo $i."<br>";
                                            // absent event by every user with not exist check
                                            $source_id = $user['user_id'];
                                            $user_type = $common->id_type_name($source_id);
                                            $filter_keys = array(
                                                "start_date" => $next_date,
                                                "source_id" => $source_id
                                            );

                                            $filter = $this->common->filter_in_array($finds,$filter_keys,false);
                                            if (!$filter){
                                                $source_name = $user['name'];
                                                $institute_name = "";
                                                $filter_institute = $common->filter_in_array($institutes,array(
                                                    "institute_id" => $institute_id
                                                ),false);
                                                if ($filter_institute){
                                                    $institute_name = $filter_institute['name'];
                                                }
                                                $new_object = array(
                                                    "type" => $user_type,
                                                    "source_id" => $source_id,
                                                    "source_name" => $source_name,
                                                    "institute_name" => $institute_name,
                                                    "title" => $absent_event_info['name'],
                                                    "color" => $absent_event_info['color'],
                                                    "event_type" => $absent_event_info['label'],
                                                    "start" => $next_date." "."00:00:00",
                                                    "end" => $next_date." "."00:00:00",
                                                    "time_for_event" => $next_date." "."00:00:00",
                                                );
                                                $new_object['start_date'] = $common->text_date_time("Y-m-d",$new_object['start']);
                                                $new_object['end_date'] = $common->text_date_time("Y-m-d",$new_object['start']);
                                                if ($next_date == $current_date){
                                                    $current_time = date("Y-m-d H:i:s");
                                                    if ($current_time > $late_time){
                                                        $finds[] = $new_object;
                                                    }
                                                }
                                                else{
                                                    $finds[] = $new_object;
                                                }

                                            }
                                        }


                                    }
                                }
                                else{
                                    $return_object['errors'][] = "Undefined shift settings of ".$user['name'];
                                }
                            }
                            else{
                                $return_object['errors'][] = "Shift not set of ".$user['name'];
                            }

                        }


                    }

                }







                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function absent_attendance_data($institute_ids,$start_date="",$end_date="",$events_user_type = "teacher",$user_id = null){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $events = $this->events_of_institutes($institute_ids,$start_date,$end_date,null,$events_user_type,$user_id);
            if (isset($events['find_data'])){
                $events = $this->common->filter_in_array($events['find_data'],array(
                    "event_type" => "absent"
                ));
                $return_object['find_data'] = $events;
            }
            $return_object['status'] = 1;
            return $return_object;
        }
        function monthly_events_of_institute($institute_id,$year,$month,$event_user_type = "teacher",$user_id=null){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $start_date = $common->first_date_of_date($year."-".$month."-"."01");
            $end_date = $common->last_date_of_date($year."-".$month."-"."01");
//            echo $institute_id;
            $events_by_institute = $this->events_of_institutes($institute_id,$start_date,$end_date,array(),$event_user_type,$user_id);

            $events_by_institute = $events_by_institute['find_data'];

            $institute_users = array();
            if ($event_user_type == "teacher"){
                $institute_users = $this->institute_teachers($institute_id,$user_id);
            }
            else{
                if($common->logged_user_type() == "student"){
                    $user_id = $common->logged_user_id();
                }
                $institute_users = $this->institute_students($institute_id,$user_id);
            }


            $institute_users = $institute_users['find_data'];
            $days_of_month = $common->text_date_time("t",$start_date." 00:00:00");

            $user_title = "Teacher";
            if ($event_user_type == "student"){
                $user_title = "Student";
            }
            $headers = array(
                array(
                    "label" => "name",
                    "name" => "$user_title name"
                ),
                array(
                    "label" => "institute_name",
                    "name" => "Institute_name"
                ),

            );
            for ($i = 1; $i <= $days_of_month;$i++){
                $headers[] = array(
                    "label" => $i,
                    "name" => $i
                );
            }
            $headers[] = array(
                "label" => "total_symbols",
                "name" => "Total"
            );

            $events_data = array();
            foreach ($institute_users as $user){
                $user_data = array(
                    "name" => $user['name'],
                    "institute_name" => $user['institute_name'],
                );
                $user_all_event_symbols = array();
                for($i = 1; $i <= $days_of_month; $i++){
                    $this_date = $year."-".$month."-".str_pad($i, 2, '0', STR_PAD_LEFT);

                    $filter_events = $common->filter_in_array($events_by_institute,array(
                        "source_id" => $user['user_id'],
                        "start_date" => $this_date,
                        "end_date" => $this_date,

                    ));

//                    print_r($filter_events);

                    $this_events = array();
                    $this_events_symbol = array();
                    $this_event_data = array();
                    foreach ($filter_events as $index => $event){
                        if (!$index){
                            $this_event_data = $event;
                        }
                        $event_info = $this->initial_data->event_types($event['event_type']);
                        if ($event_info){
                            $this_events[] = $event_info['name'];
                            $this_events_symbol[] = $event_info['short_name'];
                        }
                        else{
                            $this_events[] = $event['event_type'];
                        }

                    }
                    $event_names = join(", ",$this_events);
                    $this_events['event_names'] = $event_names;
                    $event_symbols = join(",",$this_events_symbol);;
                    $this_events['event_symbols'] = $event_symbols;
                    $user_data[$i] = $event_symbols;
                    $user_all_event_symbols = array_merge($user_all_event_symbols,$this_events_symbol);
                }
                $unique_event_symbols = array_unique($user_all_event_symbols);
                $total_array = array();
                foreach ($unique_event_symbols as $symbol){
                    $total_symbol = 0;
                    foreach ($user_all_event_symbols as $s){
                        if ($symbol == $s){
                            $total_symbol += 1;
                        }
                    }
                    $total_array[] = $symbol."($total_symbol)";
                }
                $user_data['total_symbols'] = join(", ",$total_array);
                $events_data[] = $user_data;

            }
            $return_object['headers'] = $headers;
            $return_object['find_data'] = $events_data;
            $return_object['days_of_month'] = $days_of_month;
            $return_object['date_string'] = $common->text_date_time("M Y");
            $return_object['institute_info'] = $this->institute_info($institute_id);

            return $return_object;
        }

        function estimate_applicable_institutes($keyword="",$designation=""){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $logged_user_type = $this->common->logged_user_type();
                $sql = " where institutes.active != :active and 
                (
                institutes.member_id like :member_keyword or 
                institutes.name like :name_keyword 
                ) 
                limit 10";
                $sql_data = array(
                    ":active" => 2,
                    ":member_keyword" => "%".$keyword."%",
                    ":name_keyword" => "%".$keyword."%",
                );

                $join = "";

                if ($logged_user_type == "admin"){
                    $join .= $this->common->admin_institutes_join_sql($designation);
                }

                $columns = "institutes.*";
                $finds = $this->common->retriever("institutes ".$join,$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function my_institutes($designation="",$institute_id="",$user_id = ""){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $logged_user_id = $this->common->logged_user_id();
                if ($user_id){
                    $logged_user_id = $user_id;
                }
                $logged_user_type = $this->common->logged_user_type();
                $sql = " where institutes.active != :active ";
                $sql_data = array(
                    ":active" => 2,
                );

                $join = "";

                if ($logged_user_type == "admin"){
                    $join .= $this->common->admin_institutes_join_sql($designation);
                    $sql .= " and controllers.user_id = :user_id ";
                    $sql_data[':user_id'] = $logged_user_id;
                }
                elseif ($logged_user_type == "teacher"){
                    $sql .= " and institutes.institute_id = :institute_id";
                    $sql_data[':institute_id'] = $institute_id;
                }

                if ($user_id and $logged_user_type != "admin"){
                    $join .= " left join controllers on controllers.institute_id = institutes.institute_id ";
                    $sql .= " and controllers.user_id = :user_id ";
                    $sql_data[':user_id'] = $logged_user_id;
                }


                $columns = "institutes.*";
                $finds = $this->common->retriever("institutes ".$join,$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }

        function my_area_by_type($designation,$type,$user_id = ""){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $logged_user_id = $this->common->logged_user_id();
                if ($user_id){
                    $logged_user_id = $user_id;
                }


                $logged_user_type = $this->common->logged_user_type();
                $sql = " where areas.active != :active";
                $sql_data = array(
                    ":active" => 2,
                );

                $join = "";

                if ($logged_user_type == "admin"){
                    $join_sql = "";
                    $admin_type_info = $this->initial_data->admin_types($designation);
                    if ($admin_type_info){
                        $available_area = $admin_type_info['area'];
                        $parent = "institute";
                        $join_sql .= " left join areas as $parent on $parent.id = areas.id and $parent.active = '1'";

                        foreach ($available_area as $index => $area){
                            $join_sql .= " left join areas as $area on $area.id = $parent.parent and $area.active = '1'";
                            $parent = $area;
                            if ($area == $type){
                                break;
                            }
                        }
                        $join_sql .= " left join controllers on controllers.area = $parent.id ";

                    }
                    $join .= $join_sql;
                    $sql .= " and controllers.user_id = :user_id ";
                    $sql_data[':user_id'] = $logged_user_id;
                }

                $columns = "areas.*";
                $finds = $this->common->retriever("areas ".$join,$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }

        function institute_teachers($institute_id,$user_id = ""){
            return $this->institute_users($institute_id,"teacher",$user_id);
        }
        function institute_students($institute_id,$user_id = ""){
            return $this->institute_users($institute_id,"student",$user_id);
        }
        function institute_users($institute_id,$user_type = "teacher",$user_id = ""){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $teacher_id_digit = $this->common->id_type($user_type);
                $sql = "left join tags on tags.id = controllers.designation 
                left join institutes on institutes.institute_id = controllers.institute_id 
                 where controllers.active != :active and institutes.institute_id = :institute_id and controllers.user_id like :user_id";
                $sql_data = array(
                    ":active" => 2,
                    ":institute_id" => $institute_id,
                    ":user_id" => $teacher_id_digit."%",
                );
                if ($user_id){
                    $sql .= " and controllers.user_id = :this_user_id ";
                    $sql_data[':this_user_id'] = $user_id;
                }

                $columns = "controllers.name,controllers.member_id,controllers.user_id,tags.tag as designation,controllers.image,
                controllers.id,institutes.name as institute_name,controllers.shift";
                $finds = $this->common->retriever("controllers",$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }


        function manual_attendance($institute_id,$date){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $sql = "where institute_id = :institute_id and date = :date";
                $sql_data = array(
                    ":date" => $date,
                    ":institute_id" => $institute_id,
                );

                $columns = "*";
                $finds = $this->common->retriever("manual_attendance",$columns,$sql,$sql_data,true);
                $return_object['find_data'] = $finds;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }

        function institutes_users($institute_ids,$user_type="teacher",$user_id = ""){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $teacher_id_digit = $this->common->id_type($user_type);
                if (gettype($institute_ids) != "array"){
                    $institute_ids = explode(",",$institute_ids);
                }

                $join_institute_ids = $this->common->sql_in_maker($institute_ids);
                $institutes_users_sql = "left join tags on tags.id = controllers.designation   
                where controllers.institute_id in($join_institute_ids) and controllers.active != :active and 
                controllers.user_id like :user_id";
                $institutes_users_sql_data = array(
                    ":active" => 2,
                    ":user_id" => $teacher_id_digit."%",
                );
                if ($user_id){
                    $institutes_users_sql .= " and controllers.user_id = :this_user_id ";
                    $institutes_users_sql_data[':this_user_id'] = $user_id;
                }
                $columns = "controllers.user_id,controllers.name,controllers.member_id,controllers.device_user_id,
                tags.tag as designation,controllers.institute_id,controllers.image,controllers.shift";
                $institutes_users = $this->common->retriever("controllers",$columns,$institutes_users_sql,$institutes_users_sql_data,true);

                $return_object['find_data'] = $institutes_users;
                $return_object['status'] = 1;
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }
        function institutes_teachers($institute_ids,$user_id = ""){
            return $this->institutes_users($institute_ids,"teacher",$user_id);
        }
        function institutes_students($institute_ids,$user_id = ""){
            return $this->institutes_users($institute_ids,"student",$user_id);
        }












    }
