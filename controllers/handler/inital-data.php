<?php

    require_once(project_root."controllers/packages/vendor/autoload.php");
    class initial_data{
        function __construct()
        {

        }
        function menus(){
            $list = array(
                array(
                    "id" => 12,
                    "menu_name" => "Teacher dashboard",
                    "parent_id" => 0,
                    "link" => "home",
                    "icon" => "<i class='material-icons'>dashboard</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 1,
                    "teacher" => 1,
                    "student" => 0,
                ),
                array(
                    "id" => 23,
                    "menu_name" => "Dashboard",
                    "parent_id" => 0,
                    "link" => "home",
                    "icon" => "<i class='material-icons'>dashboard</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                    "student" => 1,
                ),
                array(
                    "id" => 22,
                    "menu_name" => "Student dashboard",
                    "parent_id" => 0,
                    "link" => "home/2",
                    "icon" => "<i class='material-icons'>web_asset</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 1,
                    "teacher" => 1,
                    "student" => 0,
                ),
                array(
                    "id" => 1,
                    "menu_name" => "Settings",
                    "parent_id" => 0,
                    "link" => "",
                    "icon" => "<i class='material-icons'>settings_applications</i>",
                    "active" => 1,
                    "menu_type" => "multiple",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 1,
                    "teacher" => 1,
                    "student" => 1,

                ),
                array(
                    "id" => 2,
                    "menu_name" => "List",
                    "parent_id" => 0,
                    "link" => "",
                    "icon" => "<i class='material-icons'>list</i>",
                    "active" => 1,
                    "menu_type" => "multiple",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 1,
                    "teacher" => 1,
                    "student" => 1,
                ),
                array(
                    "id" => 3,
                    "menu_name" => "Add system designer",
                    "parent_id" => 1,
                    "link" => "add-controller/system_designer",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "admin" => 0,
                    "promoter" => 0,
                    "worker" => 0,
                    "system_designer" => 1
                ),
                array(
                    "id" => 4,
                    "menu_name" => "Add system admin",
                    "parent_id" => 1,
                    "link" => "add-controller/system_admin",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 5,
                    "menu_name" => "Add admin",
                    "parent_id" => 1,
                    "link" => "add-controller/admin",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 1,
                    "teacher" => 0,
                ),

                array(
                    "id" => 6,
                    "menu_name" => "System admin's",
                    "parent_id" => 2,
                    "link" => "handlers/System admin",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 7,
                    "menu_name" => "System designers",
                    "parent_id" => 2,
                    "link" => "handlers/System designer",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 11,
                    "menu_name" => "Admin's",
                    "parent_id" => 2,
                    "link" => "handlers/admin",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 1,
                    "teacher" => 0,
                ),



                array(
                    "id" => 8,
                    "menu_name" => "Feature settings",
                    "parent_id" => 1,
                    "link" => "settings",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 0,
                ),

                array(
                    "id" => 9,
                    "menu_name" => "Area manager",
                    "parent_id" => 1,
                    "link" => "area-manager",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 10,
                    "menu_name" => "Schools",
                    "parent_id" => 2,
                    "link" => "institutes",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 24,
                    "menu_name" => "Devices",
                    "parent_id" => 2,
                    "link" => "devices",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),

                //// other user not teacher below menu
                array(
                    "id" => 11,
                    "menu_name" => "Application",
                    "parent_id" => 1,
                    "link" => "application",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 1,
                    "teacher" => 0,
                ),
                array(
                    "id" => 13,
                    "menu_name" => "Application",
                    "parent_id" => 0,
                    "link" => "application",
                    "icon" => "<i class='material-icons'>email</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 1,
                ),
                array(
                    "id" => 14,
                    "menu_name" => "School",
                    "parent_id" => 0,
                    "link" => "school",
                    "icon" => "<i class='material-icons'>business</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 1,
                ),
                array(
                    "id" => 15,
                    "menu_name" => "Attendance",
                    "parent_id" => 0,
                    "link" => "attendance",
                    "icon" => "<i class='material-icons'>assignment_turned_in</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),

                array(
                    "id" => 16,
                    "menu_name" => "Notice",
                    "parent_id" => 1,
                    "link" => "notice#mailbox_new_message",
                    "icon" => "<i class='material-icons'>email</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 1,
                    "teacher" => 0,
                ),
                array(
                    "id" => 17,
                    "menu_name" => "Notices",
                    "parent_id" => 0,
                    "link" => "notices",
                    "icon" => "<i class='material-icons'>email</i>",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 1,
                ),
                array(
                    "id" => 18,
                    "menu_name" => "Initiate",
                    "parent_id" => 1,
                    "link" => "initiate",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 0,
                ),
//                array(
//                    "id" => 19,
//                    "menu_name" => "Create student notice",
//                    "parent_id" => 0,
//                    "link" => "attendance",
//                    "icon" => "<i class='material-icons'>assignment_turned_in</i>",
//                    "active" => 1,
//                    "menu_type" => "single",
//                    "system_designer" => 0,
//                    "system_admin" => 0,
//                    "admin" => 0,
//                    "teacher" => 1,
//                ),
                array(
                    "id" => 20,
                    "menu_name" => "Add shift",
                    "parent_id" => 1,
                    "link" => "add-shift",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 21,
                    "menu_name" => "shifts",
                    "parent_id" => 2,
                    "link" => "shifts",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 25,
                    "menu_name" => "Cron job",
                    "parent_id" => 1,
                    "link" => "http://edukitgps.com/cron-job",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 1,
                    "admin" => 0,
                    "teacher" => 0,
                ),
                array(
                    "id" => 26,
                    "menu_name" => "SMS",
                    "parent_id" => 0,
                    "link" => "",
                    "icon" => "<i class='material-icons'>textsms</i>",
                    "active" => 1,
                    "menu_type" => "multiple",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 1,
                    "teacher" => 1,
                    "student" => 0,
                ),
                array(
                    "id" => 27,
                    "menu_name" => "Send SMS",
                    "parent_id" => 26,
                    "link" => "send-bulk-sms",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 0,
                    "system_admin" => 0,
                    "admin" => 1,
                    "teacher" => 1,
                ),
                array(
                    "id" => 28,
                    "menu_name" => "Android SMS",
                    "parent_id" => 26,
                    "link" => "send-sms-test",
                    "icon" => "",
                    "active" => 1,
                    "menu_type" => "single",
                    "system_designer" => 1,
                    "system_admin" => 0,
                    "admin" => 0,
                    "teacher" => 0,
                ),




            );
            return $list;
        }

        function admin_permission_parts(){
            $data = array(
                array(
                    "name" => "School delete",
                    "id" => 1,
                    "label" => "school_delete"
                ),

            );
            return $data;
        }

        function admin_types($type=""){
            $data = array(
                array(
                    "name" => "ATEO",
                    "label" => "ATEO",
                    "options" => array(),
                    "area" => array("cluster")
                ),
                array(
                    "name" => "TEO",
                    "label" => "TEO",
                    "options" => array("ATEO"),
                    "area" => array("cluster","thana")
                ),
                array(
                    "name" => "ADPEO",
                    "label" => "ADPEO",
                    "options" => array("ATEO,TEO"),
                    "area" => array("cluster","thana","upazilla")
                ),
                array(
                    "name" => "DPEO",
                    "label" => "DPEO",
                    "options" => array("ATEO,TEO,ADPEO"),
                    "area" => array("cluster","thana","upazilla","zilla")
                ),
                array(
                    "name" => "DD",
                    "label" => "DD",
                    "options" => array("ATEO,TEO,ADPEO,DPEO"),
                    "area" => array("cluster","thana","upazilla","zilla","division")
                ),
                array(
                    "name" => "CMO",
                    "label" => "CMO",
                    "options" => array("ATEO,TEO,ADPEO,DPEO,DD"),
                    "area" => array("cluster","thana","upazilla","zilla","division","country")
                )
            );
            if ($type){
                $keys = array(
                    "label" => $type
                );
                $filter = $this->filter_in_array($data,$keys,false);
                return $filter;
            }
            else{
                return $data;
            }
        }

        function event_types($type=""){
            $data = array(
                array(
                    "name" => "Holiday",
                    "label" => "holiday",
                    "color" => "#b3676a",
                    "calendar" => 1,
                    "short_name" => "H"
                ),
                array(
                    "name" => "Leave",
                    "label" => "leave",
                    "color" => "#ffcc66",
                    "calendar" => 0,
                    "short_name" => "Le"
                ),
                array(
                    "name" => "Training",
                    "label" => "training",
                    "color" => "#006666",
                    "calendar" => 0,
                    "short_name" => "T"
                ),
                array(
                    "name" => "Present",
                    "label" => "present",
                    "color" => "#339966",
                    "calendar" => 0,
                    "short_name" => "P"
                ),
                array(
                    "name" => "Absent",
                    "label" => "absent",
                    "color" => "#990000",
                    "calendar" => 0,
                    "short_name" => "A"
                ),
                array(
                    "name" => "Late",
                    "label" => "late",
                    "color" => "#3399ff",
                    "calendar" => 0,
                    "short_name" => "L"
                ),
                array(
                    "name" => "Early",
                    "label" => "early",
                    "color" => "#0033cc",
                    "calendar" => 0,
                    "short_name" => "E"
                ),
                array(
                    "name" => "Late and Early",
                    "label" => "late_and_early",
                    "color" => "#0044cc",
                    "calendar" => 0,
                    "short_name" => "LE"
                ),





            );
            if ($type){
                $keys = array(
                    "label" => $type
                );
                $filter = $this->filter_in_array($data,$keys,false);
                return $filter;
            }
            else{
                return $data;
            }
        }

        function admin_allow($type){
            $result = [];
            $admin_type = $this->admin_types($type);
            if ($admin_type){
                $result = $admin_type['options'];
            }
            return $result;
        }
        function document_types($type=""){
            $data = array(
                array(
                    "name" => "Application",
                    "label" => "application",
                    "options" => array(
                        array(
                            "name" => "Leave",
                            "label" => "leave"
                        ),
                        array(
                            "name" => "Training",
                            "label" => "training"
                        ),

                    )
                ),



            );
            if ($type){
                $keys = array(
                    "label" => $type
                );
                $filter = $this->filter_in_array($data,$keys,false);
                return $filter;
            }
            else{
                return $data;
            }
        }
        function document_type_option_name($document_type,$type_option){
            $result = "";
            $document_type_info = $this->document_types($document_type);
            if ($document_type_info){
                $options = $document_type_info['options'];
                $keys = array(
                    "label"=> $type_option
                );
                $option_info = $this->filter_in_array($options,$keys,false);
                if ($option_info){
                    $result = $option_info['name'];
                }
            }
            return $result;
        }
        function document_type_name($document_type){
            $result = "";
            $document_type_info = $this->document_types($document_type);
            if ($document_type_info){
                $name = $document_type_info['name'];
                $result = $name;
            }
            return $result;
        }



        function type_of_activities($type=""){
            $data = array(

                array(
                    "name" => "Application",
                    "label" => "application",
                    "icon" => "<i class='material-icons'>assignment</i>",
                ),

                array(
                    "name" => "Add admin",
                    "label" => "add_admin",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit admin",
                    "label" => "edit_admin",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Add system designer",
                    "label" => "add_system_designer",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit system designer",
                    "label" => "edit_system_designer",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Add system admin",
                    "label" => "add_system_admin",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit system admin",
                    "label" => "edit_system_admin",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Add teacher",
                    "label" => "add_teacher",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit teacher",
                    "label" => "edit_teacher",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Add student",
                    "label" => "add_student",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit student",
                    "label" => "edit_student",
                    "icon" => "<i class='material-icons'>create</i>",
                ),


                array(
                    "name" => "Add institute",
                    "label" => "add_institute",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Add shift",
                    "label" => "add_shift",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),

                array(
                    "name" => "Edit institute",
                    "label" => "edit_institute",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Edit shift",
                    "label" => "edit_shift",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Shift initiate",
                    "label" => "shift_initiate",
                    "icon" => "<i class='material-icons'>create</i>",
                ),


                array(
                    "name" => "Manual attendance",
                    "label" => "manual_attendance",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Send notice",
                    "label" => "send_notice",
                    "icon" => "<i class='material-icons'>email</i>",
                ),







                array(
                    "name" => "Change software title",
                    "label" => "change_software_title",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software currency",
                    "label" => "change_software_currency",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software time zone",
                    "label" => "change_software_time_zone",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software logo",
                    "label" => "change_software_logo",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software favicon",
                    "label" => "change_software_favicon",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software login banner",
                    "label" => "change_software_login_banner",
                    "icon" => "<i class='material-icons'>create</i>",
                ),

                array(
                    "name" => "Change construction mode",
                    "label" => "change_software_construction_mode",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software email",
                    "label" => "change_software_email",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software company name",
                    "label" => "change_software_company_name",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change software HTTPS mode",
                    "label" => "change_software_https",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change absence person a day",
                    "label" => "change_absence_person_a_day",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change in time",
                    "label" => "change_in_time",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change out time",
                    "label" => "change_out_time",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change late time",
                    "label" => "change_late_time",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Change early time",
                    "label" => "change_early_time",
                    "icon" => "<i class='material-icons'>create</i>",
                ),


                array(
                    "name" => "Delete an admin",
                    "label" => "delete_admin",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete a system designer",
                    "label" => "delete_system_designer",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete a system admin",
                    "label" => "delete_system_admin",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete a teacher",
                    "label" => "delete_teacher",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete a student",
                    "label" => "delete_student",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),


                array(
                    "name" => "Delete an application",
                    "label" => "delete_application",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete leave",
                    "label" => "delete_leave",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),
                array(
                    "name" => "Delete training",
                    "label" => "delete_training",
                    "icon" => "<i class='material-icons'>delete_forever</i>",
                ),











                array(
                    "name" => "Application authorize",
                    "label" => "application_authorize",
                    "icon" => "<i class='material-icons'>gavel</i>",
                ),

                array(
                    "name" => "Add calendar event",
                    "label" => "add_calendar_event",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit calendar event ",
                    "label" => "edit_calendar_event",
                    "icon" => "<i class='material-icons'>create</i>",
                ),
                array(
                    "name" => "Add institute device",
                    "label" => "add_institute_device",
                    "icon" => "<i class='material-icons'>add_circle_outline</i>",
                ),
                array(
                    "name" => "Edit institute device ",
                    "label" => "edit_institute_device",
                    "icon" => "<i class='material-icons'>create</i>",
                ),

                array(
                    "name" => "Area manage ",
                    "label" => "area_manage",
                    "icon" => "<i class='material-icons'>create</i>",
                ),



            );
            if ($type){
                $keys = array(
                    "label" => $type
                );
                $filter = $this->filter_in_array($data,$keys,false);
                return $filter;
            }
            else{
                return $data;
            }

        }


        function filter_in_array($list,$filter_keys,$list_data=true,$negative=false,$original_index=false){
            $get_data = array_filter($list,function ($item) use($filter_keys,$list_data,$negative,$original_index){
                $key_count = count(array_keys($filter_keys));
                $true_count = 0;
                foreach ($filter_keys as $key => $value){
                    if ($negative){
                        if (isset($item[$key])){
                            if (gettype($value) == "array"){
                                $condition_symbol = $value['condition_symbol'];
                                $data = $value['data'];
                                if ($condition_symbol == "<="){
                                    if ($item[$key] <= $data){

                                    }
                                    else{
                                        $true_count++;
                                    }
                                }
                                elseif ($condition_symbol == ">="){
                                    if ($item[$key] >= $data){

                                    }
                                    else{
                                        $true_count++;
                                    }
                                }
                            }
                            else{
                                if ($item[$key] != $value){
                                    $true_count++;
                                }
                            }

                        }

                    }
                    else{
                        if (isset($item[$key])){
                            if (gettype($value) == "array"){
                                $condition_symbol = $value['condition_symbol'];
                                $data = $value['data'];
                                if ($condition_symbol == "<="){
                                    if ($item[$key] <= $data){
                                        $true_count++;
                                    }
                                }
                                elseif ($condition_symbol == ">="){
                                    if ($item[$key] >= $data){
                                        $true_count++;
                                    }
                                }
                            }
                            else{
                                if ($item[$key] == $value){
                                    $true_count++;
                                }
                            }

                        }

                    }

                }
                if ($key_count == $true_count){
                    return true;
                }
                else{
                    return false;
                }
            });

            if (!$list_data){
                foreach ($get_data as $item){
                    return $item;
                }
            }

            if ($original_index){
                $list = $get_data;
            }
            else{
                $list = array();
                foreach($get_data as $item){
                    $list[] = $item;
                }
            }
            return $list;
        }
        function estimated_access_menus($type){
            $menus = $this->menus();
            $search_array = array(
                $type => 1,
                "menu_type" => "single",
                "active" => 1
            );
            $filter = $this->filter_in_array($menus,$search_array);
            if ($filter){
                return $filter;
            }
            else{
                return array();
            }
        }
        function estimated_menus($type){
            $menus = $this->menus();
            $search_array = array(
                $type => 1,
                "active" => 1
            );
            $filter = $this->filter_in_array($menus,$search_array);
            if ($filter){
                return $filter;
            }
            else{
                return array();
            }
        }

        function permission_part_id($part_name){
            $data = $this->admin_permission_parts();
            $keys = array(
                "label" => $part_name
            );
            $filter = $this->filter_in_array($data,$keys,false);
            if ($filter){
                return $filter['id'];
            }
            else{
                return false;
            }
        }


    }


