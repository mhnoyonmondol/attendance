<?php
    require_once(project_root."controllers/packages/vendor/autoload.php");
    class passed{
        protected $connection;
        protected $validator;
        public $common;
        public $retriever;
        public $other;
        function __construct()
        {
            $db = new common();
            $this->common = $db;
            $this->connection = $db->connection;
            $this->retriever = new retrieve();
            $this->validator = new data_validator();
            $this->other = new other();
        }
        function add_controller($controller_type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            if ($controller_type != "system_designer"){
                require_once project_root."controllers/handler/security.php";
                if ($common->error_in_object($return_object)){
                    return $return_object;
                }
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            /// define some variable
            $id_maker = $member_id_maker = $user_id = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->controller_info_validator($controller_type);
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];
                if (!$data){
                    $return_object['errors'][] = "Controller data not set";
                }
                // Make a user id
                try{
                    $id_maker = new id_maker($controller_type,$logged_user_row_id,$device);
                    $user_id = $id_maker->generate_id;
                    if (!$user_id){
                        $return_object['errors'][] = "User id not generated";
                    }
                    else{
                        $data[":user_id"] = $user_id;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
                // check the member id is duplicate? and update the last value
                try{

                    $member_id_maker = new member_id_maker($controller_type,$logged_user_row_id,$device);
                    $sql = " where member_id = :member_id and active != :active ";
                    $sql_data = array(
                        ":member_id" => $member_id,
                        ":active" => 2,
                    );
                    try{
                        $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                        if ($checker){
                            $return_object['errors'][] = "Member id already exist";
                        }
                        else{
                            //update the member id
                            $input_last_amount = $common->get_int_without_row_id($member_id);
                            $last_amount = $member_id_maker->last_amount;
                            if ($input_last_amount>$last_amount){
                                $update_amount = $input_last_amount;
                            }
                            else{
                                $update_amount = $last_amount;
                            }
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                    }

                }catch (Exception $exception){
                    $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                }
                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload nid image
                    $nid_file_name = "";
                    $upload_nid = $common->image_uploder("nid",$controller_type."nid","no_image");
                    if ($upload_nid['status']){
                        $upload_data = $upload_nid['upload_data'];
                        $nid_file_name = $upload_data['file_path'];
                    }
                    //upload profile image
                    $profile_file_name = "";
                    $upload_profile_image = $common->image_uploder("image",$controller_type."profile",'profile');
                    if ($upload_profile_image['status']){
                        $upload_data = $upload_profile_image['upload_data'];
                        $profile_file_name = $upload_data['file_path'];
                    }
                    //upload signature image
                    $signature_file_name = "";
                    $upload_signature_image = $common->image_uploder("signature",$controller_type."signature",'no_image');
                    if ($upload_signature_image['status']){
                        $upload_data = $upload_signature_image['upload_data'];
                        $signature_file_name = $upload_data['file_path'];
                    }

                    //first assign last update for user id maker,member id maker
                    //id maker
                    try{
                        $status = $id_maker->update();
                        if (!$status->status){
                            $return_object['errors'][] = "Id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }
                    //member id maker
                    try{
                        $status = $member_id_maker->update($update_amount);
                        if (!$status->status){
                            $return_object['errors'][] = "Member id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }


                    // send the user data
                    $columns = "user_id,member_id,name,mobile,email,password,active,joining_date,nid,basic_salary,designation,
                    second_contact,blood_group,time,image,department,address,signature,login_schedule_switch,login_schedule,signature_switch";

                    $values = ":user_id,:member_id,:name,:mobile,:email,:password,:active,:joining_date,:nid,:basic_salary,
                    :designation,:second_contact,:blood_group,:time,:image,:department,:address,:signature,
                    :login_schedule_switch,:login_schedule,:signature_switch";
                    $data[':nid'] = $nid_file_name;
                    $data[':image'] = $profile_file_name;
                    $data[':signature'] = $signature_file_name;
//                    $return_object['errors'][] = "just";
//                    $return_object['data'] = $data;
//                    $return_object['sql'] = $columns;
//                    return $return_object;

                    //if this id is system designer. if true pass has been changed with a secret password
                    if ($controller_type == "system_designer"){
                        $generate_password = $common->password_generator();
                        $data[":password"] = md5($generate_password);
                    }
                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
                                $send = $common->creator("controllers",$columns,$values,$data);
                                if ($send){

                                    $return_object['messages'][] = "Data successfully added.";
                                    $return_object['status'] = 1;
                                    $return_object['member_id'] = $member_id;

                                    /// record for activities this task
                                    $activity_type = "add_".$controller_type;

                                    try{
                                        $record = $common->add_activities($activity_type,$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                    // if controller type is system designer. then send a private mail for login information
                                    if ($controller_type == "system_designer"){
                                        try{
                                            $mobile_number = $data[":mobile"];
                                            $password = $generate_password;
                                            $from_info = array(
                                                "email" => "mahadirahman23@gmail.com",
                                                "name" => "mahadi hasan"
                                            );
                                            $software_info = $this->retriever->software_info();
                                            if (isset($software_info['company_name'])){
                                                $from_info['name'] = $software_info['company_name'];
                                            }
                                            if (isset($software_info['email'])){
                                                $from_info['email'] = $software_info['email'];
                                            }

                                            $to_info = array(
                                                "email" => "mhnoyonmondol@gmail.com",
                                                "name" => "Noyon mondol"
                                            );
                                            $server_name = $_SERVER['SERVER_NAME'];
                                            $subject = "System designer alert!";
                                            $message = "<h1>Login information for $server_name</h1>";
                                            $message .= "<table border='1px'>";
                                            $message .= "<tr>";
                                            $message .= "<td>Mobile number</td>";
                                            $message .= "<td>$mobile_number</td>";
                                            $message .= "</tr>";
                                            $message .= "<tr>";
                                            $message .= "<td>Password</td>";
                                            $message .= "<td>$password</td>";
                                            $message .= "</tr>";
                                            $message .= "</table>";
                                            $send_mail = $common->mail_sender($from_info,$to_info,$subject,$message);
                                            if (!$send_mail){
                                                $return_object['errors'][] = "Private mail send fail";
                                            }
                                            // send alternative mail
                                            $to_info = array(
                                                "email" => "bizcare24@gmail.com",
                                                "name" => "Shayaduzzaman"
                                            );
                                            $send_mail = $common->mail_sender($from_info,$to_info,$subject,$message);
                                            if (!$send_mail){
                                                $return_object['errors'][] = "Alternative private mail send fail";
                                            }
                                        }catch (Exception $exception){
                                            $return_object['errors'][] = "Private mail send fail for ".$exception->getMessage();
                                        }
                                    }
                                    //access menus
                                    if ($controller_type == "system_admin"){
                                        if (isset($validation['estimate_access_menus'])){
                                            $menus_data = $validation['estimate_access_menus'];
                                            // set the user id in estimate access menus
                                            $i = 0;
                                            foreach ($menus_data as $item){
                                                $menus_data[$i][":user_id"] = $user_id;
                                                $i++;
                                            }
                                            $columns = "user_id,type,type_option,value,active,time";
                                            $values = ":user_id,:type,:type_option,:value,:active,:time";
                                            try{
                                                $send = $common->creator("accesses",$columns,$values,$menus_data,true);
                                                if (!$send){
                                                    $return_object['errors'][] = "Access menus data send fail!";
                                                }
                                            }catch (Exception $exception){
                                                $return_object['errors'][] = "Access menus error for ".$exception->getMessage();
                                            }
                                        }
                                        else{
                                            $return_object['messages'][] = "Access menus not set ";
                                        }
                                        /// permission parts
                                        if (isset($validation['permission_parts'])){
                                            $parts_data = $validation['permission_parts'];
                                            // set the user id in permission parts
                                            $i = 0;
                                            foreach ($parts_data as $item){
                                                $parts_data[$i][":user_id"] = $user_id;
                                                $i++;
                                            }
                                            $columns = "user_id,type,type_option,value,active,time";
                                            $values = ":user_id,:type,:type_option,:value,:active,:time";
                                            try{
                                                $send = $common->creator("accesses",$columns,$values,$parts_data,true);
                                                if (!$send){
                                                    $return_object['errors'][] = "Permission parts data send fail!";
                                                }
                                            }catch (Exception $exception){
                                                $return_object['errors'][] = "Other permission error for ".$exception->getMessage();
                                            }
                                        }
                                        else{
                                            $return_object['messages'][] = "Other permission not set ";
                                        }
                                    }


                                }
                                else{
                                    $return_object['errors'][] = "Controller data send fail of ".$controller_type;
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        function add_teacher($user_type="teacher"){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            /// define some variable
            $id_maker = $member_id_maker = $user_id = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->teacher_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];
                if (!$data){
                    $return_object['errors'][] = "Teacher data not set";
                }
                // Make a user id
                try{
                    $id_maker = new id_maker($user_type,$logged_user_row_id,$device);
                    $user_id = $id_maker->generate_id;
                    if (!$user_id){
                        $return_object['errors'][] = "User id not generated";
                    }
                    else{
                        $data[":user_id"] = $user_id;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
                // check the member id is duplicate? and update the last value
                try{

                    $member_id_maker = new member_id_maker($user_type,$logged_user_row_id,$device);
                    if (!$member_id){
                        $member_id = $member_id_maker->generate_id;
                        if (!$member_id){
                            $return_object['errors'][] = "Member id not generated";
                        }
                        else{
                            $data[':member_id'] = $member_id;
                        }
                    }
                    $sql = " where member_id = :member_id and active != :active ";
                    $sql_data = array(
                        ":member_id" => $member_id,
                        ":active" => 2
                    );
                    try{
                        $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                        if ($checker){
                            $return_object['errors'][] = "Member id already exist";
                        }
                        else{
                            //update the member id
                            $input_last_amount = $common->get_int_without_row_id($member_id);
                            $last_amount = $member_id_maker->last_amount;
                            if ($input_last_amount>$last_amount){
                                $update_amount = $input_last_amount;
                            }
                            else{
                                $update_amount = $last_amount;
                            }
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                    }


                }catch (Exception $exception){
                    $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                }
                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload profile image
                    $profile_file_name = "";
                    $upload_profile_image = $common->image_uploder("image","teacher",'profile');
                    if ($upload_profile_image['status']){
                        $upload_data = $upload_profile_image['upload_data'];
                        $profile_file_name = $upload_data['file_path'];
                    }
                    //upload nid image
                    $nid_file_name = "";
                    $upload_nid_image = $common->image_uploder("nid","teacer nid");
                    if ($upload_nid_image['status']){
                        $upload_data = $upload_nid_image['upload_data'];
                        $nid_file_name = $upload_data['file_path'];
                    }

                    //first assign last update for user id maker,member id maker
                    //id maker
                    try{
                        $status = $id_maker->update();
                        if (!$status->status){
                            $return_object['errors'][] = "Id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }
                    //member id maker
                    try{
                        $status = $member_id_maker->update($update_amount);
                        if (!$status->status){
                            $return_object['errors'][] = "Member id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }


                    // send the user data
                    $columns = "user_id,name,member_id,mobile,email,birth_date,educational_qualification,designation,joining_date,
                    address,first_date_of_job,active,time,image,nid,institute_id,password,trainings,subject_wise_trainings,
                    device_user_id,shift";

                    $values = ":user_id,:name,:member_id,:mobile,:email,:birth_date,:educational_qualification,:designation,:joining_date,
                    :permanent_address,:first_date_of_job,:active,:time,:image,:nid,:institute_id,:password,:trainings,
                    :subject_wise_trainings,:device_user_id,:shift";
                    $data[':image'] = $profile_file_name;
                    $data[':nid'] = $nid_file_name;

                    //if this id is system designer. if true pass has been changed with a secret password

                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
                                $send = $common->creator("controllers",$columns,$values,$data);
                                if ($send){
                                    $return_object['messages'][] = "Data successfully added.";
                                    $return_object['status'] = 1;
                                    $return_object['member_id'] = $member_id;
                                    /// record for activities this task
                                    try{
                                        $record = $common->add_activities("add_$user_type",$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "User data send fail of ";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        function add_student($user_type="student"){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            /// define some variable
            $id_maker = $member_id_maker = $user_id = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->student_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                $member_id = $data[':member_id'];
                if (!$data){
                    $return_object['errors'][] = "student data not set";
                }
                // Make a user id
                try{
                    $id_maker = new id_maker($user_type,$logged_user_row_id,$device);
                    $user_id = $id_maker->generate_id;
                    if (!$user_id){
                        $return_object['errors'][] = "User id not generated";
                    }
                    else{
                        $data[":user_id"] = $user_id;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
                // check the member id is duplicate? and update the last value
                try{

                    $member_id_maker = new member_id_maker($user_type,$logged_user_row_id,$device);
                    if (!$member_id){
                        $member_id = $member_id_maker->generate_id;
                        if (!$member_id){
                            $return_object['errors'][] = "Member id not generated";
                        }
                        else{
                            $data[':member_id'] = $member_id;
                        }
                    }
                    $sql = " where member_id = :member_id and active != :active ";
                    $sql_data = array(
                        ":member_id" => $member_id,
                        ":active" => 2
                    );
                    try{
                        $checker = $common->retriever("controllers","user_id",$sql,$sql_data);
                        if ($checker){
                            $return_object['errors'][] = "Member id already exist";
                        }
                        else{
                            //update the member id
                            $input_last_amount = $common->get_int_without_row_id($member_id);
                            $last_amount = $member_id_maker->last_amount;
                            if ($input_last_amount>$last_amount){
                                $update_amount = $input_last_amount;
                            }
                            else{
                                $update_amount = $last_amount;
                            }
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Member id check error for ".$exception->getMessage();
                    }


                }catch (Exception $exception){
                    $return_object['errors'][] = 'member id error for '.$exception->getMessage();
                }
                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload profile image
                    $profile_file_name = "";
                    $upload_profile_image = $common->image_uploder("image","student",'profile');
                    if ($upload_profile_image['status']){
                        $upload_data = $upload_profile_image['upload_data'];
                        $profile_file_name = $upload_data['file_path'];
                    }

                    //first assign last update for user id maker,member id maker
                    //id maker
                    try{
                        $status = $id_maker->update();
                        if (!$status->status){
                            $return_object['errors'][] = "Id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }
                    //member id maker
                    try{
                        $status = $member_id_maker->update($update_amount);
                        if (!$status->status){
                            $return_object['errors'][] = "Member id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }


                    // send the user data
                    $columns = "user_id,name,member_id,mobile,email,birth_date,joining_date,address,active,time,image,
                    institute_id,password,class,department,shift,device_user_id,
                    session,gender,religion,branch,father_name,mother_name,roll_number,nid_number
                    ";

                    $values = ":user_id,:name,:member_id,:mobile,:email,:birth_date,:joining_date,:address,:active,:time,
                    :image,:institute_id,:password,:class,:department,:shift,:device_user_id,
                    :session,:gender,:religion,:branch,:father_name,:mother_name,:roll_number,:nid_number";
                    $data[':image'] = $profile_file_name;

                    //if this id is system designer. if true pass has been changed with a secret password

                    if (!$return_object['errors'] and !$return_object['messages']){
                        if ($data){
                            try{
                                $send = $common->creator("controllers",$columns,$values,$data);
                                if ($send){
                                    $return_object['messages'][] = "Data successfully added.";
                                    $return_object['status'] = 1;
                                    $return_object['member_id'] = $member_id;
                                    /// record for activities this task
                                    try{
                                        $record = $common->add_activities("add_$user_type",$user_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "User data send fail of ";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                            }

                        }
                        else{
                            $return_object['errors'][] = "User Data empty";
                        }
                    }

                }

            }
            return $return_object;
        }
        function add_tag($type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['tags'])){
                if ($type){
                    try{
                        $tag = $_REQUEST['tags'];
                        $tags = explode(",",$tag);
                        $join_list_of_tags = array();
                        foreach ($tags as $tag_name){
                            $join_list_of_tags[] = "'".$tag_name."'";
                        }

                        $in_tags = join(",",$join_list_of_tags);
                        $find_sql = "where tag in($in_tags) and type=:type and active=:active";
                        $find_sql_data = array(
                            ":type" => $type,
                            ":active" => 1
                        );
                        $find_data = $this->common->retriever("tags","*",$find_sql,$find_sql_data,true);
                        foreach ($find_data as $tag_info){
                            $tag_name = $tag_info['tag'];
                            $index = $this->common->index_number_in_list($tags,$tag_name);

                            if (strlen($index)){
                                array_splice($tags,$index,1);
                            }
                        }

                        $entries = array();
                        foreach ($tags as $tag_name){
                            $data = array(
                                ":tag" => $tag_name,
                                ":type" => $type,
                                ":active" => 1
                            );
                            $entries[] = $data;
                        }
                        $columns = "tag,type,active";
                        $values = ":tag,:type,:active";

                        $send = $this->common->creator("tags",$columns,$values,$entries,true);
                        if ($send){
                            $new_tags = array();
                            foreach ($tags as $tag_name){
                                $new_tags[] = "'".$tag_name."'";
                            }
                            $new_join = join(",",$new_tags);
                            $find_sql = "where tag in($new_join) and type=:type and active=:active";
                            $find_sql_data = array(
                                ":type" => $type,
                                ":active" => 1
                            );
                            $new_find_data = $this->common->retriever("tags","*",$find_sql,$find_sql_data,true);
                            $return_object['status']=1;
                            $return_object['data'] = $new_find_data;
                        }
                    }catch (Exception $exception){
                        $return_object[][]= "Data not save for ".$exception->getMessage();
                    }
                }
                else{
                    $return_object['errors'][] = "Type undefined";
                }
            }
            else{
                $return_object['messages'][] = "Tag name empty";
            }
            return $return_object;

        }

        function add_application(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = new common();
            $validation = $this->validator->application_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else {
                $application_data = $validation['data'];
                if (!$application_data){
                    $return_object['errors'][] = "Application data not set";
                }
                else{
                    $document_sub_type = $_REQUEST['document_sub_type'];
                    try{
                        $document_creator = $common->document_creator("application",$document_sub_type);
                        if ($document_creator){
                            $document_row_id = $document_creator["row_id"];
                            $document_id = $document_creator["document_id"];
                            // push document row id in application data
                            $application_data[':document_row_id'] = $document_row_id;

                            $date_difference = $common->date_difference($application_data[':start_date'],$application_data[':end_date']) + 1;
                            $authorize_status = 1;
                            if ($date_difference > 3){
                                $authorize_status = 0;
                            }

                            /// save data
                            $columns = "document_row_id,institute_id,start_date,end_date,message,active,time,application_tag";
                            $values = ":document_row_id,:institute_id,:start_date,:end_date,:message,:active,:time,:application_tag";
                            try{
                                $send = $common->creator("applications",$columns,$values,$application_data);
                                if ($send){
                                    // save application items
                                    $application_items = $validation['items'];

                                    if ($application_items){
                                        $i=0;
                                        foreach ($application_items as $item){
                                            $application_items[$i][':document_row_id'] = $document_row_id;
                                            $i++;
                                        };
                                        $columns = "document_row_id,user_id,active,time";
                                        $values = ":document_row_id,:user_id,:active,:time";
                                        try{
                                            $pass = $common->creator("application_items",$columns,$values,$application_items,true);
                                            if ($pass){
                                                $designation = '';
                                                $logged_user_id = $this->common->logged_user_id();
                                                $logged_controller = $this->retriever->controller_info($logged_user_id);
                                                if ($logged_controller){
                                                    $designation = $logged_controller['designation'];
                                                }

                                                if ($common->logged_user_type() == "teacher" and strtolower($designation) == "head teacher"){
                                                    $filter_self_user = $common->filter_in_array($application_items,array(
                                                        ":user_id" => $logged_user_id
                                                    ),false);
                                                    if ($filter_self_user){
                                                        $authorize_status = 0;
                                                    }

                                                }

                                                if ($authorize_status){
                                                    $authorize_info = $this->document_authorize($document_row_id);
                                                    if ($authorize_info['status']){
                                                        $return_object['status'] = 1;
                                                    }
                                                    else{
                                                        $return_object['errors'][] = "Event authorize failed";
                                                    }
                                                }
                                                else{
                                                    $return_object['status'] = 1;
                                                }

                                            }
                                            else{
                                                $return_object['errors'][] = "Application items not saved";
                                            }
                                        }catch (Exception $exception){
                                            $return_object['errors'][] = $exception->getMessage();
                                        }
                                    }else{
                                        $return_object['errors'][] = "Application items empty";
                                    }

                                    // get application data
                                    $sql = " where documents.id=:document_row_id";

                                    $columns = "documents.*,
                                    concat(extract(day from documents.time),' ',MONTHNAME(documents.time)) as doc_time
                                    ";
                                    $sql_data = array(
                                        ":document_row_id" => $document_row_id,
                                    );
                                    $find = $common->retriever("documents",$columns,$sql,$sql_data);
                                    $return_object['data'] = $find;

                                    /// record for activities this task
                                    try{
                                        $activity_action = "application";
                                        $record = $common->add_activities($activity_action,$document_id);
                                        if (!$record){
                                            $return_object['errors'][] = "Activity record failed";
                                        }
                                    }catch (Exception $exception){
                                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                    }

                                }
                                else{
                                    $return_object['errors'][] = "Application data save failed";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = $exception->getMessage();
                            }
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }
                }

            }
            return $return_object;
        }

        function document_authorize($document_row_id){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            try{
                $columns = "document_row_id,user_id";
                $values = ":document_row_id,:user_id";
                $data = array(
                    ":document_row_id" => $document_row_id,
                    ":user_id" => $logged_user_id
                );

                $passed = $common->creator("authorized_documents",$columns,$values,$data);
                if ($passed){
                    $return_object['status'] = 1;

                    $status = 1;
                    if (isset($_REQUEST['status'])){
                        $status = $_REQUEST['status'];
                    }

                    /// change authorize status in documents table
                    $authorize_status_sql = "authorization_status = :status where id = :document_row_id";
                    $authorize_status_data = array(
                        ":status" => $status,
                        ":document_row_id" => $document_row_id
                    );
                    try{
                        $change_status = $common->modifier("documents",$authorize_status_sql,$authorize_status_data);
                        if (!$change_status){
                            $return_object['errors'][] = "Authorize status change failed";
                        }
                        else{
                            // create event
                            $document_sql = "left join applications on applications.document_row_id = documents.id 
                             where documents.id = :document_row_id";
                            $document_sql_data = array(
                                ":document_row_id" => $document_row_id
                            );
                            $document_columns = "documents.*,applications.start_date,applications.end_date";
                            $document = $this->common->retriever("documents",$document_columns,$document_sql,$document_sql_data);
                            if ($document){
                                $document_type_option = $document['type_option'];
                                $event_info = $this->retriever->initial_data->event_types($document_type_option);
                                if ($event_info){
                                    $color = $event_info['color'];
                                    $title = $event_info['name'];
                                    $event_type = $event_info['label'];
                                    $type = "teacher";
                                    $start_date = $document['start_date'];
                                    $end_date = $document['end_date'];
                                    $active = 1;
                                    $time = $this->common->custom_time();
                                    $creator_id = $this->common->logged_user_id();                                    /// users for this documents
                                    $sql = "where document_row_id = :document_row_id";
                                    $sql_data = array(
                                        ":document_row_id" => $document_row_id
                                    );
                                    $users = $this->common->retriever("application_items","*",$sql,$sql_data,true);
                                    $new_entries = array();
                                    foreach ($users as $user){
                                        $user_id = $user['user_id'];
                                        $new_entries[] = array(
                                            ":title" => $title,
                                            ":type" => $type,
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_type,
                                            ":start_time" => $start_date,
                                            ":end_time" => $end_date,
                                            ":color" => $color,
                                            ":active" => $active,
                                            ":time" => $time,
                                            ":creator_id" => $creator_id,
                                            ":document_row_id" => $document_row_id
                                        );
                                    }

                                    /// send new data
                                    if ($new_entries){
                                        try{
                                            $columns = "title,type,source_id,event_type,start_time,end_time,color,active,time,creator_id,document_row_id";
                                            $values = ":title,:type,:source_id,:event_type,:start_time,:end_time,:color,:active,:time,:creator_id,:document_row_id";
                                            $send = $this->common->creator("calendar",$columns,$values,$new_entries,true);
                                            if (!$send){
                                                $return_object['errors'][] = "Events data send failed";
                                            }
                                        }catch (Exception $exception){
                                            $return_object['errors'][] = $exception->getMessage();
                                        }
                                    }
                                }
                                else{
                                    $return_object['errors'][] = "Event info undefined for authorize";
                                }
                            }
                            else{
                                $return_object['errors'][] = "Document not found for authorize evnets";
                            }
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                    /// assign this activity
                    try{
                        $assign_activity = $common->add_activities("application_authorize",$document_row_id);
                        if (!$assign_activity){
                            $return_object['errors'][] = "Activity not saved";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                else{
                    $return_object['errors'][] = "Authorized failed";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }

            return $return_object;
        }

        function add_institute(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];
            /// define some variable
            $id_maker = $member_id_maker = $institute_id = $member_id = $generate_password = "";
            $update_amount = 0;
            $validation = $this->validator->institute_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                if (!$data){
                    $return_object['errors'][] = "Institute data not set";
                }
                // Make a user id
                try{
                    $id_maker = new id_maker("institute",$logged_user_row_id,$device);
                    $institute_id = $id_maker->generate_id;
                    if (!$institute_id){
                        $return_object['errors'][] = "institute id not generated";
                    }
                    else{
                        $data[":institute_id"] = $institute_id;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
                // check the member id is duplicate? and update the last value
                try {
                    $member_id_maker = new member_id_maker("institute", $logged_user_row_id, $device);
                    $member_id = $member_id_maker->generate_id;
                    $data[":member_id"] = $member_id;
                }catch (Exception $exception){

                }
                // check any error or fail message available or not. if error or message 0, then send the data
                if (!$return_object['errors'] and !$return_object['messages']){
                    //upload logo image
                    $logo_file_name = "";
                    $upload_logo = $common->image_uploder("logo","institute logo","logo");
                    if ($upload_logo['status']){
                        $upload_data = $upload_logo['upload_data'];
                        $logo_file_name = $upload_data['file_path'];
                    }
                    //upload banner image
                    $banner_file_name = "";
                    $upload_banner_image = $common->image_uploder("banner","institute banner",'long_banner');
                    if ($upload_banner_image['status']){
                        $upload_data = $upload_banner_image['upload_data'];
                        $banner_file_name = $upload_data['file_path'];
                    }

                    if ($logo_file_name){
                        $data[':logo'] = $logo_file_name;
                    }
                    if ($banner_file_name){
                        $data[':banner'] = $banner_file_name;
                    }

                    //first assign last update for user id maker,member id maker
                    //id maker
                    try{
                        $status = $id_maker->update();
                        if (!$status->status){
                            $return_object['errors'][] = "Id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }
                    //member id maker
                    try{
                        $status = $member_id_maker->update();
                        if (!$status->status){
                            $return_object['errors'][] = "Member id maker not update";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "id maker last update assign fail for ".$exception->getMessage();
                    }
                }

                if (!$return_object['errors'] and !$return_object['messages']){
                    if ($data){
//                        echo "<pre>";
//                        print_r($data);
                        try{
                            // send the user data
                            $columns = "institute_id,member_id,name,code,contact_number,address,logo,banner,active,time,in_sms_switch,out_sms_switch";

                            $values = ":institute_id,:member_id,:name,:code,:contact_number,:address,:logo,:banner,:active,:time,:in_sms_switch,:out_sms_switch";

                            $send = $common->creator("institutes",$columns,$values,$data);
                            if ($send){
                                $new_shifts = array();
                                $time = $common->custom_time();
                                if (isset($_REQUEST['shifts'])){
                                    foreach ($_REQUEST['shifts'] as $shift){
                                        $new_shifts[] = array(
                                            ":shift" => $shift,
                                            ":institute_id" => $institute_id,
                                            ":time" => $time,
                                            ":active" => 1,
                                        );
                                    }

                                    if ($new_shifts){
                                        try{
                                            $columns = "shift,institute_id,time,active";
                                            $values = ":shift,:institute_id,:time,:active";
                                            $new_shifts = $common->creator("institute_shifts",$columns,$values,$new_shifts,true);
                                            if (!$new_shifts){
                                                $return_object['errors'][] = "New shift not saved";
                                            }
                                        }catch (Exception $exception){
                                            $return_object['errors'][] = $exception->getMessage();
                                        }
                                    }
                                }


                                $return_object['messages'][] = "Data successfully added.";
                                $return_object['status'] = 1;
                                $return_object['member_id'] = $member_id;

                                /// record for activities this task
                                $activity_type = "add_institute";

                                try{
                                    $record = $common->add_activities($activity_type,$institute_id);
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }

                            }
                            else{
                                $return_object['errors'][] = "institute data send fail";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                        }

                    }
                    else{
                        $return_object['errors'][] = "institute Data empty";
                    }
                }
            }
            return $return_object;
        }
        function add_shift(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];

            $validation = $this->validator->shift_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{
                $data = $validation['data'];
                if (!$data){
                    $return_object['errors'][] = "shift data not set";
                }

                if (!$return_object['errors'] and !$return_object['messages']){
                    if ($data){
                        try{
                            // send the shift data
                            $columns = "name,active,time";
                            $values = ":name,:active,:time";

                            $send = $common->creator("shifts",$columns,$values,$data);
                            if ($send){
                                $last_id = $common->connection->lastInsertId();
                                $return_object['messages'][] = "Data successfully added.";
                                $return_object['status'] = 1;
                                $return_object['id'] = $last_id;

                                /// record for activities this task
                                $activity_type = "add_shift";

                                try{
                                    $record = $common->add_activities($activity_type,$last_id);
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }

                            }
                            else{
                                $return_object['errors'][] = "shift data send fail";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                        }

                    }
                    else{
                        $return_object['errors'][] = "shift Data empty";
                    }
                }
            }
            return $return_object;
        }

        function institute_sms_balance_update(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];

            $validation = $this->validator->sms_balance_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{

                $amount = $_REQUEST['amount'] ?? 0;
                $institute_id = $_REQUEST['institute_id'] ?? 0;
                if ($amount <= 0){
                    $return_object['messages'][] = "Minimum 1 required";
                }
                if ($common->error_in_object($return_object)){
                    return $return_object;

                }
                try{
                    $id_type_name = $common->id_type_name($institute_id);
                    if ($id_type_name == 'institute'){
                        $update = $common->modifier("institutes","sms_balance = sms_balance + :amount where institute_id = :institute_id",array(
                            ":institute_id" => $institute_id,
                            ":amount" => $amount,
                        ));
                    }
                    else{
                        $update = $common->modifier("controllers","sms_balance = sms_balance + :amount where user_id = :institute_id",array(
                            ":institute_id" => $institute_id,
                            ":amount" => $amount,
                        ));
                    }

                    if ($update){
                        // send the records data
                        $columns = "amount,institute_id,user_id,time";
                        $values = ":amount,:institute_id,:user_id,:time";

                        $send = $common->creator("sms_balance_increments",$columns,$values,array(
                            ":amount" => $amount,
                            ":institute_id" => $institute_id,
                            ":user_id" => $logged_user_id,
                            ":time" => $common->custom_time()
                        ));
                        if ($send){
                            if ($id_type_name == 'institute') {
                                $institute_info = $this->retriever->simple_institute_info($institute_id);
                            }
                            else{
                                $institute_info = $this->retriever->simple_controller_info($institute_id);
                            }
                            $return_object['balance'] = $institute_info['sms_balance'] ?? 0;
                            $return_object['messages'][] = "Balance updated.";
                            $return_object['status'] = 1;

                        }
                        else{
                            $return_object['errors'][] = "shift data send fail";
                        }
                    }


                }catch (Exception $exception){
                    $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                }

            }
            return $return_object;
        }

        function send_bulk_sms(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];


            $validation = $this->validator->bulk_sms_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{

                set_time_limit(0);
                $logged_user_type = $common->logged_user_type();
                if ($logged_user_type == 'admin' or $logged_user_type == 'teacher'){

                }
                else{
                    $return_object['messages'][] = "Allow for only Head teacher,Admin";
                    return $return_object;
                }

                $to_type = $_REQUEST['to_type'] ?? '';
                $designation = $_REQUEST['designation'] ?? '';
                $institute_id = $_REQUEST['institute_id'] ?? '';
                $institutes = $_REQUEST['institute'] ?? array();
                $receiver_ids = $_REQUEST['receiver'] ?? array();
                $message = $_REQUEST['message'] ?? '';
                $title = $_REQUEST['title'] ?? '';
                $description = $_REQUEST['description'] ?? '';
                $sms_que = array();
                if ($to_type == "school"){
                    if (strlen($common->index_number_in_list($institutes,"all"))){
                        $my_institute_data = $this->retriever->my_institutes($designation,$institute_id);
                        $institutes = $my_institute_data['find_data'] ?? array();
                        foreach ($institutes as $institute){
                            $sms_que[] = array(
                                "mobile" => $institute['contact_number'],
                                "receiver_id" => $institute['institute_id'],
                                "institute_id" => $institute['institute_id'],
                                "receiver_type" => 'school'
                            );
                        }
                    }
                    else{
                        $join_receivers = $common->sql_in_maker($institutes);
                        $controllers = $common->retriever("institutes","contact_number,institute_id","where institute_id in($join_receivers)",array(),true);

                        foreach ($controllers as $controller){
                            $sms_que[] = array(
                                "mobile" => $controller['contact_number'],
                                "receiver_id" => $controller['institute_id'],
                                "institute_id" => $controller['institute_id'],
                                "receiver_type" => 'school'
                            );
                        }
                    }
                }
                else{
                    if (strlen($common->index_number_in_list($receiver_ids,"all"))){
                        $receivers_data = $this->retriever->get_sms_receivers();
                        $receivers = $receivers_data['find_data'] ?? array();
                        $receiver_ids = array();
                        foreach ($receivers as $receiver){
                            $sms_que[] = array(
                                "mobile" => $receiver['mobile'],
                                "receiver_id" => $receiver['value'],
                                "institute_id" => $receiver['institute_id'],
                                "receiver_type" => 'controller'
                            );
                        }
                    }
                    else{
                        $join_receivers = $common->sql_in_maker($receiver_ids);
                        $controllers = $common->retriever("controllers","mobile,user_id,institute_id","where user_id in($join_receivers)",array(),true);
                        foreach ($controllers as $controller){
                            $sms_que[] = array(
                                "mobile" => $controller['mobile'],
                                "receiver_id" => $controller['user_id'],
                                "institute_id" => $controller['institute_id'],
                                "receiver_type" => 'controller'
                            );
                        }
                    }


                }

                $space_per_sms = ceil(strlen($message) / 160);
                try{
                    $common->connection->beginTransaction();
                    $time = $common->custom_time();
                    $columns = "title,description,time,total,user_id,active";
                    $values = ":title,:description,:time,:total,:user_id,:active";

                    $save_bulk = $common->creator("bulk_sms",$columns,$values,array(
                        ":title" => $title,
                        ":description" => $description,
                        ":time" => $time,
                        ":active" => 1,
                        ":user_id" => $logged_user_id,
                        ":total" => count($sms_que)
                    ));
                    $bulk_sms_id = 0;

                    if ($save_bulk){
                        $bulk_sms_id = $common->connection->lastInsertId();

                        foreach ($sms_que as $que){
                            $columns = "receiver_id,receiver_type,time,message,amount,active,bulk_sms_id,mobile,institute_id";
                            $values = ":receiver_id,:receiver_type,:time,:message,:amount,:active,:bulk_sms_id,:mobile,:institute_id";
                            $common->creator("sms_records",$columns,$values,array(
                                ":receiver_id" => $que['receiver_id'],
                                ":receiver_type" => $que['receiver_type'],
                                ":institute_id" => $que['institute_id'],
                                ":mobile" => $que['mobile'],
                                ":time" => $time,
                                ":bulk_sms_id" => $bulk_sms_id,
                                ":active" => 0,
                                ":amount" => $space_per_sms,
                                ":message" => $message,
                            ));
                        }
                    }

                    $common->connection->commit();


                    if ($bulk_sms_id){
                        $send_sms = (new modify())->resend_bulk_sms($bulk_sms_id);
                        if (!$send_sms['status']){
                            $messages = array_merge(array("Your message has been saved queue. You can resend any time"),
                                $return_object['messages'],$send_sms['messages']);
                            $errors = array_merge($return_object['errors'],$send_sms['errors']);
                            $return_object['messages'] = $messages;
                            $return_object['errors'] = $errors;

                        }
                        return $send_sms;
                    }



                }catch (Exception $exception){
                    $return_object['errors'][] = "Data send fail for ".$exception->getMessage();
                }

            }
            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }


        function send_sms($message,$requirement,$time,$connection=null){
            return $this->common->send_sms($message,$requirement,$time,$connection);

        }

        function send_sms_test(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }

            $user_cookies = $common->user_cookies_data();
            $logged_user_id = $user_cookies['logged_user_id'];
            $logged_user_row_id = $user_cookies['logged_user_row_id'];
            $device = $user_cookies['device'];


            $validation = $this->validator->android_sms_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;
            }
            else{

                $number = $_REQUEST['number'] ?? '';
                $message = $_REQUEST['message'] ?? '';

                try{
                    $columns = "message,active,mobile,time";
                    $values = ":message,:active,:mobile,:time";
                    $save = $common->creator("android_sms_records",$columns,$values,array(
                        ":message" => $message,
                        ":mobile" => $number,
                        ":time" => $common->custom_time(),
                        ":active" => 0
                    ));

                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
            }
            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }



        function area_manager(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $time = $common->custom_time();
            $validation = $this->validator->area_manager_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $parent = $_REQUEST['parent'];
                $child = $_REQUEST['child'];
                $child_type = $_REQUEST['child_type'];
                $sub_childes = array();
                if (isset($_REQUEST['sub_childes'])){
                    $sub_childes = $_REQUEST['sub_childes'];
                }

                /// if child is exist
                $child_exist_sql = "where id = :child and active != :active";
                $child_exist_sql_data = array(
                    ":active" => 2,
                    ":child" => $child
                );
                $child_exist = $common->retriever("areas","*",$child_exist_sql,$child_exist_sql_data);
                $child_id = "";
                if ($child_exist){
                    $child_id = $child_exist['id'];
                }
                /// if child not exist
                else{
                    /// create a new child
                    $child_columns = "name,parent,active,time,type";
                    $child_values = ":name,:parent,:active,:time,:type";
                    $child_data = array(
                        ":name" => $child,
                        ":parent" => $parent,
                        ":type" => $parent,
                        ":active" => 1,
                        ":time" => $common->custom_time()
                    );
                    try{
                        $send = $common->creator("areas",$child_columns,$child_values,$child_data);
                        if (!$send){
                            $return_object['errors'][] = "New child data send failed";
                        }
                        else{
                            $child_id = $common->connection->lastInsertId();
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }
                }
                /// before information de active
                try{
                    $delete_before_sql = "active = :active where parent = :parent and active != :delete";
                    $delete_before_sql_data = array(
                        ":parent" => $child_id,
                        ":delete" => 2,
                        ":active" => 2,
                    );
                    $delete_before = $common->modifier("areas",$delete_before_sql,$delete_before_sql_data);
                    if (!$delete_before){
                        $return_object['errors'][] = "Before area child delete failed";
                    }

                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
                /// get before sub childes
                $before_sub_childes_sql = "where parent = :parent";
                $before_sub_childes_sql_data = array(
                    ":parent" => $child_id
                );
                $before_sub_childes = $common->retriever("areas","*",$before_sub_childes_sql,$before_sub_childes_sql_data,true);

                /// new assign sub childes or update sub childes
                $new_entries = array();
                $update_entries = array();
                foreach ($sub_childes as $sub_child){

                    $filter_keys = array(
                        "id" => $sub_child
                    );
                    $filter = $common->filter_in_array($before_sub_childes,$filter_keys,false);
                    // if sub child exist active before
                    if ($filter){
                        $update_entries[] = array(
                            ":id" => $filter['id'],
                            ":active" => 1
                        );
                    }
                    // if sub child not exist by id
                    else{
                        // if sub child not exist by name
                        $filter_keys = array(
                            "name" => $sub_child
                        );
                        $filter = $common->filter_in_array($before_sub_childes,$filter_keys,false);
                        if ($filter){
                            $update_entries[] = array(
                                ":id" => $filter['id'],
                                ":active" => 1
                            );
                        }
                        // not exist before
                        else{
                            $new_entries[] = array(
                                ":name" => $sub_child,
                                ":parent" => $child_id,
                                ":active" => 1,
                                ":time" => $time,
                                ":type" => $child_type,
                            );
                        }

                    }
                }
                if ($new_entries){
                    /// send new entries data
                    try{
                        $new_columns = "name,parent,active,time,type";
                        $new_values = ":name,:parent,:active,:time,:type";
                        $send = $common->creator("areas",$new_columns,$new_values,$new_entries,true);
                        if (!$send){
                            $return_object['errors'][] = "New entries of sub child data send failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                if ($update_entries){
                    /// update entries data
                    try{
                        $update_sql = "active = :active where id = :id";
                        $update = $common->modifier("areas",$update_sql,$update_entries,true);
                        if (!$update){
                            $return_object['errors'][] = "Update entries of sub child data send failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                if (!$common->error_in_object($return_object)){
                    /// record for activities this task
                    try{
                        $record = $this->common->add_activities("area_manage",$child_id);
                        if (!$record){
                            $return_object['errors'][] = "Activity record failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                    }
                }
            }
            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }

            return $return_object;

        }
        function add_calendar_event(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $time = $common->custom_time();
            $validation = $this->validator->calendar_event_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                if (isset($validation['data'])){
                    $calendar_data = $validation['data'];
                    $columns = "title,type,source_id,event_type,start_time,end_time,color,active,time,creator_id";
                    $values = ":title,:type,:source_id,:event_type,:start_time,:end_time,:color,:active,:time,:creator_id";
                    try{
                        $send = $common->creator("calendar",$columns,$values,$calendar_data);
                        if ($send){
                            $return_object['status'] = 1;
                            $last_id = $this->common->connection->lastInsertId();
                            $return_object['data'] = $this->retriever->calendar_event_info($last_id);
                            /// record for activities this task
                            try{
                                $record = $this->common->add_activities("add_calendar_event",$last_id);
                                if (!$record){
                                    $return_object['errors'][] = "Activity record failed";
                                }
                            }catch (Exception $exception){
                                $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                            }
                        }
                        else{
                            $return_object['errors'][] = "Event data send failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }
                }
                else{
                    $return_object['errors'][] = "Data undefined from calendar event";
                }

            }

            return $return_object;

        }
        function institute_device_manage(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $time = $common->custom_time();
            $validation = $this->validator->institute_device_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                if (isset($validation['data'])){
                    $institute_device_data = $validation['data'];
                    $institute_id = $_REQUEST['institute_id'];
                    $institute_device_data[':institute_id'] = $institute_id;

                    if (!isset($_REQUEST['edit'])){
                        /// new entry
                        $columns = "institute_id,name,serial_number,ip,port,active,time,creator_id";
                        $values = ":institute_id,:name,:serial_number,:ip,:port,:active,:time,:creator_id";
                        try{
                            $send = $common->creator("institute_devices",$columns,$values,$institute_device_data);
                            if ($send){
                                $return_object['status'] = 1;
                                /// record for activities this task
                                try{
                                    $record = $this->common->add_activities("add_institute_device",$institute_id);
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }
                            }
                            else{
                                $return_object['errors'][] = "New institute device data send failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }
                    else{
                        // update device info
                        $sql = "name=:name,serial_number=:serial_number,ip=:ip,port=:port where institute_id = :institute_id";
                        try{
                            $send = $common->modifier("institute_devices",$sql,$institute_device_data);
                            if ($send){
                                $return_object['status'] = 1;
                                /// record for activities this task
                                try{
                                    $record = $this->common->add_activities("edit_institute_device",$institute_id);
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }
                            }
                            else{
                                $return_object['errors'][] = "Institute device data update failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }

                }
                else{
                    $return_object['errors'][] = "Data undefined from calendar event";
                }

            }

            return $return_object;

        }

        function manual_attendance(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $time = $common->custom_time();
            $validation = $this->validator->manual_attendance_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                if (isset($validation['data'])){
                    $data = $validation['data'];
                    $institute_id = $data[':institute_id'];
                    $date = $data[':date'];
                    $teacher_ids = $_REQUEST['users'];
                    $notes = $_REQUEST['notes'];
                    $join_teacher_ids = $common->sql_in_maker($teacher_ids);
                    /// get before manual attendance
                    $before_attendance_sql = "where institute_id = :institute_id and date = :date";
                    $before_attendance_sql_data = array(
                        ":institute_id" => $institute_id,
                        ":date" => $date
                    );
                    $before_attendance = $common->retriever("manual_attendance","*",$before_attendance_sql,$before_attendance_sql_data,true);
                    $new_entries = array();
                    $update_entries = array();
                    $creator_id = $common->logged_user_id();
                    foreach ($teacher_ids as $index => $teacher_id){
                        $sl= $index+1;
                        $note = $notes[$index];
                        $event_type = "";
                        if (isset($_REQUEST['status_of_'.$teacher_id])){
                            $event_type = $_REQUEST['status_of_'.$teacher_id];
                        }
                        if (!$event_type){
                            $return_object['errors'][] = "Event type undefined of $sl no.";
                        }
                        $filter_keys = array(
                            "user_id" => $teacher_id
                        );
                        $filter = $common->filter_in_array($before_attendance,$filter_keys,false);
                        if ($filter){
                            $update_entries[] = array(
                                ":id" => $filter['id'],
                                ":event_type" => $event_type,
                                ":note" => $note
                            );
                        }
                        else{
                            $new_entries[] = array(
                                ":institute_id" => $institute_id,
                                ":user_id" => $teacher_id,
                                ":event_type" => $event_type,
                                ":date" => $date,
                                ":creator_id" => $creator_id,
                                ":note" => $note
                            );
                        }

                    }
                    //// update  and send data
                    if ($new_entries){
                        try{
                            $columns = "institute_id,user_id,event_type,date,creator_id,note";
                            $values = ":institute_id,:user_id,:event_type,:date,:creator_id,:note";
                            $send = $common->creator("manual_attendance",$columns,$values,$new_entries,true);
                            if (!$send){
                                $return_object['errors'][] = "Attendance new data send failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }
                    if ($update_entries){
                        try{
                            $update_sql = "event_type = :event_type,note = :note where id = :id";
                            $update = $common->modifier("manual_attendance",$update_sql,$update_entries,true);
                            if (!$update){
                                $return_object['errors'][] = "Attendance data update failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }

                    /// get before events of calendar of this date
                    $events_sql = "where date(start_time) = :date and source_id in($join_teacher_ids) and active != :active and type = :type";
                    $events_sql_data = array(
                        ":date" => $date,
                        ":active" => 2,
                        ":type" => "teacher"
                    );
                    $before_events = $common->retriever("calendar","*",$events_sql,$events_sql_data,true);
                    $current_attendance = $common->retriever("manual_attendance","*",$before_attendance_sql,$before_attendance_sql_data,true);
                    $new_event_entries = array();
                    $update_event_entries = array();
                    foreach ($current_attendance as $attendance){
                        $event_type = $attendance['event_type'];
                        $user_id = $attendance['user_id'];
                        if ($event_type != "none"){

                            /* if event type present then entry for present
                                and event type late or early then entry late or early it depend and present also.
                                and event type late and early then entry late and early and present also
                            */
                            if ($event_type == "present"){
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => $event_type,
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types($event_type);
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_type,
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }
                            }
                            elseif ($event_type == "late"){
                                /// check present event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "present",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("present");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }
                                // check event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "late",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("late");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }
                            }
                            elseif ($event_type == "early"){
                                /// check present event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "present",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("present");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }
                                // check event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "early",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("early");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }
                            }
                            elseif ($event_type == "late_and_early"){
                                /// check present event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "present",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("present");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }

                                // check late event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "late",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("late");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }

                                }
                                // check early event
                                $filter = $common->filter_in_array($before_events,array(
                                    "event_type" => "early",
                                    "source_id" => $user_id
                                ),false);
                                if (!$filter){
                                    $event_info = $this->retriever->initial_data->event_types("early");
                                    if ($event_info){
                                        $new_event_entries[] = array(
                                            ":title" => $event_info['name'],
                                            ":type" => "teacher",
                                            ":source_id" => $user_id,
                                            ":event_type" => $event_info['label'],
                                            ":start_time" => $date." "."00:00:00",
                                            ":end_time" => $date." "."00:00:00",
                                            ":color" => $event_info['color'],
                                            ":active" => 1,
                                            ":time" => $common->custom_time(),
                                            ":creator_id" => $creator_id,
                                        );
                                    }
                                    else{
                                        $return_object['errors'][] = $event_type." info not found";
                                    }


                                }

                            }
                        }
                        else{
                            $update_event_entries[] = array(
                                ":source_id" => $user_id,
                                ":start_date" => $date,
                                ":active" => 2,
                                ":creator_id" => "api",
                                ":type" => "teacher",
                            );
                        }


                    }
                    if ($new_event_entries){
                        $columns = "title,type,source_id,event_type,start_time,end_time,color,active,time,creator_id";
                        $values = ":title,:type,:source_id,:event_type,:start_time,:end_time,:color,:active,:time,:creator_id";
                        try{
                            $send = $common->creator("calendar",$columns,$values,$new_event_entries,true);
                            if (!$send){
                                $return_object['errors'][] = "New events data send failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }

                    }
                    if ($update_event_entries){
                        $event_types = array("present","late","early");
                        $join_event_types = $common->sql_in_maker($event_types);
                        $event_update_sql = "active = :active where event_type in($join_event_types) and source_id = :source_id and 
                        date(start_time) = :start_date and creator_id != :creator_id and type = :type";

                        try{
                            $update = $common->modifier("calendar",$event_update_sql,$update_event_entries,true);
                            if (!$update){
                                $return_object['errors'][] = "None event not update";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }
                    /// record for activities this task
                    try{
                        $record = $this->common->add_activities("manual_attendance",$institute_id);
                        if (!$record){
                            $return_object['errors'][] = "Activity record failed";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                    }
                    if (!$common->error_in_object($return_object)){
                        $return_object['status'] = 1;
                    }

                }
                else{
                    $return_object['errors'][] = "Data undefined from calendar event";
                }

            }

            return $return_object;

        }
        function send_notice(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            $time = $common->custom_time();
            $validation = $this->validator->notice_info_validator();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                if (isset($validation['data'])){
                    $data = $validation['data'];
                    $sending_time = $data[':time'];
                    $institute_ids = $_REQUEST['institute_ids'];
                    $new_entries = array();
                    foreach ($institute_ids as $institute_id){
                        $data['receiver'] = $institute_id;
                        $new_entries[] = $data;
                    }
                    if ($new_entries){
                        $columns = "sender,receiver,subject,message,time,active";
                        $values = ":sender,:receiver,:subject,:message,:time,:active";
                        try{
                            $send = $common->creator("notices",$columns,$values,$new_entries,true);
                            if ($send){
                                $return_object['status'] = 1;
                                /// new notices
                                $join_institute_ids = $common->sql_in_maker($institute_ids);
                                $logged_user_id = $common->logged_user_id();
                                $sql = "left join institutes  on institutes.institute_id = notices.receiver 
                                 where notices.sender = :sender and notices.receiver in($join_institute_ids) and notices.time = :time and 
                                 notices.active != :active";
                                $sql_data = array(
                                    ":sender" => $logged_user_id,
                                    ":time" => $sending_time,
                                    ":active" => 2
                                );
                                $new_notice_columns = "notices.*,institutes.name as receiver_name,institutes.logo as receiver_image,
                                concat(extract(day from notices.time),' ',MONTHNAME(notices.time)) as time_string";
                                $new_notices = $common->retriever("notices",$new_notice_columns,$sql,$sql_data,true);
                                $return_object['data'] = $new_notices;

                                /// record for activities this task
                                try{
                                    $record = $this->common->add_activities("send_notice","");
                                    if (!$record){
                                        $return_object['errors'][] = "Activity record failed";
                                    }
                                }catch (Exception $exception){
                                    $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                                }
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = $exception->getMessage();
                        }
                    }


                }
                else{
                    $return_object['errors'][] = "Data undefined from calendar event";
                }

            }

            return $return_object;

        }






    }