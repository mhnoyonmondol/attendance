<?php
    require_once project_root."controllers/packages/vendor/autoload.php";
    $return_object = array(
        "status" => 0,
        "errors" => array(),
        "messages" => array()
    );
    if (!isset($_SESSION['logged_user_id'])){
        $return_object['messages'][] = "Login first";
    }
