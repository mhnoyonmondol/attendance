<?php
    require_once(project_root."controllers/packages/vendor/autoload.php");
    use Rakit\Validation\Validator;
    class data_validator{
        protected $connection;
        public $common;
        public $retriever;
        public $initial;
        function __construct()
        {
            $db = new common();
            $initial = new initial_data();
            $this->retriever = new retrieve();
            $this->initial = $initial;
            $this->common = $db;
            $this->connection = $db->connection;
        }
        function validate($rules = [],$messages = [], $methods = false){

            $validate_object = new stdClass();
            $validate_object->status = true;
            $validate_object->messages = [];
            $data_methods = $_REQUEST + $_FILES;
            if ($methods){
                $data_methods = $methods;
            }
            $validator = new Validator($messages);

            $validation = $validator->validate($data_methods, $rules);

            if ($validation->fails()) {
                // handling errors
                $errors = $validation->errors();
                $error_list = $errors->all();
                $validate_object->status = false;
                $validate_object->messages = $error_list;
            }
            return $validate_object;
        }
        function controller_info_validator($controller_type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'name'                  => 'required',
                'member_id'             => 'required',
//                'email'                 => 'required|email',
                'mobile'                => 'required|numeric|min:10',
                'designation'           => 'required',
//                'joining_date'          => 'required|date',
//                'basic_salary'          => 'required|numeric',
//                'nid'                   => 'required|uploaded_file',
                'password'              => 'required|min:8',
//                'second_contact'        => 'integer|min:10',
            ];
            if ($controller_type !== "system_designer" and $controller_type !== "admin" and $controller_type !== "system_admin"){
                $rules['department'] = "required";
            }
            $edit = false;
            $same_user = false;
            // if editable is true. then nid is optional
            if(isset($_REQUEST['edit'])){
                unset($rules['nid']);
                unset($rules['password']);
                $edit = true;
                if ($_REQUEST['user_id'] == $this->common->logged_user_id()){
                    $same_user = true;
                }
            }
            $validation = $this->validate($rules);
            if (!$validation->status){
                $return_object['messages'] = $validation->messages;
            }
            else{
                $md_5_password = md5($_REQUEST['password']);
                $active = 0;
                if (isset($_REQUEST['active'])){
                    $active = $_REQUEST['active'];
                }
                $signature_switch = 0;
                if (isset($_REQUEST['signature_switch'])){
                    $signature_switch = $_REQUEST['signature_switch'];
                }

                $department = "";
                if (isset($_REQUEST['department'])){
                    $department = $_REQUEST['department'];
                }
                $data = array(
                    ":name" => $_REQUEST['name'],
                    ":member_id" => $_REQUEST['member_id'],
                    ":mobile" => $_REQUEST['mobile'],
                    ":email" => $_REQUEST['email'],
                    ":second_contact" => $_REQUEST['second_contact'],
                    ":password" => $md_5_password,
                    ":designation" => $_REQUEST['designation'],
                    ":joining_date" => $_REQUEST['joining_date'],
                    ":basic_salary" => $_REQUEST['basic_salary'],
                    ":blood_group" => $_REQUEST['blood_group'],
                    ":active" => $active,
                    ":department" => $department,
                    ":address" => $_REQUEST['address'],
                    ":time" => $this->common->custom_time(),
                    ":login_schedule_switch" => $_REQUEST['login_schedule_switch'],
                    ":login_schedule" => $_REQUEST['login_schedule'],
                    ":signature_switch" => $signature_switch,

                );
//                if (!$_REQUEST['login_schedule_switch']){
//                    $data[":login_schedule"] = $_REQUEST['login_schedule'];
//                }

                // mobile number existence
                try{
                    $mobile_full_number = $_REQUEST['mobile'];
                    $mobile = substr($mobile_full_number,-10);
                    $sql = " where mobile like :mobile and active != :active ";
                    $sql_data = array(
                        ":mobile" => "%".$mobile,
                        ":active" => 2,
                    );
                    /// editable is true mobile number will be with not self user id
                    if (isset($_REQUEST['edit'])){
                        $sql .= " and user_id != :user_id";
                        $sql_data[":user_id"] = $_REQUEST['user_id'];
                    }
                    $check_mobile = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                    if ($check_mobile){
                        $return_object['messages'][] = "Mobile number already exist";
                        $return_object['status'] = 0;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = "Mobile check error for ".$exception->getMessage();
                }
//                // email existence
//                try{
//                    $email = $_REQUEST['email'];
//                    $sql = " where email = :email and active != :active ";
//                    $sql_data = array(
//                        ":email" => $email,
//                        ":active" => 2
//                    );
//                    /// editable is true email will be with not self user id
//                    if (isset($_REQUEST['edit'])){
//                        $sql .= " and user_id != :user_id";
//                        $sql_data[":user_id"] = $_REQUEST['user_id'];
//                    }
//                    $check_email = $this->common->retriever("controllers","user_id",$sql,$sql_data);
//                    if ($check_email){
//                        $return_object['messages'][] = "Email already exist";
//                        $return_object['status'] = 0;
//                    }
//                }catch (Exception $exception){
//                    $return_object['errors'][] = "Email check error for ".$exception->getMessage();
//                }

                if (isset($_REQUEST['area'])){
                    // area existence
                    $area = $_REQUEST['area'];
                    $data[':area'] = $area;
                    if ($area){
                        try{
                            $sql = " where area = :area and active != :active ";
                            $sql_data = array(
                                ":area" => $area,
                                ":active" => 2
                            );
                            /// editable is true email will be with not self user id
                            if (isset($_REQUEST['edit'])){
                                $sql .= " and user_id != :user_id";
                                $sql_data[":user_id"] = $_REQUEST['user_id'];
                            }
                            $check_area = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($check_area){
                                $return_object['messages'][] = "Area already assigned to other user";
                                $return_object['status'] = 0;
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Area check error for ".$exception->getMessage();
                        }
                    }

                }




                /// access menu  entry
                if (isset($_REQUEST['menu_ids'])){
                    $menus_ids = $_REQUEST['menu_ids'];
                    $data_for_entry = array();
                    $estimate_menu_ids = $this->initial->estimated_access_menus($controller_type);
                    $user_id = "";
                    $type = "access_menu";
                    $type_option = "system_admin";
                    $active = 0;
                    if ($menus_ids[0] == "all"){
                        $active = 1;
                    }
                    $time = $this->common->custom_time();
                    foreach ($estimate_menu_ids as $item){
                        $menu_id = $item['id'];
                        if ($menus_ids[0] != "all"){
                            if (in_array($menu_id,$menus_ids)){
                                $active = 1;
                            }
                            else{
                                $active = 0;
                            }
                        }


                        $object = array(
                            ":user_id" => $user_id,
                            ":type" => $type,
                            ":type_option" => $type_option,
                            ":active" => $active,
                            ":time" => $time,
                            ":value" => $menu_id,
                        );
                        $data_for_entry[] = $object;
                    }
                    $return_object['estimate_access_menus'] = $data_for_entry;
                }
                else{
                    if (isset($_REQUEST['edit'])){
                        $user_type = $this->common->id_type_name($_REQUEST['user_id']);
                        if ($user_type == "system_admin"){
                            if (isset($_REQUEST['edit']) and ($_REQUEST['user_id'] != $_SESSION['logged_user_id'])){
                                $return_object['messages'][] = "Access menus not set";
                            }
                        }
                    }


                }

                /// permission parts entry
                if (isset($_REQUEST['permission_parts'])){
                    $part_ids = $_REQUEST['permission_parts'];
                    $data_for_entry = array();

                    $estimate_ids = $this->initial->admin_permission_parts();
                    $user_id = "";
                    $type = "permission_part";
                    $type_option = "system_admin";
                    $active = 0;
                    if ($part_ids[0] == "all"){
                        $active = 1;
                    }
                    $time = $this->common->custom_time();
                    foreach ($estimate_ids as $item){
                        $menu_id = $item['id'];
                        if ($part_ids[0] != "all"){
                            if (in_array($menu_id,$part_ids)){
                                $active = 1;
                            }
                            else{
                                $active = 0;
                            }
                        }
                        $object = array(
                            ":user_id" => $user_id,
                            ":type" => $type,
                            ":type_option" => $type_option,
                            ":active" => $active,
                            ":time" => $time,
                            ":value" => $menu_id,
                        );
                        $data_for_entry[] = $object;
                    }
                    $return_object['permission_parts'] = $data_for_entry;
                }
            }
            //// check ... or access menu available or not
            if($controller_type == "system_admin"){
                if (!$same_user){
                    if (!isset($_REQUEST['menu_ids'])){
                        $return_object['messages'][] = "At least one menu required";
                    }

                }

            }
            $return_object['data'] = $data;

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;

        }

        function teacher_info_validator(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }
            $rules = [
                'name'                  => 'required',
                'member_id'             => 'required',
//                'birth_date'             => 'required|date',
                'designation'             => 'required',
                'joining_date'             => 'required|date',
                'mobile'                => 'required|numeric|min:10',
                'password'              => 'required|min:8',
                'institute_id'              => 'required',
                'shift'              => 'required',
            ];

            if (isset($_REQUEST['from_document'])){
                unset($rules['member_id']);
                unset($rules['company_name']);
                unset($rules['email']);
                $_REQUEST['member_id'] = "";
            }
            // if editable is true. then institute id and password is optional
            if(isset($_REQUEST['edit'])){
                unset($rules['password']);
                unset($rules['institute_id']);
            }
            $validation = $this->validate($rules);
            if (!$validation->status){
                $return_object['messages'] = $validation->messages;
            }
            else{
                $joining_date = $_REQUEST['joining_date'];
                $explode = explode("-",$joining_date);
                $year = $explode[0];
                $month = $explode[1];
                $day = $explode[2];
                if (!$this->common->number($year) or !$this->common->number($day) or !$this->common->number($month)){
                    $return_object['messages'][] = "Invalid joining date";
                }

                $md_5_password = md5($_REQUEST['password']);
                $active = 0;
                if (isset($_REQUEST['active'])){
                    $active = $_REQUEST['active'];
                }
                $data = array(
                    ":name" => $_REQUEST['name'],
                    ":member_id" => $_REQUEST['member_id'],
                    ":mobile" => $_REQUEST['mobile'],
                    ":email" => $_REQUEST['email'],
                    ":birth_date" => $_REQUEST['birth_date'],
                    ":educational_qualification" => $_REQUEST['educational_qualification'],
                    ":designation" => $_REQUEST['designation'],
                    ":joining_date" => $_REQUEST['joining_date'],
                    ":permanent_address" => $_REQUEST['permanent_address'],
                    ":first_date_of_job" => $_REQUEST['first_date_of_job'],
                    ":image" => $this->common->default_image("profile"),
                    ":nid" => "",
                    ":active" => $active,
                    ":time" => $this->common->custom_time(),
                    ":institute_id" => $_REQUEST['institute_id'],
                    ":password" => $md_5_password,
                    ":trainings" => "",
                    ":subject_wise_trainings" => "",
                    ":device_user_id" => $_REQUEST['device_user_id'],
                    ":shift" => $_REQUEST['shift'],
                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':institute_id']);
                }
                if (isset($_REQUEST['trainings'])){
                    $trainings = $_REQUEST['trainings'];
                    $data[':trainings'] = json_encode($trainings);
                }
                if (isset($_REQUEST['subject_wise_trainings'])){
                    $trainings = $_REQUEST['subject_wise_trainings'];
                    $data[':subject_wise_trainings'] = json_encode($trainings);
                }

                // mobile number existence
                try{
                    $mobile_full_number = $_REQUEST['mobile'];
                    $mobile = substr($mobile_full_number,-10);
                    $sql = " where mobile like :mobile and active != :active ";
                    $sql_data = array(
                        ":mobile" => "%".$mobile,
                        ":active" => 2,
                    );
                    /// editable is true mobile number will be with not self user id
                    if (isset($_REQUEST['edit'])){
                        $sql .= " and user_id != :user_id";
                        $sql_data[":user_id"] = $_REQUEST['user_id'];
                    }
                    $check_mobile = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                    if ($check_mobile){
                        $return_object['messages'][] = "Mobile number already exist";
                        $return_object['status'] = 0;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = "Mobile check error for ".$exception->getMessage();
                }
                // email existence
//                try{
//                    $email = $_REQUEST['email'];
//                    $sql = " where email = :email and active != :active ";
//                    $sql_data = array(
//                        ":email" => $email,
//                        ":active" => 2
//                    );
//                    /// editable is true email will be with not self user id
//                    if (isset($_REQUEST['edit'])){
//                        $sql .= " and user_id != :user_id";
//                        $sql_data[":user_id"] = $_REQUEST['user_id'];
//                    }
//                    $check_email = $this->common->retriever("controllers","user_id",$sql,$sql_data);
//                    if ($check_email){
//                        $return_object['messages'][] = "Email already exist";
//                        $return_object['status'] = 0;
//                    }
//                }catch (Exception $exception){
//                    $return_object['errors'][] = "Email check error for ".$exception->getMessage();
//                }

                // institute existence
                if (isset($_REQUEST['institute_id'])){
                    $institute_id = $_REQUEST['institute_id'];
                    try{
                        $sql = " where institute_id = :institute_id and active != :active ";
                        $sql_data = array(
                            ":institute_id" => $institute_id,
                            ":active" => 2
                        );
                        $check_institute = $this->common->retriever("institutes","institute_id",$sql,$sql_data);
                        if (!$check_institute){
                            $return_object['messages'][] = "Institute not found";

                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Institute check error for ".$exception->getMessage();
                    }
                    // device user id existence
                    try{

                        $device_user_id = $_REQUEST['device_user_id'];
                        if ($device_user_id){
                            $sql = " where device_user_id = :device_user_id and active != :active and institute_id = :institute_id ";
                            $sql_data = array(
                                ":device_user_id" => $device_user_id,
                                ":active" => 2,
                                ":institute_id" => $institute_id,
                            );
                            /// editable is true device user id will be with not self user id
                            if (isset($_REQUEST['edit'])){
                                $sql .= " and user_id != :user_id";
                                $sql_data[":user_id"] = $_REQUEST['user_id'];
                            }
                            $check_device_user_id = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($check_device_user_id){
                                $return_object['messages'][] = "Device user id already exist";

                            }
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = "Device user id check error for ".$exception->getMessage();
                    }
                }
                else{
                    $return_object['errors'][] = "Institute id undefined";
                }

                $return_object['data'] = $data;


            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;

        }
        function student_info_validator(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $_REQUEST['password'] = $this->common->generateRandomString(8);
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }
            $rules = [
                'name'                  => 'required',
                'member_id'             => 'required',
                'birth_date'             => 'required|date',
                'joining_date'             => 'required|date',
                'mobile'                => 'required|numeric|min:10',
//                'password'              => 'required|min:8',
                'institute_id'              => 'required',
                'class'              => 'required',
                'department'              => 'required',
                'address'              => 'required',
                'shift'              => 'required',
            ];

            // if editable is true. then institute id and password is optional
            if(isset($_REQUEST['edit'])){
                unset($rules['password']);
                unset($rules['institute_id']);
            }
            $validation = $this->validate($rules);
            if (!$validation->status){
                $return_object['messages'] = $validation->messages;
            }
            else{
                $joining_date = $_REQUEST['joining_date'];
                $explode = explode("-",$joining_date);
                $year = $explode[0];
                $month = $explode[1];
                $day = $explode[2];
                if (!$this->common->number($year) or !$this->common->number($day) or !$this->common->number($month)){
                    $return_object['messages'][] = "Invalid joining date";
                }

                $md_5_password = md5($_REQUEST['password']);
                $active = 0;
                if (isset($_REQUEST['active'])){
                    $active = $_REQUEST['active'];
                }
                $data = array(
                    ":name" => $_REQUEST['name'],
                    ":member_id" => $_REQUEST['member_id'],
                    ":mobile" => $_REQUEST['mobile'],
                    ":email" => $_REQUEST['email'],
                    ":birth_date" => $_REQUEST['birth_date'],
                    ":joining_date" => $_REQUEST['joining_date'],
                    ":address" => $_REQUEST['address'],
                    ":image" => $this->common->default_image("profile"),
                    ":active" => $active,
                    ":time" => $this->common->custom_time(),
                    ":institute_id" => $_REQUEST['institute_id'],
                    ":password" => $md_5_password,
                    ":class" => $_REQUEST['class'],
                    ":department" => $_REQUEST['department'],
                    ":shift" => $_REQUEST['shift'],
                    ":device_user_id" => $_REQUEST['device_user_id'],

                    ":session" => $_REQUEST['session'] ?? '',
                    ":gender" => $_REQUEST['gender'] ?? '',
                    ":religion" => $_REQUEST['religion'] ?? '',
                    ":branch" => $_REQUEST['branch'] ?? '',

                    ":father_name" => $_REQUEST['father_name'] ?? '',
                    ":mother_name" => $_REQUEST['mother_name'] ?? '',
                    ":roll_number" => $_REQUEST['roll_number'] ?? '',
                    ":nid_number" => $_REQUEST['nid_number'] ?? '',

                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':institute_id']);
                }

                // mobile number existence
                try{
                    $mobile_full_number = $_REQUEST['mobile'];
                    $mobile = substr($mobile_full_number,-10);
                    $sql = " where mobile like :mobile and active != :active ";
                    $sql_data = array(
                        ":mobile" => "%".$mobile,
                        ":active" => 2,
                    );
                    /// editable is true mobile number will be with not self user id
                    if (isset($_REQUEST['edit'])){
                        $sql .= " and user_id != :user_id";
                        $sql_data[":user_id"] = $_REQUEST['user_id'];
                    }
                    $check_mobile = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                    if ($check_mobile){
                        $return_object['messages'][] = "Mobile number already exist";
                        $return_object['status'] = 0;
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = "Mobile check error for ".$exception->getMessage();
                }
                // email existence
//                try{
//                    $email = $_REQUEST['email'];
//                    $sql = " where email = :email and active != :active ";
//                    $sql_data = array(
//                        ":email" => $email,
//                        ":active" => 2
//                    );
//                    /// editable is true email will be with not self user id
//                    if (isset($_REQUEST['edit'])){
//                        $sql .= " and user_id != :user_id";
//                        $sql_data[":user_id"] = $_REQUEST['user_id'];
//                    }
//                    $check_email = $this->common->retriever("controllers","user_id",$sql,$sql_data);
//                    if ($check_email){
//                        $return_object['messages'][] = "Email already exist";
//                        $return_object['status'] = 0;
//                    }
//                }catch (Exception $exception){
//                    $return_object['errors'][] = "Email check error for ".$exception->getMessage();
//                }

                // institute existence
                if (isset($_REQUEST['institute_id'])){
                    $institute_id = $_REQUEST['institute_id'];
                    try{
                        $sql = " where institute_id = :institute_id and active != :active ";
                        $sql_data = array(
                            ":institute_id" => $institute_id,
                            ":active" => 2
                        );
                        $check_institute = $this->common->retriever("institutes","institute_id",$sql,$sql_data);
                        if (!$check_institute){
                            $return_object['messages'][] = "Institute not found";

                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Institute check error for ".$exception->getMessage();
                    }

                    // device user id existence
                    try{

                        $device_user_id = $_REQUEST['device_user_id'];
                        if ($device_user_id){
                            $sql = " where device_user_id = :device_user_id and active != :active and institute_id = :institute_id ";
                            $sql_data = array(
                                ":device_user_id" => $device_user_id,
                                ":active" => 2,
                                ":institute_id" => $institute_id,
                            );
                            /// editable is true device user id will be with not self user id
                            if (isset($_REQUEST['edit'])){
                                $sql .= " and user_id != :user_id";
                                $sql_data[":user_id"] = $_REQUEST['user_id'];
                            }
                            $check_device_user_id = $this->common->retriever("controllers","user_id",$sql,$sql_data);
                            if ($check_device_user_id){
                                $return_object['messages'][] = "Device user id already exist";

                            }
                        }

                    }catch (Exception $exception){
                        $return_object['errors'][] = "Device user id check error for ".$exception->getMessage();
                    }

                }
                else{
                    $return_object['errors'][] = "Institute id undefined";
                }

                $return_object['data'] = $data;


            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;

        }

        function find_mobile_validation()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'mobile' => 'required|numeric|min:5',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function send_code_validation()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'mobile' => 'required',
                'email' => 'required',
                'user_id' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function confirm_code_validation()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'confirmation_code' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function new_password_validation()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'new_password' => 'required|min:8',
                'repeat_password' => 'required|same:new_password',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }


        function application_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'institute_id' => 'required',
                'receiver_ids' => 'array',
                'receiver_ids.*' => 'required',
                'message' => 'required',
                'start_date' => 'required|date',
                'end_date' => 'required|date',
                'document_sub_type' => 'required',
            ];
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }
            if ($edit){
                $rules['document_row_id'] = "required";
            }

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{

                /// check have any existence by current date range and users
                $document_sub_type = $_REQUEST['document_sub_type'];
                $users = $_REQUEST['receiver_ids'];
                $start_date = $_REQUEST['start_date']." "."00:00:00";
                $end_date = $_REQUEST['end_date']." "."00:00:00";

                if ($start_date > $end_date){
                    $return_object['messages'][] = "End date must be (greater than or equal) start date ";
                }
                $join_users = $this->common->sql_in_maker($users);
                $check_sql = "left join applications on applications.document_row_id = application_items.document_row_id 
                left join documents on documents.id = applications.document_row_id 
                left join controllers on controllers.user_id = application_items.user_id 
                where application_items.user_id in($join_users)  and applications.start_date>=:start_date and applications.end_date<=:end_date and 
                documents.type_option = :type_option and documents.active != :delete and documents.authorization_status != :canceled ";
                $check_sql_data = array(
                    ":delete" => 2,
                    ":canceled" => 2,
                    ":start_date" => $start_date,
                    ":end_date" => $end_date,
                    ":type_option" => $document_sub_type,
                );
                if ($edit){
                    $check_sql .= " and application_items.document_row_id != :document_row_id";
                    $check_sql_data[':document_row_id'] = $_REQUEST['document_row_id'];
                }
                $check_columns = "controllers.name";
                $get_before_data = $this->common->retriever("application_items",$check_columns,$check_sql,$check_sql_data,true);
                foreach ($get_before_data as $user){
                    $return_object['messages'][] = "Application assigned for included date range of (".$user['name'].")";
                }

                $data = array(
                    ":institute_id" => $_REQUEST['institute_id'],
                    ":message" => $_REQUEST['message'],
                    ":start_date" => $start_date,
                    ":end_date" => $end_date,
                    ":application_tag" => $_REQUEST['application_tag'],
                    ":active" => 1,
                    ":time" => $this->common->custom_time(),
                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':active']);
                }
                $application_items = array();

                foreach ($users as $user){
                    $new_object = array(
                        ":user_id" => $user,
                        ":active" => 1,
                        ":time" => $this->common->custom_time(),
                    );
                    $application_items[] = $new_object;
                }
                $return_object['data'] = $data;
                $return_object['items'] = $application_items;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }


        function institute_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }
            $rules = [
                'name' => 'required',
                'code' => 'required',
                'address' => 'required',
            ];

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{
                $active = 0;
                if (isset($_REQUEST['active'])){
                    $active = $_REQUEST['active'];
                }
                
                $in_sms_switch = 0;
                if (isset($_REQUEST['in_sms_switch'])){
                    $in_sms_switch = $_REQUEST['in_sms_switch'];
                }
                $out_sms_switch = 0;
                if (isset($_REQUEST['out_sms_switch'])){
                    $out_sms_switch = $_REQUEST['out_sms_switch'];
                }
                
                
                // check institute existence
                $sql = "where name=:name and active != :active";
                $sql_data = array(
                    ":name" => $_REQUEST['name'],
                    ":active" => 2,
                );
                if ($edit){
                    $institute_id = $_REQUEST['institute_id'];
                    $sql .= " and institute_id != :institute_id";
                    $sql_data[":institute_id"] = $institute_id;
                }
                $check = $this->common->retriever("institutes","id",$sql,$sql_data);
                if ($check){
                    $return_object['messages'][] = "This institute name already exist";
                }
                $data = array(
                    ":name" => $_REQUEST['name'],
                    ":code" => $_REQUEST['code'],
                    ":contact_number" => $_REQUEST['contact_number'],
                    ":address" => $_REQUEST['address'],
                    ":active" => $active,
                    ":in_sms_switch" => $in_sms_switch,
                    ":out_sms_switch" => $out_sms_switch,
                    ":time" => $this->common->custom_time(),
                    ":banner" => $this->common->default_image("long_banner"),
                    ":logo" => $this->common->default_image("logo"),
                );
                if ($edit){
                    unset($data[':time']);
                }
                $return_object['data'] = $data;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function shift_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }
            $rules = [
                'name' => 'required',
            ];
            if ($edit){
                $rules['id'] = "required";
            }

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{
                $active = 1;
                if (isset($_REQUEST['active'])){
                    $active = $_REQUEST['active'];
                }
                // check shift existence
                $sql = "where name=:name and active != :active";
                $sql_data = array(
                    ":name" => $_REQUEST['name'],
                    ":active" => 2,
                );
                if ($edit){
                    $id = $_REQUEST['id'];
                    $sql .= " and id != :id ";
                    $sql_data[':id'] = $id;
                }
                $check = $this->common->retriever("shifts","id",$sql,$sql_data);
                if ($check){
                    $return_object['messages'][] = "This shift name already exist";
                }
                $data = array(
                    ":name" => $_REQUEST['name'],
                    ":active" => $active,
                    ":time" => $this->common->custom_time(),
                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':active']);
                }
                $return_object['data'] = $data;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function sms_balance_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            $rules = [
                'amount' => 'required',
                'institute_id' => 'required',
            ];

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function bulk_sms_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            $rules = [
                'message' => 'required',
                'institute' => 'required',
                'to_type' => 'required',
            ];

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function android_sms_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            $rules = [
                'message' => 'required',
                'number' => 'required',
            ];

            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }





        function area_manager_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $rules = [
                'parent' => 'required',
                'child' => 'required',
                'child_type' => 'required',
//                'sub_childes' => 'required',
            ];



            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }

            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function calendar_event_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }

            $rules = [
                'title' => 'required',
                'start_time' => 'required',
                'end_time' => 'required',
                'color' => 'required',
                'event_type' => 'required',
                'type' => 'required',
            ];
            if ($_REQUEST['type'] != "default"){
                $rules['source_id'] = "required";
            }
            if ($edit){
                unset($rules['start_time']);
                unset($rules['end_time']);
                unset($rules['type']);
            }


            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{
                $end_time = $_REQUEST['end_time']."";
                $end_time = $this->common->next_time(-1,0,0,"Y-m-d H:i:s",$end_time);
                $data = array(
                    ':title' => $_REQUEST['title'],
                    ':type' => $_REQUEST['type'],
                    ':event_type' => $_REQUEST['event_type'],
                    ':start_time' => $_REQUEST['start_time'],
                    ':end_time' => $end_time,
                    ':source_id' => $_REQUEST['source_id'],
                    ':color' => $_REQUEST['color'],
                    ':time' => $this->common->custom_time(),
                    ':active' => 1,
                    ':creator_id' => $this->common->logged_user_id(),
                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':creator_id']);
                    unset($data[':active']);
                    unset($data[':source_id']);
                    unset($data[':type']);
                    unset($data[':start_time']);
                    unset($data[':end_time']);
                }

                $return_object['data'] = $data;

            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function institute_device_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }

            $rules = [
                'institute_id' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{
                // check institute device existence
                $sql = "where active != :active and serial_number = :serial_number";
                $sql_data = array(
                    ":serial_number" => $_REQUEST['serial_number'],
                    ":active" => 2,
                );

                if ($edit){
                    $institute_id = $_REQUEST['institute_id'];
                    $sql .= " and institute_id != :institute_id ";
                    $sql_data[":institute_id"] = $institute_id;
                }
                $check = $this->common->retriever("institute_devices","id",$sql,$sql_data);
                if ($check){
                    $return_object['messages'][] = "This institute device already exist";
                }
                $data = array(
                    ':name' => $_REQUEST['name'],
                    ':serial_number' => $_REQUEST['serial_number'],
                    ':ip' => $_REQUEST['ip'],
                    ':port' => $_REQUEST['port'],
                    ':time' => $this->common->custom_time(),
                    ':active' => 1,
                    ':creator_id' => $this->common->logged_user_id(),
                );
                if ($edit){
                    unset($data[':time']);
                    unset($data[':active']);
                    unset($data[':creator_id']);
                }

                $return_object['data'] = $data;

            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }
        function manual_attendance_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }

            $rules = [
                'institute_id' => 'required',
                'date' => 'required|date',
                'users' => 'array',
                'users.*' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{

                $data = array(
                    ':institute_id' => $_REQUEST['institute_id'],
                    ':date' => $_REQUEST['date'],
                    ':creator_id' => $this->common->logged_user_id(),
                );

                $return_object['data'] = $data;

            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }

        function notice_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }

            $rules = [
                'institute_ids' => 'array',
                'institute_ids.*' => 'required',
                'subject' => 'required',
                'message' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            else{

                $data = array(
                    ':subject' => $_REQUEST['subject'],
                    ':message' => $_REQUEST['message'],
                    ':sender' => $this->common->logged_user_id(),
                    ':time' => $this->common->custom_time(),
                    ':active' => 1,
                );

                $return_object['data'] = $data;

            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }

        function shift_initiate_info_validator()
        {
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $edit = false;
            if (isset($_REQUEST['edit'])){
                $edit = true;
            }

            $rules = [
                'shift' => 'required',
                'user_type' => 'required',
            ];
            $validation = $this->validate($rules);
            if (!$validation->status) {
                $return_object['messages'] = $validation->messages;
            }
            if (!$this->common->error_in_object($return_object)){
                $return_object['status'] = 1;
            }
            return $return_object;
        }






    }
