<?php
    require_once(project_root."controllers/packages/vendor/autoload.php");
    use MatthiasMullie\Minify;
    use Rah\Danpu\Dump;
    use Rah\Danpu\Export;
    use Ifsnop\Mysqldump as IMysqldump;
    class other{
        public $connection;
        public $common;
        public $validator;
        public $retriever;
        function __construct()
        {
            $con = new common();
            $this->validator = new data_validator();
            $this->retriever = new retrieve();
            $this->common = $con;
            $this->connection = $con->connection;
        }


        function install(){
            $db_host = $db_name = $db_user_name = $db_password = null;
            extract($_REQUEST);
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($db_host) and isset($db_name) and isset($db_user_name) and isset($db_password)){

                if (empty($db_host)){
                    $return_object['messages'][] = "Host name empty!";
                }
                if (empty($db_name)){
                    $return_object['messages'][] = "Database name empty!";
                }
                if (empty($db_user_name)){
                    $return_object['messages'][] = "Database user name empty!";
                }

            }
            else{
                $return_object['errors'][] = "Data not defined";
            }
            // Check any error for next execution
            if (count($return_object['errors']) or count($return_object['messages'])){
                return $return_object;
            }

            $config_file = "/controllers/installer/db-config.php";
            $new_data = "";
            $open_file = fopen($config_file,'r+');
            while (!feof($open_file)){
                $line = fgets($open_file);
                $explode = explode('=',$line);
                $variable = trim($explode[0]);
                if ($variable == '$db_host'){
                    $new_data .= "\t".'$db_host = "'.$db_host.'";'."\n";
                }
                elseif($variable == '$db_name'){
                    $new_data .= "\t".'$db_name = "'.$db_name.'";'."\n";
                }
                elseif($variable == '$db_user_name'){
                    $new_data .= "\t".'$db_user_name = "'.$db_user_name.'";'."\n";
                }
                elseif($variable == '$db_password'){
                    $new_data .= "\t".'$db_password = "'.$db_password.'";'."\n";
                }
                else{
                    $new_data .= $line;
                }
            }
            $new_data .= "";
            file_put_contents($config_file,$new_data);
            $return_object['status'] = 1;
            return $return_object;
        }
        function compress(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );

            $css_source_list = array(
                project_root."static/app/bower_components/uikit/css/uikit.almost-flat.min.css",
                project_root."static/app/assets/skins/dropify/css/dropify.css",
                project_root."static/app/assets/css/main.min.css",
                project_root."static/app/assets/css/themes/themes_combined.min.css",
                project_root."static/app/assets/css/login_page.min.css",
                project_root."static/app/assets/css/print.css",
                project_root."static/app/assets/css/style.css"
            );
            $js_source_list = array(
                project_root."static/app/assets/js/angular.min.js",
                project_root."static/app/assets/js/angular-route.min.js",
                project_root."static/app/assets/js/angular-sanitize.min.js",
                project_root."static/app/assets/js/common.min.js",
                project_root."static/app/assets/js/uikit_custom.js",
                project_root."static/app/assets/js/altair_admin_common.js",
                project_root."static/app/assets/js/custom/uikit_fileinput.min.js",
                project_root."static/app/bower_components/dropify/dist/js/dropify.min.js",
                project_root."static/app/bower_components/jquery-ui/jquery-ui.min.js",
                project_root."static/app/bower_components/handlebars/handlebars.min.js",
                project_root."static/app/assets/js/custom/handlebars_helpers.min.js",
                project_root."static/app/assets/js/functions.js",
                project_root."static/app/assets/js/custom.js",
                project_root."static/app/assets/js/app.js",
                project_root."static/app/assets/js/high-density.js",
            );
            // Css compress
            // Start with first css element
            $css_minifier = new Minify\CSS($css_source_list[0]);
            // delete first css element in css list
            unset($css_source_list[0]);
            foreach ($css_source_list as $item){
                $css_minifier->add($item);
            }
            $css_save_path = project_root."static/app/assets/css/compress.min.css";
            $status = $css_minifier->minify($css_save_path);
            if ($status){
                $return_object['messages'][] = "Css compress done";
            }
            else{
                $return_object['errors'][] = "Css compress error";
            }

            // Js compress
            // Start with first js element
            $js_minifier = new Minify\JS($js_source_list[0]);
            // delete first js element in js list
            unset($js_source_list[0]);
            foreach ($js_source_list as $item){
                $js_minifier->add($item);
            }
            $js_save_path = project_root."static/app/assets/js/compress.min.js";
            $status = $js_minifier->minify($js_save_path);
            if ($status){
                $return_object['messages'][] = "Js compress done";
            }
            else{
                $return_object['errors'][] = "Js compress error";
            }
            $return_object['status'] = 1;
            return $return_object;
        }
        function main_database_backup(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            /// generate format backup
            try {
                $db_info = $common->db_info();
                $db_engine = "mysql";
                $db_name = $db_info['db_name'];
                $db_host = $db_info['db_host'];
                $db_user_name = $db_info['db_user_name'];
                $db_password = $db_info['db_password'];
                $dump = new IMysqldump\Mysqldump("$db_engine:host=$db_host;dbname=$db_name", $db_user_name, $db_password);
                $dump->start(project_root.'initializer/attendance-generated.sql');
            } catch (Exception $e) {
                $return_object['errors'][] = 'Backup generate failed with message: ' . $e->getMessage();
            }


            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
                $return_object['messages'][] = "Done";
            }
            return $return_object;
        }
        function main_domain_database_update(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;

            $main_db_info = $common->db_info();
            $requirement = array(
                "old_db" => array(
                    "db_host" => $main_db_info['db_host'],
                    "db_name" => $main_db_info['db_name'],
                    "db_user_name" => $main_db_info['db_user_name'],
                    "db_password" => $main_db_info['db_password'],
                ),
                "latest_db" => array(
                    "db_host" => "localhost",
                    "db_name" => "admin_att_l_copy",
                    "db_user_name" => "admin_att_l_copy",
                    "db_password" => "QOD6PmF8fB",
                ),
            );

            try {
                $db_host = $requirement['latest_db']['db_host'];
                $db_name = $requirement['latest_db']['db_name'];
                $db_user_name = $requirement['latest_db']['db_user_name'];;
                $db_password = $requirement['latest_db']['db_password'];;
                $pdo = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user_name, $db_password);

            } catch (PDOException $e) {
                $return_object['errors'][] = "Online localhost database connection error";
                $return_object['messages'][] = "Upload localhost database";
            }
            $update_db = $common->db_schema_update($requirement);
            $return_object['messages'] = array_merge($update_db['messages'],$return_object['messages']);
            $return_object['errors'] = array_merge($update_db['errors'],$return_object['errors']);
            if (!$common->error_in_object($return_object)){
                $return_object['status'] = 1;
                $return_object['messages'][] = "Done";
            }
            return $return_object;

        }
        function logout(){
            $return_object = array(
                "status" => 1,
                "errors" => array(),
                "messages" => array()
            );
            session_unset();
//            session_destroy();
            return $return_object;
        }
        function deleter($table_name,$id){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                if ($table_name == "shifts"){
                    $check_sql = "where shift = :shift and active != :active";
                    $check_shift_use = $this->common->retriever("controllers","controllers.id",$check_sql,array(
                        ":shift" => $id,
                        ":active" => 2
                    ));
                    if ($check_shift_use){
                        $return_object['messages'][] = "Usable shift delete not allow.";
                        return $return_object;
                    }
                }
                $sql = " active=:active where id=:id";
                $data = array(
                    ":id" => $id,
                    ":active" => 2
                );
                $delete = $this->common->modifier($table_name,$sql,$data);
                if ($delete){
                    $return_object['status'] = 1;
                    $return_object['id'] = $id;
                    $return_object['table_name'] = $table_name;

                    /// record for activities this task
                    $activity = "";
                    $affected_id = "";
                    $sql = "where id = :id";
                    $sql_data = array(
                        ":id" => $id
                    );
                    $find = $this->common->retriever($table_name,"*",$sql,$sql_data);
                    if ($find){
                        if ($table_name == "controllers"){
                            $user_id = $find['user_id'];
                            $id_type_name = $this->common->id_type_name($user_id);
                            $activity = "delete_".$id_type_name;
                            $affected_id = $user_id;
                        }
                        elseif ($table_name == "items"){
                            $affected_id = $find['item_id'];
                            $activity = "delete_item";
                        }
                        elseif ($table_name == "chemists"){
                            $affected_id = $find['user_id'];
                            $activity = "delete_chemist";
                        }
                        elseif ($table_name == "vendors"){
                            $affected_id = $find['user_id'];
                            $activity = "delete_vendor";
                        }
                        elseif ($table_name == "raw_items"){
                            $affected_id = $find['item_id'];
                            $activity = "delete_raw_item";
                        }
                        elseif ($table_name == "batches"){
                            $affected_id = $find['batch_id'];
                            $activity = "delete_batch";
                        }
                        elseif ($table_name == "bank_cashes"){
                            $affected_id = $find['bank_id'];
                            $activity = "delete_bank_cash";
                        }
                        elseif ($table_name == "documents"){
                            $affected_id = $find['document_id'];
                            $activity = "delete_application";
                        }


                        try{
                            $record = $this->common->add_activities($activity,$affected_id);
                            if (!$record){
                                $return_object['errors'][] = "Activity record failed";
                            }
                        }catch (Exception $exception){
                            $return_object['errors'][] = "Record activities failed for ".$exception->getMessage();
                        }
                    }


                }
                else{
                    $return_object['errors'][] = "Delete failed!";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Delete fail for ".$exception->getMessage();
            }

            return $return_object;
        }
        function permanent_deleter($table_name,$column_name,$value){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                $sql = "where $column_name=:$column_name";
                $data = array(
                    ":$column_name" => $value,
                );
                $delete = $this->common->deleter($table_name,$sql,$data);
                if ($delete){
                    $return_object['status'] = 1;
                }
                else{
                    $return_object['errors'][] = "Delete failed!";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = "Delete fail for ".$exception->getMessage();
            }

            return $return_object;
        }


        function document_helper_delete(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['document_type']) and isset($_REQUEST['document_type_option']) and isset($_REQUEST['type'])){
                $document_type = $_REQUEST['document_type'];
                $document_type_option = $_REQUEST['document_type_option'];
                $type = $_REQUEST['type'];
                $value = $_REQUEST['value'];
                $user_cookies = $this->common->user_cookies_data();
                $logged_user_id = $user_cookies['logged_user_id'];

                try{
                    $sql = "where document_type=:document_type and document_type_option = :document_type_option 
                    and user_id=:user_id and type=:type and value=:value";
                    $data = array(
                        ":document_type" => $document_type,
                        ":document_type_option" => $document_type_option,
                        ":user_id" => $logged_user_id,
                        ":type" => $type,
                        ":value" => $value,
                    );
                    $delete = $this->common->deleter("document_helpers",$sql,$data);
                    if ($delete){
                        $return_object['status'] = 1;
                    }
                    else{
                        $return_object['errors'][] = "Delete failed!";
                    }
                }catch (Exception $exception){
                    $return_object['errors'][] = "Delete fail for ".$exception->getMessage();
                }
            }
            else{
                $return_object['errors'][] = "Information empty for delete";
            }


            return $return_object;
        }



        function send_code_forgot_user(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $validation = $this->validator->send_code_validation();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $user_id = $_REQUEST['user_id'];
                $mobile = $_REQUEST['mobile'];
                $email = $_REQUEST['email'];
                $confirmation_number = rand(100000, 999999);
                $conf_object = array(
                    "number" => $confirmation_number,
                    "expire" => $this->common->next_time(0,0,5),
                    "user_id" => $user_id,
                );
                /// save the session info
                $_SESSION['confirmation'] = $conf_object;
                // confirm again this user info is correct
                $sql = "where user_id = :user_id and active = :active and email = :email and mobile = :mobile";
                $sql_data = array(
                    ":user_id" => $user_id,
                    ":active" => 1,
                    ":email" => $email,
                    ":mobile" => $mobile
                );

                $find = $this->common->retriever("controllers","*",$sql,$sql_data);
                if ($find){
                    $user_email = $find['email'];
                    $user_name = $find['name'];

                    $from_email = "info.tims@page71.org";
                    $from_name = "Tims";
                    $software_info = $this->retriever->software_info();
                    if (isset($software_info['company_name'])){
                        $from_name = $software_info['company_name'];
                    }
                    if (isset($software_info['email'])){
                        $from_email = $software_info['email'];
                    }

                    $from_info = array(
                        "name" => $from_name,
                        "email" => $from_email
                    );
                    $to_info = array(
                        "name" => $user_name,
                        "email" => $user_email
                    );
                    $subject = "New password confirmation code";
                    $messages = "<p>Your password recovery confirmation code is below</p>";
                    $messages .= "<h2>".$confirmation_number."</h2>";
                    // send this mail
                    try{
                        $send = $this->common->mail_sender($from_info,$to_info,$subject,$messages);
                        if ($send){
                            $return_object['status'] = 1;
                            $return_object['confirmation_code'] = $confirmation_number;
                        }
                        else{
                            $return_object['errors'][] = "Code send fail";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = "Code send fail for ".$exception->getMessage();
                    }
                }
                else{
                    $return_object['errors'][] = "Wrong information";
                }


            }
            return $return_object;
        }
        function confirm_code(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $validation = $this->validator->confirm_code_validation();
            if (!$validation['status']){
                $messages = $validation['messages'];
                $errors = $validation['errors'];
                $return_object['errors'] = $errors;
                $return_object['messages'] = $messages;

            }
            else{
                $confirmation_code = $_REQUEST['confirmation_code'];
                if (isset($_SESSION['confirmation'])){
                    $confirmation_info = $_SESSION['confirmation'];
                    $expire = $confirmation_info['expire'];
                    $number = $confirmation_info['number'];
                    if ($expire < date("Y-m-d H:i:s")){
                        $return_object['messages'][] = "Token expire";
                    }
                    elseif($confirmation_code != $number){
                        $return_object['messages'][] = "Confirmation code not matched!";
                    }
                    else{
                        $return_object['status'] = 1;
                    }
                }
                else{
                    $return_object['messages'][] = "We don't have any secret code from you";
                }
            }
            return $return_object;
        }

        function document_deleter($document_id,$document_sub_type){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            $common = $this->common;
            require_once project_root."controllers/handler/security.php";
            if ($common->error_in_object($return_object)){
                return $return_object;
            }
            try{
                /// delete/deactive also parts of this document like calendar events
                if ($document_sub_type == "leave" or $document_sub_type == "training"){
                    try{
                        $delete = $this->application_delete($document_id);
                        if (!$delete){
                            $return_object['errors'][] = "Document delete error in ".$document_sub_type." for ";
                        }
                    }catch (Exception $exception){
                        $return_object['errors'][] = $exception->getMessage();
                    }

                }
                elseif($document_sub_type == "gdn"){
                    $delete = $this->gdn_data_deleter($document_id);
                    if (!$delete){
                        $return_object['errors'][] = "Document delete error in ".$document_sub_type;
                    }
                }
                else{
                    $return_object["errors"][] = "Document sub type undefined";
                }
                if (!$common->error_in_object($return_object)){
                    // deactivate this document data
                    $delete_sql = "active=:active where document_id = :document_id";
                    $delete_sql_data = array(
                        ":document_id" => $document_id,
                        ":active" => 2
                    );
                    $document_delete = $common->modifier("documents",$delete_sql,$delete_sql_data);
                    if (!$document_delete){
                        $return_object['errors'][] = "Document delete error";
                    }
                    else{
                        $action = "delete_".$document_sub_type;
                        $add_activity = $common->add_activities($action,$document_id);
                        if (!$add_activity){
                            $return_object['errors'][] = "Activity not added";
                        }
                        $return_object['status'] = 1;
                    }
                }


            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }


            return $return_object;
        }
        function application_delete($document_row_id){
            $result = false;
            $common = $this->common;
            try{
                /// delete calendar events
                $sql = "active = :delete where document_row_id = :document_row_id";
                $sql_data = array(
                    ":delete" => 2,
                    ":document_row_id" => $document_row_id
                );
                $delete = $common->modifier("calendar",$sql,$sql_data);
                if ($delete){
                    /// user deleter
                    $action = $this->deleter("documents",$document_row_id);
                    if ($action['status']){
                        $result = true;
                    }
                }

            }catch (Exception $exception){

            }



            return $result;
        }


        function tags_deleter(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            try{
                if (isset($_REQUEST['tags'])){
                    $tags = explode(",",$_REQUEST['tags']);
                    $sql = "active=:active where id=:id";
                    $delete_data = array();
                    foreach ($tags as $tag_id){
                        $delete_data[] = array(
                            ":active" => 2,
                            ":id" => $tag_id
                        );
                    }
                    $delete = $this->common->modifier("tags",$sql,$delete_data,true);
                    if (!$delete){
                        $return_object['errors'][] = "Tags delete failed";
                    }
                    else{
                        $return_object['status'] = 1;
                    }
                }
                else{
                    $return_object['errors'][] = "Tags empty";
                }
            }catch (Exception $exception){
                $return_object['errors'][] = $exception->getMessage();
            }
            return $return_object;
        }

        function marked_notice_delete(){
            $return_object = array(
                "status" => 0,
                "errors" => array(),
                "messages" => array()
            );
            if (isset($_REQUEST['ids'])){
                $update_entries = array();
                foreach ($_REQUEST['ids'] as $id){
                    $update_entries[] = array(
                        ":id" => $id,
                        ":active" => 2
                    );

                }
                try{
                    $update_sql = "active = :active where id = :id";
                    $update = $this->common->modifier("notices",$update_sql,$update_entries,true);
                    if ($update){
                        $return_object['status'] = 1;
                    }
                    else{
                        $return_object['errors'][] = "Marked notice delete failed";
                    }

                }catch (Exception $exception){
                    $return_object['errors'][] = $exception->getMessage();
                }
            }
            return $return_object;
        }


    }