<?php
header('Access-Control-Allow-Origin: *');

require_once "../../define.php";
require_once(project_root."controllers/packages/vendor/autoload.php");
$return_object = array(
    "status" => 0,
    "errors" => array(),
    "messages" => array(),
    "data" => array()
);

$common = new common();

try{
    $sql = "where active = :active order by id desc";
    $get_last_sms = $common->retriever("android_sms_records","*",$sql,array(
        ":active" => 0
    ));

    if ($get_last_sms){
        $get_last_sms['mobile'] = $common->make_proper_mobile_number($get_last_sms['mobile']);

        $sql = "active = :active where id = :id";
        $update_sms = $common->modifier("android_sms_records",$sql,array(
            ":active" => 1,
            ":id" => $get_last_sms['id']
        ));

        $return_object['data'] = $get_last_sms;
        $return_object['status'] = 1;

    }
}catch (Exception $exception){

}


header('Content-Type: application/json');
echo json_encode($return_object);