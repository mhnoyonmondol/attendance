<?php
/**
 * Created by PhpStorm.
 * User: bizca
 * Date: 11/14/2021
 * Time: 10:17 AM
 */

class AttendanceApi
{
    public $common;
    public $retriever;
    public $present_type_info;
    public $late_type_info;
    public $early_type_info;
    public $institute_settings;
    public $machine;
    public $shifts = array();
    public $machine_sn;
    public $current_date;
    public $current_time;
    public $net_api_sms_balance = 0;

    public function __construct($common,$retriever,$machine_sn,$net_api_sms_balance)
    {

        $this->common = $common;
        $this->retriever = $retriever;
        if (!$common){
            $this->common = new common();
        }

        if (!$retriever){
            $this->retriever = new retrieve();
        }

        $this->machine_sn = $machine_sn;
        $this->net_api_sms_balance = $net_api_sms_balance;
        $this->current_date = date("Y-m-d");
        $this->current_time = date("Y-m-d H:i:s");

        $this->present_type_info = $this->common->initial_data->event_types("present");
        $this->late_type_info = $this->common->initial_data->event_types("late");
        $this->early_type_info = $this->common->initial_data->event_types("early");

        $this->machine = $this->get_machine($machine_sn);
        $this->institute_settings = $this->get_institute_settings($this->machine['institute_id'] ?? '');

        $shifts = $this->retriever->shifts(true);
        $this->shifts = $shifts['find_data'] ?? array();


    }
    public function get_institute_settings($institute_id){
        /// get institute settings
        $institutes_settings_sql = "where type_option = :type_option and source = :source_id";
        $institutes_settings_sql_data = array(
            ":type_option" => "institute_settings",
            ":source_id" => $institute_id,
        );
        $institute_settings = $this->common->retriever("settings","*",$institutes_settings_sql,$institutes_settings_sql_data,true);

        $settings = $this->retriever->institute_settings("",true);
        foreach ($settings as $key => $value){
            $filter_keys = array(
                "type" => $key
            );
            $filter = $this->common->filter_in_array($institute_settings,$filter_keys,false);
            if($filter){
                $settings[$key] = $filter['value'];
            }

        }
        return $settings;
    }
    public function get_machine($machine_sn){
        //// machines by data
        $machine_sql = "left join institutes on institutes.institute_id = institute_devices.institute_id 
        where institute_devices.serial_number = :sn and institute_devices.active != :active";
        $machine_columns = "institute_devices.*,institutes.in_sms_switch,institutes.out_sms_switch";
        $machine_sql_data = array(
            ":active" => 2,
            ":sn" => $machine_sn,
        );

        return $this->common->retriever("institute_devices",$machine_columns,$machine_sql,$machine_sql_data);
    }

    public function check_before_calendar_assign($attendance_time,$user_id){

        $event_types = array("present","late","early");
        $join_event_types = $this->common->sql_in_maker($event_types);
        $before_events_sql = "where source_id = :source_id and active != :active and
        DATE(start_time) = :start_date and event_type in($join_event_types)";
        $before_events_sql_data = array(
            ":active" => 2,
            ":start_date" => $this->common->text_date_time("Y-m-d",$attendance_time),
            ":source_id" => $user_id
        );
        $before_events_sql_columns = "*,date(start_time) as start_date";
        $before_event = $this->common->retriever("calendar",$before_events_sql_columns,$before_events_sql,$before_events_sql_data,true);
        return $before_event;
    }

    public function get_attendance_event_type($shift_settings,$attendance_time){
        $event_type = "present";

        $attendance_date = $this->common->text_date_time("Y-m-d",$attendance_time);
        $time_format = "Y-m-d H:i:s";
        /// in time settings
        $in_time = $attendance_date . " " . $shift_settings['in_time'] . ":00";
        $late_time = $this->common->next_time(0, 0, $shift_settings['late_time'], $time_format, $in_time);
        $min_in_time = $attendance_date . " " . $shift_settings['in_range_from'] . ":00";
        $max_in_time = $attendance_date . " " . $shift_settings['in_range_to'] . ":00";

        if ($attendance_time >= $min_in_time and $attendance_time <= $max_in_time) {
            if ($attendance_time <= $late_time) {
                $event_type = "present";
            }
            else{
                $event_type = "late";
            }
        }

        $out_time = $attendance_date." ".$shift_settings['out_time'].":00";
        $early_time = $this->common->next_time(0,0,$this->common->negative_number($shift_settings['early_time']),$time_format,$out_time);
        $min_out_time = $attendance_date." ".$shift_settings['out_range_from'].":00";
        $max_out_time = $attendance_date." ".$shift_settings['out_range_to'].":00";
        if ($attendance_time >= $min_out_time and $attendance_time <= $max_out_time){
            if ($attendance_time >= $early_time){

            }
            else{
                $event_type = "early";
            }
        }

        return $event_type;
    }

    public function assign_event($event_type,$user_id,$attendance_time,$before_calendar_events){
        $new_event = array();
        $update_event = array();
        $attendance_date = $this->common->text_date_time("Y-m-d",$attendance_time);
        $event_title = $this->present_type_info['name'];
        $event_color = $this->present_type_info['color'];

        if ($event_type == "late"){
            $event_title = $this->late_type_info['name'];
            $event_color = $this->late_type_info['color'];
        }
        elseif ($event_type == "early"){
            $event_title = $this->early_type_info['name'];
            $event_color = $this->early_type_info['color'];
        }


        if ($event = $this->common->filter_in_array($before_calendar_events,array(
            "event_type" => $event_type
        ),false)){

            $update_event = array(
                ":id" => $event['id'],
                ":color" => $event_color,
                ":title" => $event_title,
                ":event_type" => $event_type,
                ":time_for_event" => $attendance_time
            );
        }
        else{
            $new_event = array(
                ":type" => $this->common->id_type_name($user_id),
                ":source_id" => $user_id,
                ":event_type" => $event_type,
                ":title" => $event_title,
                ":color" => $event_color,
                ":time" => $this->current_time,
                ":active" => 1,
                ":start_time" => $attendance_date." "."00:00:00",
                ":end_time" => $attendance_date." "."00:00:00",
                ":creator_id" => "api",
                ":time_for_event" => $attendance_time
            );
        }

        if ($update_event and $event_type == "early"){
            $sql = "event_type=:event_type,color=:color,title=:title,time_for_event=:time_for_event where id = :id";
            $update = $this->common->modifier("calendar",$sql,$update_event);
            if (!$update){
                throw new Exception("Events update failed");
            }
        }

        if ($new_event){
            $columns = "title,type,source_id,event_type,start_time,end_time,color,active,time,creator_id,time_for_event";
            $values = ":title,:type,:source_id,:event_type,:start_time,:end_time,:color,:active,:time,:creator_id,:time_for_event";
            $send = $this->common->creator("calendar",$columns,$values,$new_event);
            if (!$send){
                throw new Exception("New event data send failed");
            }
        }
        return true;

    }


    public function assign_calendar_event($event_type,$user_id,$attendance_time,$user_info=null){
        $result = false;
        $sms_send_possibility = false;
        $before_calendar_events = $this->check_before_calendar_assign($attendance_time,$user_id);
        if ($event_type == "present"){
            $result = $this->assign_event($event_type,$user_id,$attendance_time,$before_calendar_events);
        }
        else{
            $result = $this->assign_event($event_type,$user_id,$attendance_time,$before_calendar_events);
            $result = $this->assign_event("present",$user_id,$attendance_time,$before_calendar_events);

        }


        if ($event_type == "present" or $event_type == "late"){
            if (!$this->common->filter_in_array($before_calendar_events,array(
                "event_type" => "present"
            ),false)){
                $sms_send_possibility = true;
            }
        }
        elseif($event_type == "early"){
            if (!$this->common->filter_in_array($before_calendar_events,array(
                "event_type" => "early"
            ),false)){
                $sms_send_possibility = true;
            }
        }


        if ($sms_send_possibility){
            $this->send_sms($attendance_time,$user_info,$this->machine,$event_type);

        }
        return $result;
    }

    public function send_sms($attendance_time,$user_info,$institute_info,$event_type){
        if ($this->common->text_date_time("Y-m-d",$attendance_time) == date("Y-m-d")){
            if ($this->net_api_sms_balance){
                if ($user_info){
                    $check_type = "in";
                    if ($event_type == "early"){
                        $check_type = "out";
                    }

                    $sms_switch = $institute_info['in_sms_switch'];
                    if ($check_type == "out"){
                        $sms_switch = $institute_info['out_sms_switch'];
                    }

                    if ($sms_switch){
                        $sms_requirement = array(
                            "receiver_id" => $user_info['user_id'],
                            "receiver_type" => "controller",
                            "institute_id" => $user_info['institute_id'],
                            "mobile" => $user_info['mobile'],
                        );

                        try{
                            $message = "Present at ";
                            if ($event_type == "late"){
                                $message = "Late present at ";
                            }
                            elseif ($event_type == "early"){
                                $message = "Left early at ";
                            }

                            $message = $user_info['name']." ".$message;

                            $message .= $attendance_time;
                            $this->common->send_sms($message,$sms_requirement,$this->current_time,null,$institute_info);
                            $this->net_api_sms_balance++;
                        }catch (Exception $exception){

                        }

                    }

                }
            }

        }
    }

    public function generate_calendar_event($user,$attendance_time){
        $return_object = array(
            "status" => 0,
            "errors" => array(),
            "messages" => array()
        );

        $user_id = $user['user_id'];
        $shift_info = $this->common->filter_in_array($this->shifts,array(
            "id" => $user['shift']
        ),false);
        $shift_settings = $shift_info['settings'] ?? array();
        if (!$shift_settings ){
            $return_object['messages'][] = "Shift settings not found of ".$user_id;
        }
        if (!$this->machine){
            $return_object['messages'][] = "Machine not found of ".$this->machine_sn;
        }

        if (!$this->common->error_in_object($return_object)){
            $event_type  = $this->get_attendance_event_type($shift_settings,$attendance_time);

            try{
                $assign = $this->assign_calendar_event($event_type,$user_id,$attendance_time,$user);
                if ($assign){
                    $return_object['status'] = 1;
                }
            }catch (Exception $exception){
                $return_object['messages'][] = $exception->getMessage();
            }

        }

        return $return_object;


    }
}