<?php
header('Access-Control-Allow-Origin: *');
/* request data params

    data = json string <=>
                        array(
                            array(
                                "user_id" => "",
                                "check_type => "", i||o
                                "check_time" => "timestamp",
                            )

                        )



    request_type = attendance
    machine_sn = 'xxxxxxxxxx'
*/
require_once "../../define.php";
require_once(project_root."controllers/packages/vendor/autoload.php");
$return_object = array(
    "status" => 0,
    "errors" => array(),
    "messages" => array()
);

$common = new common();
$retriever = new retrieve();
$time = $common->custom_time();
$net_sms_balance = $common->get_api_sms_balance();

if (isset($_REQUEST['data']) and isset($_REQUEST['machine_sn']) and isset($_REQUEST['request_type'])){
    $check_type_exchange = array(
        "I" => "in",
        "O" => "out",
        "i" => "in",
        "o" => "out",
        "0" => "in",
        "1" => "out",
        "in" => "in",
        "out" => "out",
    );

    $request_type = $_REQUEST['request_type'];
    $machine_sn = $_REQUEST['machine_sn'];
    $data = json_decode($_REQUEST['data'],true);

    try{

        if ($net_sms_balance < 100){
            $notify_data = $common->retriever("sms_records","id","where receiver_id = :receiver_id and date(time) = :date",array(
                ":receiver_id" => 'alamin_vai',
                ":date" => date("Y-m-d")
            ));

            if (!$notify_data){
                $notify_columns = "receiver_id,receiver_type,time,message,amount,active,mobile,institute_id";
                $notify_values = ":receiver_id,:receiver_type,:time,:message,:amount,:active,:mobile,:institute_id";
                $common->creator("sms_records",$notify_columns,$notify_values,array(
                    ":receiver_id" => 'alamin_vai',
                    ":receiver_type" => 'admin',
                    ":institute_id" => '',
                    ":mobile" => "01756018979",
                    ":time" => $time,
                    ":active" => 2,
                    ":amount" => 1,
                    ":message" => "SMS Quota finished purchase some sms for (sms api)",
                ));

                if ($net_sms_balance){
                    $common->sms_sender("SMS Quota finished purchase some sms for (sms api)","01756018979");
                }

                $net_sms_balance++;
            }



        }
    }catch (Exception $exception){
//            $return_object['errors'][] = $exception->getMessage();
    }

    $user_ids = array();
    foreach ($data as $attendance){
        $user_ids[] = $attendance['user_id'];
    }

    $attendance_api_instance = new AttendanceApi($common,$retriever,$machine_sn,$net_sms_balance);

    if (!$attendance_api_instance->machine){
        $return_object['messages'][] = "Machine not found";

    }
    else{
        $institute_device_row_id = $attendance_api_instance->machine['id'];
        $institute_id = $attendance_api_instance->machine['institute_id'];

        //// user ids by data
        $join_user_ids = $common->sql_in_maker($user_ids);
        $users_sql = "where device_user_id in($join_user_ids) and institute_id = :institute_id and active != :active and 
        institute_id is not null and institute_id != :empty";
        $users_columns = "controllers.user_id,controllers.institute_id,controllers.device_user_id,controllers.mobile,controllers.name,controllers.shift";
        $users_sql_data = array(
            ":active" => 2,
            ":empty" => "",
            ":institute_id" => $attendance_api_instance->machine['institute_id'] ?? '',
        );
        $users = $common->retriever("controllers",$users_columns,$users_sql,$users_sql_data,true);

        $check_in_user_ids = array();
        $check_out_user_ids = array();
        /// arrange data
        ///

        $new_entries = array();

        foreach ($data as $attendance){
            $attendance_type = $attendance['check_type'];
            $attendance_time = $attendance['check_time'];
            $machine_serial_number = $machine_sn;
            $machine_user_id = $attendance['user_id'];

            $user_id = "";


            $filter_keys = array(
                "device_user_id" => $machine_user_id,
                "institute_id" => $institute_id
            );
            $filter_user = $common->filter_in_array($users,$filter_keys,false);
            if ($filter_user){
                $user_id = $filter_user['user_id'];
            }
            else{
                $return_object['errors'][] = "User not found (".$machine_user_id.")";
            }

            $check_type = $check_type_exchange[$attendance_type] ?? 'in';
            if ($check_type == "in"){
                $check_in_user_ids[] = $user_id;
            }
            elseif ($check_type == "out"){
                $check_out_user_ids[] = $user_id;
            }
            $new_entries[] = array(
                ":attendance_type" => $check_type,
                ":attendance_time" => $attendance_time,
                ":institute_device_row_id" => $institute_device_row_id,
                ":user_id" => $user_id,
                ":device_user_id" => $machine_user_id,
                ":time" => $time,
                ":active" => 1,
                ":institute_id" => $institute_id
            );
        }

        $join_check_in_user_ids = $common->sql_in_maker(array_unique($check_in_user_ids));
        $join_check_out_user_ids = $common->sql_in_maker(array_unique($check_out_user_ids));

        $filtered_data = array();
        /// check and delete before attendance longs


        //// remove before existence in time

        $attendance_time_list = $common->filter_in_array($new_entries,array(
            ":institute_id" => $institute_id,
            ":attendance_type" => "in"
        ),true,false,true);
        $attendance_times = array();
        foreach ($attendance_time_list as $attendance){
            $attendance_times[] = $attendance[':attendance_time'];
        }
        $join_times = $common->sql_in_maker($attendance_times);

        $sql = "where institute_id = :institute_id and user_id in($join_check_in_user_ids) and active != :active and attendance_time in ($join_times) and attendance_type = :attendance_type ";
        $sql_data = array(
            ":active" => 2,
            ":attendance_type" => "in",
            ":institute_id" => $institute_id,
        );
        $before_times = $common->retriever("attendance_logs","attendance_time",$sql,$sql_data,true);
        foreach ($before_times as $attendance){
            $attendance_time = $attendance['attendance_time'];
            $index = $common->index_number_in_array($attendance_time_list,":attendance_time",$attendance_time);
            if (strlen($index)){
                array_splice($attendance_time_list,$index,1);
            }
        }
        $filtered_data = array_merge($filtered_data,$attendance_time_list);

        //// remove existence out time

        $attendance_time_list = $common->filter_in_array($new_entries,array(
            ":institute_id" => $institute_id,
            ":attendance_type" => "out"
        ),true,false,true);

        $attendance_times = array();
        foreach ($attendance_time_list as $attendance){
            $attendance_times[] = $attendance[':attendance_time'];
        }
        $join_times = $common->sql_in_maker($attendance_times);
        $sql = "where institute_id = :institute_id and user_id in($join_check_out_user_ids) and active != :active and attendance_time in ($join_times) and attendance_type = :attendance_type ";
        $sql_data = array(
            ":active" => 2,
            ":attendance_type" => "out",
            ":institute_id" => $institute_id,
        );
        $before_times = $common->retriever("attendance_logs","attendance_time",$sql,$sql_data,true);
        foreach ($before_times as $attendance){
            $attendance_time = $attendance['attendance_time'];
            $index = $common->index_number_in_array($attendance_time_list,":attendance_time",$attendance_time);
            if (strlen($index)){
                array_splice($attendance_time_list,$index,1);
            }
        }

        $filtered_data = array_merge($filtered_data,$attendance_time_list);

        $new_entries = array_values($filtered_data);
    }


    if ($new_entries){
        /// save new entries
        $columns = "user_id,device_user_id,institute_device_row_id,attendance_type,attendance_time,time,active,institute_id";
        $values = ":user_id,:device_user_id,:institute_device_row_id,:attendance_type,:attendance_time,:time,:active,:institute_id";
        try{
            $send = $common->creator("attendance_logs",$columns,$values,$new_entries,true);
            if ($send){

                foreach ($new_entries as $item){
                    $user = $common->filter_in_array($users,array(
                        "user_id" => $item[':user_id']
                    ),false);
                    if ($user){
                        $generate_calendar_event = $attendance_api_instance->generate_calendar_event($user,$item[':attendance_time']);
                        if (!$generate_calendar_event['status']){
                            $return_object['messages'] = array_merge($return_object['messages'],$generate_calendar_event['messages']);
                            $return_object['errors'] = array_merge($return_object['errors'],$generate_calendar_event['errors']);
                        }
                    }
                    else{
                        $return_object['errors'] = "User not found";
                    }

                }


                if (!$common->error_in_object($return_object)){
                    $return_object['status'] = 1;
                }

            }
            else{
                $return_object['errors'][] = "Attendance data send failed";
            }
        }catch (Exception $exception){
            $return_object['errors'][] = $exception->getMessage();
        }
    }
    else{
        $return_object['status'] = 1;
    }

}
else{
    $return_object['messages'][] = "Invalid format of your request";
}

header('Content-Type: application/json');
echo json_encode($return_object);

?>