<?php
header('Access-Control-Allow-Origin: *');

require_once "../../define.php";
require_once(project_root."controllers/packages/vendor/autoload.php");
$return_object = array(
    "status" => 0,
    "errors" => array(),
    "messages" => array(),
    "data" => array()
);

$common = new common();
$delivered_id = $_REQUEST['id'] ?? 0;
try{

    $sql = "active = :active where id = :id";
    $update_sms = $common->modifier("android_sms_records",$sql,array(
        ":active" => 1,
        ":id" => $delivered_id
    ));

    if ($update_sms){
        $return_object['status'] = 1;

    }
}catch (Exception $exception){

}


header('Content-Type: application/json');
echo json_encode($return_object);