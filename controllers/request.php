<?php
    require_once "../define.php";
    require_once(project_root.'controllers/packages/vendor/autoload.php');
    $return_object = array(
        "status" => 0,
        "messages" => array(),
        "errors" => array()
    );
    extract($_REQUEST);
    $common = new common();
    $retriever = new retrieve();
    $software_info = $retriever->software_info(true);
    $construction_mode = $software_info['construction_mode'];
    if (isset($request_name) and isset($request_type)){
        $class = "";
        $object_name = "";
        $data = array();
        // try find out class name not empty
        if ($request_type == "get"){
            $class = "retrieve()";
        }
        elseif ($request_type == "update"){
            $class = "modify()";
        }
        elseif ($request_type == "post"){
            $class = "passed()";
        }
        elseif ($request_type == "other"){
            $class = "other()";
        }
        else{
            $return_object['errors'][] = "Request type undefined";
        }
        // if class name not empty
        if ($class){
            if ($common->logged_user_id()){
                $logged_user_type = $common->logged_user_type();
                if ($logged_user_type != "system_designer" and  $construction_mode['active']){
                    if ($request_type == "post" or $request_type == "update" ){
                        echo "Please wait some time. Because construction mode active";
                        die();
                    }

                }
            }

            $object_name = '$object = new '.$class.';';
            eval($object_name);
            $data_string = '$data = $object->'.$request_name.';';
            eval($data_string);
        }
        else{
            $return_object['errors'][] = "Class name not found!";
        }
        echo json_encode($data);
    }
    else{
        $return_object['errors'][] = "Request name or type not found!";
        $return_object['form_data'][] = $_REQUEST;
        echo json_encode($return_object);
    }
?>