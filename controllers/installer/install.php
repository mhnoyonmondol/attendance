<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>
    <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
    <title>App installer</title>
    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>
    <!-- uikit -->
    <link rel="stylesheet" href="./static/app/bower_components/uikit/css/uikit.almost-flat.min.css"/>
    <!-- altair admin login page -->
    <link rel="stylesheet" href="./static/app/assets/css/login_page.min.css" />
    <style>
        .error-box.active{
            right: 0;
        }
        .error-box{
            position: fixed;
            right: -350px;
            top: 70px;
            width: 350px;
            transition: right 0.3s;
        }


    </style>
</head>
<body class="login_page">

<div class="login_page_wrapper">
    <h3 class="heading_a uk-margin-bottom">Install the app</h3>
    <div class="md-card" id="login_card">
        <div class="md-card-content large-padding" id="login_form">
            <div class="login_heading">
                <div class="user_avatar" style="background-image:url('./static/app/assets/img/default/heshablogo.png')"></div>
            </div>
            <p class="uk-text-danger error"></p>
            <form class="install">
                <div class="uk-form-row">
                    <label for="host_name">Host name</label>
                    <input class="md-input" type="text" id="host_name" name="db_host" />
                </div>
                <div class="uk-form-row">
                    <label for="db_name">Database name</label>
                    <input class="md-input" type="text" id="db_name" name="db_name" />
                </div>
                <div class="uk-form-row">
                    <label for="db_user_name">User name</label>
                    <input class="md-input" type="text" id="db_user_name" name="db_user_name" />
                </div>
                <div class="uk-form-row">
                    <label for="db_password">Password</label>
                    <input class="md-input" type="password" id="db_password" name="db_password" />
                </div>

                <div class="uk-margin-medium-top">
                    <button class="md-btn md-btn-primary md-btn-block md-btn-large install-button">Install</button>
                </div>
            </form>
        </div>
    </div>
    <div class="md-card error-box" >
        <div class="md-card-content">

        </div>
    </div>
</div>

<!-- common functions -->
<script src="./static/app/assets/js/common.min.js"></script>
<!-- uikit functions -->
<script src="./static/app/assets/js/uikit_custom.js"></script>
<!-- altair core functions -->
<script src="./static/app/assets/js/altair_admin_common.js"></script>

<script src="./static/app/assets/js/install.js"></script>

<script>
    // check for theme
    if (typeof(Storage) !== "undefined") {
        var root = document.getElementsByTagName( 'html' )[0],
            theme = localStorage.getItem("altair_theme");
        if(theme == 'app_theme_dark' || root.classList.contains('app_theme_dark')) {
            root.className += ' app_theme_dark';
        }
    }
</script>

</body>
</html>