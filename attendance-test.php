<?php
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://localhost/march-edukit/controllers/api/attendance.php");
curl_setopt($ch, CURLOPT_POST, 1);
$value_set = array(
    'request_type' => 'attendance',
    'data' => json_encode(
        array(
            array(
                "machine_serial_number" => "",
                "user_id" => "",
                "check_type" => "",
                "check_time" => "",
            )
        )
    )
);

// In real life you should use something like:
 curl_setopt($ch, CURLOPT_POSTFIELDS,
          http_build_query($value_set));

// Receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

$server_output = curl_exec($ch);

curl_close ($ch);