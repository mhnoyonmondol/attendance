<?php
    if (isset($_SERVER['SERVER_ADDR'])){
        session_start();
    }

    function get_base_url($atRoot=FALSE, $atCore=FALSE, $parse=FALSE){
        if (isset($_SERVER['HTTP_HOST'])) {
            $http = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
            $hostname = $_SERVER['HTTP_HOST'];
            $dir =  str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

            $core = preg_split('@/@', str_replace($_SERVER['DOCUMENT_ROOT'], '', realpath(dirname(__FILE__))), NULL, PREG_SPLIT_NO_EMPTY);
            if (isset($core[0])){
                $core = $core[0];
            }


            $tmplt = $atRoot ? ($atCore ? "%s://%s/%s/" : "%s://%s/") : ($atCore ? "%s://%s/%s/" : "%s://%s%s");
            $end = $atRoot ? ($atCore ? $core : $hostname) : ($atCore ? $core : $dir);
            $base_url = sprintf( $tmplt, $http, $hostname, $end );
        }
        else $base_url = 'http://localhost/';

        if ($parse) {
            $base_url = parse_url($base_url);
            if (isset($base_url['path'])) if ($base_url['path'] == '/') $base_url['path'] = '';
        }

        return $base_url;
    }


    //define the project root for all file access correctly reference by project_root
    define("project_root",__DIR__."/");
    // define base for angular js file system and homepage static files
    $root_info = get_base_url(NULL, NULL, TRUE);
    $base = $root_info['path'];
    if (!$base){
        $base = "/";
    }
    define("project_base",$base);
    /// load autoload for timezone
    require_once project_root."controllers/packages/vendor/autoload.php";
    $retriever = new retrieve();
    $software_info = $retriever->software_info();
    if ($software_info){
        if (isset($software_info['time_zone'])){
            $timezone = $software_info['time_zone'];
            date_default_timezone_set($timezone);
        }
    }
